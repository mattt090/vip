﻿// Decompiled with JetBrains decompiler
// Type: TrayPreviewSpace.Window
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;

namespace TrayPreviewSpace
{
  internal class Window
  {
    public string Title;
    public IntPtr Handle;

    public override string ToString()
    {
      return Title;
    }
  }
}
