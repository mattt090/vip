﻿// Decompiled with JetBrains decompiler
// Type: TrayPreviewSpace.Program
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using SingleInstance;

namespace TrayPreviewSpace
{
  internal static class Program
  {
    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern bool DwmIsCompositionEnabled();

    [STAThread]
    private static void Main()
    {
      if (!DwmIsCompositionEnabled())
      {
        int num = (int) MessageBox.Show("Video in Picture requires Vista, with Aero enabled. Please restart.");
        Application.Exit();
      }
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.ThreadException += Form1_UIThreadException;
      Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
      AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
      if (SingleApplication.Run(new TrayPreview()))
        ;
    }

    private static void Form1_UIThreadException(object sender, ThreadExceptionEventArgs t)
    {
      DialogResult dialogResult = DialogResult.Cancel;
      try
      {
        dialogResult = ShowThreadExceptionDialog("Windows Forms Error", t.Exception);
      }
      catch
      {
        try
        {
          int num = (int) MessageBox.Show("Fatal Windows Forms Error", "Fatal Windows Forms Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Hand);
        }
        finally
        {
          Application.Exit();
        }
      }
      if (dialogResult != DialogResult.Abort)
        return;
      Application.Exit();
    }

    private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      try
      {
        Exception exception = (Exception) e.ExceptionObject;
        Console.WriteLine("An application error occurred. Please contact the adminstrator with the following information:\n\n" + exception.Message + "\n\nStack Trace:\n" + exception.StackTrace);
      }
      catch (Exception ex)
      {
        try
        {
          int num = (int) MessageBox.Show("Fatal Non-UI Error", "Fatal Non-UI Error. Could not write the error to the event log. Reason: " + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        finally
        {
          Application.Exit();
        }
      }
    }

    private static DialogResult ShowThreadExceptionDialog(string title, Exception e)
    {
      return MessageBox.Show("An application error occurred. Please contact the adminstrator with the following information:\n\n" + e.Message + "\n\nStack Trace:\n" + e.StackTrace, title, MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Hand);
    }
  }
}
