﻿// Decompiled with JetBrains decompiler
// Type: TrayPreviewSpace.TrayPreview
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using gma.System.Windows;
using Hook;
using MinimizeCapture;
using TaskbarList;
using ThumbnailPersist;

namespace TrayPreviewSpace
{
  public class TrayPreview : Form
  {
    private static readonly int GWL_STYLE = -16;
    private static readonly int DWM_TNP_VISIBLE = 8;
    private static readonly int DWM_TNP_OPACITY = 4;
    private static readonly int DWM_TNP_RECTDESTINATION = 1;
    private static readonly int DWM_TNP_RECTSOURCE = 2;
    private static readonly int DWM_TNP_SOURCECLIENTAREAONLY = 16;
    private static readonly ulong WS_VISIBLE = 268435456UL;
    private static readonly ulong WS_BORDER = 8388608UL;
    private static readonly ulong TARGETWINDOW = WS_BORDER | WS_VISIBLE;
    private static Hooks hook = new Hooks();
    private static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
    private IContainer components;
    public WINDOWINFO winfo = new WINDOWINFO();
    public WINDOWINFO winfo2 = new WINDOWINFO();
    public WINDOWINFO winfo3 = new WINDOWINFO();
    public bool window_Destroyed_Processing;
    public bool NonTarget;
    public bool emptydesktop;
    public ArrayList CacheTargetHandles = new ArrayList();
    public ArrayList CacheGridOrder = new ArrayList();
    public ArrayList CachePreviewSize = new ArrayList();
    private bool triploop;
    private int LastUsedX;
    private int LastUsedY;
    public WINDOWINFO TargetWindowProperties = new WINDOWINFO();
    public ArrayList TargetProperties = new ArrayList();
    public ArrayList TaskbarTabShown = new ArrayList();
    public ArrayList AlwaysOnTop = new ArrayList();
    public ArrayList NoMove = new ArrayList();
    public bool TrayPreviewProcessing;
    private bool window_MinimizeStart_Processing;
    private int ArrangeLargestPreviewHeight;
    private int ArrangeLargestPreviewWidth;
    public ArrayList FormCache = new ArrayList();
    private List<Window> windows = new List<Window>();
    public ArrayList PreviewDataLeft = new ArrayList();
    public ArrayList PreviewDataTop = new ArrayList();
    public ArrayList PreviewDataRight = new ArrayList();
    public ArrayList PreviewDataBottom = new ArrayList();
    public ArrayList PreviewDataHandle = new ArrayList();
    public ArrayList ThumbList = new ArrayList();
    public ArrayList TargettedWindows = new ArrayList();
    public ArrayList ViPPreviewHandles = new ArrayList();
    public ArrayList WindowLongStyles = new ArrayList();
    public ArrayList PreviewControls = new ArrayList();
    private int PreviewStartofMinimized = 0;
    private bool MinimizingPreviews;
    private bool AllMinimized;
    public bool MinimizeHotkeyDown;
    public bool Modifier1;
    public const int WM_NCLBUTTONDOWN = 161;
    public const int HTCAPTION = 2;
    private const int SC_RESTORE = 61728;
    private const int WM_SYSCOMMAND = 274;
    private const int SC_MINIMIZE = 61472;
    public const int SC_MAXIMIZE = 61488;
    private const int WM_CLOSE = 16;
    private const int SW_SHOWNORMAL = 1;
    private const int SW_SHOWMINIMIZED = 2;
    private const int SW_SHOWMAXIMIZED = 3;
    public const int WM_LBUTTONDOWN = 513;
    public const int WM_LBUTTONUP = 514;
    public const int WM_RBUTTONDOWN = 516;
    public const int WM_RBUTTONUP = 517;
    public const int WM_COMMAND2 = 274;
    public const int WM_CLOSE2 = 61536;
    private const uint WM_PAINT = 15U;
    private const int WS_EX_LAYERED = 524288;
    private const int LWA_ALPHA = 2;
    private const int SW_SHOWNOACTIVATE = 4;
    private const int HWND_TOPMOST = -1;
    private const uint SWP_NOACTIVATE = 16U;
    private const uint SWP_NOSIZE = 1U;
    private const uint SWP_NOMOVE = 2U;
    private const uint SWP_SHOWWINDOW = 64U;
    private const int GWL_EXSTYLE = -20;
    private const int WS_EX_TOOLWINDOW = 128;
    private const int WS_EX_APPWINDOW = 262144;
    private const int RDW_INVALIDATE = 1;
    private const int RDW_INTERNALPAINT = 2;
    private const int RDW_ERASE = 4;
    private const int RDW_VALIDATE = 8;
    private const int RDW_NOINTERNALPAINT = 16;
    private const int RDW_NOERASE = 32;
    private const int RDW_NOCHILDREN = 64;
    private const int RDW_ALLCHILDREN = 128;
    private const int RDW_UPDATENOW = 256;
    private const int RDW_ERASENOW = 512;
    private const int RDW_FRAME = 1024;
    private const int RDW_NOFRAME = 2048;
    private const int WM_MOUSEWHEEL = 522;
    private const int WM_SIZE = 5;
    private const int SIZE_MINIMIZED = 1;
    public const int LWA_COLORKEY = 1;
    private NotifyIcon notifyIcon1;
    private PictureBox pictureBox1;
    private ContextMenuStrip contextMenuStrip1;
    private ToolStripMenuItem quitToolStripMenuItem;
    private ToolStripMenuItem makePreviewsIgnoreInputToolStripMenuItem;
    private ToolStripMenuItem makePreviewsRespondToInputToolStripMenuItem;
    private ToolStripMenuItem settingsToolStripMenuItem;
    private ToolStripMenuItem restoreAllTargetWindowsToolStripMenuItem;
    private ToolStripMenuItem CreatePreviewsOnMinimize;
    private ToolStripMenuItem PreviewAllOpenWindows;
    private ToolStripMenuItem MinimizePreviews;
    private ToolStripMenuItem ShowPreviews;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripSeparator toolStripSeparator4;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripMenuItem RecoverLostWindows;
    private ToolStripMenuItem requireCTRLKeyToCreatePreviewToolStripMenuItem;
    private ToolStripMenuItem pinPreviewsToToolStripMenuItem;
    private ToolStripMenuItem topmostToolStripMenuItem;
    private ToolStripMenuItem normalToolStripMenuItem;
    private ToolStripMenuItem desktopToolStripMenuItem;
    private ToolStripMenuItem refreshLayoutToolStripMenuItem;
    private ToolStripMenuItem alignmentToolStripMenuItem;
    private ToolStripMenuItem noneToolStripMenuItem;
    private ToolStripMenuItem snapToGridToolStripMenuItem;
    private Timer click_timer;
    private ContextMenuStrip contextMenuStrip2;
    private IntPtr thumb;
    public static long WS_Transparent_Reverse;
    public bool quitting;
    private static UserActivityHook actHook;
    private TaskbarList.TaskbarList tblc;
    private bool TripResizeLoop;
    public Point TrayClickPoint;
    private MARGINS margins;

    public TrayPreview()
    {
      quitting = false;
      TripResizeLoop = true;
      cleardata();
      Hide();
      InitializeComponent();
      hook.OnWindowDestroy += window_Destroyed;
      hook.OnWindowMinimizeStart += window_MinimizeStart;
      hook.OnForegroundWindowChanged += window_ForegroundChanged;
      GC.KeepAlive(hook);
      tblc = new TaskbarListClass();
      tblc.HrInit();
      actHook = new UserActivityHook();
      actHook.KeyDown += MyKeyDown;
      actHook.KeyUp += MyKeyUp;
      actHook.Start();
      GC.KeepAlive(actHook);
      click_timer.Interval = SystemInformation.DoubleClickTime;
      click_timer.Enabled = true;
      SetWindowLong(Handle, -20, GetWindowLong(Handle, -20) | 128L);
      GetWindows();
      UserSettings.Default.FirstPreview = true;
      TripResizeLoop = false;
      GC.KeepAlive(this);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && components != null)
        components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      components = new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (TrayPreview));
      notifyIcon1 = new NotifyIcon(components);
      contextMenuStrip1 = new ContextMenuStrip(components);
      quitToolStripMenuItem = new ToolStripMenuItem();
      settingsToolStripMenuItem = new ToolStripMenuItem();
      RecoverLostWindows = new ToolStripMenuItem();
      toolStripSeparator2 = new ToolStripSeparator();
      makePreviewsIgnoreInputToolStripMenuItem = new ToolStripMenuItem();
      makePreviewsRespondToInputToolStripMenuItem = new ToolStripMenuItem();
      toolStripSeparator4 = new ToolStripSeparator();
      PreviewAllOpenWindows = new ToolStripMenuItem();
      restoreAllTargetWindowsToolStripMenuItem = new ToolStripMenuItem();
      toolStripSeparator3 = new ToolStripSeparator();
      MinimizePreviews = new ToolStripMenuItem();
      ShowPreviews = new ToolStripMenuItem();
      refreshLayoutToolStripMenuItem = new ToolStripMenuItem();
      alignmentToolStripMenuItem = new ToolStripMenuItem();
      noneToolStripMenuItem = new ToolStripMenuItem();
      snapToGridToolStripMenuItem = new ToolStripMenuItem();
      pinPreviewsToToolStripMenuItem = new ToolStripMenuItem();
      topmostToolStripMenuItem = new ToolStripMenuItem();
      normalToolStripMenuItem = new ToolStripMenuItem();
      desktopToolStripMenuItem = new ToolStripMenuItem();
      toolStripSeparator1 = new ToolStripSeparator();
      CreatePreviewsOnMinimize = new ToolStripMenuItem();
      requireCTRLKeyToCreatePreviewToolStripMenuItem = new ToolStripMenuItem();
      pictureBox1 = new PictureBox();
      click_timer = new Timer(components);
      contextMenuStrip2 = new ContextMenuStrip(components);
      contextMenuStrip1.SuspendLayout();
      ((ISupportInitialize) pictureBox1).BeginInit();
      SuspendLayout();
      notifyIcon1.ContextMenuStrip = contextMenuStrip1;
      notifyIcon1.Icon = (Icon) componentResourceManager.GetObject("notifyIcon1.Icon");
      notifyIcon1.Text = "Video in Picture";
      notifyIcon1.Visible = true;
      notifyIcon1.MouseMove += notifyIcon1_MouseMove;
      notifyIcon1.MouseClick += notifyIcon1_MouseClick;
      notifyIcon1.MouseDoubleClick += notifyIcon1_MouseDoubleClick;
      contextMenuStrip1.Items.AddRange(new ToolStripItem[18]
      {
        quitToolStripMenuItem,
        settingsToolStripMenuItem,
        RecoverLostWindows,
        toolStripSeparator2,
        makePreviewsIgnoreInputToolStripMenuItem,
        makePreviewsRespondToInputToolStripMenuItem,
        toolStripSeparator4,
        PreviewAllOpenWindows,
        restoreAllTargetWindowsToolStripMenuItem,
        toolStripSeparator3,
        MinimizePreviews,
        ShowPreviews,
        refreshLayoutToolStripMenuItem,
        alignmentToolStripMenuItem,
        pinPreviewsToToolStripMenuItem,
        toolStripSeparator1,
        CreatePreviewsOnMinimize,
        requireCTRLKeyToCreatePreviewToolStripMenuItem
      });
      contextMenuStrip1.Name = "contextMenuStrip1";
      contextMenuStrip1.ShowCheckMargin = true;
      contextMenuStrip1.ShowImageMargin = false;
      contextMenuStrip1.Size = new Size(327, 358);
      contextMenuStrip1.Opened += contextMenuStrip1_Opened;
      contextMenuStrip1.Opening += contextMenuStrip1_Opening;
      contextMenuStrip1.Closing += contextMenuStrip1_Closing;
      quitToolStripMenuItem.Name = "quitToolStripMenuItem";
      quitToolStripMenuItem.Size = new Size(326, 22);
      quitToolStripMenuItem.Text = "Quit";
      quitToolStripMenuItem.Click += quitToolStripMenuItem_Click;
      settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
      settingsToolStripMenuItem.Size = new Size(326, 22);
      settingsToolStripMenuItem.Text = "Settings";
      settingsToolStripMenuItem.Click += settingsToolStripMenuItem_Click;
      RecoverLostWindows.Name = "RecoverLostWindows";
      RecoverLostWindows.Size = new Size(326, 22);
      RecoverLostWindows.Text = "Recover Lost Windows";
      RecoverLostWindows.Click += RecoverLostWindows_Click;
      toolStripSeparator2.Name = "toolStripSeparator2";
      toolStripSeparator2.Size = new Size(323, 6);
      makePreviewsIgnoreInputToolStripMenuItem.Name = "makePreviewsIgnoreInputToolStripMenuItem";
      makePreviewsIgnoreInputToolStripMenuItem.ShortcutKeys = Keys.W | Keys.Control;
      makePreviewsIgnoreInputToolStripMenuItem.Size = new Size(326, 22);
      makePreviewsIgnoreInputToolStripMenuItem.Text = "Make Previews Ignore Input";
      makePreviewsIgnoreInputToolStripMenuItem.Click += makePreviewsIgnoreInputToolStripMenuItem_Click;
      makePreviewsRespondToInputToolStripMenuItem.Name = "makePreviewsRespondToInputToolStripMenuItem";
      makePreviewsRespondToInputToolStripMenuItem.ShortcutKeys = Keys.Q | Keys.Control;
      makePreviewsRespondToInputToolStripMenuItem.Size = new Size(326, 22);
      makePreviewsRespondToInputToolStripMenuItem.Text = "Make Previews Respond to Input";
      makePreviewsRespondToInputToolStripMenuItem.Click += makePreviewsRespondToInputToolStripMenuItem_Click;
      toolStripSeparator4.Name = "toolStripSeparator4";
      toolStripSeparator4.Size = new Size(323, 6);
      PreviewAllOpenWindows.Name = "PreviewAllOpenWindows";
      PreviewAllOpenWindows.ShortcutKeys = Keys.A | Keys.Control;
      PreviewAllOpenWindows.Size = new Size(326, 22);
      PreviewAllOpenWindows.Text = "Preview All Open Windows";
      PreviewAllOpenWindows.Click += PreviewAllOpenWindows_Click;
      restoreAllTargetWindowsToolStripMenuItem.Name = "restoreAllTargetWindowsToolStripMenuItem";
      restoreAllTargetWindowsToolStripMenuItem.ShortcutKeys = Keys.X | Keys.Control;
      restoreAllTargetWindowsToolStripMenuItem.Size = new Size(326, 22);
      restoreAllTargetWindowsToolStripMenuItem.Text = "Restore All Target Windows";
      restoreAllTargetWindowsToolStripMenuItem.Click += restoreAllTargetWindowsToolStripMenuItem_Click;
      toolStripSeparator3.Name = "toolStripSeparator3";
      toolStripSeparator3.Size = new Size(323, 6);
      MinimizePreviews.Name = "MinimizePreviews";
      MinimizePreviews.ShortcutKeys = Keys.Z | Keys.Control;
      MinimizePreviews.Size = new Size(326, 22);
      MinimizePreviews.Text = "Minimize All Previews";
      MinimizePreviews.Click += MinimizePreviews_Click;
      ShowPreviews.Name = "ShowPreviews";
      ShowPreviews.ShortcutKeys = Keys.S | Keys.Control;
      ShowPreviews.Size = new Size(326, 22);
      ShowPreviews.Text = "Show All Previews";
      ShowPreviews.Click += ShowPreviews_Click;
      refreshLayoutToolStripMenuItem.Name = "refreshLayoutToolStripMenuItem";
      refreshLayoutToolStripMenuItem.ShortcutKeys = Keys.R | Keys.Control;
      refreshLayoutToolStripMenuItem.Size = new Size(326, 22);
      refreshLayoutToolStripMenuItem.Text = "Refresh Layout";
      refreshLayoutToolStripMenuItem.Click += refreshLayoutToolStripMenuItem_Click;
      alignmentToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[2]
      {
        noneToolStripMenuItem,
        snapToGridToolStripMenuItem
      });
      alignmentToolStripMenuItem.Name = "alignmentToolStripMenuItem";
      alignmentToolStripMenuItem.Size = new Size(326, 22);
      alignmentToolStripMenuItem.Text = "Alignment";
      noneToolStripMenuItem.CheckOnClick = true;
      noneToolStripMenuItem.Name = "noneToolStripMenuItem";
      noneToolStripMenuItem.Size = new Size(139, 22);
      noneToolStripMenuItem.Text = "None";
      noneToolStripMenuItem.Click += noneToolStripMenuItem_Click;
      snapToGridToolStripMenuItem.CheckOnClick = true;
      snapToGridToolStripMenuItem.Name = "snapToGridToolStripMenuItem";
      snapToGridToolStripMenuItem.Size = new Size(139, 22);
      snapToGridToolStripMenuItem.Text = "Snap to Grid";
      snapToGridToolStripMenuItem.Click += snapToGridToolStripMenuItem_Click;
      pinPreviewsToToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
      {
        topmostToolStripMenuItem,
        normalToolStripMenuItem,
        desktopToolStripMenuItem
      });
      pinPreviewsToToolStripMenuItem.Name = "pinPreviewsToToolStripMenuItem";
      pinPreviewsToToolStripMenuItem.Size = new Size(326, 22);
      pinPreviewsToToolStripMenuItem.Text = "Pin Previews To......";
      topmostToolStripMenuItem.CheckOnClick = true;
      topmostToolStripMenuItem.Name = "topmostToolStripMenuItem";
      topmostToolStripMenuItem.Size = new Size(122, 22);
      topmostToolStripMenuItem.Text = "Topmost";
      topmostToolStripMenuItem.Click += topmostToolStripMenuItem_Click;
      normalToolStripMenuItem.CheckOnClick = true;
      normalToolStripMenuItem.Name = "normalToolStripMenuItem";
      normalToolStripMenuItem.Size = new Size(122, 22);
      normalToolStripMenuItem.Text = "Normal";
      normalToolStripMenuItem.Click += normalToolStripMenuItem_Click;
      desktopToolStripMenuItem.CheckOnClick = true;
      desktopToolStripMenuItem.Name = "desktopToolStripMenuItem";
      desktopToolStripMenuItem.Size = new Size(122, 22);
      desktopToolStripMenuItem.Text = "Desktop";
      desktopToolStripMenuItem.Click += desktopToolStripMenuItem_Click;
      toolStripSeparator1.Name = "toolStripSeparator1";
      toolStripSeparator1.Size = new Size(323, 6);
      CreatePreviewsOnMinimize.CheckOnClick = true;
      CreatePreviewsOnMinimize.Name = "CreatePreviewsOnMinimize";
      CreatePreviewsOnMinimize.ShortcutKeys = Keys.Space | Keys.Control;
      CreatePreviewsOnMinimize.Size = new Size(326, 22);
      CreatePreviewsOnMinimize.Text = "Create Previews on Minimize";
      CreatePreviewsOnMinimize.Click += CreatePreviewsOnMinimize_Click;
      requireCTRLKeyToCreatePreviewToolStripMenuItem.CheckOnClick = true;
      requireCTRLKeyToCreatePreviewToolStripMenuItem.Name = "requireCTRLKeyToCreatePreviewToolStripMenuItem";
      requireCTRLKeyToCreatePreviewToolStripMenuItem.ShortcutKeys = Keys.Space | Keys.Alt;
      requireCTRLKeyToCreatePreviewToolStripMenuItem.Size = new Size(326, 22);
      requireCTRLKeyToCreatePreviewToolStripMenuItem.Text = "Require SHIFT Key to Create Preview";
      requireCTRLKeyToCreatePreviewToolStripMenuItem.Click += requireCTRLKeyToCreatePreviewToolStripMenuItem_Click;
      pictureBox1.AccessibleRole = AccessibleRole.Window;
      pictureBox1.BackColor = Color.Transparent;
      pictureBox1.Cursor = Cursors.Hand;
      pictureBox1.Dock = DockStyle.Fill;
      pictureBox1.Location = new Point(0, 0);
      pictureBox1.Name = "pictureBox1";
      pictureBox1.Size = new Size(91, 24);
      pictureBox1.TabIndex = 0;
      pictureBox1.TabStop = false;
      pictureBox1.MouseLeave += pictureBox1_MouseLeave;
      pictureBox1.MouseClick += pictureBox1_MouseClick;
      pictureBox1.MouseHover += pictureBox1_MouseHover;
      click_timer.Tick += click_timer_Tick;
      contextMenuStrip2.Name = "contextMenuStrip2";
      contextMenuStrip2.Size = new Size(61, 4);
      AccessibleRole = AccessibleRole.Window;
      AutoScaleDimensions = new SizeF(6f, 13f);
      AutoScaleMode = AutoScaleMode.Font;
      AutoScroll = true;
      BackColor = SystemColors.GradientActiveCaption;
      ClientSize = new Size(108, 20);
      ControlBox = false;
      Controls.Add(pictureBox1);
      KeyPreview = true;
      Name = "TrayPreview";
      Opacity = 0.0;
      ShowIcon = false;
      ShowInTaskbar = false;
      StartPosition = FormStartPosition.Manual;
      TopMost = true;
      TransparencyKey = Color.Transparent;
      Deactivate += TrayPreview_Deactivate;
      Load += TrayPreview_Load;
      Shown += TrayPreview_Shown;
      FormClosed += TrayPreview_FormClosed;
      FormClosing += TrayPreview_FormClosing;
      Resize += TrayPreview_Resize;
      KeyDown += TrayPreview_KeyDown;
      MouseHover += TrayPreview_MouseHover;
      contextMenuStrip1.ResumeLayout(false);
      ((ISupportInitialize) pictureBox1).EndInit();
      ResumeLayout(false);
    }

    [DllImport("dwmapi.dll")]
    private static extern int DwmRegisterThumbnail(IntPtr dest, IntPtr src, out IntPtr thumb);

    [DllImport("dwmapi.dll")]
    private static extern int DwmUnregisterThumbnail(IntPtr thumb);

    [DllImport("dwmapi.dll")]
    private static extern int DwmQueryThumbnailSourceSize(IntPtr thumb, out PSIZE size);

    [DllImport("dwmapi.dll")]
    private static extern int DwmUpdateThumbnailProperties(IntPtr hThumb, ref DWM_THUMBNAIL_PROPERTIES props);

    [DllImport("SHELL32", CallingConvention = CallingConvention.StdCall)]
    private static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);

    [DllImport("USER32")]
    private static extern int GetSystemMetrics(int Index);

    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    private static extern bool MoveWindow(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);

    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    private static extern int RegisterWindowMessage(string msg);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr GetParent(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern ulong GetWindowLongA(IntPtr hWnd, int nIndex);

    [DllImport("user32.dll")]
    private static extern int EnumWindows(EnumWindowsCallback lpEnumFunc, int lParam);

    [DllImport("user32.dll")]
    public static extern void GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

    [DllImport("user32.dll")]
    private static extern bool SetForegroundWindow(int hwnd);

    [DllImport("User32.dll")]
    private static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern int GetDesktopWindow();

    [DllImport("user32.dll")]
    private static extern int FindWindow(string className, string windowText);

    [DllImport("user32")]
    public static extern int MoveWindow(int hwnd, int x, int y, int nWidth, int nHeight, int bRepaint);

    [DllImport("user32.dll")]
    private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);

    [DllImport("user32")]
    private static extern int SetLayeredWindowAttributes(IntPtr hWnd, byte crey, byte alpha, int flags);

    [DllImport("user32.dll")]
    private static extern uint SendMessage(IntPtr hWnd, uint msg, uint wParam, uint lParam);

    [DllImport("user32.dll")]
    public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

    [DllImport("user32.dll")]
    private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPos(int hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

    [DllImport("user32")]
    public static extern int SetCursorPos(int x, int y);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

    [DllImport("user32.dll")]
    private static extern long GetWindowLong(IntPtr hWnd, int nIndex);

    [DllImport("user32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool ShowWindow(IntPtr hWnd, ShowWindowEnum flags);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

    [DllImport("user32.dll")]
    private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, [In] ref RECT lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool IsIconic(IntPtr hWnd);

    [DllImport("user32")]
    private static extern uint GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

    public static int GetWindowProcessID(int hwnd)
    {
      int lpdwProcessId = 1;
      int num = (int) GetWindowThreadProcessId(hwnd, out lpdwProcessId);
      return lpdwProcessId;
    }

    private void window_Destroyed(IntPtr hWnd)
    {
      GC.KeepAlive(hook);
      if (quitting)
        return;
      NonTarget = false;
      window_Destroyed_Processing = false;
      if (!TargettedWindows.Contains(hWnd) && !ViPPreviewHandles.Contains(hWnd))
      {
        if (CacheTargetHandles.Contains(hWnd))
        {
          int index = CacheTargetHandles.IndexOf(hWnd);
          CacheTargetHandles.RemoveAt(index);
          CacheGridOrder.RemoveAt(index);
          CachePreviewSize.RemoveAt(index);
          if (CacheTargetHandles.Count > 500)
            Debug.WriteLine("Number of caches is over 500");
        }
        foreach (Window window in windows)
        {
          if (window.Handle == hWnd)
          {
            NonTarget = true;
            HideTaskbarTab();
            return;
          }
        }
        GetWindows();
        if (TargettedWindows.Count >= windows.Count)
        {
          NonTarget = true;
          emptydesktop = true;
          HideTaskbarTab();
          return;
        }
      }
      else if (TargettedWindows.Contains(hWnd))
      {
        int index1 = TargettedWindows.IndexOf(hWnd);
        window_Destroyed_Processing = true;
        SendMessage((IntPtr) long.Parse(ViPPreviewHandles[index1].ToString()), 274, 61536, 0);
        if (CacheTargetHandles.Contains(hWnd))
        {
          int index2 = CacheTargetHandles.IndexOf(hWnd);
          CacheTargetHandles.RemoveAt(index2);
          CacheGridOrder.RemoveAt(index2);
          CachePreviewSize.RemoveAt(index2);
          if (CacheTargetHandles.Count > 500)
            Debug.WriteLine("Number of caches is over 500");
        }
      }
      else if (ViPPreviewHandles.Contains(hWnd))
      {
        int index = ViPPreviewHandles.IndexOf(hWnd);
        window_Destroyed_Processing = true;
        TaskbarTabShown.RemoveAt(index);
        ViPPreviewHandles.RemoveAt(index);
        TargettedWindows.RemoveAt(index);
        PreviewControls.RemoveAt(index);
        AlwaysOnTop.RemoveAt(index);
        NoMove.RemoveAt(index);
        if (UserSettings.Default.PreviewGridAlign == "Snap")
          ReAlign();
      }
      HideTaskbarTab();
    }

    public void CheckAndInsert(IntPtr hWnd, Point p)
    {
      if (UserSettings.Default.PreviewGridAlign != "Snap")
        return;
      foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
      {
        if (thumbnailWindow.Handle != hWnd)
        {
          Point location = thumbnailWindow.Location;
          int x1 = location.X;
          location = thumbnailWindow.Location;
          int y1 = location.Y;
          Point point1 = new Point(x1, y1);
          location = thumbnailWindow.Location;
          int x2 = location.X;
          Size size = thumbnailWindow.Size;
          int width = size.Width;
          int x3 = x2 + width;
          location = thumbnailWindow.Location;
          int y2 = location.Y;
          size = thumbnailWindow.Size;
          int height = size.Height;
          int y3 = y2 + height;
          Point point2 = new Point(x3, y3);
          if (p.X >= point1.X && p.X <= point2.X && p.Y >= point1.Y && p.Y <= point2.Y)
          {
            SwapAndRemove(PreviewControls.IndexOf(thumbnailWindow), ViPPreviewHandles.IndexOf(hWnd));
            ReAlign();
            break;
          }
        }
      }
    }

    public void SwapAndRemove(int NewIndex, int OldIndex)
    {
      if (NewIndex < OldIndex)
      {
        TaskbarTabShown.Insert(NewIndex, (bool) TaskbarTabShown[OldIndex]);
        TargettedWindows.Insert(NewIndex, (IntPtr) TargettedWindows[OldIndex]);
        AlwaysOnTop.Insert(NewIndex, (bool) AlwaysOnTop[OldIndex]);
        NoMove.Insert(NewIndex, (bool) NoMove[OldIndex]);
        PreviewControls.Insert(NewIndex, (ThumbnailWindow) PreviewControls[OldIndex]);
        ViPPreviewHandles.Insert(NewIndex, (IntPtr) ViPPreviewHandles[OldIndex]);
        TaskbarTabShown.RemoveAt(OldIndex + 1);
        TargettedWindows.RemoveAt(OldIndex + 1);
        AlwaysOnTop.RemoveAt(OldIndex + 1);
        NoMove.RemoveAt(OldIndex + 1);
        PreviewControls.RemoveAt(OldIndex + 1);
        ViPPreviewHandles.RemoveAt(OldIndex + 1);
      }
      else
      {
        TaskbarTabShown.Insert(NewIndex + 1, (bool) TaskbarTabShown[OldIndex]);
        TargettedWindows.Insert(NewIndex + 1, (IntPtr) TargettedWindows[OldIndex]);
        AlwaysOnTop.Insert(NewIndex + 1, (bool) AlwaysOnTop[OldIndex]);
        NoMove.Insert(NewIndex + 1, (bool) NoMove[OldIndex]);
        PreviewControls.Insert(NewIndex + 1, (ThumbnailWindow) PreviewControls[OldIndex]);
        ViPPreviewHandles.Insert(NewIndex + 1, (IntPtr) ViPPreviewHandles[OldIndex]);
        TaskbarTabShown.RemoveAt(OldIndex);
        TargettedWindows.RemoveAt(OldIndex);
        AlwaysOnTop.RemoveAt(OldIndex);
        NoMove.RemoveAt(OldIndex);
        PreviewControls.RemoveAt(OldIndex);
        ViPPreviewHandles.RemoveAt(OldIndex);
      }
      RenumberCache();
    }

    public IntPtr FirstPreviewNotAlwaysOnTop()
    {
      int index = 0;
      IntPtr num1 = IntPtr.Zero;
      foreach (IntPtr num2 in ViPPreviewHandles)
      {
        if (!(bool) AlwaysOnTop[index])
        {
          num1 = num2;
          break;
        }
        ++index;
      }
      return num1;
    }

    public void RenumberCache()
    {
      foreach (IntPtr num in TargettedWindows)
      {
        int index = CacheTargetHandles.IndexOf(num);
        if (index >= 0)
          CacheGridOrder[index] = TargettedWindows.IndexOf(num);
      }
    }

    public void ZoomAll(string UpDown)
    {
      KeyEventArgs keyEventArgs;
      if (UpDown == "Down")
      {
        keyEventArgs = new KeyEventArgs(Keys.Down);
        foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
          thumbnailWindow.Zoom(UpDown);
      }
      else
      {
        keyEventArgs = new KeyEventArgs(Keys.Up);
        foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
          thumbnailWindow.Zoom(UpDown);
      }
      ReAlign();
    }

    private void window_MinimizeEnd(IntPtr hWnd)
    {
      GC.KeepAlive(hook);
      GetWindows();
      HideTaskbarTab();
    }

    private void window_ForegroundChanged(IntPtr hWnd)
    {
      GC.KeepAlive(hook);
      GetWindows();
      if (triploop)
        return;
      if (emptydesktop)
        emptydesktop = false;
      if (hWnd == Handle && !TargettedWindows.Contains(hWnd))
      {
        window_MinimizeStart_Processing = false;
        window_Destroyed_Processing = false;
        NonTarget = false;
        HideTaskbarTab();
      }
      else if (NonTarget || window_MinimizeStart_Processing || window_Destroyed_Processing || TrayPreviewProcessing)
      {
        window_MinimizeStart_Processing = false;
        window_Destroyed_Processing = false;
        NonTarget = false;
        TrayPreviewProcessing = false;
        HideTaskbarTab();
      }
      else
      {
        window_MinimizeStart_Processing = false;
        window_Destroyed_Processing = false;
        NonTarget = false;
        int index = TargettedWindows.IndexOf(hWnd);
        if (index >= 0 && !TrayPreviewProcessing && !MinimizingPreviews && TargettedWindows.Contains(hWnd))
        {
          ThumbnailWindow thumbnailWindow = (ThumbnailWindow) PreviewControls[index];
          if (!thumbnailWindow.GetProcessingState() && !thumbnailWindow.GetShownState())
          {
            triploop = true;
            ShowWindow((IntPtr) ViPPreviewHandles[index], 1);
            SetForegroundWindow((IntPtr) ViPPreviewHandles[index]);
            triploop = false;
          }
        }
        HideTaskbarTab();
      }
    }

    private void window_MinimizeStart(IntPtr hWnd)
    {
      GC.KeepAlive(hook);
      GetWindows();
      TrayPreviewProcessing = true;
      window_MinimizeStart_Processing = true;
      bool flag1;
      if (UserSettings.Default.KeepTargettedFromMinimizing && UserSettings.Default.MoveTargetOffscreen && TargettedWindows.Contains(hWnd))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        ((ThumbnailWindow) PreviewControls[TargettedWindows.IndexOf(hWnd)]).hideTargetWindowToolStripMenuItem_Click(this, null);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      else if (UserSettings.Default.KeepTargettedFromMinimizing && TargettedWindows.Contains(hWnd))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        ((ThumbnailWindow) PreviewControls[TargettedWindows.IndexOf(hWnd)]).hideTargetWindowToolStripMenuItem_Click(this, null);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      else if (ViPPreviewHandles.Contains(hWnd))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        Debug.WriteLine("preview minimized");
        SendMessage(hWnd, 274, 61728, 0);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      else if (UserSettings.Default.CreatePreviewsOnMinimize)
      {
        if (UserSettings.Default.CreatePreviewOnlyOnHotkey && !MinimizeHotkeyDown)
          return;
        bool flag2 = false;
        foreach (Window window in windows)
        {
          if (window.Handle == hWnd)
          {
            flag2 = true;
            break;
          }
        }
        if (flag2 && hWnd != Handle && !TargettedWindows.Contains(hWnd))
        {
          if (IsIconic(hWnd))
            CreatePreview(hWnd, true);
          else
            CreatePreview(hWnd, false);
        }
      }
      HideTaskbarTab();
      TrayPreviewProcessing = false;
    }

    private void restoreAllTargetWindowsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (IntPtr hWnd in ViPPreviewHandles)
      {
        TrayPreviewProcessing = true;
        SendMessage(hWnd, 274, 61536, 0);
        TrayPreviewProcessing = false;
      }
    }

    public void ClosePreview(IntPtr hWnd)
    {
      SendMessage(hWnd, 274, 61536, 0);
    }

    private void CreatePreview(IntPtr hWnd, bool NeedtoRestore)
    {
      TrayPreviewProcessing = true;
      long windowLong = GetWindowLong(hWnd, -20);
      bool WindowIsMaximized = false;
      TaskbarTabShown.Add(false);
      TargettedWindows.Add(hWnd);
      AlwaysOnTop.Add(false);
      NoMove.Add(false);
      ThumbnailWindow newpreview;
      if (CacheTargetHandles.Contains(hWnd))
      {
        CacheWindowSize cacheWindowSize = new CacheWindowSize();
        CacheWindowSize OverRidePreviewParams = (CacheWindowSize) CachePreviewSize[CacheTargetHandles.IndexOf(hWnd)];
        newpreview = new ThumbnailWindow(4, hWnd, windowLong, WindowIsMaximized, this, OverRidePreviewParams);
      }
      else
        newpreview = new ThumbnailWindow(3, hWnd, windowLong, WindowIsMaximized, this);
      newpreview.Show();
      newpreview.BringToFront();
      newpreview.TopMost = true;
      PreviewControls.Add(newpreview);
      ViPPreviewHandles.Add(newpreview.Handle);
      bool flag1 = false;
      if (CacheTargetHandles.Contains(hWnd))
      {
        int index1 = (int) CacheGridOrder[CacheTargetHandles.IndexOf(hWnd)];
        int index2 = TargettedWindows.IndexOf(hWnd);
        CacheWindowSize cacheWindowSize1 = new CacheWindowSize();
        CacheWindowSize cacheWindowSize2 = (CacheWindowSize) CachePreviewSize[index1];
        bool flag2 = (bool) TaskbarTabShown[index2];
        bool flag3 = cacheWindowSize2.alwaysontop;
        bool flag4 = cacheWindowSize2.nomove;
        if (index1 > TargettedWindows.Count - 1)
          index1 = TargettedWindows.Count - 1;
        if (index1 != index2)
        {
          TaskbarTabShown.RemoveAt(index2);
          TargettedWindows.RemoveAt(index2);
          AlwaysOnTop.RemoveAt(index2);
          NoMove.RemoveAt(index2);
          PreviewControls.RemoveAt(index2);
          ViPPreviewHandles.RemoveAt(index2);
          TaskbarTabShown.Insert(index1, flag2);
          TargettedWindows.Insert(index1, hWnd);
          AlwaysOnTop.Insert(index1, flag3);
          NoMove.Insert(index1, flag4);
          PreviewControls.Insert(index1, newpreview);
          ViPPreviewHandles.Insert(index1, newpreview.Handle);
        }
        newpreview.SavePreviewCacheParams();
      }
      else
      {
        CacheTargetHandles.Add(hWnd);
        CacheGridOrder.Add(TargettedWindows.IndexOf(hWnd));
        CachePreviewSize.Add(new CacheWindowSize());
        newpreview.SavePreviewCacheParams();
        flag1 = true;
      }
      TrayPreviewProcessing = false;
      int index = PreviewControls.IndexOf(newpreview);
      if ((bool) AlwaysOnTop[index] && (bool) NoMove[index])
      {
        newpreview.SetAlwaysTopNoMove();
        newpreview.Show();
        newpreview.RestoreTransparency();
      }
      else if (!(UserSettings.Default.PreviewGridAlign == "Snap") && !flag1)
      {
        newpreview.Show();
        newpreview.RestoreTransparency();
      }
      else if (UserSettings.Default.PreviewGridAlign == "Snap")
        ReAlign(newpreview);
      else
        arrangepreviews(newpreview, 0);
    }

    public void ReAlign(ThumbnailWindow newpreview)
    {
      UserSettings.Default.FirstPreview = true;
      int index = 0;
      foreach (ThumbnailWindow newpreview1 in PreviewControls)
      {
        if (newpreview1 == newpreview)
          arrangepreviews(newpreview, 0);
        else if (!(bool) AlwaysOnTop[index] && !(bool) NoMove[index])
          arrangepreviews(newpreview1, 1);
        ++index;
      }
    }

    public void ReAlign()
    {
      UserSettings.Default.FirstPreview = true;
      int index = 0;
      foreach (ThumbnailWindow newpreview in PreviewControls)
      {
        if (!(bool) AlwaysOnTop[index] && !(bool) NoMove[index])
          arrangepreviews(newpreview, 1);
        ++index;
      }
    }

    private void refreshLayoutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.FirstPreview = true;
      int index = 0;
      foreach (ThumbnailWindow newpreview in PreviewControls)
      {
        if (!(bool) AlwaysOnTop[index] && !(bool) NoMove[index])
        {
          newpreview.Position_Screen_Start();
          arrangepreviews(newpreview, 0);
        }
        ++index;
      }
    }

    private void arrangepreviews(ThumbnailWindow newpreview, int state)
    {
      APPBARDATA pData = new APPBARDATA();
      int num1 = (int) SHAppBarMessage(5, ref pData);
      int num2 = (int) SHAppBarMessage(4, ref pData);
      int num3 = 0;
      int num4 = 0;
      int y1 = 0;
      int x1 = SystemInformation.PrimaryMonitorSize.Width;
      int num5 = SystemInformation.PrimaryMonitorSize.Height;
      Size size;
      if (!UserSettings.Default.CustomTaskbarMargins)
      {
        int num6 = pData.rc.bottom;
        size = SystemInformation.PrimaryMonitorSize;
        int height1 = size.Height;
        int num7;
        if (num6 == height1 && pData.rc.left == 0)
        {
          int num8 = pData.rc.right;
          size = SystemInformation.PrimaryMonitorSize;
          int width = size.Width;
          num7 = num8 != width ? 1 : 0;
        }
        else
          num7 = 1;
        if (num7 == 0)
        {
          num3 = 1;
          int num8 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
          num5 += num8;
        }
        else
        {
          int num8;
          if (pData.rc.top == 0 && pData.rc.left == 0)
          {
            int num9 = pData.rc.right;
            size = SystemInformation.PrimaryMonitorSize;
            int width = size.Width;
            num8 = num9 != width ? 1 : 0;
          }
          else
            num8 = 1;
          if (num8 == 0)
          {
            num3 = 2;
            int num9 = pData.rc.bottom;
            y1 += num9;
          }
          else
          {
            int num9;
            if (pData.rc.top == 0 && pData.rc.left == 0)
            {
              int num10 = pData.rc.bottom;
              size = SystemInformation.PrimaryMonitorSize;
              int height2 = size.Height;
              num9 = num10 != height2 ? 1 : 0;
            }
            else
              num9 = 1;
            if (num9 == 0)
            {
              num3 = 3;
              int num10 = pData.rc.right;
              num4 += num10;
            }
            else
            {
              int num10;
              if (pData.rc.top == 0)
              {
                int num11 = pData.rc.right;
                size = SystemInformation.PrimaryMonitorSize;
                int width = size.Width;
                if (num11 == width)
                {
                  int num12 = pData.rc.bottom;
                  size = SystemInformation.PrimaryMonitorSize;
                  int height2 = size.Height;
                  num10 = num12 != height2 ? 1 : 0;
                  goto label_20;
                }
              }
              num10 = 1;
label_20:
              if (num10 == 0)
              {
                num3 = 4;
                int num11 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
                x1 += num11;
              }
            }
          }
        }
      }
      else
      {
        num4 = UserSettings.Default.CustomTaskbarMarginsLeft;
        y1 = UserSettings.Default.CustomTaskbarMarginsTop;
        x1 = SystemInformation.PrimaryMonitorSize.Width - UserSettings.Default.CustomTaskbarMarginsRight;
        num5 = SystemInformation.PrimaryMonitorSize.Height - UserSettings.Default.CustomTaskbarMarginsBottom;
      }
      if (UserSettings.Default.DockPreview && (UserSettings.Default.startbottom || UserSettings.Default.starttop))
      {
        if (UserSettings.Default.FirstPreview)
        {
          int x2 = newpreview.Location.X;
          size = newpreview.Size;
          int width = size.Width;
          LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
          UserSettings.Default.FirstPreview = false;
        }
        else
        {
          int num6 = LastUsedX;
          size = newpreview.Size;
          int width1 = size.Width;
          if (num6 + width1 > x1)
          {
            LastUsedX = num4;
          }
          else
          {
            if (newpreview.Location.X != LastUsedX)
              newpreview.Location = new Point(LastUsedX, newpreview.Location.Y);
            int x2 = newpreview.Location.X;
            size = newpreview.Size;
            int width2 = size.Width;
            LastUsedX = x2 + width2 + UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
        }
      }
      else if (UserSettings.Default.DockPreview && (UserSettings.Default.startleft || UserSettings.Default.startright))
      {
        if (UserSettings.Default.FirstPreview)
        {
          int y2 = newpreview.Location.Y;
          size = newpreview.Size;
          int height = size.Height;
          LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
          UserSettings.Default.FirstPreview = false;
        }
        else
        {
          int num6 = LastUsedY;
          size = newpreview.Size;
          int height1 = size.Height;
          if (num6 + height1 > num5)
          {
            LastUsedY = num4;
          }
          else
          {
            if (newpreview.Location.Y != LastUsedY)
              newpreview.Location = new Point(newpreview.Location.X, LastUsedY);
            int y2 = newpreview.Location.Y;
            size = newpreview.Size;
            int height2 = size.Height;
            LastUsedY = y2 + height2 + UserSettings.Default.PreviewLayoutVerticalSpacing;
          }
        }
      }
      else if (ViPPreviewHandles.Count == 0 || UserSettings.Default.FirstPreview)
      {
        if (UserSettings.Default.RowsFirst)
        {
          if (UserSettings.Default.ArrangePreviewXDirection == "Right")
          {
            int x2 = newpreview.Location.X;
            size = newpreview.Size;
            int width = size.Width;
            LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int y2 = newpreview.Location.Y;
              size = newpreview.Size;
              int height = size.Height;
              LastUsedY = y2 + height;
            }
            else
              LastUsedY = newpreview.Location.Y;
          }
          else
          {
            LastUsedX = newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int y2 = newpreview.Location.Y;
              size = newpreview.Size;
              int height = size.Height;
              LastUsedY = y2 + height;
            }
            else
              LastUsedY = newpreview.Location.Y;
          }
          if (LastUsedX < num4 || LastUsedX > x1)
          {
            LastUsedX = !(UserSettings.Default.ArrangePreviewXDirection == "Right") ? x1 : num4;
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              LastUsedY = newpreview.Location.Y - UserSettings.Default.PreviewLayoutVerticalSpacing;
            }
            else
            {
              int y2 = newpreview.Location.Y;
              size = newpreview.Size;
              int height = size.Height;
              LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
            }
          }
        }
        else
        {
          if (UserSettings.Default.ArrangePreviewYDirection == "Down")
          {
            int y2 = newpreview.Location.Y;
            size = newpreview.Size;
            int height = size.Height;
            LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
            LastUsedX = !(UserSettings.Default.ArrangePreviewXDirection == "Left") ? newpreview.Location.X : newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
          else
          {
            LastUsedY = newpreview.Location.Y - UserSettings.Default.PreviewLayoutVerticalSpacing;
            LastUsedX = !(UserSettings.Default.ArrangePreviewXDirection == "Left") ? newpreview.Location.X : newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
          if (LastUsedY < y1 || LastUsedY > num5)
          {
            LastUsedY = !(UserSettings.Default.ArrangePreviewYDirection == "Down") ? num5 : y1;
            if (UserSettings.Default.ArrangePreviewXDirection == "Left")
            {
              LastUsedX = newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
            }
            else
            {
              int x2 = newpreview.Location.X;
              size = newpreview.Size;
              int width = size.Width;
              LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
            }
          }
        }
        size = newpreview.Size;
        ArrangeLargestPreviewHeight = size.Height;
        size = newpreview.Size;
        ArrangeLargestPreviewWidth = size.Width;
        UserSettings.Default.FirstPreview = false;
      }
      else
      {
        size = newpreview.Size;
        if (size.Height > ArrangeLargestPreviewHeight)
        {
          size = newpreview.Size;
          ArrangeLargestPreviewHeight = size.Height;
        }
        size = newpreview.Size;
        if (size.Width > ArrangeLargestPreviewWidth)
        {
          size = newpreview.Size;
          ArrangeLargestPreviewWidth = size.Width;
        }
        if (UserSettings.Default.RowsFirst)
        {
          if (UserSettings.Default.ArrangePreviewXDirection == "Right")
          {
            int num6 = LastUsedX;
            size = newpreview.Size;
            int width = size.Width;
            if (num6 + width > x1)
            {
              LastUsedX = num4;
              if (UserSettings.Default.ArrangePreviewYDirection == "Up")
              {
                LastUsedY = LastUsedY - (ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing);
                int num7 = LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 - height < y1)
                  LastUsedY = num5;
              }
              else
              {
                LastUsedY = LastUsedY + ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing;
                int num7 = LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 + height > num5)
                  LastUsedY = y1;
              }
            }
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int num7 = LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 - height1 < y1)
              {
                if (newpreview.Location.X != LastUsedX || newpreview.Location.Y != y1)
                  newpreview.Location = new Point(LastUsedX, y1);
              }
              else
              {
                int num8;
                if (newpreview.Location.X == LastUsedX)
                {
                  int y2 = newpreview.Location.Y;
                  int num9 = LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num10 = num9 - height2;
                  num8 = y2 == num10 ? 1 : 0;
                }
                else
                  num8 = 0;
                if (num8 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int x2 = LastUsedX;
                  int num9 = LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num9 - height2;
                  Point point = new Point(x2, y2);
                  thumbnailWindow.Location = point;
                }
              }
            }
            else
            {
              int num7 = LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 + height1 > num5)
              {
                int num8;
                if (newpreview.Location.X == LastUsedX)
                {
                  int y2 = newpreview.Location.Y;
                  int num9 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num10 = num9 - height2;
                  num8 = y2 == num10 ? 1 : 0;
                }
                else
                  num8 = 0;
                if (num8 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int x2 = LastUsedX;
                  int num9 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num9 - height2;
                  Point point = new Point(x2, y2);
                  thumbnailWindow.Location = point;
                }
              }
              else if (newpreview.Location.X != LastUsedX || newpreview.Location.Y != LastUsedY)
                newpreview.Location = new Point(LastUsedX, LastUsedY);
            }
          }
          else
          {
            int num6 = LastUsedX;
            size = newpreview.Size;
            int width1 = size.Width;
            if (num6 - width1 < num4)
            {
              LastUsedX = x1;
              if (UserSettings.Default.ArrangePreviewYDirection == "Up")
              {
                LastUsedY = LastUsedY - (ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing);
                int num7 = LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 - height < y1)
                  LastUsedY = num5;
              }
              else
              {
                LastUsedY = LastUsedY + ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing;
                int num7 = LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 + height > num5)
                  LastUsedY = y1;
              }
            }
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int num7 = LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 - height1 < y1)
              {
                int x2 = newpreview.Location.X;
                int num8 = LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != y1)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, y1);
                  thumbnailWindow.Location = point;
                }
              }
              else
              {
                int x2 = newpreview.Location.X;
                int num8 = LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                int num10;
                if (x2 == num9)
                {
                  int y2 = newpreview.Location.Y;
                  int num11 = LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num12 = num11 - height2;
                  num10 = y2 == num12 ? 1 : 0;
                }
                else
                  num10 = 0;
                if (num10 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num11 = LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  int x3 = num11 - width3;
                  int num12 = LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num12 - height2;
                  Point point = new Point(x3, y2);
                  thumbnailWindow.Location = point;
                }
              }
            }
            else
            {
              int num7 = LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 + height1 > num5)
              {
                int x2 = newpreview.Location.X;
                int num8 = LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                int num10;
                if (x2 == num9)
                {
                  int y2 = newpreview.Location.Y;
                  int num11 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num12 = num11 - height2;
                  num10 = y2 == num12 ? 1 : 0;
                }
                else
                  num10 = 0;
                if (num10 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num11 = LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  int x3 = num11 - width3;
                  int num12 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num12 - height2;
                  Point point = new Point(x3, y2);
                  thumbnailWindow.Location = point;
                }
              }
              else
              {
                int x2 = newpreview.Location.X;
                int num8 = LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != LastUsedY)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, LastUsedY);
                  thumbnailWindow.Location = point;
                }
              }
            }
          }
          if (UserSettings.Default.ArrangePreviewXDirection == "Right")
          {
            int x2 = newpreview.Location.X;
            size = newpreview.Size;
            int width = size.Width;
            LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
          else
            LastUsedX = newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
        }
        else
        {
          if (UserSettings.Default.ArrangePreviewYDirection == "Down")
          {
            int num6 = LastUsedY;
            size = newpreview.Size;
            int height = size.Height;
            if (num6 + height > num5)
            {
              LastUsedY = y1;
              if (UserSettings.Default.ArrangePreviewXDirection == "Left")
              {
                LastUsedX = LastUsedX - (ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing);
                int num7 = LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 - width < num4)
                  LastUsedX = x1;
              }
              else
              {
                LastUsedX = LastUsedX + ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing;
                int num7 = LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 + width > x1)
                  LastUsedX = num4;
              }
            }
            if (UserSettings.Default.ArrangePreviewXDirection == "Left")
            {
              int num7 = LastUsedX;
              size = newpreview.Size;
              int width1 = size.Width;
              if (num7 - width1 < x1)
              {
                if (newpreview.Location.X != x1 || newpreview.Location.Y != LastUsedY)
                  newpreview.Location = new Point(x1, LastUsedY);
              }
              else
              {
                int x2 = newpreview.Location.X;
                int num8 = LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != LastUsedY)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, LastUsedY);
                  thumbnailWindow.Location = point;
                }
              }
            }
            else
            {
              int num7 = LastUsedX;
              size = newpreview.Size;
              int width1 = size.Width;
              if (num7 + width1 > x1)
              {
                int x2 = newpreview.Location.X;
                int num8 = x1;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != LastUsedY)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = x1;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, LastUsedY);
                  thumbnailWindow.Location = point;
                }
              }
              else if (newpreview.Location.X != LastUsedX || newpreview.Location.Y != LastUsedY)
                newpreview.Location = new Point(LastUsedX, LastUsedY);
            }
          }
          else
          {
            int num6 = LastUsedY;
            size = newpreview.Size;
            int height1 = size.Height;
            if (num6 - height1 < y1)
            {
              LastUsedY = num5;
              if (UserSettings.Default.ArrangePreviewXDirection == "Left")
              {
                LastUsedX = LastUsedX - (ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing);
                int num7 = LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 - width < num4)
                  LastUsedX = x1;
              }
              else
              {
                LastUsedX = LastUsedX + ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing;
                int num7 = LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 + width > x1)
                  LastUsedX = num4;
              }
            }
            if (UserSettings.Default.ArrangePreviewXDirection == "Left")
            {
              int x2 = newpreview.Location.X;
              int num7 = LastUsedX;
              size = newpreview.Size;
              int width1 = size.Width;
              int num8 = num7 - width1;
              int num9;
              if (x2 == num8)
              {
                int y2 = newpreview.Location.Y;
                int num10 = LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int num11 = num10 - height2;
                num9 = y2 == num11 ? 1 : 0;
              }
              else
                num9 = 0;
              if (num9 == 0)
              {
                ThumbnailWindow thumbnailWindow = newpreview;
                int num10 = LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int x3 = num10 - width2;
                int num11 = LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int y2 = num11 - height2;
                Point point = new Point(x3, y2);
                thumbnailWindow.Location = point;
              }
            }
            else
            {
              int num7;
              if (newpreview.Location.X == LastUsedX)
              {
                int y2 = newpreview.Location.Y;
                int num8 = LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int num9 = num8 - height2;
                num7 = y2 == num9 ? 1 : 0;
              }
              else
                num7 = 0;
              if (num7 == 0)
              {
                ThumbnailWindow thumbnailWindow = newpreview;
                int x2 = LastUsedX;
                int num8 = LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int y2 = num8 - height2;
                Point point = new Point(x2, y2);
                thumbnailWindow.Location = point;
              }
            }
          }
          if (UserSettings.Default.ArrangePreviewYDirection == "Down")
          {
            int y2 = newpreview.Location.Y;
            size = newpreview.Size;
            int height = size.Height;
            LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
          }
          else
            LastUsedY = newpreview.Location.Y - UserSettings.Default.PreviewLayoutVerticalSpacing;
        }
      }
      if (state != 0)
        return;
      if (UserSettings.Default.PinToTop)
        newpreview.TopMost = true;
      else if (UserSettings.Default.PinToNormal)
        newpreview.TopMost = false;
      else if (UserSettings.Default.PinToBottom)
      {
        newpreview.TopMost = false;
        newpreview.SendToBack();
      }
      newpreview.Show();
      newpreview.RestoreTransparency();
    }

    public void HideTaskbarTab(IntPtr hWnd)
    {
      if (!UserSettings.Default.HideTaskBarButtons || (bool) TaskbarTabShown[TargettedWindows.IndexOf(hWnd)])
        return;
      tblc.DeleteTab((int) hWnd);
    }

    public void HideTaskbarTab()
    {
      if (!UserSettings.Default.HideTaskBarButtons)
        return;
      int index = 0;
      foreach (IntPtr num in TargettedWindows)
      {
        if (!(bool) TaskbarTabShown[index])
          tblc.DeleteTab((int) num);
        ++index;
      }
    }

    public void ShowTaskbarTab(IntPtr hWnd)
    {
      tblc.AddTab((int) hWnd);
    }

    public void ShowTaskbarTab()
    {
      foreach (IntPtr num in TargettedWindows)
        tblc.AddTab((int) num);
    }

    public void ToggleShowTaskbarTab(IntPtr hWnd, bool Show)
    {
      TaskbarTabShown[TargettedWindows.IndexOf(hWnd)] = Show;
    }

    private void PositionPreviewMenu()
    {
      TripResizeLoop = true;
      APPBARDATA pData = new APPBARDATA();
      int num1 = (int) SHAppBarMessage(5, ref pData);
      int num2 = (int) SHAppBarMessage(4, ref pData);
      int num3 = 0;
      if (UserSettings.Default.CustomTaskbarMargins)
      {
        int taskbarMarginsLeft = UserSettings.Default.CustomTaskbarMarginsLeft;
        int num4 = -UserSettings.Default.CustomTaskbarMarginsBottom;
        int taskbarMarginsTop = UserSettings.Default.CustomTaskbarMarginsTop;
        int num5 = -UserSettings.Default.CustomTaskbarMarginsRight;
        Point mousePosition = MousePosition;
        int x = mousePosition.X - Size.Width / 2;
        mousePosition = MousePosition;
        int y = mousePosition.Y;
        int num6 = x + Size.Width;
        Size size = SystemInformation.PrimaryMonitorSize;
        int num7 = size.Width + num5;
        if (num6 > num7)
        {
          size = SystemInformation.PrimaryMonitorSize;
          int num8 = size.Width + num5;
          size = Size;
          int width = size.Width;
          x = num8 - width;
        }
        if (x < taskbarMarginsLeft)
          x = taskbarMarginsLeft;
        size = Size;
        int height1 = size.Height;
        size = SystemInformation.PrimaryMonitorSize;
        int num9 = size.Height - taskbarMarginsTop + num4;
        if (height1 > num9)
        {
          size = Size;
          int width = size.Width;
          size = SystemInformation.PrimaryMonitorSize;
          int height2 = size.Height - taskbarMarginsTop + num4;
          Size = new Size(width, height2);
        }
        int num10 = y;
        size = Size;
        int height3 = size.Height;
        int num11 = num10 + height3;
        size = SystemInformation.PrimaryMonitorSize;
        int num12 = size.Height - taskbarMarginsTop + num4;
        if (num11 > num12)
        {
          size = SystemInformation.PrimaryMonitorSize;
          int num8 = size.Height - taskbarMarginsTop + num4;
          size = Size;
          int height2 = size.Height;
          y = num8 - height2;
        }
        if (y < taskbarMarginsTop)
          y = taskbarMarginsTop;
        Location = new Point(x, y);
      }
      else
      {
        int num4 = pData.rc.bottom;
        Size size = SystemInformation.PrimaryMonitorSize;
        int height1 = size.Height;
        int num5;
        if (num4 == height1 && pData.rc.left == 0)
        {
          int num6 = pData.rc.right;
          size = SystemInformation.PrimaryMonitorSize;
          int width = size.Width;
          num5 = num6 != width ? 1 : 0;
        }
        else
          num5 = 1;
        if (num5 == 0)
        {
          num3 = 1;
          int num6 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
          int x1 = MousePosition.X;
          size = Size;
          int num7 = size.Width / 2;
          int num8 = x1 - num7;
          int num9 = num8;
          size = Size;
          int width1 = size.Width;
          int num10 = num9 + width1;
          size = SystemInformation.PrimaryMonitorSize;
          int width2 = size.Width;
          if (num10 > width2)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int width3 = size.Width;
            size = Size;
            int width4 = size.Width;
            num8 = width3 - width4;
          }
          size = Size;
          int num11 = size.Height - num6;
          size = SystemInformation.PrimaryMonitorSize;
          int height2 = size.Height;
          if (num11 > height2)
          {
            size = Size;
            int width3 = size.Width;
            size = SystemInformation.PrimaryMonitorSize;
            int height3 = size.Height + num6;
            Size = new Size(width3, height3);
          }
          int x2 = num8;
          size = SystemInformation.PrimaryMonitorSize;
          int height4 = size.Height;
          size = Size;
          int height5 = size.Height;
          int y = height4 - height5 + num6;
          Location = new Point(x2, y);
        }
        else
        {
          int num6;
          if (pData.rc.top == 0 && pData.rc.left == 0)
          {
            int num7 = pData.rc.right;
            size = SystemInformation.PrimaryMonitorSize;
            int width = size.Width;
            num6 = num7 != width ? 1 : 0;
          }
          else
            num6 = 1;
          if (num6 == 0)
          {
            num3 = 2;
            int y = pData.rc.bottom;
            int x1 = MousePosition.X;
            size = Size;
            int num7 = size.Width / 2;
            int x2 = x1 - num7;
            int num8 = x2;
            size = Size;
            int width1 = size.Width;
            int num9 = num8 + width1;
            size = SystemInformation.PrimaryMonitorSize;
            int width2 = size.Width;
            if (num9 > width2)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int width3 = size.Width;
              size = Size;
              int width4 = size.Width;
              x2 = width3 - width4;
            }
            size = Size;
            int num10 = size.Height + y;
            size = SystemInformation.PrimaryMonitorSize;
            int height2 = size.Height;
            if (num10 > height2)
            {
              size = Size;
              int width3 = size.Width;
              size = SystemInformation.PrimaryMonitorSize;
              int height3 = size.Height - y;
              Size = new Size(width3, height3);
            }
            Location = new Point(x2, y);
          }
          else
          {
            int num7;
            if (pData.rc.top == 0 && pData.rc.left == 0)
            {
              int num8 = pData.rc.bottom;
              size = SystemInformation.PrimaryMonitorSize;
              int height2 = size.Height;
              num7 = num8 != height2 ? 1 : 0;
            }
            else
              num7 = 1;
            if (num7 == 0)
            {
              num3 = 3;
              int x = pData.rc.right;
              int y = MousePosition.Y;
              size = Size;
              Rectangle bounds;
              if (size.Height > Screen.PrimaryScreen.Bounds.Height)
              {
                size = Size;
                int width = size.Width;
                bounds = Screen.PrimaryScreen.Bounds;
                int height2 = bounds.Height;
                Size = new Size(width, height2);
              }
              int num8 = y;
              size = Size;
              int height3 = size.Height;
              int num9 = num8 + height3;
              bounds = Screen.PrimaryScreen.Bounds;
              int height4 = bounds.Height;
              if (num9 > height4)
              {
                bounds = Screen.PrimaryScreen.Bounds;
                int bottom = bounds.Bottom;
                size = Size;
                int height2 = size.Height;
                y = bottom - height2;
              }
              Location = new Point(x, y);
            }
            else
            {
              int num8;
              if (pData.rc.top == 0)
              {
                int num9 = pData.rc.right;
                size = SystemInformation.PrimaryMonitorSize;
                int width = size.Width;
                if (num9 == width)
                {
                  int num10 = pData.rc.bottom;
                  size = SystemInformation.PrimaryMonitorSize;
                  int height2 = size.Height;
                  num8 = num10 != height2 ? 1 : 0;
                  goto label_43;
                }
              }
              num8 = 1;
label_43:
              if (num8 == 0)
              {
                num3 = 4;
                size = SystemInformation.PrimaryMonitorSize;
                int num9 = size.Width - (pData.rc.right - pData.rc.left);
                size = Size;
                int width1 = size.Width;
                int x = num9 - width1;
                int y = MousePosition.Y;
                size = Size;
                Rectangle bounds;
                if (size.Height > Screen.PrimaryScreen.Bounds.Height)
                {
                  size = Size;
                  int width2 = size.Width;
                  bounds = Screen.PrimaryScreen.Bounds;
                  int height2 = bounds.Height;
                  Size = new Size(width2, height2);
                }
                int num10 = y;
                size = Size;
                int height3 = size.Height;
                int num11 = num10 + height3;
                bounds = Screen.PrimaryScreen.Bounds;
                int height4 = bounds.Height;
                if (num11 > height4)
                {
                  bounds = Screen.PrimaryScreen.Bounds;
                  int bottom = bounds.Bottom;
                  size = Size;
                  int height2 = size.Height;
                  y = bottom - height2;
                }
                Location = new Point(x, y);
              }
            }
          }
        }
      }
      TripResizeLoop = false;
    }

    private void GetWindows()
    {
      windows.Clear();
      EnumWindows(Callback, 0);
    }

    private bool Callback(IntPtr hwnd, int lParam)
    {
      if (Handle != hwnd && ((long) GetWindowLongA(hwnd, GWL_STYLE) & (long) TARGETWINDOW) == (long) TARGETWINDOW)
      {
        StringBuilder lpString = new StringBuilder(100);
        GetWindowText(hwnd, lpString, lpString.Capacity);
        windows.Add(new Window
        {
          Handle = hwnd,
          Title = lpString.ToString()
        });
      }
      return true;
    }

    private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      cleardata();
      Hide();
      if (!AllMinimized)
        MinimizePreviews_Click(this, null);
      else
        ShowPreviews_Click(this, null);
    }

    private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left && MinimizeHotkeyDown)
      {
        ShowPreviews_Click(this, null);
      }
      else
      {
        if (e.Button == MouseButtons.Right)
          return;
        if (e.Button == MouseButtons.Middle)
        {
          ShowPreviews_Click(this, null);
        }
        else
        {
          TripResizeLoop = true;
          cleardata();
          Point mousePosition = MousePosition;
          int x = mousePosition.X;
          mousePosition = MousePosition;
          int y = mousePosition.Y;
          TrayClickPoint = new Point(x, y);
          GetWindows();
          int index = 0;
          int top1 = 0;
          if (thumb != IntPtr.Zero)
            DwmUnregisterThumbnail(thumb);
          DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
          foreach (Window window in windows)
          {
            DwmRegisterThumbnail(Handle, windows[index].Handle, out thumb);
            ThumbList.Add(thumb);
            PSIZE size1;
            DwmQueryThumbnailSourceSize(thumb, out size1);
            int previewMenuSizeX = UserSettings.Default.PreviewMenuSizeX;
            Size size2 = Size;
            int height1 = size2.Height;
            Size = new Size(previewMenuSizeX, height1);
            props.fVisible = true;
            props.dwFlags = DWM_TNP_VISIBLE | DWM_TNP_RECTDESTINATION | DWM_TNP_OPACITY | DWM_TNP_RECTSOURCE;
            props.opacity = byte.MaxValue;
            // ISSUE: explicit reference operation
            // ISSUE: variable of a reference type
            DWM_THUMBNAIL_PROPERTIES local = @props;
            size2 = SystemInformation.FrameBorderSize;
            int left = -size2.Width;
            size2 = SystemInformation.FrameBorderSize;
            int top2 = -size2.Height;
            int num1 = size1.x;
            size2 = SystemInformation.FrameBorderSize;
            int width = size2.Width;
            int right = num1 + width;
            int num2 = size1.y;
            size2 = SystemInformation.FrameBorderSize;
            int height2 = size2.Height;
            int bottom = num2 + height2;
            Rect rect = new Rect(left, top2, right, bottom);
            // ISSUE: explicit reference operation
            (local).rcSource = rect;
            Opacity = 1.0;
            props.rcDestination = new Rect(0, top1, pictureBox1.Width, top1 + (int) (pictureBox1.Width * (double) size1.y / size1.x) + UserSettings.Default.VerticalSpaceBetweenPreviews);
            PreviewDataLeft.Add(0);
            PreviewDataTop.Add(top1);
            PreviewDataRight.Add(pictureBox1.Width);
            PreviewDataBottom.Add(top1 + (int) (pictureBox1.Width * (double) size1.y / size1.x));
            PreviewDataHandle.Add(windows[index].Handle);
            top1 = top1 + (int) (pictureBox1.Width * (double) size1.y / size1.x) + UserSettings.Default.VerticalSpaceBetweenPreviews;
            ++index;
            DwmUpdateThumbnailProperties(thumb, ref props);
          }
          int num3 = top1 - UserSettings.Default.VerticalSpaceBetweenPreviews;
          int previewMenuSizeX1 = UserSettings.Default.PreviewMenuSizeX;
          int num4 = num3;
          Size size = Size;
          int height3 = size.Height;
          size = pictureBox1.Size;
          int height4 = size.Height;
          int num5 = height3 - height4;
          int height5 = num4 + num5;
          Size = new Size(previewMenuSizeX1, height5);
          PositionPreviewMenu();
          TopMost = true;
          Show();
          BringToFront();
          Activate();
          TripResizeLoop = false;
        }
      }
    }

    private void click_timer_Tick(object sender, EventArgs e)
    {
      click_timer.Stop();
    }

    private void TrayPreview_Load(object sender, EventArgs e)
    {
      cleardata();
      Hide();
    }

    private void TrayPreview_Shown(object sender, EventArgs e)
    {
      cleardata();
      Hide();
    }

    private void cleardata()
    {
      foreach (IntPtr thumb in ThumbList)
      {
        try
        {
          if (thumb != IntPtr.Zero)
            DwmUnregisterThumbnail(thumb);
        }
        catch (Exception ex)
        {
        }
      }
      ThumbList.Clear();
      this.thumb = IntPtr.Zero;
      PreviewDataLeft.Clear();
      PreviewDataTop.Clear();
      PreviewDataRight.Clear();
      PreviewDataBottom.Clear();
      PreviewDataHandle.Clear();
    }

    private void pictureBox1_MouseLeave(object sender, EventArgs e)
    {
    }

    private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
    {
      
      // ISSUE: explicit reference operation
      // ISSUE: variable of a reference type
      
      Point mousePosition = MousePosition;
      int x1 = mousePosition.X;
      mousePosition = MousePosition;
      int y1 = mousePosition.Y;
      // ISSUE: explicit reference operation
      Point p = new Point(x1, y1);
      Point local = p;
      Point point = PointToClient(p);
      int x2 = point.X;
      int y2 = point.Y;
      int index = 0;
      if (!(e.Button.ToString() == "Left"))
        return;
      try
      {
        foreach (IntPtr hWnd in PreviewDataHandle)
        {
          if (x2 >= int.Parse(PreviewDataLeft[index].ToString()) && x2 <= int.Parse(PreviewDataRight[index].ToString()) && y2 >= int.Parse(PreviewDataTop[index].ToString()) && y2 <= int.Parse(PreviewDataBottom[index].ToString()))
          {
            if (!TargettedWindows.Contains(hWnd))
            {
              if (IsIconic(hWnd))
              {
                CreatePreview(hWnd, true);
                break;
              }
              CreatePreview(hWnd, false);
              break;
            }
            SendMessage((IntPtr) long.Parse(ViPPreviewHandles[TargettedWindows.IndexOf(hWnd)].ToString()), 274, 61728, 0);
            break;
          }
          ++index;
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Array Out of Bounds");
      }
    }

    private void notifyIcon1_MouseMove(object sender, MouseEventArgs e)
    {
    }

    protected override void WndProc(ref Message m)
    {
      if (m.Msg == 522)
      {
        int num = (int) m.WParam <= 0 ? -1 : 1;
        if (num > 0)
        {
          myscroll(UserSettings.Default.PreviewMenuScrollSensitivity);
        }
        else
        {
          if (num >= 0)
            return;
          myscroll(-UserSettings.Default.PreviewMenuScrollSensitivity);
        }
      }
      else
        base.WndProc(ref m);
    }

    private void myscroll(int updown)
    {
      if (PreviewDataTop.Count <= 0)
        return;
      Activate();
      int num1 = int.Parse(PreviewDataBottom[PreviewDataBottom.Count - 1].ToString());
      Size size1 = pictureBox1.Size;
      int height1 = size1.Height;
      if (num1 <= height1 && updown < 0 || int.Parse(PreviewDataTop[0].ToString()) >= 0 && updown > 0)
        return;
      int num2 = int.Parse(PreviewDataBottom[PreviewDataBottom.Count - 1].ToString()) + updown;
      size1 = pictureBox1.Size;
      int height2 = size1.Height;
      if (num2 < height2 && updown < 0)
      {
        size1 = pictureBox1.Size;
        updown = size1.Height - int.Parse(PreviewDataBottom[PreviewDataBottom.Count - 1].ToString());
      }
      else if (int.Parse(PreviewDataTop[0].ToString()) + updown > 0 && updown > 0)
        updown = -int.Parse(PreviewDataTop[0].ToString());
      int index = 0;
      DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
      foreach (IntPtr thumb in ThumbList)
      {
        PSIZE size2;
        DwmQueryThumbnailSourceSize(thumb, out size2);
        props.fVisible = true;
        props.dwFlags = DWM_TNP_VISIBLE | DWM_TNP_RECTDESTINATION | DWM_TNP_OPACITY | DWM_TNP_RECTSOURCE;
        props.opacity = byte.MaxValue;
        Opacity = 1.0;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        DWM_THUMBNAIL_PROPERTIES local = props;
        size1 = SystemInformation.FrameBorderSize;
        int left = -size1.Width;
        size1 = SystemInformation.FrameBorderSize;
        int top = -size1.Height;
        int num3 = size2.x;
        size1 = SystemInformation.FrameBorderSize;
        int width = size1.Width;
        int right = num3 + width;
        int num4 = size2.y;
        size1 = SystemInformation.FrameBorderSize;
        int height3 = size1.Height;
        int bottom = num4 + height3;
        Rect rect = new Rect(left, top, right, bottom);
        // ISSUE: explicit reference operation
        (local).rcSource = rect;
        props.rcDestination = new Rect(0, int.Parse(PreviewDataTop[index].ToString()) + updown, pictureBox1.Width, int.Parse(PreviewDataBottom[index].ToString()) + updown);
        PreviewDataTop[index] = int.Parse(PreviewDataTop[index].ToString()) + updown;
        PreviewDataBottom[index] = int.Parse(PreviewDataBottom[index].ToString()) + updown;
        DwmUpdateThumbnailProperties((IntPtr) ThumbList[index], ref props);
        ++index;
      }
    }

    private void TrayPreview_FormClosing(object sender, FormClosingEventArgs e)
    {
    }

    private void TrayPreview_FormClosed(object sender, FormClosedEventArgs e)
    {
    }

    private void pictureBox1_MouseHover(object sender, EventArgs e)
    {
    }

    private void TrayPreview_MouseHover(object sender, EventArgs e)
    {
    }

    private void TrayPreview_Deactivate(object sender, EventArgs e)
    {
      TripResizeLoop = true;
      cleardata();
      Hide();
      GC.KeepAlive(this);
      TripResizeLoop = false;
    }

    private void quitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      quitting = true;
      foreach (IntPtr hWnd in ViPPreviewHandles)
      {
        try
        {
          if (ViPPreviewHandles.Contains(hWnd))
            SendMessage(hWnd, 274, 61536, 0);
        }
        catch (Exception ex)
        {
          Console.WriteLine("closing exception");
        }
      }
      notifyIcon1.Dispose();
      Application.Exit();
    }

    private void contextMenuStrip1_Opened(object sender, EventArgs e)
    {
      contextMenuStrip1.BringToFront();
    }

    private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
    {
      CreatePreviewsOnMinimize.Checked = UserSettings.Default.CreatePreviewsOnMinimize;
      requireCTRLKeyToCreatePreviewToolStripMenuItem.Checked = UserSettings.Default.CreatePreviewOnlyOnHotkey;
      if (UserSettings.Default.PinToTop)
      {
        topmostToolStripMenuItem.Checked = true;
        desktopToolStripMenuItem.Checked = false;
        normalToolStripMenuItem.Checked = false;
      }
      else if (UserSettings.Default.PinToNormal)
      {
        topmostToolStripMenuItem.Checked = false;
        desktopToolStripMenuItem.Checked = false;
        normalToolStripMenuItem.Checked = true;
      }
      else if (UserSettings.Default.PinToBottom)
      {
        topmostToolStripMenuItem.Checked = false;
        desktopToolStripMenuItem.Checked = true;
        normalToolStripMenuItem.Checked = false;
      }
      if (UserSettings.Default.PreviewGridAlign == "Snap")
      {
        snapToGridToolStripMenuItem.Checked = true;
        noneToolStripMenuItem.Checked = false;
      }
      else
      {
        snapToGridToolStripMenuItem.Checked = false;
        noneToolStripMenuItem.Checked = true;
      }
      contextMenuStrip1.Visible = true;
      contextMenuStrip1.BringToFront();
    }

    private void makePreviewsIgnoreInputToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (IntPtr hWnd in ViPPreviewHandles)
        SetWindowLong(hWnd, -20, GetWindowLong(hWnd, -20) | 32L);
    }

    private void makePreviewsRespondToInputToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (IntPtr hWnd in ViPPreviewHandles)
        SetWindowLong(hWnd, -20, GetWindowLong(hWnd, -20) + -32L);
    }

    private void TrayPreview_KeyDown(object sender, KeyEventArgs e)
    {
      switch (e.KeyCode.ToString())
      {
        case "Down":
          myscroll(UserSettings.Default.PreviewMenuScrollSensitivity);
          break;
        case "Up":
          myscroll(-UserSettings.Default.PreviewMenuScrollSensitivity);
          break;
      }
    }

    private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Form form = new SettingsPanel(this);
      form.StartPosition = FormStartPosition.CenterScreen;
      form.Show();
      form.TopMost = true;
      form.BringToFront();
    }

    public void ChangeTransparency(int thumbtransparencyvalue, int backtransparencyvalue)
    {
      foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
        thumbnailWindow.ChangeTransparency(thumbtransparencyvalue, backtransparencyvalue);
    }

    private void TrayPreview_Resize(object sender, EventArgs e)
    {
      if (TripResizeLoop)
        return;
      UserSettings.Default.PreviewMenuSizeX = Size.Width;
      UserSettings.Default.Save();
      int width1 = Size.Width;
      Size size1 = SystemInformation.MinimizedWindowSize;
      int width2 = size1.Width;
      if (width1 <= width2)
      {
        // ISSUE: variable of a compiler-generated type
        UserSettings @default = UserSettings.Default;
        size1 = SystemInformation.MinimizedWindowSize;
        int width3 = size1.Width;
        @default.PreviewMenuSizeX = width3;
        UserSettings.Default.Save();
      }
      else
      {
        int index = 0;
        int top1 = 0;
        DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
        PreviewDataLeft.Clear();
        PreviewDataTop.Clear();
        PreviewDataRight.Clear();
        PreviewDataBottom.Clear();
        PreviewDataHandle.Clear();
        foreach (IntPtr num1 in ThumbList)
        {
          PSIZE size2;
          DwmQueryThumbnailSourceSize(num1, out size2);
          props.fVisible = true;
          props.dwFlags = DWM_TNP_VISIBLE | DWM_TNP_RECTDESTINATION | DWM_TNP_OPACITY | DWM_TNP_RECTSOURCE;
          // ISSUE: explicit reference operation
          // ISSUE: variable of a reference type
          DWM_THUMBNAIL_PROPERTIES local = @props;
          size1 = SystemInformation.FrameBorderSize;
          int left = -size1.Width;
          size1 = SystemInformation.FrameBorderSize;
          int top2 = -size1.Height;
          int num2 = size2.x;
          size1 = SystemInformation.FrameBorderSize;
          int width3 = size1.Width;
          int right = num2 + width3;
          int num3 = size2.y;
          size1 = SystemInformation.FrameBorderSize;
          int height = size1.Height;
          int bottom = num3 + height;
          Rect rect = new Rect(left, top2, right, bottom);
          // ISSUE: explicit reference operation
          (local).rcSource = rect;
          props.opacity = byte.MaxValue;
          Opacity = 1.0;
          props.rcDestination = new Rect(0, top1, pictureBox1.Width, top1 + (int) (pictureBox1.Width * (double) size2.y / size2.x) + UserSettings.Default.VerticalSpaceBetweenPreviews);
          DwmUpdateThumbnailProperties(num1, ref props);
          PreviewDataLeft.Add(0);
          PreviewDataTop.Add(top1);
          PreviewDataRight.Add(pictureBox1.Width);
          PreviewDataBottom.Add(top1 + (int) (pictureBox1.Width * (double) size2.y / size2.x));
          PreviewDataHandle.Add(windows[index].Handle);
          top1 = top1 + (int) (pictureBox1.Width * (double) size2.y / size2.x) + UserSettings.Default.VerticalSpaceBetweenPreviews;
          ++index;
        }
        Size size3 = new Size(UserSettings.Default.PreviewMenuSizeX, top1 - UserSettings.Default.VerticalSpaceBetweenPreviews + (Size.Height - pictureBox1.Size.Height));
      }
    }

    private void CreatePreviewsOnMinimize_Click(object sender, EventArgs e)
    {
      UserSettings.Default.CreatePreviewsOnMinimize = CreatePreviewsOnMinimize.Checked;
      UserSettings.Default.Save();
    }

    private void PreviewAllOpenWindows_Click(object sender, EventArgs e)
    {
      GetWindows();
      try
      {
        foreach (Window window in windows)
        {
          if (!TargettedWindows.Contains(window.Handle))
          {
            if (IsIconic(window.Handle))
              CreatePreview(window.Handle, true);
            else
              CreatePreview(window.Handle, false);
          }
        }
      }
      catch (Exception ex)
      {
      }
    }

    private void MinimizePreviews_Click(object sender, EventArgs e)
    {
      MinimizingPreviews = true;
      foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
        thumbnailWindow.minimizeToolStripMenuItem_Click(this, null);
      MinimizingPreviews = false;
      AllMinimized = true;
    }

    private void ShowPreviews_Click(object sender, EventArgs e)
    {
      foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
      {
        if (!thumbnailWindow.TopMost)
        {
          thumbnailWindow.TopMost = true;
          thumbnailWindow.TopMost = false;
        }
        thumbnailWindow.Show();
        thumbnailWindow.BringToFront();
      }
      AllMinimized = false;
    }

    public void AeroBackGroundRefresh()
    {
      foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
      {
        thumbnailWindow.ResizeFormP(thumbnailWindow.Size.Width, thumbnailWindow.Size.Height);
        thumbnailWindow.Invalidate();
      }
    }

    private void RecoverLostWindows_Click(object sender, EventArgs e)
    {
      restoreAllTargetWindowsToolStripMenuItem_Click(this, null);
      GetWindows();
      foreach (Window window in windows)
      {
        if (window.Handle != Handle)
        {
          SetWindowLong(window.Handle, -20, 0);
          ShowTaskbarTab(window.Handle);
          StringBuilder lpString = new StringBuilder(100);
          GetWindowText(window.Handle, lpString, lpString.Capacity);
          if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(GetWindowProcessID((int) window.Handle)).ProcessName.Equals("Photoshop"))
          {
            bool flag = false;
            if (XPAppearance.MinAnimate)
            {
              XPAppearance.MinAnimate = false;
              flag = true;
            }
            SetWindowPos(window.Handle, IntPtr.Zero, 0, 0, 0, 0, 17U);
            if (flag)
              XPAppearance.MinAnimate = true;
          }
        }
      }
    }

    public void MyKeyDown(object sender, KeyEventArgs e)
    {
      GC.KeepAlive(actHook);
      if (e.KeyData.ToString() == "LShiftKey" || e.KeyData.ToString() == "RShiftKey")
        MinimizeHotkeyDown = true;
      else if (e.KeyData.ToString() == "LWin" || e.KeyData.ToString() == "RWin")
        Modifier1 = true;
      else if (e.KeyData.ToString() == "F5")
      {
        if (Modifier1)
          PreviewAllOpenWindows_Click(this, null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
      else if (e.KeyData.ToString() == "F2")
      {
        if (Modifier1)
          ShowPreviews_Click(this, null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
      else if (e.KeyData.ToString() == "F3")
      {
        if (Modifier1)
          MinimizePreviews_Click(this, null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
      else
      {
        if (!(e.KeyData.ToString() == "F4"))
          return;
        if (Modifier1)
          restoreAllTargetWindowsToolStripMenuItem_Click(this, null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
    }

    public void MyKeyUp(object sender, KeyEventArgs e)
    {
      GC.KeepAlive(actHook);
      if (e.KeyData.ToString() == "LShiftKey" || e.KeyData.ToString() == "RShiftKey")
      {
        MinimizeHotkeyDown = false;
      }
      else
      {
        if (!(e.KeyData.ToString() == "LWin") && !(e.KeyData.ToString() == "RWin"))
          return;
        Modifier1 = false;
      }
    }

    private void requireCTRLKeyToCreatePreviewToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.CreatePreviewOnlyOnHotkey = requireCTRLKeyToCreatePreviewToolStripMenuItem.Checked;
      UserSettings.Default.Save();
    }

    private void topmostToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.PinToTop = true;
      UserSettings.Default.PinToNormal = false;
      UserSettings.Default.PinToBottom = false;
      UserSettings.Default.Save();
      topmostToolStripMenuItem.Checked = true;
      desktopToolStripMenuItem.Checked = false;
      normalToolStripMenuItem.Checked = false;
      foreach (Form form in PreviewControls)
        form.TopMost = true;
    }

    private void desktopToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.PinToTop = false;
      UserSettings.Default.PinToNormal = false;
      UserSettings.Default.PinToBottom = true;
      UserSettings.Default.Save();
      topmostToolStripMenuItem.Checked = false;
      desktopToolStripMenuItem.Checked = true;
      normalToolStripMenuItem.Checked = false;
      int index = 0;
      foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
      {
        if (!(bool) AlwaysOnTop[index])
        {
          thumbnailWindow.TopMost = false;
          thumbnailWindow.SendToBack();
        }
        ++index;
      }
    }

    private void normalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.PinToTop = false;
      UserSettings.Default.PinToNormal = true;
      UserSettings.Default.PinToBottom = false;
      UserSettings.Default.Save();
      topmostToolStripMenuItem.Checked = false;
      desktopToolStripMenuItem.Checked = false;
      normalToolStripMenuItem.Checked = true;
      int index = 0;
      foreach (ThumbnailWindow thumbnailWindow in PreviewControls)
      {
        if (!(bool) AlwaysOnTop[index])
          thumbnailWindow.TopMost = false;
        ++index;
      }
    }

    private void contextMenuStrip1_Closing(object sender, ToolStripDropDownClosingEventArgs e)
    {
    }

    private void noneToolStripMenuItem_Click(object sender, EventArgs e)
    {
      snapToGridToolStripMenuItem.Checked = false;
      noneToolStripMenuItem.Checked = true;
      UserSettings.Default.PreviewGridAlign = "None";
      UserSettings.Default.Save();
    }

    private void snapToGridToolStripMenuItem_Click(object sender, EventArgs e)
    {
      snapToGridToolStripMenuItem.Checked = true;
      noneToolStripMenuItem.Checked = false;
      UserSettings.Default.PreviewGridAlign = "Snap";
      UserSettings.Default.Save();
      ReAlign();
    }

    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern void DwmExtendFrameIntoClientArea(IntPtr hwnd, ref MARGINS margins);

    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern bool DwmIsCompositionEnabled();

    private delegate bool EnumWindowsCallback(IntPtr hwnd, int lParam);

    public enum MouseEventType
    {
      LeftDown = 2,
      LeftUp = 4,
      RightDown = 8,
      RightUp = 16
    }

    private enum ShowWindowEnum
    {
      Hide = 0,
      ShowNormal = 1,
      ShowMinimized = 2,
      Maximize = 3,
      ShowMaximized = 3,
      ShowNormalNoActivate = 4,
      Show = 5,
      Minimize = 6,
      ShowMinNoActivate = 7,
      ShowNoActivate = 8,
      Restore = 9,
      ShowDefault = 10,
      ForceMinimized = 11
    }

    public struct MARGINS
    {
      public int Left;
      public int Right;
      public int Top;
      public int Bottom;
    }
  }
}
