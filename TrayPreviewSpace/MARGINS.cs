﻿// Decompiled with JetBrains decompiler
// Type: TrayPreviewSpace.MARGINS
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

namespace TrayPreviewSpace
{
  public struct MARGINS
  {
    public int Left;
    public int Right;
    public int Top;
    public int Bottom;
  }
}
