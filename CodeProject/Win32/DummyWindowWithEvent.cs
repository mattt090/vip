﻿// Decompiled with JetBrains decompiler
// Type: CodeProject.Win32.DummyWindowWithEvent
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Windows.Forms;

namespace CodeProject.Win32
{
  public class DummyWindowWithEvent : NativeWindowWithEvent, IDisposable
  {
    public DummyWindowWithEvent()
    {
      CreateHandle(new CreateParams());
    }

    public void Dispose()
    {
      if (!(Handle != (IntPtr) 0))
        return;
      DestroyHandle();
    }
  }
}
