﻿// Decompiled with JetBrains decompiler
// Type: CodeProject.Win32.NativeWindowWithEvent
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System.Windows.Forms;

namespace CodeProject.Win32
{
  public class NativeWindowWithEvent : NativeWindow
  {
    public event MessageEventHandler ProcessMessage;

    protected override void WndProc(ref Message m)
    {
      if (ProcessMessage != null)
      {
        bool Handled = false;
        ProcessMessage(this, ref m, ref Handled);
        if (Handled)
          return;
        base.WndProc(ref m);
      }
      else
        base.WndProc(ref m);
    }
  }
}
