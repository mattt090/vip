﻿// Decompiled with JetBrains decompiler
// Type: CodeProject.SystemHotkey.SystemHotkey
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using CodeProject.Win32;

namespace CodeProject.SystemHotkey
{
  public class SystemHotkey : Component, IDisposable
  {
    private Container components;
    protected DummyWindowWithEvent m_Window = new DummyWindowWithEvent();
    protected Shortcut m_HotKey = Shortcut.None;
    protected bool isRegistered;

    public bool IsRegistered
    {
      get
      {
        return isRegistered;
      }
    }

    [DefaultValue(Shortcut.None)]
    public Shortcut Shortcut
    {
      get
      {
        return m_HotKey;
      }
      set
      {
        if (DesignMode)
        {
          m_HotKey = value;
        }
        else
        {
          if (isRegistered && m_HotKey != value)
          {
            if (UnregisterHotkey())
            {
              Debug.WriteLine("Unreg: OK");
              isRegistered = false;
            }
            else
            {
              if (Error != null)
                Error(this, EventArgs.Empty);
              Debug.WriteLine("Unreg: ERR");
            }
          }
          if (value == Shortcut.None)
          {
            m_HotKey = value;
          }
          else
          {
            if (RegisterHotkey(value))
            {
              Debug.WriteLine("Reg: OK");
              isRegistered = true;
            }
            else
            {
              if (Error != null)
                Error(this, EventArgs.Empty);
              Debug.WriteLine("Reg: ERR");
            }
            m_HotKey = value;
          }
        }
      }
    }

    public event EventHandler Pressed;

    public event EventHandler Error;

    public SystemHotkey(IContainer container)
    {
      container.Add(this);
      InitializeComponent();
      m_Window.ProcessMessage += MessageEvent;
    }

    public SystemHotkey()
    {
      InitializeComponent();
      if (DesignMode)
        return;
      m_Window.ProcessMessage += MessageEvent;
    }

    public new void Dispose()
    {
      if (isRegistered && UnregisterHotkey())
        Debug.WriteLine("Unreg: OK");
      Debug.WriteLine("Disposed");
    }

    private void InitializeComponent()
    {
      components = new Container();
    }

    protected void MessageEvent(object sender, ref Message m, ref bool Handled)
    {
      if (m.Msg != 786 || !(m.WParam == (IntPtr) GetType().GetHashCode()))
        return;
      Handled = true;
      Debug.WriteLine("HOTKEY pressed!");
      if (Pressed != null)
        Pressed(this, EventArgs.Empty);
    }

    protected bool UnregisterHotkey()
    {
      return User32.UnregisterHotKey(m_Window.Handle, GetType().GetHashCode());
    }

    protected bool RegisterHotkey(Shortcut key)
    {
      int fsModifiers = 0;
      Keys keys = Keys.None;
      if ((key & (Shortcut) 262144) == (Shortcut) 262144)
      {
        ++fsModifiers;
        keys = Keys.Alt;
      }
      if ((key & (Shortcut) 65536) == (Shortcut) 65536)
      {
        fsModifiers += 4;
        keys = Keys.Shift;
      }
      if ((key & (Shortcut) 131072) == (Shortcut) 131072)
      {
        fsModifiers += 2;
        keys = Keys.Control;
      }
      Debug.Write(fsModifiers + " ");
      Debug.WriteLine(((int) key - (int) keys).ToString());
      return User32.RegisterHotKey(m_Window.Handle, GetType().GetHashCode(), fsModifiers, (int) key - (int) keys);
    }
  }
}
