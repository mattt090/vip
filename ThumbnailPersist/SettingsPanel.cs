﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.SettingsPanel
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ThumbnailPersist.Properties;
using TrayPreviewSpace;

namespace ThumbnailPersist
{
  public class SettingsPanel : Form
  {
    private IContainer components;
    public bool TB1changed;
    public bool TB2changed;
    public bool TB3changed;
    public bool TB4changed;
    public bool TB5changed;
    public bool TB6changed;
    private bool OriginalEnableAeroBack = true;
    private TabControl tabControl1;
    private TabPage tabPage1;
    private TabPage tabPage2;
    private TrackBar trackBar5;
    private TrackBar trackBar4;
    private TrackBar trackBar3;
    private TrackBar trackBar2;
    private TrackBar trackBar1;
    private Button MainOK;
    private Button MainCancel;
    private Button ApplyButton;
    private CheckBox DockPreviewCB;
    private TabPage tabPage3;
    private TabPage tabPage4;
    private GroupBox groupBox1;
    private CheckBox CustomCB;
    private CheckBox WindowsDefaultCB;
    private CheckBox TopRightCB;
    private CheckBox BottomRightCB;
    private CheckBox TopMiddleCB;
    private CheckBox BottomMiddleCB;
    private CheckBox TopLeftCB;
    private CheckBox BottomLeftCB;
    private TabPage tabPage5;
    private Label label1;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label label5;
    private NumericUpDown CustomLocationX;
    private Label label7;
    private Label label6;
    private NumericUpDown CustomLocationY;
    private CheckBox CentreCB;
    private Label MinPreviewSize;
    private Label label8;
    private Label label9;
    private NumericUpDown MinPreviewSizeY;
    private NumericUpDown MinPreviewSizeX;
    private GroupBox groupBox2;
    private Label MaxPreviewSize;
    private Label label11;
    private Label label12;
    private NumericUpDown MaxPreviewSizeY;
    private NumericUpDown MaxPreviewSizeX;
    private Label label10;
    private CheckBox ShowBordersByDefault;
    private Label TransparencyLabel;
    private TrackBar ThumbTransparencyTB;
    private Label label13;
    private TrackBar BackTransparency;
    private CheckBox ShowBackText;
    private CheckBox RightCB;
    private CheckBox LeftCB;
    private CheckBox TopCB;
    private CheckBox BottomCB;
    private Label ZoomMouseScroll;
    private TrackBar trackBar6;
    private CheckBox ShowListofWindows;
    private GroupBox groupBox3;
    private Label label14;
    private NumericUpDown PreviewLaunchWidth;
    private Label label15;
    private NumericUpDown PreviewLaunchVerticalSpace;
    private Label label16;
    private TrackBar PreviewLauncherScrollSensitivity;
    private CheckBox HideTargetByDefault;
    private CheckBox KeepTargetFromMinimize;
    private Label label17;
    private CheckBox FreeFormEdgeDocking;
    private GroupBox groupBox4;
    private CheckBox FreeFormCentreDocking;
    private NumericUpDown CentreDockingThickness;
    private NumericUpDown EdgeDockingThickness;
    private ToolTip EdgeDockingThicknessToolTip;
    private ToolTip CentreDockingThicknessToolTip;
    private CheckBox HideTaskbarButtons;
    private CheckBox CreatePreviewsOnMinimize;
    private Label label18;
    private CheckBox RequireHotkeyOnMinimize;
    private Label label19;
    private TabPage Layout;
    private GroupBox groupBox8;
    private GroupBox groupBox5;
    private GroupBox groupBox9;
    private CheckBox ColumnsFirst;
    private CheckBox RowsFirst;
    private GroupBox groupBox7;
    private CheckBox DownDirectionCB;
    private CheckBox UpDirectionCB;
    private GroupBox groupBox6;
    private CheckBox RightDirectionCB;
    private CheckBox LeftDirectionCB;
    private CheckBox PinDesktop;
    private CheckBox PinNormal;
    private CheckBox PinTopMost;
    private Label label20;
    private Label VerticalSpacingLabel;
    private NumericUpDown PreviewHorizontalSpacing;
    private NumericUpDown PreviewVerticalSpacing;
    private GroupBox groupBox10;
    private GroupBox groupBox11;
    private NumericUpDown MarginBottom;
    private NumericUpDown MarginTop;
    private PictureBox pictureBox1;
    private NumericUpDown MarginLeft;
    private NumericUpDown MarginRight;
    private CheckBox CustomTaskbarMargins;
    private CheckBox AutomaticTaskbarMargins;
    private CheckBox EnableAeroBack;
    private Label label21;
    private Label label22;
    private TrayPreview qparent;

    public SettingsPanel(TrayPreview frm1)
    {
      InitializeComponent();
      qparent = frm1;
      AcceptButton = MainOK;
      CancelButton = MainCancel;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && components != null)
        components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      components = new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (SettingsPanel));
      tabControl1 = new TabControl();
      tabPage2 = new TabPage();
      groupBox4 = new GroupBox();
      CentreDockingThickness = new NumericUpDown();
      EdgeDockingThickness = new NumericUpDown();
      FreeFormCentreDocking = new CheckBox();
      FreeFormEdgeDocking = new CheckBox();
      DockPreviewCB = new CheckBox();
      groupBox1 = new GroupBox();
      RightCB = new CheckBox();
      LeftCB = new CheckBox();
      TopCB = new CheckBox();
      BottomCB = new CheckBox();
      CentreCB = new CheckBox();
      label7 = new Label();
      label6 = new Label();
      CustomLocationY = new NumericUpDown();
      CustomLocationX = new NumericUpDown();
      CustomCB = new CheckBox();
      WindowsDefaultCB = new CheckBox();
      TopRightCB = new CheckBox();
      BottomRightCB = new CheckBox();
      TopMiddleCB = new CheckBox();
      BottomMiddleCB = new CheckBox();
      TopLeftCB = new CheckBox();
      BottomLeftCB = new CheckBox();
      Layout = new TabPage();
      groupBox11 = new GroupBox();
      CustomTaskbarMargins = new CheckBox();
      AutomaticTaskbarMargins = new CheckBox();
      MarginLeft = new NumericUpDown();
      MarginRight = new NumericUpDown();
      pictureBox1 = new PictureBox();
      MarginBottom = new NumericUpDown();
      MarginTop = new NumericUpDown();
      groupBox10 = new GroupBox();
      label20 = new Label();
      VerticalSpacingLabel = new Label();
      PreviewHorizontalSpacing = new NumericUpDown();
      PreviewVerticalSpacing = new NumericUpDown();
      groupBox8 = new GroupBox();
      PinDesktop = new CheckBox();
      PinNormal = new CheckBox();
      PinTopMost = new CheckBox();
      groupBox5 = new GroupBox();
      groupBox9 = new GroupBox();
      ColumnsFirst = new CheckBox();
      RowsFirst = new CheckBox();
      groupBox7 = new GroupBox();
      DownDirectionCB = new CheckBox();
      UpDirectionCB = new CheckBox();
      groupBox6 = new GroupBox();
      RightDirectionCB = new CheckBox();
      LeftDirectionCB = new CheckBox();
      tabPage3 = new TabPage();
      label22 = new Label();
      label21 = new Label();
      EnableAeroBack = new CheckBox();
      ShowBackText = new CheckBox();
      label13 = new Label();
      BackTransparency = new TrackBar();
      TransparencyLabel = new Label();
      ThumbTransparencyTB = new TrackBar();
      ShowBordersByDefault = new CheckBox();
      tabPage1 = new TabPage();
      label16 = new Label();
      PreviewLauncherScrollSensitivity = new TrackBar();
      ZoomMouseScroll = new Label();
      label5 = new Label();
      label4 = new Label();
      label3 = new Label();
      label2 = new Label();
      label1 = new Label();
      trackBar5 = new TrackBar();
      trackBar4 = new TrackBar();
      trackBar3 = new TrackBar();
      trackBar2 = new TrackBar();
      trackBar1 = new TrackBar();
      trackBar6 = new TrackBar();
      tabPage4 = new TabPage();
      groupBox3 = new GroupBox();
      label15 = new Label();
      PreviewLaunchVerticalSpace = new NumericUpDown();
      label14 = new Label();
      PreviewLaunchWidth = new NumericUpDown();
      groupBox2 = new GroupBox();
      label10 = new Label();
      MaxPreviewSize = new Label();
      label11 = new Label();
      label12 = new Label();
      MaxPreviewSizeY = new NumericUpDown();
      MaxPreviewSizeX = new NumericUpDown();
      MinPreviewSize = new Label();
      label8 = new Label();
      label9 = new Label();
      MinPreviewSizeY = new NumericUpDown();
      MinPreviewSizeX = new NumericUpDown();
      tabPage5 = new TabPage();
      label19 = new Label();
      RequireHotkeyOnMinimize = new CheckBox();
      label18 = new Label();
      HideTaskbarButtons = new CheckBox();
      CreatePreviewsOnMinimize = new CheckBox();
      label17 = new Label();
      KeepTargetFromMinimize = new CheckBox();
      HideTargetByDefault = new CheckBox();
      ShowListofWindows = new CheckBox();
      MainOK = new Button();
      MainCancel = new Button();
      ApplyButton = new Button();
      EdgeDockingThicknessToolTip = new ToolTip(components);
      CentreDockingThicknessToolTip = new ToolTip(components);
      tabControl1.SuspendLayout();
      tabPage2.SuspendLayout();
      groupBox4.SuspendLayout();
      CentreDockingThickness.BeginInit();
      EdgeDockingThickness.BeginInit();
      groupBox1.SuspendLayout();
      CustomLocationY.BeginInit();
      CustomLocationX.BeginInit();
      Layout.SuspendLayout();
      groupBox11.SuspendLayout();
      MarginLeft.BeginInit();
      MarginRight.BeginInit();
      ((ISupportInitialize) pictureBox1).BeginInit();
      MarginBottom.BeginInit();
      MarginTop.BeginInit();
      groupBox10.SuspendLayout();
      PreviewHorizontalSpacing.BeginInit();
      PreviewVerticalSpacing.BeginInit();
      groupBox8.SuspendLayout();
      groupBox5.SuspendLayout();
      groupBox9.SuspendLayout();
      groupBox7.SuspendLayout();
      groupBox6.SuspendLayout();
      tabPage3.SuspendLayout();
      BackTransparency.BeginInit();
      ThumbTransparencyTB.BeginInit();
      tabPage1.SuspendLayout();
      PreviewLauncherScrollSensitivity.BeginInit();
      trackBar5.BeginInit();
      trackBar4.BeginInit();
      trackBar3.BeginInit();
      trackBar2.BeginInit();
      trackBar1.BeginInit();
      trackBar6.BeginInit();
      tabPage4.SuspendLayout();
      groupBox3.SuspendLayout();
      PreviewLaunchVerticalSpace.BeginInit();
      PreviewLaunchWidth.BeginInit();
      groupBox2.SuspendLayout();
      MaxPreviewSizeY.BeginInit();
      MaxPreviewSizeX.BeginInit();
      MinPreviewSizeY.BeginInit();
      MinPreviewSizeX.BeginInit();
      tabPage5.SuspendLayout();
      SuspendLayout();
      tabControl1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      tabControl1.Controls.Add(tabPage2);
      tabControl1.Controls.Add(Layout);
      tabControl1.Controls.Add(tabPage3);
      tabControl1.Controls.Add(tabPage1);
      tabControl1.Controls.Add(tabPage4);
      tabControl1.Controls.Add(tabPage5);
      tabControl1.Location = new Point(5, 5);
      tabControl1.Margin = new Padding(0);
      tabControl1.Name = "tabControl1";
      tabControl1.SelectedIndex = 0;
      tabControl1.Size = new Size(309, 417);
      tabControl1.TabIndex = 0;
      tabPage2.Controls.Add(groupBox4);
      tabPage2.Controls.Add(groupBox1);
      tabPage2.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
      tabPage2.Location = new Point(4, 22);
      tabPage2.Margin = new Padding(0);
      tabPage2.Name = "tabPage2";
      tabPage2.Size = new Size(301, 391);
      tabPage2.TabIndex = 1;
      tabPage2.Text = "Location";
      tabPage2.UseVisualStyleBackColor = true;
      groupBox4.Controls.Add(CentreDockingThickness);
      groupBox4.Controls.Add(EdgeDockingThickness);
      groupBox4.Controls.Add(FreeFormCentreDocking);
      groupBox4.Controls.Add(FreeFormEdgeDocking);
      groupBox4.Controls.Add(DockPreviewCB);
      groupBox4.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox4.Location = new Point(6, 0);
      groupBox4.Name = "groupBox4";
      groupBox4.Size = new Size(289, 116);
      groupBox4.TabIndex = 3;
      groupBox4.TabStop = false;
      groupBox4.Text = "Docking";
      CentreDockingThickness.Location = new Point(167, 77);
      NumericUpDown numericUpDown1 = CentreDockingThickness;
      int[] bits1 = new int[4];
      bits1[0] = 10000;
      Decimal num1 = new Decimal(bits1);
      numericUpDown1.Maximum = num1;
      CentreDockingThickness.Name = "CentreDockingThickness";
      CentreDockingThickness.Size = new Size(60, 27);
      CentreDockingThickness.TabIndex = 13;
      EdgeDockingThickness.Location = new Point(157, 45);
      NumericUpDown numericUpDown2 = EdgeDockingThickness;
      int[] bits2 = new int[4];
      bits2[0] = 10000;
      Decimal num2 = new Decimal(bits2);
      numericUpDown2.Maximum = num2;
      EdgeDockingThickness.Name = "EdgeDockingThickness";
      EdgeDockingThickness.Size = new Size(60, 27);
      EdgeDockingThickness.TabIndex = 12;
      FreeFormCentreDocking.AutoSize = true;
      FreeFormCentreDocking.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      FreeFormCentreDocking.Location = new Point(7, 82);
      FreeFormCentreDocking.Name = "FreeFormCentreDocking";
      FreeFormCentreDocking.Size = new Size(160, 19);
      FreeFormCentreDocking.TabIndex = 3;
      FreeFormCentreDocking.Text = "Freeform Centre Docking";
      FreeFormCentreDocking.UseVisualStyleBackColor = true;
      FreeFormCentreDocking.CheckedChanged += FreeFormCentreDocking_CheckedChanged;
      FreeFormEdgeDocking.AutoSize = true;
      FreeFormEdgeDocking.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      FreeFormEdgeDocking.Location = new Point(7, 48);
      FreeFormEdgeDocking.Name = "FreeFormEdgeDocking";
      FreeFormEdgeDocking.Size = new Size(150, 19);
      FreeFormEdgeDocking.TabIndex = 2;
      FreeFormEdgeDocking.Text = "Freeform Edge Docking";
      FreeFormEdgeDocking.UseVisualStyleBackColor = true;
      FreeFormEdgeDocking.CheckedChanged += FreeFormEdgeDocking_CheckedChanged;
      DockPreviewCB.AutoSize = true;
      DockPreviewCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      DockPreviewCB.Location = new Point(6, 20);
      DockPreviewCB.Name = "DockPreviewCB";
      DockPreviewCB.Size = new Size(191, 19);
      DockPreviewCB.TabIndex = 0;
      DockPreviewCB.Text = "Dock Preview to Start Location";
      DockPreviewCB.UseVisualStyleBackColor = true;
      DockPreviewCB.CheckedChanged += DockPreviewCB_CheckedChanged;
      groupBox1.Controls.Add(RightCB);
      groupBox1.Controls.Add(LeftCB);
      groupBox1.Controls.Add(TopCB);
      groupBox1.Controls.Add(BottomCB);
      groupBox1.Controls.Add(CentreCB);
      groupBox1.Controls.Add(label7);
      groupBox1.Controls.Add(label6);
      groupBox1.Controls.Add(CustomLocationY);
      groupBox1.Controls.Add(CustomLocationX);
      groupBox1.Controls.Add(CustomCB);
      groupBox1.Controls.Add(WindowsDefaultCB);
      groupBox1.Controls.Add(TopRightCB);
      groupBox1.Controls.Add(BottomRightCB);
      groupBox1.Controls.Add(TopMiddleCB);
      groupBox1.Controls.Add(BottomMiddleCB);
      groupBox1.Controls.Add(TopLeftCB);
      groupBox1.Controls.Add(BottomLeftCB);
      groupBox1.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox1.Location = new Point(22, 117);
      groupBox1.Name = "groupBox1";
      groupBox1.Size = new Size(256, 270);
      groupBox1.TabIndex = 1;
      groupBox1.TabStop = false;
      groupBox1.Text = "Preview Start Location";
      RightCB.AutoSize = true;
      RightCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      RightCB.Location = new Point(142, 143);
      RightCB.Name = "RightCB";
      RightCB.Size = new Size(54, 19);
      RightCB.TabIndex = 16;
      RightCB.Text = "Right";
      RightCB.UseVisualStyleBackColor = true;
      RightCB.CheckedChanged += RightCB_CheckedChanged;
      LeftCB.AutoSize = true;
      LeftCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      LeftCB.Location = new Point(16, 145);
      LeftCB.Name = "LeftCB";
      LeftCB.Size = new Size(45, 19);
      LeftCB.TabIndex = 15;
      LeftCB.Text = "Left";
      LeftCB.UseVisualStyleBackColor = true;
      LeftCB.CheckedChanged += LeftCB_CheckedChanged;
      TopCB.AutoSize = true;
      TopCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      TopCB.Location = new Point(142, 113);
      TopCB.Name = "TopCB";
      TopCB.Size = new Size(45, 19);
      TopCB.TabIndex = 14;
      TopCB.Text = "Top";
      TopCB.UseVisualStyleBackColor = true;
      TopCB.CheckedChanged += TopCB_CheckedChanged;
      BottomCB.AutoSize = true;
      BottomCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      BottomCB.Location = new Point(16, 119);
      BottomCB.Name = "BottomCB";
      BottomCB.Size = new Size(65, 19);
      BottomCB.TabIndex = 13;
      BottomCB.Text = "Bottom";
      BottomCB.UseVisualStyleBackColor = true;
      BottomCB.CheckedChanged += BottomCB_CheckedChanged;
      CentreCB.AutoSize = true;
      CentreCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      CentreCB.Location = new Point(143, 170);
      CentreCB.Name = "CentreCB";
      CentreCB.Size = new Size(61, 19);
      CentreCB.TabIndex = 12;
      CentreCB.Text = "Centre";
      CentreCB.UseVisualStyleBackColor = true;
      CentreCB.CheckedChanged += CentreCB_CheckedChanged;
      label7.AutoSize = true;
      label7.Location = new Point(111, 235);
      label7.Name = "label7";
      label7.Size = new Size(17, 19);
      label7.TabIndex = 11;
      label7.Text = "Y";
      label6.AutoSize = true;
      label6.Location = new Point(111, 203);
      label6.Name = "label6";
      label6.Size = new Size(18, 19);
      label6.TabIndex = 10;
      label6.Text = "X";
      CustomLocationY.Location = new Point(135, 235);
      NumericUpDown numericUpDown3 = CustomLocationY;
      int[] bits3 = new int[4];
      bits3[0] = 10000;
      Decimal num3 = new Decimal(bits3);
      numericUpDown3.Maximum = num3;
      CustomLocationY.Name = "CustomLocationY";
      CustomLocationY.Size = new Size(84, 27);
      CustomLocationY.TabIndex = 9;
      CustomLocationX.Location = new Point(135, 201);
      NumericUpDown numericUpDown4 = CustomLocationX;
      int[] bits4 = new int[4];
      bits4[0] = 10000;
      Decimal num4 = new Decimal(bits4);
      numericUpDown4.Maximum = num4;
      CustomLocationX.Name = "CustomLocationX";
      CustomLocationX.Size = new Size(84, 27);
      CustomLocationX.TabIndex = 8;
      CustomCB.AutoSize = true;
      CustomCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      CustomCB.Location = new Point(39, 221);
      CustomCB.Name = "CustomCB";
      CustomCB.Size = new Size(67, 19);
      CustomCB.TabIndex = 7;
      CustomCB.Text = "Custom";
      CustomCB.UseVisualStyleBackColor = true;
      CustomCB.CheckedChanged += CustomCB_CheckedChanged;
      WindowsDefaultCB.AutoSize = true;
      WindowsDefaultCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      WindowsDefaultCB.Location = new Point(17, 170);
      WindowsDefaultCB.Name = "WindowsDefaultCB";
      WindowsDefaultCB.Size = new Size(121, 19);
      WindowsDefaultCB.TabIndex = 6;
      WindowsDefaultCB.Text = "Windows Default";
      WindowsDefaultCB.UseVisualStyleBackColor = true;
      WindowsDefaultCB.CheckedChanged += WindowsDefaultCB_CheckedChanged;
      TopRightCB.AutoSize = true;
      TopRightCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      TopRightCB.Location = new Point(143, 87);
      TopRightCB.Name = "TopRightCB";
      TopRightCB.Size = new Size(76, 19);
      TopRightCB.TabIndex = 5;
      TopRightCB.Text = "Top Right";
      TopRightCB.UseVisualStyleBackColor = true;
      TopRightCB.CheckedChanged += TopRightCB_CheckedChanged;
      BottomRightCB.AutoSize = true;
      BottomRightCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      BottomRightCB.Location = new Point(17, 90);
      BottomRightCB.Name = "BottomRightCB";
      BottomRightCB.Size = new Size(96, 19);
      BottomRightCB.TabIndex = 4;
      BottomRightCB.Text = "Bottom Right";
      BottomRightCB.UseVisualStyleBackColor = true;
      BottomRightCB.CheckedChanged += BottomRightCB_CheckedChanged;
      TopMiddleCB.AutoSize = true;
      TopMiddleCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      TopMiddleCB.Location = new Point(143, 60);
      TopMiddleCB.Name = "TopMiddleCB";
      TopMiddleCB.Size = new Size(87, 19);
      TopMiddleCB.TabIndex = 3;
      TopMiddleCB.Text = "Top Middle";
      TopMiddleCB.UseVisualStyleBackColor = true;
      TopMiddleCB.CheckedChanged += TopMiddleCB_CheckedChanged;
      BottomMiddleCB.AutoSize = true;
      BottomMiddleCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      BottomMiddleCB.Location = new Point(17, 61);
      BottomMiddleCB.Name = "BottomMiddleCB";
      BottomMiddleCB.Size = new Size(107, 19);
      BottomMiddleCB.TabIndex = 2;
      BottomMiddleCB.Text = "Bottom Middle";
      BottomMiddleCB.UseVisualStyleBackColor = true;
      BottomMiddleCB.CheckedChanged += BottomMiddleCB_CheckedChanged;
      TopLeftCB.AutoSize = true;
      TopLeftCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      TopLeftCB.Location = new Point(144, 32);
      TopLeftCB.Name = "TopLeftCB";
      TopLeftCB.Size = new Size(67, 19);
      TopLeftCB.TabIndex = 1;
      TopLeftCB.Text = "Top Left";
      TopLeftCB.UseVisualStyleBackColor = true;
      TopLeftCB.CheckedChanged += TopLeftCB_CheckedChanged;
      BottomLeftCB.AutoSize = true;
      BottomLeftCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      BottomLeftCB.Location = new Point(17, 28);
      BottomLeftCB.Name = "BottomLeftCB";
      BottomLeftCB.Size = new Size(87, 19);
      BottomLeftCB.TabIndex = 0;
      BottomLeftCB.Text = "Bottom Left";
      BottomLeftCB.UseVisualStyleBackColor = true;
      BottomLeftCB.CheckedChanged += BottomLeftCB_CheckedChanged;
      Layout.Controls.Add(groupBox11);
      Layout.Controls.Add(groupBox10);
      Layout.Controls.Add(groupBox8);
      Layout.Controls.Add(groupBox5);
      Layout.Location = new Point(4, 22);
      Layout.Margin = new Padding(0);
      Layout.Name = "Layout";
      Layout.Size = new Size(301, 391);
      Layout.TabIndex = 5;
      Layout.Text = "Layout";
      Layout.UseVisualStyleBackColor = true;
      groupBox11.Controls.Add(CustomTaskbarMargins);
      groupBox11.Controls.Add(AutomaticTaskbarMargins);
      groupBox11.Controls.Add(MarginLeft);
      groupBox11.Controls.Add(MarginRight);
      groupBox11.Controls.Add(pictureBox1);
      groupBox11.Controls.Add(MarginBottom);
      groupBox11.Controls.Add(MarginTop);
      groupBox11.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox11.Location = new Point(9, 215);
      groupBox11.Name = "groupBox11";
      groupBox11.Size = new Size(283, 173);
      groupBox11.TabIndex = 39;
      groupBox11.TabStop = false;
      groupBox11.Text = "Account for Vista Taskbar Margins";
      CustomTaskbarMargins.AutoSize = true;
      CustomTaskbarMargins.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      CustomTaskbarMargins.Location = new Point(217, 26);
      CustomTaskbarMargins.Name = "CustomTaskbarMargins";
      CustomTaskbarMargins.Size = new Size(67, 19);
      CustomTaskbarMargins.TabIndex = 40;
      CustomTaskbarMargins.Text = "Custom";
      CustomTaskbarMargins.UseVisualStyleBackColor = true;
      CustomTaskbarMargins.CheckedChanged += CustomTaskbarMargins_CheckedChanged;
      AutomaticTaskbarMargins.AutoSize = true;
      AutomaticTaskbarMargins.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      AutomaticTaskbarMargins.Location = new Point(6, 26);
      AutomaticTaskbarMargins.Name = "AutomaticTaskbarMargins";
      AutomaticTaskbarMargins.Size = new Size(81, 19);
      AutomaticTaskbarMargins.TabIndex = 39;
      AutomaticTaskbarMargins.Text = "Automatic";
      AutomaticTaskbarMargins.UseVisualStyleBackColor = true;
      AutomaticTaskbarMargins.CheckedChanged += AutomaticTaskbarMargins_CheckedChanged;
      MarginLeft.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MarginLeft.Location = new Point(4, 81);
      MarginLeft.Maximum = new Decimal(new int[4]);
      MarginLeft.Name = "MarginLeft";
      MarginLeft.Size = new Size(60, 23);
      MarginLeft.TabIndex = 38;
      MarginRight.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MarginRight.Location = new Point(218, 81);
      MarginRight.Maximum = new Decimal(new int[4]);
      MarginRight.Name = "MarginRight";
      MarginRight.Size = new Size(60, 23);
      MarginRight.TabIndex = 37;
      pictureBox1.Image = Resources.Desktop;
      pictureBox1.Location = new Point(68, 53);
      pictureBox1.Name = "pictureBox1";
      pictureBox1.Size = new Size(146, 86);
      pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
      pictureBox1.TabIndex = 36;
      pictureBox1.TabStop = false;
      MarginBottom.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MarginBottom.Location = new Point(109, 143);
      MarginBottom.Maximum = new Decimal(new int[4]);
      MarginBottom.Name = "MarginBottom";
      MarginBottom.Size = new Size(60, 23);
      MarginBottom.TabIndex = 35;
      MarginTop.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MarginTop.Location = new Point(110, 26);
      MarginTop.Maximum = new Decimal(new int[4]);
      MarginTop.Name = "MarginTop";
      MarginTop.Size = new Size(60, 23);
      MarginTop.TabIndex = 34;
      groupBox10.Controls.Add(label20);
      groupBox10.Controls.Add(VerticalSpacingLabel);
      groupBox10.Controls.Add(PreviewHorizontalSpacing);
      groupBox10.Controls.Add(PreviewVerticalSpacing);
      groupBox10.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox10.Location = new Point(8, 155);
      groupBox10.Name = "groupBox10";
      groupBox10.Size = new Size(283, 56);
      groupBox10.TabIndex = 38;
      groupBox10.TabStop = false;
      groupBox10.Text = "Spacing Between Previews";
      label20.AutoSize = true;
      label20.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label20.Location = new Point(132, 24);
      label20.Name = "label20";
      label20.Size = new Size(65, 15);
      label20.TabIndex = 37;
      label20.Text = "Horizontal";
      VerticalSpacingLabel.AutoSize = true;
      VerticalSpacingLabel.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      VerticalSpacingLabel.Location = new Point(13, 24);
      VerticalSpacingLabel.Name = "VerticalSpacingLabel";
      VerticalSpacingLabel.Size = new Size(48, 15);
      VerticalSpacingLabel.TabIndex = 36;
      VerticalSpacingLabel.Text = "Vertical";
      PreviewHorizontalSpacing.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      PreviewHorizontalSpacing.Location = new Point(198, 22);
      PreviewHorizontalSpacing.Minimum = new Decimal(new int[4]
      {
        100,
        0,
        0,
        int.MinValue
      });
      PreviewHorizontalSpacing.Name = "PreviewHorizontalSpacing";
      PreviewHorizontalSpacing.Size = new Size(60, 23);
      PreviewHorizontalSpacing.TabIndex = 35;
      PreviewVerticalSpacing.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      PreviewVerticalSpacing.Location = new Point(66, 22);
      PreviewVerticalSpacing.Minimum = new Decimal(new int[4]
      {
        100,
        0,
        0,
        int.MinValue
      });
      PreviewVerticalSpacing.Name = "PreviewVerticalSpacing";
      PreviewVerticalSpacing.Size = new Size(60, 23);
      PreviewVerticalSpacing.TabIndex = 34;
      groupBox8.Controls.Add(PinDesktop);
      groupBox8.Controls.Add(PinNormal);
      groupBox8.Controls.Add(PinTopMost);
      groupBox8.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox8.Location = new Point(8, 95);
      groupBox8.Name = "groupBox8";
      groupBox8.Size = new Size(284, 55);
      groupBox8.TabIndex = 33;
      groupBox8.TabStop = false;
      groupBox8.Text = "Pin Previews To.....";
      PinDesktop.AutoSize = true;
      PinDesktop.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      PinDesktop.Location = new Point(205, 26);
      PinDesktop.Name = "PinDesktop";
      PinDesktop.Size = new Size(70, 19);
      PinDesktop.TabIndex = 29;
      PinDesktop.Text = "Desktop";
      PinDesktop.UseVisualStyleBackColor = true;
      PinDesktop.CheckedChanged += PinDesktop_CheckedChanged;
      PinNormal.AutoSize = true;
      PinNormal.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      PinNormal.Location = new Point(113, 26);
      PinNormal.Name = "PinNormal";
      PinNormal.Size = new Size(67, 19);
      PinNormal.TabIndex = 28;
      PinNormal.Text = "Normal";
      PinNormal.UseVisualStyleBackColor = true;
      PinNormal.CheckedChanged += PinNormal_CheckedChanged;
      PinTopMost.AutoSize = true;
      PinTopMost.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      PinTopMost.Location = new Point(15, 26);
      PinTopMost.Name = "PinTopMost";
      PinTopMost.Size = new Size(72, 19);
      PinTopMost.TabIndex = 27;
      PinTopMost.Text = "Topmost";
      PinTopMost.UseVisualStyleBackColor = true;
      PinTopMost.CheckedChanged += PinTopMost_CheckedChanged;
      groupBox5.Controls.Add(groupBox9);
      groupBox5.Controls.Add(groupBox7);
      groupBox5.Controls.Add(groupBox6);
      groupBox5.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox5.Location = new Point(8, 3);
      groupBox5.Name = "groupBox5";
      groupBox5.Size = new Size(284, 91);
      groupBox5.TabIndex = 31;
      groupBox5.TabStop = false;
      groupBox5.Text = "Preview Layout Direction";
      groupBox9.Controls.Add(ColumnsFirst);
      groupBox9.Controls.Add(RowsFirst);
      groupBox9.Location = new Point(159, 13);
      groupBox9.Name = "groupBox9";
      groupBox9.Size = new Size(119, 68);
      groupBox9.TabIndex = 32;
      groupBox9.TabStop = false;
      ColumnsFirst.AutoSize = true;
      ColumnsFirst.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      ColumnsFirst.Location = new Point(14, 41);
      ColumnsFirst.Name = "ColumnsFirst";
      ColumnsFirst.Size = new Size(102, 19);
      ColumnsFirst.TabIndex = 29;
      ColumnsFirst.Text = "Columns First";
      ColumnsFirst.UseVisualStyleBackColor = true;
      ColumnsFirst.CheckedChanged += ColumnsFirst_CheckedChanged;
      RowsFirst.AutoSize = true;
      RowsFirst.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      RowsFirst.Location = new Point(14, 16);
      RowsFirst.Name = "RowsFirst";
      RowsFirst.Size = new Size(83, 19);
      RowsFirst.TabIndex = 28;
      RowsFirst.Text = "Rows First";
      RowsFirst.UseVisualStyleBackColor = true;
      RowsFirst.CheckedChanged += RowsFirst_CheckedChanged;
      groupBox7.Controls.Add(DownDirectionCB);
      groupBox7.Controls.Add(UpDirectionCB);
      groupBox7.Location = new Point(78, 14);
      groupBox7.Name = "groupBox7";
      groupBox7.Size = new Size(75, 68);
      groupBox7.TabIndex = 31;
      groupBox7.TabStop = false;
      DownDirectionCB.AutoSize = true;
      DownDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      DownDirectionCB.Location = new Point(11, 41);
      DownDirectionCB.Name = "DownDirectionCB";
      DownDirectionCB.Size = new Size(57, 19);
      DownDirectionCB.TabIndex = 29;
      DownDirectionCB.Text = "Down";
      DownDirectionCB.UseVisualStyleBackColor = true;
      DownDirectionCB.CheckedChanged += DownDirectionCB_CheckedChanged;
      UpDirectionCB.AutoSize = true;
      UpDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      UpDirectionCB.Location = new Point(11, 16);
      UpDirectionCB.Name = "UpDirectionCB";
      UpDirectionCB.Size = new Size(41, 19);
      UpDirectionCB.TabIndex = 28;
      UpDirectionCB.Text = "Up";
      UpDirectionCB.UseVisualStyleBackColor = true;
      UpDirectionCB.CheckedChanged += UpDirectionCB_CheckedChanged;
      groupBox6.Controls.Add(RightDirectionCB);
      groupBox6.Controls.Add(LeftDirectionCB);
      groupBox6.Location = new Point(7, 14);
      groupBox6.Name = "groupBox6";
      groupBox6.Size = new Size(65, 68);
      groupBox6.TabIndex = 30;
      groupBox6.TabStop = false;
      RightDirectionCB.AutoSize = true;
      RightDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      RightDirectionCB.Location = new Point(9, 42);
      RightDirectionCB.Name = "RightDirectionCB";
      RightDirectionCB.Size = new Size(54, 19);
      RightDirectionCB.TabIndex = 27;
      RightDirectionCB.Text = "Right";
      RightDirectionCB.UseVisualStyleBackColor = true;
      RightDirectionCB.CheckedChanged += RightDirectionCB_CheckedChanged;
      LeftDirectionCB.AutoSize = true;
      LeftDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      LeftDirectionCB.Location = new Point(9, 17);
      LeftDirectionCB.Name = "LeftDirectionCB";
      LeftDirectionCB.Size = new Size(45, 19);
      LeftDirectionCB.TabIndex = 26;
      LeftDirectionCB.Text = "Left";
      LeftDirectionCB.UseVisualStyleBackColor = true;
      LeftDirectionCB.CheckedChanged += LeftDirectionCB_CheckedChanged;
      tabPage3.Controls.Add(label22);
      tabPage3.Controls.Add(label21);
      tabPage3.Controls.Add(EnableAeroBack);
      tabPage3.Controls.Add(ShowBackText);
      tabPage3.Controls.Add(label13);
      tabPage3.Controls.Add(BackTransparency);
      tabPage3.Controls.Add(TransparencyLabel);
      tabPage3.Controls.Add(ThumbTransparencyTB);
      tabPage3.Controls.Add(ShowBordersByDefault);
      tabPage3.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
      tabPage3.Location = new Point(4, 22);
      tabPage3.Margin = new Padding(0);
      tabPage3.Name = "tabPage3";
      tabPage3.Size = new Size(301, 391);
      tabPage3.TabIndex = 2;
      tabPage3.Text = "Appearance";
      tabPage3.UseVisualStyleBackColor = true;
      label22.AutoSize = true;
      label22.Location = new Point(76, 171);
      label22.Name = "label22";
      label22.Size = new Size(126, 13);
      label22.TabIndex = 29;
      label22.Text = "with Aero Background on.";
      label21.AutoSize = true;
      label21.Location = new Point(18, 157);
      label21.Name = "label21";
      label21.Size = new Size(251, 13);
      label21.TabIndex = 28;
      label21.Text = "Note: Context menu may appear behind the preview";
      EnableAeroBack.AutoSize = true;
      EnableAeroBack.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      EnableAeroBack.Location = new Point(27, 134);
      EnableAeroBack.Name = "EnableAeroBack";
      EnableAeroBack.Size = new Size(225, 19);
      EnableAeroBack.TabIndex = 27;
      EnableAeroBack.Text = "Enable Aero Background in Previews";
      EnableAeroBack.UseVisualStyleBackColor = true;
      EnableAeroBack.CheckedChanged += EnableAeroBack_CheckedChanged;
      ShowBackText.AutoSize = true;
      ShowBackText.Enabled = false;
      ShowBackText.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      ShowBackText.Location = new Point(64, 329);
      ShowBackText.Name = "ShowBackText";
      ShowBackText.Size = new Size(147, 19);
      ShowBackText.TabIndex = 18;
      ShowBackText.Text = "Show Background Text";
      ShowBackText.UseVisualStyleBackColor = true;
      ShowBackText.Visible = false;
      ShowBackText.CheckedChanged += ShowBackText_CheckedChanged;
      label13.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      label13.AutoSize = true;
      label13.Location = new Point(78, 67);
      label13.Name = "label13";
      label13.Size = new Size(106, 13);
      label13.TabIndex = 17;
      label13.Text = "Overall Transparency";
      BackTransparency.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      BackTransparency.BackColor = SystemColors.Window;
      BackTransparency.Location = new Point(18, 80);
      BackTransparency.Margin = new Padding(0);
      BackTransparency.Maximum = 254;
      BackTransparency.Minimum = 1;
      BackTransparency.Name = "BackTransparency";
      BackTransparency.Size = new Size(269, 45);
      BackTransparency.TabIndex = 16;
      BackTransparency.TickStyle = TickStyle.None;
      BackTransparency.Value = 1;
      BackTransparency.Scroll += TransparencyTB_Scroll;
      TransparencyLabel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      TransparencyLabel.AutoSize = true;
      TransparencyLabel.Location = new Point(78, 22);
      TransparencyLabel.Name = "TransparencyLabel";
      TransparencyLabel.Size = new Size(110, 13);
      TransparencyLabel.TabIndex = 15;
      TransparencyLabel.Text = "Preview Transparency";
      ThumbTransparencyTB.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      ThumbTransparencyTB.BackColor = SystemColors.Window;
      ThumbTransparencyTB.Location = new Point(18, 35);
      ThumbTransparencyTB.Margin = new Padding(0);
      ThumbTransparencyTB.Maximum = byte.MaxValue;
      ThumbTransparencyTB.Name = "ThumbTransparencyTB";
      ThumbTransparencyTB.Size = new Size(269, 45);
      ThumbTransparencyTB.TabIndex = 14;
      ThumbTransparencyTB.TickStyle = TickStyle.None;
      ThumbTransparencyTB.Value = 1;
      ThumbTransparencyTB.Scroll += TransparencyTB_Scroll;
      ShowBordersByDefault.AutoSize = true;
      ShowBordersByDefault.Enabled = false;
      ShowBordersByDefault.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      ShowBordersByDefault.Location = new Point(64, 363);
      ShowBordersByDefault.Name = "ShowBordersByDefault";
      ShowBordersByDefault.Size = new Size(160, 19);
      ShowBordersByDefault.TabIndex = 13;
      ShowBordersByDefault.Text = "Show Borders By Default";
      ShowBordersByDefault.UseVisualStyleBackColor = true;
      ShowBordersByDefault.Visible = false;
      tabPage1.Controls.Add(label16);
      tabPage1.Controls.Add(PreviewLauncherScrollSensitivity);
      tabPage1.Controls.Add(ZoomMouseScroll);
      tabPage1.Controls.Add(label5);
      tabPage1.Controls.Add(label4);
      tabPage1.Controls.Add(label3);
      tabPage1.Controls.Add(label2);
      tabPage1.Controls.Add(label1);
      tabPage1.Controls.Add(trackBar5);
      tabPage1.Controls.Add(trackBar4);
      tabPage1.Controls.Add(trackBar3);
      tabPage1.Controls.Add(trackBar2);
      tabPage1.Controls.Add(trackBar1);
      tabPage1.Controls.Add(trackBar6);
      tabPage1.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
      tabPage1.Location = new Point(4, 22);
      tabPage1.Margin = new Padding(0);
      tabPage1.Name = "tabPage1";
      tabPage1.Size = new Size(301, 391);
      tabPage1.TabIndex = 0;
      tabPage1.Text = "Sensitivity";
      tabPage1.UseVisualStyleBackColor = true;
      label16.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      label16.AutoSize = true;
      label16.Location = new Point(65, 251);
      label16.Name = "label16";
      label16.Size = new Size(169, 13);
      label16.TabIndex = 13;
      label16.Text = "Preview Launcher Scroll Sensitivity";
      PreviewLauncherScrollSensitivity.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      PreviewLauncherScrollSensitivity.BackColor = SystemColors.Window;
      PreviewLauncherScrollSensitivity.Location = new Point(23, 260);
      PreviewLauncherScrollSensitivity.Maximum = 200;
      PreviewLauncherScrollSensitivity.Minimum = 1;
      PreviewLauncherScrollSensitivity.Name = "PreviewLauncherScrollSensitivity";
      PreviewLauncherScrollSensitivity.Size = new Size(271, 45);
      PreviewLauncherScrollSensitivity.TabIndex = 12;
      PreviewLauncherScrollSensitivity.TickStyle = TickStyle.None;
      PreviewLauncherScrollSensitivity.Value = 50;
      ZoomMouseScroll.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      ZoomMouseScroll.AutoSize = true;
      ZoomMouseScroll.Location = new Point(65, 174);
      ZoomMouseScroll.Name = "ZoomMouseScroll";
      ZoomMouseScroll.Size = new Size(175, 13);
      ZoomMouseScroll.TabIndex = 11;
      ZoomMouseScroll.Text = "Zoom Mouse Scroll Wheel Senstivity";
      label5.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      label5.AutoSize = true;
      label5.Location = new Point(81, 136);
      label5.Name = "label5";
      label5.Size = new Size(134, 13);
      label5.TabIndex = 9;
      label5.Text = "Zoom Arrow Keys Senstivity";
      label4.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      label4.AutoSize = true;
      label4.Location = new Point(87, 210);
      label4.Name = "label4";
      label4.Size = new Size(125, 13);
      label4.TabIndex = 8;
      label4.Text = "Window Resize Senstivity";
      label3.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      label3.AutoSize = true;
      label3.Location = new Point(93, 94);
      label3.Name = "label3";
      label3.Size = new Size(104, 13);
      label3.TabIndex = 7;
      label3.Text = "Zoom Click Senstivity";
      label2.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      label2.AutoSize = true;
      label2.Location = new Point(70, 52);
      label2.Name = "label2";
      label2.Size = new Size(165, 13);
      label2.TabIndex = 6;
      label2.Text = "Zoom Divisions (Affects All Zooms)";
      label1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      label1.AutoSize = true;
      label1.Location = new Point(87, 13);
      label1.Name = "label1";
      label1.Size = new Size(109, 13);
      label1.TabIndex = 5;
      label1.Text = "Default Scaling Factor";
      trackBar5.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      trackBar5.BackColor = SystemColors.Window;
      trackBar5.Location = new Point(23, 219);
      trackBar5.Maximum = 150;
      trackBar5.Minimum = 50;
      trackBar5.Name = "trackBar5";
      trackBar5.Size = new Size(271, 45);
      trackBar5.TabIndex = 4;
      trackBar5.TickStyle = TickStyle.None;
      trackBar5.Value = 50;
      trackBar5.Scroll += trackBar5_Scroll;
      trackBar4.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      trackBar4.BackColor = SystemColors.Window;
      trackBar4.Location = new Point(23, 143);
      trackBar4.Margin = new Padding(0);
      trackBar4.Maximum = 25;
      trackBar4.Minimum = 1;
      trackBar4.Name = "trackBar4";
      trackBar4.Size = new Size(269, 45);
      trackBar4.TabIndex = 3;
      trackBar4.TickStyle = TickStyle.None;
      trackBar4.Value = 1;
      trackBar4.Scroll += trackBar4_Scroll;
      trackBar3.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      trackBar3.BackColor = SystemColors.Window;
      trackBar3.Location = new Point(23, 102);
      trackBar3.Margin = new Padding(0);
      trackBar3.Maximum = 40;
      trackBar3.Minimum = 1;
      trackBar3.Name = "trackBar3";
      trackBar3.Size = new Size(269, 45);
      trackBar3.TabIndex = 2;
      trackBar3.TickStyle = TickStyle.None;
      trackBar3.Value = 1;
      trackBar3.Scroll += trackBar3_Scroll;
      trackBar2.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      trackBar2.BackColor = SystemColors.Window;
      trackBar2.Location = new Point(25, 59);
      trackBar2.Maximum = 40;
      trackBar2.Minimum = 1;
      trackBar2.Name = "trackBar2";
      trackBar2.Size = new Size(269, 45);
      trackBar2.TabIndex = 1;
      trackBar2.TickStyle = TickStyle.None;
      trackBar2.Value = 1;
      trackBar2.Scroll += trackBar2_Scroll;
      trackBar1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      trackBar1.BackColor = SystemColors.Window;
      trackBar1.Location = new Point(23, 20);
      trackBar1.Margin = new Padding(0);
      trackBar1.Maximum = 80;
      trackBar1.Minimum = 1;
      trackBar1.Name = "trackBar1";
      trackBar1.Size = new Size(269, 45);
      trackBar1.TabIndex = 0;
      trackBar1.TickStyle = TickStyle.None;
      trackBar1.Value = 1;
      trackBar1.Scroll += trackBar1_Scroll;
      trackBar6.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      trackBar6.BackColor = SystemColors.Window;
      trackBar6.Location = new Point(28, 185);
      trackBar6.Margin = new Padding(0);
      trackBar6.Maximum = 140;
      trackBar6.Minimum = 100;
      trackBar6.Name = "trackBar6";
      trackBar6.Size = new Size(269, 45);
      trackBar6.TabIndex = 10;
      trackBar6.TickStyle = TickStyle.None;
      trackBar6.Value = 130;
      trackBar6.Scroll += trackBar6_Scroll;
      tabPage4.Controls.Add(groupBox3);
      tabPage4.Controls.Add(groupBox2);
      tabPage4.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      tabPage4.Location = new Point(4, 22);
      tabPage4.Margin = new Padding(0);
      tabPage4.Name = "tabPage4";
      tabPage4.Size = new Size(301, 391);
      tabPage4.TabIndex = 3;
      tabPage4.Text = "Size";
      tabPage4.UseVisualStyleBackColor = true;
      groupBox3.Controls.Add(label15);
      groupBox3.Controls.Add(PreviewLaunchVerticalSpace);
      groupBox3.Controls.Add(label14);
      groupBox3.Controls.Add(PreviewLaunchWidth);
      groupBox3.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox3.Location = new Point(17, 141);
      groupBox3.Name = "groupBox3";
      groupBox3.Size = new Size(271, 93);
      groupBox3.TabIndex = 23;
      groupBox3.TabStop = false;
      groupBox3.Text = "Preview Launch Menu";
      label15.AutoSize = true;
      label15.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label15.Location = new Point(8, 55);
      label15.Name = "label15";
      label15.Size = new Size(94, 15);
      label15.TabIndex = 18;
      label15.Text = "Vertical Spacing";
      PreviewLaunchVerticalSpace.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      PreviewLaunchVerticalSpace.Location = new Point(108, 53);
      NumericUpDown numericUpDown5 = PreviewLaunchVerticalSpace;
      int[] bits5 = new int[4];
      bits5[0] = 30000;
      Decimal num5 = new Decimal(bits5);
      numericUpDown5.Maximum = num5;
      NumericUpDown numericUpDown6 = PreviewLaunchVerticalSpace;
      int[] bits6 = new int[4];
      bits6[0] = 1;
      Decimal num6 = new Decimal(bits6);
      numericUpDown6.Minimum = num6;
      PreviewLaunchVerticalSpace.Name = "PreviewLaunchVerticalSpace";
      PreviewLaunchVerticalSpace.Size = new Size(84, 23);
      PreviewLaunchVerticalSpace.TabIndex = 17;
      NumericUpDown numericUpDown7 = PreviewLaunchVerticalSpace;
      int[] bits7 = new int[4];
      bits7[0] = 1;
      Decimal num7 = new Decimal(bits7);
      numericUpDown7.Value = num7;
      label14.AutoSize = true;
      label14.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label14.Location = new Point(8, 26);
      label14.Name = "label14";
      label14.Size = new Size(41, 15);
      label14.TabIndex = 16;
      label14.Text = "Width";
      PreviewLaunchWidth.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      PreviewLaunchWidth.Location = new Point(55, 24);
      NumericUpDown numericUpDown8 = PreviewLaunchWidth;
      int[] bits8 = new int[4];
      bits8[0] = 30000;
      Decimal num8 = new Decimal(bits8);
      numericUpDown8.Maximum = num8;
      NumericUpDown numericUpDown9 = PreviewLaunchWidth;
      int[] bits9 = new int[4];
      bits9[0] = 1;
      Decimal num9 = new Decimal(bits9);
      numericUpDown9.Minimum = num9;
      PreviewLaunchWidth.Name = "PreviewLaunchWidth";
      PreviewLaunchWidth.Size = new Size(84, 23);
      PreviewLaunchWidth.TabIndex = 15;
      NumericUpDown numericUpDown10 = PreviewLaunchWidth;
      int[] bits10 = new int[4];
      bits10[0] = 1;
      Decimal num10 = new Decimal(bits10);
      numericUpDown10.Value = num10;
      groupBox2.Controls.Add(label10);
      groupBox2.Controls.Add(MaxPreviewSize);
      groupBox2.Controls.Add(label11);
      groupBox2.Controls.Add(label12);
      groupBox2.Controls.Add(MaxPreviewSizeY);
      groupBox2.Controls.Add(MaxPreviewSizeX);
      groupBox2.Controls.Add(MinPreviewSize);
      groupBox2.Controls.Add(label8);
      groupBox2.Controls.Add(label9);
      groupBox2.Controls.Add(MinPreviewSizeY);
      groupBox2.Controls.Add(MinPreviewSizeX);
      groupBox2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, 0);
      groupBox2.Location = new Point(14, 5);
      groupBox2.Name = "groupBox2";
      groupBox2.Size = new Size(274, 120);
      groupBox2.TabIndex = 22;
      groupBox2.TabStop = false;
      groupBox2.Text = "Default Initial Preview Size Limits";
      label10.AutoSize = true;
      label10.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label10.Location = new Point(40, 98);
      label10.Name = "label10";
      label10.Size = new Size(203, 15);
      label10.TabIndex = 22;
      label10.Text = "Limits used if none others specified.";
      MaxPreviewSize.AutoSize = true;
      MaxPreviewSize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MaxPreviewSize.Location = new Point(141, 21);
      MaxPreviewSize.Name = "MaxPreviewSize";
      MaxPreviewSize.Size = new Size(132, 15);
      MaxPreviewSize.TabIndex = 21;
      MaxPreviewSize.Text = "Maximum Preview Size";
      label11.AutoSize = true;
      label11.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label11.Location = new Point(148, 68);
      label11.Name = "label11";
      label11.Size = new Size(13, 15);
      label11.TabIndex = 20;
      label11.Text = "Y";
      label12.AutoSize = true;
      label12.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label12.Location = new Point(147, 41);
      label12.Name = "label12";
      label12.Size = new Size(14, 15);
      label12.TabIndex = 19;
      label12.Text = "X";
      MaxPreviewSizeY.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MaxPreviewSizeY.Location = new Point(171, 66);
      NumericUpDown numericUpDown11 = MaxPreviewSizeY;
      int[] bits11 = new int[4];
      bits11[0] = 30000;
      Decimal num11 = new Decimal(bits11);
      numericUpDown11.Maximum = num11;
      MaxPreviewSizeY.Name = "MaxPreviewSizeY";
      MaxPreviewSizeY.Size = new Size(84, 23);
      MaxPreviewSizeY.TabIndex = 18;
      MaxPreviewSizeY.ValueChanged += MaxPreviewSizeY_ValueChanged;
      MaxPreviewSizeX.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MaxPreviewSizeX.Location = new Point(171, 39);
      NumericUpDown numericUpDown12 = MaxPreviewSizeX;
      int[] bits12 = new int[4];
      bits12[0] = 30000;
      Decimal num12 = new Decimal(bits12);
      numericUpDown12.Maximum = num12;
      MaxPreviewSizeX.Name = "MaxPreviewSizeX";
      MaxPreviewSizeX.Size = new Size(84, 23);
      MaxPreviewSizeX.TabIndex = 17;
      MaxPreviewSizeX.ValueChanged += MaxPreviewSizeX_ValueChanged;
      MinPreviewSize.AutoSize = true;
      MinPreviewSize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MinPreviewSize.Location = new Point(4, 21);
      MinPreviewSize.Name = "MinPreviewSize";
      MinPreviewSize.Size = new Size(130, 15);
      MinPreviewSize.TabIndex = 16;
      MinPreviewSize.Text = "Minimum Preview Size";
      label8.AutoSize = true;
      label8.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label8.Location = new Point(11, 68);
      label8.Name = "label8";
      label8.Size = new Size(13, 15);
      label8.TabIndex = 15;
      label8.Text = "Y";
      label9.AutoSize = true;
      label9.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label9.Location = new Point(10, 41);
      label9.Name = "label9";
      label9.Size = new Size(14, 15);
      label9.TabIndex = 14;
      label9.Text = "X";
      MinPreviewSizeY.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MinPreviewSizeY.Location = new Point(34, 66);
      NumericUpDown numericUpDown13 = MinPreviewSizeY;
      int[] bits13 = new int[4];
      bits13[0] = 30000;
      Decimal num13 = new Decimal(bits13);
      numericUpDown13.Maximum = num13;
      MinPreviewSizeY.Name = "MinPreviewSizeY";
      MinPreviewSizeY.Size = new Size(84, 23);
      MinPreviewSizeY.TabIndex = 13;
      MinPreviewSizeY.ValueChanged += MinPreviewSizeY_ValueChanged;
      MinPreviewSizeX.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MinPreviewSizeX.Location = new Point(34, 39);
      NumericUpDown numericUpDown14 = MinPreviewSizeX;
      int[] bits14 = new int[4];
      bits14[0] = 30000;
      Decimal num14 = new Decimal(bits14);
      numericUpDown14.Maximum = num14;
      MinPreviewSizeX.Name = "MinPreviewSizeX";
      MinPreviewSizeX.Size = new Size(84, 23);
      MinPreviewSizeX.TabIndex = 12;
      MinPreviewSizeX.ValueChanged += MinPreviewSizeX_ValueChanged;
      tabPage5.Controls.Add(label19);
      tabPage5.Controls.Add(RequireHotkeyOnMinimize);
      tabPage5.Controls.Add(label18);
      tabPage5.Controls.Add(HideTaskbarButtons);
      tabPage5.Controls.Add(CreatePreviewsOnMinimize);
      tabPage5.Controls.Add(label17);
      tabPage5.Controls.Add(KeepTargetFromMinimize);
      tabPage5.Controls.Add(HideTargetByDefault);
      tabPage5.Controls.Add(ShowListofWindows);
      tabPage5.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
      tabPage5.Location = new Point(4, 22);
      tabPage5.Margin = new Padding(0);
      tabPage5.Name = "tabPage5";
      tabPage5.Size = new Size(301, 391);
      tabPage5.TabIndex = 4;
      tabPage5.Text = "Behavior";
      tabPage5.UseVisualStyleBackColor = true;
      label19.AutoSize = true;
      label19.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      label19.Location = new Point(34, 104);
      label19.Name = "label19";
      label19.Size = new Size(sbyte.MaxValue, 15);
      label19.TabIndex = 31;
      label19.Text = "Previews on Minimize";
      RequireHotkeyOnMinimize.AutoSize = true;
      RequireHotkeyOnMinimize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      RequireHotkeyOnMinimize.Location = new Point(18, 85);
      RequireHotkeyOnMinimize.Name = "RequireHotkeyOnMinimize";
      RequireHotkeyOnMinimize.Size = new Size(260, 19);
      RequireHotkeyOnMinimize.TabIndex = 28;
      RequireHotkeyOnMinimize.Text = "Require SHIFT to be Pressed When Creating";
      RequireHotkeyOnMinimize.UseVisualStyleBackColor = true;
      label18.AutoSize = true;
      label18.Location = new Point(6, 178);
      label18.Name = "label18";
      label18.Size = new Size(278, 13);
      label18.TabIndex = 25;
      label18.Text = "Note: Some options have been disabled for further coding.";
      HideTaskbarButtons.AutoSize = true;
      HideTaskbarButtons.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      HideTaskbarButtons.Location = new Point(18, sbyte.MaxValue);
      HideTaskbarButtons.Name = "HideTaskbarButtons";
      HideTaskbarButtons.Size = new Size(241, 19);
      HideTaskbarButtons.TabIndex = 24;
      HideTaskbarButtons.Text = "Hide Taskbar Button of Target Windows";
      HideTaskbarButtons.UseVisualStyleBackColor = true;
      HideTaskbarButtons.CheckedChanged += HideTaskbarButtons_CheckedChanged;
      CreatePreviewsOnMinimize.AutoSize = true;
      CreatePreviewsOnMinimize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      CreatePreviewsOnMinimize.Location = new Point(18, 62);
      CreatePreviewsOnMinimize.Name = "CreatePreviewsOnMinimize";
      CreatePreviewsOnMinimize.Size = new Size(184, 19);
      CreatePreviewsOnMinimize.TabIndex = 23;
      CreatePreviewsOnMinimize.Text = "Create Previews on Minimize";
      CreatePreviewsOnMinimize.UseVisualStyleBackColor = true;
      label17.AutoSize = true;
      label17.Location = new Point(6, 160);
      label17.Name = "label17";
      label17.Size = new Size(286, 13);
      label17.TabIndex = 22;
      label17.Text = "Note: Live previews require non-minimized target windows.";
      KeepTargetFromMinimize.AutoSize = true;
      KeepTargetFromMinimize.Enabled = false;
      KeepTargetFromMinimize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      KeepTargetFromMinimize.Location = new Point(18, 37);
      KeepTargetFromMinimize.Name = "KeepTargetFromMinimize";
      KeepTargetFromMinimize.Size = new Size(237, 19);
      KeepTargetFromMinimize.TabIndex = 21;
      KeepTargetFromMinimize.Text = "Keep Target Windows from Minimizing";
      KeepTargetFromMinimize.UseVisualStyleBackColor = true;
      HideTargetByDefault.AutoSize = true;
      HideTargetByDefault.Enabled = false;
      HideTargetByDefault.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      HideTargetByDefault.Location = new Point(18, 12);
      HideTargetByDefault.Name = "HideTargetByDefault";
      HideTargetByDefault.Size = new Size(201, 19);
      HideTargetByDefault.TabIndex = 20;
      HideTargetByDefault.Text = "Hide Target Windows by Default";
      HideTargetByDefault.UseVisualStyleBackColor = true;
      ShowListofWindows.AutoSize = true;
      ShowListofWindows.Enabled = false;
      ShowListofWindows.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      ShowListofWindows.Location = new Point(18, 363);
      ShowListofWindows.Name = "ShowListofWindows";
      ShowListofWindows.Size = new Size(256, 19);
      ShowListofWindows.TabIndex = 19;
      ShowListofWindows.Text = "Show List of Windows in Right-Click Menu";
      ShowListofWindows.UseVisualStyleBackColor = true;
      ShowListofWindows.Visible = false;
      MainOK.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      MainOK.Font = new Font("Calibri", 14.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
      MainOK.Location = new Point(108, 429);
      MainOK.Name = "MainOK";
      MainOK.Size = new Size(100, 34);
      MainOK.TabIndex = 1;
      MainOK.Text = "OK";
      MainOK.UseVisualStyleBackColor = true;
      MainOK.Click += MainOK_Click;
      MainCancel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      MainCancel.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      MainCancel.Location = new Point(214, 433);
      MainCancel.Name = "MainCancel";
      MainCancel.Size = new Size(89, 25);
      MainCancel.TabIndex = 2;
      MainCancel.Text = "Cancel";
      MainCancel.UseVisualStyleBackColor = true;
      MainCancel.Click += MainCancel_Click;
      ApplyButton.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      ApplyButton.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
      ApplyButton.Location = new Point(13, 433);
      ApplyButton.Margin = new Padding(0);
      ApplyButton.Name = "ApplyButton";
      ApplyButton.Size = new Size(89, 25);
      ApplyButton.TabIndex = 3;
      ApplyButton.Text = "Apply";
      ApplyButton.UseVisualStyleBackColor = true;
      ApplyButton.MouseClick += ApplyButton_MouseClick;
      AutoScaleDimensions = new SizeF(6f, 13f);
      AutoScaleMode = AutoScaleMode.Font;
      ClientSize = new Size(316, 471);
      Controls.Add(ApplyButton);
      Controls.Add(MainCancel);
      Controls.Add(MainOK);
      Controls.Add(tabControl1);
      FormBorderStyle = FormBorderStyle.Fixed3D;
      Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      Name = "SettingsPanel";
      Text = "ViP 0.2 (Alpha) - Settings Panel";
      TopMost = true;
      Load += SettingsPanel_Load;
      FormClosing += SettingsPanel_FormClosing;
      tabControl1.ResumeLayout(false);
      tabPage2.ResumeLayout(false);
      groupBox4.ResumeLayout(false);
      groupBox4.PerformLayout();
      CentreDockingThickness.EndInit();
      EdgeDockingThickness.EndInit();
      groupBox1.ResumeLayout(false);
      groupBox1.PerformLayout();
      CustomLocationY.EndInit();
      CustomLocationX.EndInit();
      Layout.ResumeLayout(false);
      groupBox11.ResumeLayout(false);
      groupBox11.PerformLayout();
      MarginLeft.EndInit();
      MarginRight.EndInit();
      ((ISupportInitialize) pictureBox1).EndInit();
      MarginBottom.EndInit();
      MarginTop.EndInit();
      groupBox10.ResumeLayout(false);
      groupBox10.PerformLayout();
      PreviewHorizontalSpacing.EndInit();
      PreviewVerticalSpacing.EndInit();
      groupBox8.ResumeLayout(false);
      groupBox8.PerformLayout();
      groupBox5.ResumeLayout(false);
      groupBox9.ResumeLayout(false);
      groupBox9.PerformLayout();
      groupBox7.ResumeLayout(false);
      groupBox7.PerformLayout();
      groupBox6.ResumeLayout(false);
      groupBox6.PerformLayout();
      tabPage3.ResumeLayout(false);
      tabPage3.PerformLayout();
      BackTransparency.EndInit();
      ThumbTransparencyTB.EndInit();
      tabPage1.ResumeLayout(false);
      tabPage1.PerformLayout();
      PreviewLauncherScrollSensitivity.EndInit();
      trackBar5.EndInit();
      trackBar4.EndInit();
      trackBar3.EndInit();
      trackBar2.EndInit();
      trackBar1.EndInit();
      trackBar6.EndInit();
      tabPage4.ResumeLayout(false);
      groupBox3.ResumeLayout(false);
      groupBox3.PerformLayout();
      PreviewLaunchVerticalSpace.EndInit();
      PreviewLaunchWidth.EndInit();
      groupBox2.ResumeLayout(false);
      groupBox2.PerformLayout();
      MaxPreviewSizeY.EndInit();
      MaxPreviewSizeX.EndInit();
      MinPreviewSizeY.EndInit();
      MinPreviewSizeX.EndInit();
      tabPage5.ResumeLayout(false);
      tabPage5.PerformLayout();
      ResumeLayout(false);
    }

    private void SettingsPanel_Load(object sender, EventArgs e)
    {
      trackBar1.Value = (int) (UserSettings.Default.scalingfactor * 100.0);
      trackBar2.Value = (int) UserSettings.Default.ZoomDivisions;
      trackBar3.Value = (int) (UserSettings.Default.ZoomClickSensitivity * 10.0);
      trackBar4.Value = (int) (UserSettings.Default.ZoomArrowSensitivity * 10.0);
      trackBar5.Value = (int) (UserSettings.Default.ZoomScalingFactor * 100.0);
      trackBar6.Value = (int) (UserSettings.Default.MouseScrollZoomSensitivity * 100.0);
      PreviewLauncherScrollSensitivity.Value = UserSettings.Default.PreviewMenuScrollSensitivity;
      PreviewVerticalSpacing.Value = UserSettings.Default.PreviewLayoutVerticalSpacing;
      PreviewHorizontalSpacing.Value = UserSettings.Default.PreviewLayoutHorizontalSpacing;
      RowsFirst.Checked = UserSettings.Default.RowsFirst;
      ColumnsFirst.Checked = UserSettings.Default.ColumnsFirst;
      MarginTop.Maximum = SystemInformation.PrimaryMonitorSize.Height;
      MarginBottom.Maximum = SystemInformation.PrimaryMonitorSize.Height;
      MarginLeft.Maximum = SystemInformation.PrimaryMonitorSize.Width;
      MarginRight.Maximum = SystemInformation.PrimaryMonitorSize.Width;
      MarginTop.Value = UserSettings.Default.CustomTaskbarMarginsTop <= SystemInformation.PrimaryMonitorSize.Height ? UserSettings.Default.CustomTaskbarMarginsTop : SystemInformation.PrimaryMonitorSize.Height;
      MarginBottom.Value = UserSettings.Default.CustomTaskbarMarginsBottom <= SystemInformation.PrimaryMonitorSize.Height ? UserSettings.Default.CustomTaskbarMarginsBottom : SystemInformation.PrimaryMonitorSize.Height;
      MarginLeft.Value = UserSettings.Default.CustomTaskbarMarginsLeft <= SystemInformation.PrimaryMonitorSize.Width ? UserSettings.Default.CustomTaskbarMarginsLeft : SystemInformation.PrimaryMonitorSize.Width;
      MarginRight.Value = UserSettings.Default.CustomTaskbarMarginsRight <= SystemInformation.PrimaryMonitorSize.Width ? UserSettings.Default.CustomTaskbarMarginsRight : SystemInformation.PrimaryMonitorSize.Width;
      if (UserSettings.Default.AutomaticTaskbarMargin)
      {
        AutomaticTaskbarMargins.Checked = true;
        CustomTaskbarMargins.Checked = false;
        MarginTop.Enabled = false;
        MarginBottom.Enabled = false;
        MarginLeft.Enabled = false;
        MarginRight.Enabled = false;
      }
      else
      {
        AutomaticTaskbarMargins.Checked = false;
        CustomTaskbarMargins.Checked = true;
        MarginTop.Enabled = true;
        MarginBottom.Enabled = true;
        MarginLeft.Enabled = true;
        MarginRight.Enabled = true;
      }
      DockPreviewCB.Checked = UserSettings.Default.DockPreview;
      FreeFormEdgeDocking.Checked = UserSettings.Default.FreeFormEdgeDocking;
      FreeFormCentreDocking.Checked = UserSettings.Default.FreeFormCentreDocking;
      CentreDockingThickness.Value = UserSettings.Default.FreeFormCentreDockThickness;
      EdgeDockingThickness.Value = UserSettings.Default.FreeFormEdgeDockThickness;
      CentreDockingThicknessToolTip.SetToolTip(CentreDockingThickness, "Thickness of Border Around Centre");
      EdgeDockingThicknessToolTip.SetToolTip(EdgeDockingThickness, "Thickness of Edge Border");
      BottomLeftCB.Checked = UserSettings.Default.startbottomleft;
      BottomRightCB.Checked = UserSettings.Default.startbottomright;
      BottomMiddleCB.Checked = UserSettings.Default.startbottommiddle;
      CentreCB.Checked = UserSettings.Default.startincenter;
      TopLeftCB.Checked = UserSettings.Default.starttopleft;
      TopRightCB.Checked = UserSettings.Default.starttopright;
      TopMiddleCB.Checked = UserSettings.Default.starttopmiddle;
      BottomCB.Checked = UserSettings.Default.startbottom;
      TopCB.Checked = UserSettings.Default.starttop;
      LeftCB.Checked = UserSettings.Default.startleft;
      RightCB.Checked = UserSettings.Default.startright;
      WindowsDefaultCB.Checked = UserSettings.Default.StartWindowsDefault;
      CustomCB.Checked = UserSettings.Default.StartCustomLocation;
      CustomLocationX.Value = UserSettings.Default.startpositionx;
      CustomLocationY.Value = UserSettings.Default.startpositiony;
      if (!CustomCB.Checked)
      {
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        CustomLocationX.ReadOnly = false;
        CustomLocationY.ReadOnly = false;
        CustomLocationX.Increment = new Decimal(1);
        CustomLocationY.Increment = new Decimal(1);
      }
      ShowBordersByDefault.Checked = UserSettings.Default.ShowBordersByDefault;
      ThumbTransparencyTB.Value = UserSettings.Default.ThumbTransparency;
      BackTransparency.Value = UserSettings.Default.BackTransparency;
      ShowBackText.Checked = UserSettings.Default.ShowBackText;
      EnableAeroBack.Checked = UserSettings.Default.EnableAeroBackground;
      OriginalEnableAeroBack = UserSettings.Default.EnableAeroBackground;
      MinPreviewSizeX.Value = UserSettings.Default.minpixelsx;
      MinPreviewSizeY.Value = UserSettings.Default.minpixelsy;
      MaxPreviewSizeX.Value = UserSettings.Default.maxpixelsx;
      MaxPreviewSizeY.Value = UserSettings.Default.maxpixelsy;
      PreviewLaunchWidth.Value = UserSettings.Default.PreviewMenuSizeX;
      PreviewLaunchVerticalSpace.Value = UserSettings.Default.VerticalSpaceBetweenPreviews;
      ShowListofWindows.Checked = UserSettings.Default.ShowWindowListInContextMenu;
      HideTargetByDefault.Checked = UserSettings.Default.MoveTargetOffscreen;
      KeepTargetFromMinimize.Checked = UserSettings.Default.KeepTargettedFromMinimizing;
      CreatePreviewsOnMinimize.Checked = UserSettings.Default.CreatePreviewsOnMinimize;
      HideTaskbarButtons.Checked = UserSettings.Default.HideTaskBarButtons;
      if (UserSettings.Default.ArrangePreviewXDirection == "Right")
      {
        RightDirectionCB.Checked = true;
        LeftDirectionCB.Checked = false;
      }
      else
      {
        RightDirectionCB.Checked = false;
        LeftDirectionCB.Checked = true;
      }
      if (UserSettings.Default.ArrangePreviewYDirection == "Up")
      {
        UpDirectionCB.Checked = true;
        DownDirectionCB.Checked = false;
      }
      else
      {
        UpDirectionCB.Checked = false;
        DownDirectionCB.Checked = true;
      }
      RequireHotkeyOnMinimize.Checked = UserSettings.Default.CreatePreviewOnlyOnHotkey;
      PinTopMost.Checked = UserSettings.Default.PinToTop;
      PinNormal.Checked = UserSettings.Default.PinToNormal;
      PinDesktop.Checked = UserSettings.Default.PinToBottom;
    }

    private void MainOK_Click(object sender, EventArgs e)
    {
      ApplyButton_MouseClick(this, null);
      try
      {
        Dispose();
        Close();
      }
      catch (Exception ex)
      {
      }
    }

    private void MainCancel_Click(object sender, EventArgs e)
    {
      qparent.ChangeTransparency(UserSettings.Default.ThumbTransparency, UserSettings.Default.BackTransparency);
      if (HideTaskbarButtons.Checked != UserSettings.Default.HideTaskBarButtons)
      {
        if (UserSettings.Default.HideTaskBarButtons)
          qparent.HideTaskbarTab();
        else
          qparent.ShowTaskbarTab();
      }
      if (EnableAeroBack.Checked != OriginalEnableAeroBack)
      {
        UserSettings.Default.EnableAeroBackground = OriginalEnableAeroBack;
        qparent.AeroBackGroundRefresh();
      }
      try
      {
        Dispose();
        Close();
      }
      catch (Exception ex)
      {
      }
    }

    private void trackBar1_Scroll(object sender, EventArgs e)
    {
      TB1changed = true;
    }

    private void trackBar2_Scroll(object sender, EventArgs e)
    {
      TB2changed = true;
    }

    private void trackBar3_Scroll(object sender, EventArgs e)
    {
      TB3changed = true;
    }

    private void trackBar4_Scroll(object sender, EventArgs e)
    {
      TB4changed = true;
    }

    private void trackBar5_Scroll(object sender, EventArgs e)
    {
      TB5changed = true;
    }

    private void trackBar6_Scroll(object sender, EventArgs e)
    {
      TB6changed = true;
    }

    private void BottomLeftCB_CheckedChanged(object sender, EventArgs e)
    {
      if (BottomLeftCB.Checked)
      {
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomMiddleCB.Checked || BottomRightCB.Checked || (TopLeftCB.Checked || TopMiddleCB.Checked) || (TopRightCB.Checked || BottomCB.Checked || (TopCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void TopLeftCB_CheckedChanged(object sender, EventArgs e)
    {
      if (TopLeftCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopMiddleCB.Checked) || (TopRightCB.Checked || BottomCB.Checked || (TopCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void BottomMiddleCB_CheckedChanged(object sender, EventArgs e)
    {
      if (BottomMiddleCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomRightCB.Checked || (TopLeftCB.Checked || TopMiddleCB.Checked) || (TopRightCB.Checked || BottomCB.Checked || (TopCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void TopMiddleCB_CheckedChanged(object sender, EventArgs e)
    {
      if (TopMiddleCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopRightCB.Checked || BottomCB.Checked || (TopCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void BottomRightCB_CheckedChanged(object sender, EventArgs e)
    {
      if (BottomRightCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (TopLeftCB.Checked || TopMiddleCB.Checked) || (TopRightCB.Checked || BottomCB.Checked || (TopCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void TopRightCB_CheckedChanged(object sender, EventArgs e)
    {
      if (TopRightCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || BottomCB.Checked || (TopCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void WindowsDefaultCB_CheckedChanged(object sender, EventArgs e)
    {
      if (WindowsDefaultCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || TopRightCB.Checked || (BottomCB.Checked || TopCB.Checked)) || (LeftCB.Checked || RightCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void CentreCB_CheckedChanged(object sender, EventArgs e)
    {
      if (CentreCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || TopRightCB.Checked || (BottomCB.Checked || TopCB.Checked)) || (LeftCB.Checked || RightCB.Checked || WindowsDefaultCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void CustomCB_CheckedChanged(object sender, EventArgs e)
    {
      if (CustomCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomRightCB.Checked = false;
        BottomMiddleCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomLocationX.ReadOnly = false;
        CustomLocationY.ReadOnly = false;
        CustomLocationX.Increment = new Decimal(1);
        CustomLocationY.Increment = new Decimal(1);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || TopRightCB.Checked || WindowsDefaultCB.Checked) || CentreCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void BottomCB_CheckedChanged(object sender, EventArgs e)
    {
      if (BottomCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || TopRightCB.Checked || (TopCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void TopCB_CheckedChanged(object sender, EventArgs e)
    {
      if (TopCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        LeftCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || TopRightCB.Checked || (BottomCB.Checked || LeftCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void LeftCB_CheckedChanged(object sender, EventArgs e)
    {
      if (LeftCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        RightCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || TopRightCB.Checked || (BottomCB.Checked || TopCB.Checked)) || (RightCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void RightCB_CheckedChanged(object sender, EventArgs e)
    {
      if (RightCB.Checked)
      {
        BottomLeftCB.Checked = false;
        BottomMiddleCB.Checked = false;
        BottomRightCB.Checked = false;
        TopLeftCB.Checked = false;
        TopMiddleCB.Checked = false;
        TopRightCB.Checked = false;
        WindowsDefaultCB.Checked = false;
        CentreCB.Checked = false;
        CustomCB.Checked = false;
        BottomCB.Checked = false;
        TopCB.Checked = false;
        LeftCB.Checked = false;
        CustomLocationX.ReadOnly = true;
        CustomLocationY.ReadOnly = true;
        CustomLocationX.Increment = new Decimal(0);
        CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (BottomLeftCB.Checked || BottomMiddleCB.Checked || (BottomRightCB.Checked || TopLeftCB.Checked) || (TopMiddleCB.Checked || TopRightCB.Checked || (BottomCB.Checked || TopCB.Checked)) || (LeftCB.Checked || WindowsDefaultCB.Checked || CentreCB.Checked) || CustomCB.Checked)
          return;
        WindowsDefaultCB.Checked = true;
      }
    }

    private void MinPreviewSizeX_ValueChanged(object sender, EventArgs e)
    {
      if (!(MinPreviewSizeX.Value > MaxPreviewSizeX.Value))
        return;
      MaxPreviewSizeX.Value = MinPreviewSizeX.Value;
    }

    private void MaxPreviewSizeX_ValueChanged(object sender, EventArgs e)
    {
      if (!(MinPreviewSizeX.Value > MaxPreviewSizeX.Value))
        return;
      MinPreviewSizeX.Value = MaxPreviewSizeX.Value;
    }

    private void MinPreviewSizeY_ValueChanged(object sender, EventArgs e)
    {
      if (!(MinPreviewSizeY.Value > MaxPreviewSizeY.Value))
        return;
      MaxPreviewSizeY.Value = MinPreviewSizeY.Value;
    }

    private void MaxPreviewSizeY_ValueChanged(object sender, EventArgs e)
    {
      if (!(MinPreviewSizeY.Value > MaxPreviewSizeY.Value))
        return;
      MinPreviewSizeY.Value = MaxPreviewSizeY.Value;
    }

    private void TransparencyTB_Scroll(object sender, EventArgs e)
    {
      qparent.ChangeTransparency(ThumbTransparencyTB.Value, BackTransparency.Value);
    }

    private void SettingsPanel_FormClosing(object sender, FormClosingEventArgs e)
    {
      qparent.ChangeTransparency(UserSettings.Default.ThumbTransparency, UserSettings.Default.BackTransparency);
    }

    private void ShowBackText_CheckedChanged(object sender, EventArgs e)
    {
    }

    private void FreeFormEdgeDocking_CheckedChanged(object sender, EventArgs e)
    {
      if (!FreeFormEdgeDocking.Checked)
        return;
      DockPreviewCB.Checked = false;
    }

    private void DockPreviewCB_CheckedChanged(object sender, EventArgs e)
    {
      if (!DockPreviewCB.Checked)
        return;
      FreeFormEdgeDocking.Checked = false;
    }

    private void FreeFormCentreDocking_CheckedChanged(object sender, EventArgs e)
    {
      if (!FreeFormCentreDocking.Checked)
        return;
      DockPreviewCB.Checked = false;
    }

    private void LeftDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (LeftDirectionCB.Checked)
        RightDirectionCB.Checked = false;
      else
        RightDirectionCB.Checked = true;
    }

    private void RightDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (RightDirectionCB.Checked)
        LeftDirectionCB.Checked = false;
      else
        LeftDirectionCB.Checked = true;
    }

    private void UpDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (UpDirectionCB.Checked)
        DownDirectionCB.Checked = false;
      else
        DownDirectionCB.Checked = true;
    }

    private void DownDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (DownDirectionCB.Checked)
        UpDirectionCB.Checked = false;
      else
        UpDirectionCB.Checked = true;
    }

    private void PinTopMost_CheckedChanged(object sender, EventArgs e)
    {
      if (!PinTopMost.Checked)
        return;
      PinDesktop.Checked = false;
      PinNormal.Checked = false;
    }

    private void PinNormal_CheckedChanged(object sender, EventArgs e)
    {
      if (!PinNormal.Checked)
        return;
      PinTopMost.Checked = false;
      PinDesktop.Checked = false;
    }

    private void PinDesktop_CheckedChanged(object sender, EventArgs e)
    {
      if (!PinDesktop.Checked)
        return;
      PinTopMost.Checked = false;
      PinNormal.Checked = false;
    }

    private void ApplyButton_MouseClick(object sender, MouseEventArgs e)
    {
      if (TB1changed)
        UserSettings.Default.scalingfactor = trackBar1.Value / 100.0;
      if (TB2changed)
        UserSettings.Default.ZoomDivisions = trackBar2.Value;
      if (TB3changed)
        UserSettings.Default.ZoomClickSensitivity = trackBar3.Value / 10.0;
      if (TB4changed)
        UserSettings.Default.ZoomArrowSensitivity = trackBar4.Value / 10.0;
      if (TB5changed)
        UserSettings.Default.ZoomScalingFactor = trackBar5.Value / 100.0;
      if (TB6changed)
        UserSettings.Default.MouseScrollZoomSensitivity = trackBar6.Value / 100.0;
      UserSettings.Default.PreviewMenuScrollSensitivity = PreviewLauncherScrollSensitivity.Value;
      UserSettings.Default.PreviewLayoutVerticalSpacing = (int) PreviewVerticalSpacing.Value;
      UserSettings.Default.PreviewLayoutHorizontalSpacing = (int) PreviewHorizontalSpacing.Value;
      UserSettings.Default.RowsFirst = RowsFirst.Checked;
      UserSettings.Default.ColumnsFirst = ColumnsFirst.Checked;
      UserSettings.Default.CustomTaskbarMarginsTop = (int) MarginTop.Value;
      UserSettings.Default.CustomTaskbarMarginsBottom = (int) MarginBottom.Value;
      UserSettings.Default.CustomTaskbarMarginsLeft = (int) MarginLeft.Value;
      UserSettings.Default.CustomTaskbarMarginsRight = (int) MarginRight.Value;
      UserSettings.Default.AutomaticTaskbarMargin = AutomaticTaskbarMargins.Checked;
      UserSettings.Default.CustomTaskbarMargins = CustomTaskbarMargins.Checked;
      UserSettings.Default.DockPreview = DockPreviewCB.Checked;
      UserSettings.Default.FreeFormEdgeDocking = FreeFormEdgeDocking.Checked;
      UserSettings.Default.FreeFormCentreDocking = FreeFormCentreDocking.Checked;
      UserSettings.Default.FreeFormCentreDockThickness = (int) CentreDockingThickness.Value;
      UserSettings.Default.FreeFormEdgeDockThickness = (int) EdgeDockingThickness.Value;
      UserSettings.Default.startbottomleft = BottomLeftCB.Checked;
      UserSettings.Default.startbottomright = BottomRightCB.Checked;
      UserSettings.Default.startbottommiddle = BottomMiddleCB.Checked;
      UserSettings.Default.startincenter = CentreCB.Checked;
      UserSettings.Default.starttopleft = TopLeftCB.Checked;
      UserSettings.Default.starttopright = TopRightCB.Checked;
      UserSettings.Default.starttopmiddle = TopMiddleCB.Checked;
      UserSettings.Default.startbottom = BottomCB.Checked;
      UserSettings.Default.starttop = TopCB.Checked;
      UserSettings.Default.startleft = LeftCB.Checked;
      UserSettings.Default.startright = RightCB.Checked;
      UserSettings.Default.StartWindowsDefault = WindowsDefaultCB.Checked;
      UserSettings.Default.StartCustomLocation = CustomCB.Checked;
      UserSettings.Default.startpositionx = (int) CustomLocationX.Value;
      UserSettings.Default.startpositiony = (int) CustomLocationY.Value;
      UserSettings.Default.FirstPreview = true;
      UserSettings.Default.ShowBordersByDefault = ShowBordersByDefault.Checked;
      UserSettings.Default.ThumbTransparency = ThumbTransparencyTB.Value;
      UserSettings.Default.BackTransparency = BackTransparency.Value;
      UserSettings.Default.ShowBackText = ShowBackText.Checked;
      UserSettings.Default.EnableAeroBackground = EnableAeroBack.Checked;
      UserSettings.Default.minpixelsx = (int) MinPreviewSizeX.Value;
      UserSettings.Default.minpixelsy = (int) MinPreviewSizeY.Value;
      UserSettings.Default.maxpixelsx = (int) MaxPreviewSizeX.Value;
      UserSettings.Default.maxpixelsy = (int) MaxPreviewSizeY.Value;
      UserSettings.Default.PreviewMenuSizeX = (int) PreviewLaunchWidth.Value;
      UserSettings.Default.VerticalSpaceBetweenPreviews = (int) PreviewLaunchVerticalSpace.Value;
      UserSettings.Default.ShowWindowListInContextMenu = ShowListofWindows.Checked;
      UserSettings.Default.MoveTargetOffscreen = HideTargetByDefault.Checked;
      UserSettings.Default.KeepTargettedFromMinimizing = KeepTargetFromMinimize.Checked;
      UserSettings.Default.CreatePreviewsOnMinimize = CreatePreviewsOnMinimize.Checked;
      UserSettings.Default.HideTaskBarButtons = HideTaskbarButtons.Checked;
      UserSettings.Default.ArrangePreviewXDirection = !RightDirectionCB.Checked ? "Left" : "Right";
      UserSettings.Default.ArrangePreviewYDirection = !UpDirectionCB.Checked ? "Down" : "Up";
      UserSettings.Default.CreatePreviewOnlyOnHotkey = RequireHotkeyOnMinimize.Checked;
      UserSettings.Default.PinToTop = PinTopMost.Checked;
      UserSettings.Default.PinToNormal = PinNormal.Checked;
      UserSettings.Default.PinToBottom = PinDesktop.Checked;
      UserSettings.Default.Save();
    }

    private void AutomaticTaskbarMargins_CheckedChanged(object sender, EventArgs e)
    {
      if (AutomaticTaskbarMargins.Checked)
      {
        CustomTaskbarMargins.Checked = false;
        MarginTop.Enabled = false;
        MarginBottom.Enabled = false;
        MarginLeft.Enabled = false;
        MarginRight.Enabled = false;
      }
      else
      {
        CustomTaskbarMargins.Checked = true;
        MarginTop.Enabled = true;
        MarginBottom.Enabled = true;
        MarginLeft.Enabled = true;
        MarginRight.Enabled = true;
      }
    }

    private void CustomTaskbarMargins_CheckedChanged(object sender, EventArgs e)
    {
      if (CustomTaskbarMargins.Checked)
      {
        AutomaticTaskbarMargins.Checked = false;
        MarginTop.Enabled = true;
        MarginBottom.Enabled = true;
        MarginLeft.Enabled = true;
        MarginRight.Enabled = true;
      }
      else
      {
        AutomaticTaskbarMargins.Checked = true;
        MarginTop.Enabled = false;
        MarginBottom.Enabled = false;
        MarginLeft.Enabled = false;
        MarginRight.Enabled = false;
      }
    }

    private void RowsFirst_CheckedChanged(object sender, EventArgs e)
    {
      if (RowsFirst.Checked)
        ColumnsFirst.Checked = false;
      else
        ColumnsFirst.Checked = true;
    }

    private void ColumnsFirst_CheckedChanged(object sender, EventArgs e)
    {
      if (ColumnsFirst.Checked)
        RowsFirst.Checked = false;
      else
        RowsFirst.Checked = true;
    }

    private void HideTaskbarButtons_CheckedChanged(object sender, EventArgs e)
    {
      if (HideTaskbarButtons.Checked)
        qparent.HideTaskbarTab();
      else
        qparent.ShowTaskbarTab();
    }

    private void EnableAeroBack_CheckedChanged(object sender, EventArgs e)
    {
      UserSettings.Default.EnableAeroBackground = EnableAeroBack.Checked;
      qparent.AeroBackGroundRefresh();
    }
  }
}
