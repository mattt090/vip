﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.TransparentContainer
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using MinimizeCapture;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ThumbnailPersist
{
  public class TransparentContainer : Form
  {
    private static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
    public WINDOWINFO winfo = new WINDOWINFO();
    private bool TargetShown = true;
    private IContainer components = (IContainer) null;
    private const int SW_SHOWNOACTIVATE = 4;
    private const int HWND_TOPMOST = -1;
    private const uint SWP_NOACTIVATE = 16U;
    private const uint SWP_NOSIZE = 1U;
    private const uint SWP_NOMOVE = 2U;
    private const uint SWP_SHOWWINDOW = 64U;
    private const int SWP_NOZORDER = 4;
    private const int SWP_DRAWFRAME = 32;
    private const int GWL_EXSTYLE = -20;
    private const int WS_EX_LAYERED = 524288;
    private const int LWA_ALPHA = 2;
    public IntPtr OriginalParent;
    public IntPtr TheTargetWindow;
    private TransparentContainer.WINDOWPLACEMENT properties;
    private bool MaximizeTarget;
    private long TargetLongExStyle;
    private List<IntPtr> TargetChilds;

    public TransparentContainer(IntPtr TargetWindow, bool TargetIsMaximized, long TargetwinLong)
    {
      this.InitializeComponent();
      this.TargetLongExStyle = TargetwinLong;
      this.MaximizeTarget = TargetIsMaximized;
      TransparentContainer.SetWindowLong(this.Handle, -20, TransparentContainer.GetWindowLong(this.Handle, -20) | 524288L | 32L);
      TransparentContainer.SetLayeredWindowAttributes(this.Handle, (byte) 0, (byte) 0, 2);
      this.ProcessTarget(TargetWindow);
    }

    [DllImport("user32.dll")]
    private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);

    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    private static extern bool MoveWindow(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);

    [DllImport("user32")]
    public static extern int MoveWindow(int hwnd, int x, int y, int nWidth, int nHeight, int bRepaint);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr GetParent(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern int GetDesktopWindow();

    [DllImport("user32.dll")]
    private static extern bool SetForegroundWindow(int hwnd);

    [DllImport("User32.dll")]
    private static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, [In] ref RECT lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPos(int hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

    [DllImport("user32.dll")]
    private static extern long GetWindowLong(IntPtr hWnd, int nIndex);

    [DllImport("user32")]
    private static extern int SetLayeredWindowAttributes(IntPtr hWnd, byte crey, byte alpha, int flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetWindowPlacement(IntPtr hWnd, ref TransparentContainer.WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref TransparentContainer.WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    public static extern void GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

    private void ProcessTarget(IntPtr TargetWindow)
    {
      if (!this.TargetShown)
        return;
      this.TheTargetWindow = TargetWindow;
      bool flag1 = 1 == 0;
      this.SuspendLayout();
      bool flag2 = false;
      if (XPAppearance.MinAnimate)
      {
        XPAppearance.MinAnimate = false;
        flag2 = true;
      }
      StringBuilder lpString = new StringBuilder(100);
      TransparentContainer.GetWindowText(TargetWindow, lpString, lpString.Capacity);
      Size size;
      if (lpString.ToString().Contains("Adobe Photoshop CS"))
      {
        int x = SystemInformation.PrimaryMonitorSize.Width - 1;
        size = SystemInformation.PrimaryMonitorSize;
        int y = size.Height - 1;
        this.Location = new Point(x, y);
      }
      else
        this.Location = new Point(0, 0);
      this.properties = new TransparentContainer.WINDOWPLACEMENT();
      TransparentContainer.GetWindowPlacement(this.TheTargetWindow, ref this.properties);
      if (this.properties.flags == 2)
      {
        this.properties.showCmd = 3;
        this.winfo.rcWindow.left = 0;
        this.winfo.rcWindow.top = 0;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        RECT local1 = this.winfo.rcWindow;
        size = SystemInformation.PrimaryMonitorMaximizedWindowSize;
        int width1 = size.Width;
        int num1 = 2;
        size = SystemInformation.FrameBorderSize;
        int width2 = size.Width;
        int num2 = num1 * width2;
        int num3 = width1 - num2;
        // ISSUE: explicit reference operation
        (local1).right = num3;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        RECT local2 = this.winfo.rcWindow;
        size = SystemInformation.PrimaryMonitorMaximizedWindowSize;
        int height1 = size.Height;
        int num4 = 2;
        size = SystemInformation.FrameBorderSize;
        int height2 = size.Height;
        int num5 = num4 * height2;
        int num6 = height1 - num5;
        // ISSUE: explicit reference operation
        (local2).bottom = num6;
      }
      else
      {
        this.properties.showCmd = 1;
        this.winfo.rcWindow.left = this.properties.rcNormalPosition.left;
        this.winfo.rcWindow.top = this.properties.rcNormalPosition.top;
        this.winfo.rcWindow.right = this.properties.rcNormalPosition.right;
        this.winfo.rcWindow.bottom = this.properties.rcNormalPosition.bottom;
      }
      this.Size = new Size(this.winfo.rcWindow.right - this.winfo.rcWindow.left, this.winfo.rcWindow.bottom - this.winfo.rcWindow.top);
      IntPtr hWnd = this.TheTargetWindow;
      IntPtr hWndInsertAfter = IntPtr.Zero;
      size = SystemInformation.PrimaryMonitorSize;
      int X = size.Width - 1;
      size = SystemInformation.PrimaryMonitorSize;
      int Y = size.Height - 1;
      int cx = this.winfo.rcWindow.right - this.winfo.rcWindow.left;
      int cy = this.winfo.rcWindow.bottom - this.winfo.rcWindow.top;
      int num = 17;
      TransparentContainer.SetWindowPos(hWnd, hWndInsertAfter, X, Y, cx, cy, (uint) num);
      TransparentContainer.ShowWindow(this.TheTargetWindow, 1);
      TransparentContainer.SetWindowLong(this.TheTargetWindow, -20, this.TargetLongExStyle);
      this.OriginalParent = TransparentContainer.GetParent(this.TheTargetWindow);
      TransparentContainer.SetParent(this.TheTargetWindow, this.Handle);
      TransparentContainer.SetWindowPos(this.TheTargetWindow, IntPtr.Zero, 0, 0, this.winfo.rcWindow.right - this.winfo.rcWindow.left, this.winfo.rcWindow.bottom - this.winfo.rcWindow.top, 16U);
      TransparentContainer.SetWindowPos(this.TheTargetWindow, (IntPtr) (-1), 0, 0, this.winfo.rcWindow.right - this.winfo.rcWindow.left, this.winfo.rcWindow.bottom - this.winfo.rcWindow.top, 19U);
      TransparentContainer.SetWindowPos(this.Handle, TransparentContainer.HWND_BOTTOM, 0, 0, 0, 0, 19U);
      TransparentContainer.RedrawWindow(this.TheTargetWindow, IntPtr.Zero, IntPtr.Zero, 257U);
      this.ResumeLayout(true);
      this.TopMost = true;
      this.Show();
      if (flag2)
        XPAppearance.MinAnimate = true;
      this.TargetShown = false;
    }

    private void TransparentContainer_MouseDoubleClick(object sender, MouseEventArgs e)
    {
    }

    private void TransparentContainer_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (this.TargetShown)
      {
        TransparentContainer.SetForegroundWindow(this.TheTargetWindow);
      }
      else
      {
        bool flag1 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag1 = true;
        }
        bool flag2 = 1 == 0;
        TransparentContainer.SetWindowLong(this.TheTargetWindow, -20, TransparentContainer.GetWindowLong(this.TheTargetWindow, -20) | 524288L);
        TransparentContainer.SetLayeredWindowAttributes(this.TheTargetWindow, (byte) 0, (byte) 0, 2);
        TransparentContainer.SetWindowPos(this.TheTargetWindow, IntPtr.Zero, SystemInformation.PrimaryMonitorSize.Width - 1, SystemInformation.PrimaryMonitorSize.Height - 1, this.winfo.rcWindow.right - this.winfo.rcWindow.left, this.winfo.rcWindow.bottom - this.winfo.rcWindow.top, 17U);
        flag2 = 1 == 0;
        TransparentContainer.SetParent(this.TheTargetWindow, this.OriginalParent);
        long windowLong = TransparentContainer.GetWindowLong(this.TheTargetWindow, -20);
        TransparentContainer.SetWindowLong(this.TheTargetWindow, -20, windowLong | 524288L);
        TransparentContainer.SetLayeredWindowAttributes(this.TheTargetWindow, (byte) 0, (byte) 0, 2);
        TransparentContainer.SetWindowPlacement(this.TheTargetWindow, ref this.properties);
        TransparentContainer.SetForegroundWindow(this.TheTargetWindow);
        TransparentContainer.SetWindowLong(this.TheTargetWindow, -20, windowLong);
        if (!flag1)
          return;
        XPAppearance.MinAnimate = true;
      }
    }

    private void TransparentContainer_Load(object sender, EventArgs e)
    {
    }

    public void ShowTarget()
    {
      if (this.TargetShown)
        return;
      bool flag = 1 == 0;
      TransparentContainer.SetWindowLong(this.TheTargetWindow, -20, TransparentContainer.GetWindowLong(this.TheTargetWindow, -20) | 524288L);
      TransparentContainer.SetLayeredWindowAttributes(this.TheTargetWindow, (byte) 0, (byte) 0, 2);
      TransparentContainer.SetWindowPos(this.TheTargetWindow, IntPtr.Zero, SystemInformation.PrimaryMonitorSize.Width - 1, SystemInformation.PrimaryMonitorSize.Height - 1, this.winfo.rcWindow.right - this.winfo.rcWindow.left, this.winfo.rcWindow.bottom - this.winfo.rcWindow.top, 16U);
      flag = 1 == 0;
      TransparentContainer.SetParent(this.TheTargetWindow, this.OriginalParent);
      long windowLong = TransparentContainer.GetWindowLong(this.TheTargetWindow, -20);
      TransparentContainer.SetWindowLong(this.TheTargetWindow, -20, windowLong | 524288L);
      TransparentContainer.SetLayeredWindowAttributes(this.TheTargetWindow, (byte) 0, (byte) 0, 2);
      TransparentContainer.SetWindowPlacement(this.TheTargetWindow, ref this.properties);
      TransparentContainer.SetForegroundWindow(this.TheTargetWindow);
      TransparentContainer.SetWindowLong(this.TheTargetWindow, -20, windowLong);
      TransparentContainer.RedrawWindow(this.TheTargetWindow, IntPtr.Zero, IntPtr.Zero, 257U);
      this.TargetShown = true;
    }

    public void HideTarget(IntPtr TargetWindow, long NewExStyle)
    {
      this.TargetLongExStyle = NewExStyle;
      this.ProcessTarget(TargetWindow);
    }

    [DllImport("user32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool EnumChildWindows(IntPtr window, TransparentContainer.EnumWindowProc callback, IntPtr i);

    public static List<IntPtr> GetChildWindows(IntPtr parent)
    {
      List<IntPtr> list = new List<IntPtr>();
      GCHandle gcHandle = GCHandle.Alloc((object) list);
      try
      {
        TransparentContainer.EnumWindowProc callback = new TransparentContainer.EnumWindowProc(TransparentContainer.EnumWindow);
        TransparentContainer.EnumChildWindows(parent, callback, GCHandle.ToIntPtr(gcHandle));
      }
      finally
      {
        if (gcHandle.IsAllocated)
          gcHandle.Free();
      }
      return list;
    }

    private static bool EnumWindow(IntPtr handle, IntPtr pointer)
    {
      List<IntPtr> list = GCHandle.FromIntPtr(pointer).Target as List<IntPtr>;
      if (list == null)
        throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
      list.Add(handle);
      return true;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.SuspendLayout();
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = Color.Black;
      this.ClientSize = new Size(500, 500);
      this.ControlBox = false;
      this.DoubleBuffered = true;
      this.FormBorderStyle = FormBorderStyle.None;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "TransparentContainer";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.Manual;
      this.TopMost = true;
      this.Hide();
      this.TransparencyKey = Color.Black;
      this.Load += new EventHandler(this.TransparentContainer_Load);
      this.MouseDoubleClick += new MouseEventHandler(this.TransparentContainer_MouseDoubleClick);
      this.FormClosing += new FormClosingEventHandler(this.TransparentContainer_FormClosing);
      this.ResumeLayout(false);
    }

    public struct POINTAPI
    {
      public int x;
      public int y;
    }

    public struct WINDOWPLACEMENT
    {
      public int length;
      public int flags;
      public int showCmd;
      public TransparentContainer.POINTAPI ptMinPosition;
      public TransparentContainer.POINTAPI ptMaxPosition;
      public RECT rcNormalPosition;
    }

    public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);
  }
}
