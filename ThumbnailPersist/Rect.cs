﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.Rect
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

namespace ThumbnailPersist
{
  internal struct Rect
  {
    public int Left;
    public int Top;
    public int Right;
    public int Bottom;

    internal Rect(int left, int top, int right, int bottom)
    {
      Left = left;
      Top = top;
      Right = right;
      Bottom = bottom;
    }
  }
}
