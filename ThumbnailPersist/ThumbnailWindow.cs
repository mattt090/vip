﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.ThumbnailWindow
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using MinimizeCapture;
using TrayPreviewSpace;

namespace ThumbnailPersist
{
    public class ThumbnailWindow : Form
    {
        private static readonly int GWL_STYLE = -16;
        private static readonly int DWM_TNP_VISIBLE = 8;
        private static readonly int DWM_TNP_OPACITY = 4;
        private static readonly int DWM_TNP_RECTDESTINATION = 1;
        private static readonly int DWM_TNP_RECTSOURCE = 2;
        private static readonly int DWM_TNP_SOURCECLIENTAREAONLY = 16;
        private static readonly ulong WS_VISIBLE = 268435456UL;
        private static readonly ulong WS_BORDER = 8388608UL;
        private static readonly ulong TARGETWINDOW = WS_BORDER | WS_VISIBLE;
        private static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
        private IContainer components;
        public string currentwindow;
        public bool restorewindow;
        public bool emptywindow = true;
        public bool minimizing;
        private bool isMouseDown;
        public bool controlkeyisdown;
        public int panoffsetx1;
        public int panoffsety1;
        public int panoffsetx2;
        public int panoffsety2;
        public int deltapanx;
        public int deltapany;
        public bool isDocked = true;
        public bool isbrowsermode = false;
        public WINDOWINFO winfo;
        public WINDOWINFO winfo2;
        public WINDOWINFO winfo3 = new WINDOWINFO();
        public long PreviewOriginalExStyle;
        private CacheWindowSize CachedPreviewParams;
        private bool UseCacheParams;
        private List<Window> windows = new List<Window>();
        private bool TargetShown;
        private ArrayList ChildLongs = new ArrayList();
        private bool isProcessing = true;
        private IntPtr TempThumb = IntPtr.Zero;
        private IntPtr StoredCurrentWindowHandle = IntPtr.Zero;
        private bool clicked;
        private bool inRegion;
        private bool inN;
        private bool inS;
        private bool inE;
        private bool inW;
        public bool wallpapermode = false;
        private bool ShiftisDown;
        private bool DockinN;
        private bool DockinS;
        private bool DockinE;
        private bool DockinW;
        private bool DockinCentre;
        private bool Moved;
        private int FrameBorderSizeWidth;
        private int FrameBorderSizeHeight;
        public const int WM_NCLBUTTONDOWN = 161;
        public const int HTCAPTION = 2;
        private const int SC_RESTORE = 61728;
        private const int WM_SYSCOMMAND = 274;
        private const int SC_MINIMIZE = 61472;
        public const int SC_MAXIMIZE = 61488;
        private const int WM_CLOSE = 16;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        public const int WM_LBUTTONDOWN = 513;
        public const int WM_LBUTTONUP = 514;
        public const int WM_RBUTTONDOWN = 516;
        public const int WM_RBUTTONUP = 517;
        private const uint WM_PAINT = 15U;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int HWND_TOPMOST = -1;
        private const uint SWP_NOACTIVATE = 16U;
        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_TOOLWINDOW = 128;
        private const int WS_EX_APPWINDOW = 262144;
        private const int WS_EX_LAYERED = 524288;
        private const int LWA_ALPHA = 2;
        public const int LWA_COLORKEY = 1;
        private const int WS_EX_NOACTIVATE = 134217728;
        private const int WS_EX_TRANSPARENT = 32;
        private const uint SWP_NOSIZE = 1U;
        private const uint SWP_NOMOVE = 2U;
        private const uint SWP_SHOWWINDOW = 64U;
        private const int WM_MOUSEWHEEL = 522;
        private const int WM_SIZE = 5;
        private const int SIZE_MINIMIZED = 1;
        public const int WM_CLOSE2 = 61536;
        private PictureBox image;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem quitToolStripMenuItem;
        private ToolStripMenuItem minimizeToolStripMenuItem;
        private ToolStripMenuItem settingsToolStripMenuItem;
        private Timer click_timer;
        private ToolStripMenuItem learnToolStripMenuItem;
        public WebBrowser webBrowser1;
        private ToolStripMenuItem closeToolStripMenuItem;
        public NotifyIcon notifyIcon1;
        private Label AuthorLabel;
        private Label TitleLabel;
        private ToolStripMenuItem hideTargetWindowToolStripMenuItem;
        private ToolStripMenuItem showTargetWindowToolStripMenuItem;
        private ToolTip FormToolTip;
        private ToolStripMenuItem alwaysOnTopNoMoveToolStripMenuItem;
        private WindowSize currentwindowsize;
        public string currentwindowname;
        private int userwindowindexnum;
        public int zoomleftoffset;
        public int zoomtopoffset;
        public int zoomrightoffset;
        public int zoombottomoffset;
        public double zoomfactor;
        public IntPtr currentwindowhandle;
        public int zoomcenterx;
        public int zoomcentery;
        public int storedwindowlocationx;
        public int storedwindowlocationy;
        private IntPtr thumb;
        private Point mouseOffset;
        public bool isYoutube;
        public int mouseclickx;
        public int mouseclicky;
        public double aspectratio;
        public int cropleftoffset;
        public int croptopoffset;
        public int croprightoffset;
        public int cropbottomoffset;
        public int xsizethumb;
        public int ysizethumb;
        public int PreviousXSizeThumb;
        public int PreviousYSizeThumb;
        public int PreviousXImageSize;
        public int PreviousYImageSize;
        public int PreviousXFormSize;
        public int PreviousYFormSize;
        public double croppedratioxleft;
        public double croppedratioxright;
        public double croppedratioytop;
        public double croppedratioybottom;
        public double zoomscalingfactor;
        public double resizeratio;
        public bool triploop;
        public bool zoomingOP;
        public int zoombyxpixels;
        public int zoombyypixels;
        public bool isvideo;
        public int xsizeform;
        public int ysizeform;
        public bool matchedwanted;
        public bool matchednotwanted;
        public static long WS_Transparent_Reverse;
        public int initializeparam;
        public IntPtr initializetaskbarsrc;
        private TrayPreview qparent;
        private long winLong;
        private WINDOWPLACEMENT placement;
        private List<IntPtr> TargetChilds;
        private Point ptOffset;
        private Size PreviousSize;
        private Point LockedPoint;
        private MARGINS margins;

        private int North
        {
            get
            {
                return Height - Height + 10;
            }
            set
            {
                North = value;
            }
        }

        private int South
        {
            get
            {
                return Height - 10;
            }
            set
            {
                South = value;
            }
        }

        private int East
        {
            get
            {
                return Width - 10;
            }
            set
            {
                East = value;
            }
        }

        private int West
        {
            get
            {
                return Width - Width + 10;
            }
            set
            {
                West = value;
            }
        }

        public ThumbnailWindow()
        {
        }

        public ThumbnailWindow(int param, IntPtr taskbarsrc)
        {
            initializeparam = param;
            initializetaskbarsrc = taskbarsrc;
            InitializeComponent();
        }

        public ThumbnailWindow(int param, IntPtr taskbarsrc, long TargetExStyle, bool WindowIsMaximized, TrayPreview TrayPreviewHandle)
        {
            PreviewOriginalExStyle = GetWindowLong(Handle, -20);
            SetWindowLong(Handle, -20, PreviewOriginalExStyle | 524288L);
            SetLayeredWindowAttributes(Handle, 0, 0, 2);
            initializeparam = param;
            initializetaskbarsrc = taskbarsrc;
            winLong = TargetExStyle;
            qparent = TrayPreviewHandle;
            InitializeComponent();
            click_timer.Enabled = false;
            GC.KeepAlive(this);
        }

        public ThumbnailWindow(int param, IntPtr taskbarsrc, long TargetExStyle, bool WindowIsMaximized, TrayPreview TrayPreviewHandle, CacheWindowSize OverRidePreviewParams)
        {
            PreviewOriginalExStyle = GetWindowLong(Handle, -20);
            SetWindowLong(Handle, -20, PreviewOriginalExStyle | 524288L);
            SetLayeredWindowAttributes(Handle, 0, 0, 2);
            initializeparam = param;
            initializetaskbarsrc = taskbarsrc;
            winLong = TargetExStyle;
            qparent = TrayPreviewHandle;
            CachedPreviewParams = OverRidePreviewParams;
            InitializeComponent();
            click_timer.Enabled = false;
            GC.KeepAlive(this);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
                components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            components = new Container();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(ThumbnailWindow));
            contextMenuStrip1 = new ContextMenuStrip(components);
            closeToolStripMenuItem = new ToolStripMenuItem();
            hideTargetWindowToolStripMenuItem = new ToolStripMenuItem();
            showTargetWindowToolStripMenuItem = new ToolStripMenuItem();
            minimizeToolStripMenuItem = new ToolStripMenuItem();
            learnToolStripMenuItem = new ToolStripMenuItem();
            quitToolStripMenuItem = new ToolStripMenuItem();
            settingsToolStripMenuItem = new ToolStripMenuItem();
            alwaysOnTopNoMoveToolStripMenuItem = new ToolStripMenuItem();
            notifyIcon1 = new NotifyIcon(components);
            click_timer = new Timer(components);
            image = new PictureBox();
            AuthorLabel = new Label();
            TitleLabel = new Label();
            FormToolTip = new ToolTip(components);
            contextMenuStrip1.SuspendLayout();
            ((ISupportInitialize)image).BeginInit();
            SuspendLayout();
            contextMenuStrip1.BackColor = SystemColors.ControlLightLight;
            contextMenuStrip1.BackgroundImageLayout = ImageLayout.None;
            contextMenuStrip1.Items.AddRange(new ToolStripItem[8]
      {
        closeToolStripMenuItem,
        hideTargetWindowToolStripMenuItem,
        showTargetWindowToolStripMenuItem,
        minimizeToolStripMenuItem,
        learnToolStripMenuItem,
        quitToolStripMenuItem,
        settingsToolStripMenuItem,
        alwaysOnTopNoMoveToolStripMenuItem
      });
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.RenderMode = ToolStripRenderMode.System;
            contextMenuStrip1.ShowCheckMargin = true;
            contextMenuStrip1.ShowImageMargin = false;
            contextMenuStrip1.ShowItemToolTips = false;
            contextMenuStrip1.Size = new Size(188, 180);
            contextMenuStrip1.TabStop = true;
            contextMenuStrip1.MouseUp += contextMenuStrip1_MouseUp;
            contextMenuStrip1.ItemClicked += contextMenuStrip1_ItemClicked;
            contextMenuStrip1.Opening += contextMenuStrip1_Opening;
            contextMenuStrip1.Closing += contextMenuStrip1_Closing;
            contextMenuStrip1.MouseLeave += contextMenuStrip1_MouseLeave;
            closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            closeToolStripMenuItem.Size = new Size(187, 22);
            closeToolStripMenuItem.Text = "Close";
            closeToolStripMenuItem.Click += closeToolStripMenuItem_Click;
            hideTargetWindowToolStripMenuItem.Name = "hideTargetWindowToolStripMenuItem";
            hideTargetWindowToolStripMenuItem.Size = new Size(187, 22);
            hideTargetWindowToolStripMenuItem.Text = "Hide Target Window";
            hideTargetWindowToolStripMenuItem.Click += hideTargetWindowToolStripMenuItem_Click;
            showTargetWindowToolStripMenuItem.Name = "showTargetWindowToolStripMenuItem";
            showTargetWindowToolStripMenuItem.Size = new Size(187, 22);
            showTargetWindowToolStripMenuItem.Text = "Show Target Window";
            showTargetWindowToolStripMenuItem.Click += showTargetWindowToolStripMenuItem_Click;
            minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
            minimizeToolStripMenuItem.Size = new Size(187, 22);
            minimizeToolStripMenuItem.Text = "Minimize";
            minimizeToolStripMenuItem.Click += minimizeToolStripMenuItem_Click;
            learnToolStripMenuItem.Name = "learnToolStripMenuItem";
            learnToolStripMenuItem.Size = new Size(187, 22);
            learnToolStripMenuItem.Text = "Learn...";
            learnToolStripMenuItem.Click += learnToolStripMenuItem_Click;
            quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            quitToolStripMenuItem.Size = new Size(187, 22);
            quitToolStripMenuItem.Text = "Quit";
            quitToolStripMenuItem.Click += quitToolStripMenuItem_Click;
            settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            settingsToolStripMenuItem.Size = new Size(187, 22);
            settingsToolStripMenuItem.Text = "Settings";
            settingsToolStripMenuItem.Click += settingsToolStripMenuItem_Click;
            alwaysOnTopNoMoveToolStripMenuItem.CheckOnClick = true;
            alwaysOnTopNoMoveToolStripMenuItem.Name = "alwaysOnTopNoMoveToolStripMenuItem";
            alwaysOnTopNoMoveToolStripMenuItem.Size = new Size(187, 22);
            alwaysOnTopNoMoveToolStripMenuItem.Text = "Always on Top";
            alwaysOnTopNoMoveToolStripMenuItem.Click += alwaysOnTopNoMoveToolStripMenuItem_Click;
            notifyIcon1.BalloonTipText = "No Name";
            notifyIcon1.BalloonTipTitle = "Video in Picture";
            notifyIcon1.ContextMenuStrip = contextMenuStrip1;
            notifyIcon1.Icon = (Icon)componentResourceManager.GetObject("notifyIcon1.Icon");
            notifyIcon1.Text = "Video in Picture";
            notifyIcon1.MouseClick += notifyIcon1_MouseClick;
            notifyIcon1.MouseDoubleClick += notifyIcon1_MouseDoubleClick;
            click_timer.Tick += click_timer_Tick;
            image.AccessibleDescription = "Image";
            image.AccessibleName = "ViP";
            image.AccessibleRole = AccessibleRole.Window;
            image.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            image.BackColor = Color.Transparent;
            image.BackgroundImageLayout = ImageLayout.Center;
            image.Location = new Point(0, 0);
            image.Margin = new Padding(0);
            image.Name = "image";
            image.Size = new Size(307, 110);
            image.SizeMode = PictureBoxSizeMode.StretchImage;
            image.TabIndex = 5;
            image.TabStop = false;
            image.DoubleClick += image_DoubleClick;
            image.MouseLeave += image_MouseLeave;
            image.MouseMove += image_MouseMove;
            image.Click += image_Click;
            image.MouseClick += image_MouseClick;
            image.MouseDown += image_MouseDown;
            image.MouseHover += image_MouseHover;
            image.MouseUp += image_MouseUp;
            image.MouseEnter += image_MouseEnter;
            AuthorLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            AuthorLabel.AutoSize = true;
            AuthorLabel.BackColor = Color.Transparent;
            AuthorLabel.Font = new Font("Brush Script MT", 9.75f, FontStyle.Italic, GraphicsUnit.Point, 0);
            AuthorLabel.ForeColor = Color.DarkGray;
            AuthorLabel.Location = new Point(221, 65);
            AuthorLabel.Name = "AuthorLabel";
            AuthorLabel.Size = new Size(74, 16);
            AuthorLabel.TabIndex = 8;
            AuthorLabel.Text = "By: Eric Wong";
            AuthorLabel.TextAlign = ContentAlignment.MiddleCenter;
            TitleLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            TitleLabel.AutoSize = true;
            TitleLabel.BackColor = Color.Transparent;
            TitleLabel.Font = new Font("Brush Script MT", 18f, FontStyle.Italic, GraphicsUnit.Point, 0);
            TitleLabel.ForeColor = Color.Silver;
            TitleLabel.Location = new Point(4, 25);
            TitleLabel.Name = "TitleLabel";
            TitleLabel.Size = new Size(304, 29);
            TitleLabel.TabIndex = 9;
            TitleLabel.Text = "~ Video in Picture (0.2 Preview) ~";
            TitleLabel.TextAlign = ContentAlignment.MiddleCenter;
            FormToolTip.Active = false;
            FormToolTip.AutomaticDelay = 300;
            FormToolTip.AutoPopDelay = 3000;
            FormToolTip.InitialDelay = 10;
            FormToolTip.ReshowDelay = 10;
            AccessibleDescription = "Video in Picture";
            AccessibleName = "ViP";
            AccessibleRole = AccessibleRole.Window;
            AutoScaleMode = AutoScaleMode.None;
            AutoValidate = AutoValidate.EnableAllowFocusChange;
            BackColor = Color.Black;
            BackgroundImageLayout = ImageLayout.None;
            ClientSize = new Size(307, 110);
            ContextMenuStrip = contextMenuStrip1;
            ControlBox = false;
            Controls.Add(AuthorLabel);
            Controls.Add(TitleLabel);
            Controls.Add(image);
            DoubleBuffered = true;
            Font = new Font("Segoe UI", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            FormBorderStyle = FormBorderStyle.None;
            KeyPreview = true;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ThumbnailWindow";
            Opacity = 0.0;
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.Manual;
            TransparencyKey = Color.Transparent;
            Deactivate += ThumbnailWindow_Deactivate;
            Load += Form1_Load;
            Shown += ThumbnailWindow_Shown;
            Leave += ThumbnailWindow_Leave;
            KeyUp += Form1_KeyUp;
            Move += ThumbnailWindow_Move;
            FormClosing += ThumbnailWindow_FormClosing;
            Resize += Form1_Resize;
            Validated += ThumbnailWindow_Validated;
            KeyDown += Form1_KeyDown;
            contextMenuStrip1.ResumeLayout(false);
            ((ISupportInitialize)image).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        [DllImport("dwmapi.dll")]
        private static extern int DwmRegisterThumbnail(IntPtr dest, IntPtr src, out IntPtr thumb);

        [DllImport("dwmapi.dll")]
        private static extern int DwmUnregisterThumbnail(IntPtr thumb);

        [DllImport("dwmapi.dll")]
        private static extern int DwmQueryThumbnailSourceSize(IntPtr thumb, out PSIZE size);

        [DllImport("dwmapi.dll")]
        private static extern int DwmUpdateThumbnailProperties(IntPtr hThumb, ref DWM_THUMBNAIL_PROPERTIES props);

        [DllImport("SHELL32", CallingConvention = CallingConvention.StdCall)]
        private static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);

        [DllImport("USER32")]
        private static extern int GetSystemMetrics(int Index);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern bool MoveWindow(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        private static extern int RegisterWindowMessage(string msg);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool IsIconic(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern ulong GetWindowLongA(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int EnumWindows(EnumWindowsCallback lpEnumFunc, int lParam);

        [DllImport("user32.dll")]
        public static extern void GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(int hwnd);

        [DllImport("user32.dll")]
        private static extern int GetDesktopWindow();

        [DllImport("user32.dll")]
        private static extern int FindWindow(string className, string windowText);

        [DllImport("user32")]
        public static extern int MoveWindow(int hwnd, int x, int y, int nWidth, int nHeight, int bRepaint);

        [DllImport("user32.dll")]
        private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern uint SendMessage(IntPtr hWnd, uint msg, uint wParam, uint lParam);

        [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll")]
        private static extern bool SetWindowPos(int hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        [DllImport("user32")]
        public static extern int SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

        [DllImport("user32.dll")]
        private static extern long GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, IntPtr windowTitle);

        [DllImport("user32")]
        public static extern int SetParent(int hWndChild, int hWndNewParent);

        [DllImport("user32")]
        private static extern int SetLayeredWindowAttributes(IntPtr hWnd, byte crey, byte alpha, int flags);

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow(IntPtr hWnd, ShowWindowEnum flags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RedrawWindow(IntPtr hWnd, [In] ref RECT lprcUpdate, IntPtr hrgnUpdate, uint flags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll")]
        private static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WINDOWPLACEMENT lpwndpl);

        public void RestoreTransparency()
        {
            SetLayeredWindowAttributes(Handle, 0, (byte)(byte.MaxValue - UserSettings.Default.BackTransparency), 2);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (initializeparam == 1)
                return;
            if (initializeparam == 2)
            {
                if (UserSettings.Default.ShowBordersByDefault)
                    FormBorderStyle = FormBorderStyle.Sizable;
                else
                    FormBorderStyle = FormBorderStyle.None;
                if (UserSettings.Default.ShowBackText)
                {
                    TitleLabel.Visible = true;
                    AuthorLabel.Visible = true;
                }
                else
                {
                    TitleLabel.Visible = false;
                    AuthorLabel.Visible = false;
                }
                if (thumb != IntPtr.Zero)
                    DwmUnregisterThumbnail(thumb);
                DwmRegisterThumbnail(Handle, initializetaskbarsrc, out thumb);
                currentwindowhandle = initializetaskbarsrc;
                SetDefaultViewNoPosition();
            }
            else if (initializeparam == 3)
            {
                if (UserSettings.Default.ShowBordersByDefault)
                    FormBorderStyle = FormBorderStyle.Sizable;
                else
                    FormBorderStyle = FormBorderStyle.None;
                if (UserSettings.Default.ShowBackText)
                {
                    TitleLabel.Visible = true;
                    AuthorLabel.Visible = true;
                }
                else
                {
                    TitleLabel.Visible = false;
                    AuthorLabel.Visible = false;
                }
                if (thumb != IntPtr.Zero)
                    DwmUnregisterThumbnail(thumb);
                DwmRegisterThumbnail(Handle, initializetaskbarsrc, out thumb);
                currentwindowhandle = initializetaskbarsrc;
                SetDefaultViewNoPosition();
            }
            else
            {
                if (initializeparam != 4)
                    return;
                if (UserSettings.Default.ShowBordersByDefault)
                    FormBorderStyle = FormBorderStyle.Sizable;
                else
                    FormBorderStyle = FormBorderStyle.None;
                if (UserSettings.Default.ShowBackText)
                {
                    TitleLabel.Visible = true;
                    AuthorLabel.Visible = true;
                }
                else
                {
                    TitleLabel.Visible = false;
                    AuthorLabel.Visible = false;
                }
                if (thumb != IntPtr.Zero)
                    DwmUnregisterThumbnail(thumb);
                DwmRegisterThumbnail(Handle, initializetaskbarsrc, out thumb);
                currentwindowhandle = initializetaskbarsrc;
                UseCacheParams = true;
                SetDefaultViewNoPosition2();
                UseCacheParams = false;
            }
        }

        public void LoadPreviewCacheParams()
        {
            aspectratio = CachedPreviewParams.aspectratio;
            resizeratio = CachedPreviewParams.resizeratio;
            xsizethumb = CachedPreviewParams.xsizethumb;
            ysizethumb = CachedPreviewParams.ysizethumb;
            cropbottomoffset = CachedPreviewParams.cropbottomoffset;
            croprightoffset = CachedPreviewParams.croprightoffset;
            croptopoffset = CachedPreviewParams.croptopoffset;
            cropleftoffset = CachedPreviewParams.cropleftoffset;
            int index = qparent.TargettedWindows.IndexOf(currentwindowhandle);
            qparent.AlwaysOnTop[index] = CachedPreviewParams.alwaysontop;
            qparent.NoMove[index] = CachedPreviewParams.nomove;
            Location = new Point(CachedPreviewParams.locationx, CachedPreviewParams.locationy);
        }

        public void SavePreviewCacheParams()
        {
            int index1 = qparent.CacheTargetHandles.IndexOf(currentwindowhandle);
            if (index1 < 0)
                return;
            CacheWindowSize cacheWindowSize = new CacheWindowSize();
            cacheWindowSize.aspectratio = aspectratio;
            cacheWindowSize.resizeratio = resizeratio;
            cacheWindowSize.xsizethumb = xsizethumb;
            cacheWindowSize.ysizethumb = ysizethumb;
            cacheWindowSize.cropbottomoffset = cropbottomoffset;
            cacheWindowSize.croprightoffset = croprightoffset;
            cacheWindowSize.croptopoffset = croptopoffset;
            cacheWindowSize.cropleftoffset = cropleftoffset;
            int index2 = qparent.ViPPreviewHandles.IndexOf(Handle);
            cacheWindowSize.alwaysontop = (bool)qparent.AlwaysOnTop[index2];
            cacheWindowSize.nomove = (bool)qparent.NoMove[index2];
            cacheWindowSize.locationx = Location.X;
            cacheWindowSize.locationy = Location.Y;
            qparent.CachePreviewSize[index1] = cacheWindowSize;
        }

        public void SetDefaultView()
        {
            ResetSettings();
            Hide();
            SizeWindow(1);
            Position_Screen();
            Show();
        }

        public void SetDefaultViewNoPosition()
        {
            ResetSettings();
            Hide();
            SizeWindow(1);
            Position_Screen_Start();
        }

        public void SetDefaultViewNoPosition2()
        {
            ResetSettings();
            Hide();
            SizeWindow(1);
        }

        public void ResetSettings()
        {
            zoomfactor = UserSettings.Default.ZoomDefaultFactor;
            deltapanx = 0;
            deltapany = 0;
            croppedratioxleft = 0.0;
            croppedratioxright = 0.0;
            croppedratioytop = 0.0;
            croppedratioybottom = 0.0;
            zoomscalingfactor = UserSettings.Default.ZoomScalingFactor;
            zoombyxpixels = 0;
            zoombyypixels = 0;
            zoomleftoffset = 0;
            zoomtopoffset = 0;
            zoomrightoffset = 0;
            zoombottomoffset = 0;
            cropbottomoffset = 0;
            cropleftoffset = 0;
            croptopoffset = 0;
            croprightoffset = 0;
            TargetShown = false;
        }

        private void Position_Screen()
        {
            if (initializeparam == 1)
                return;
            APPBARDATA pData = new APPBARDATA();
            int num1 = (int)SHAppBarMessage(5, ref pData);
            int num2 = (int)SHAppBarMessage(4, ref pData);
            int num3 = 0;
            int num4 = 0;
            int num5 = 0;
            int x1 = 0;
            int num6 = 0;
            int y1 = 0;
            int num7 = 0;
            if (UserSettings.Default.CustomTaskbarMargins)
            {
                x1 = UserSettings.Default.CustomTaskbarMarginsLeft;
                num7 = -UserSettings.Default.CustomTaskbarMarginsBottom;
                y1 = UserSettings.Default.CustomTaskbarMarginsTop;
                num6 = -UserSettings.Default.CustomTaskbarMarginsRight;
            }
            else if (pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
            {
                num5 = 1;
                num4 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
            }
            else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
            {
                num5 = 2;
                num4 = pData.rc.bottom;
            }
            else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
            {
                num5 = 3;
                num3 = pData.rc.right;
            }
            else if (pData.rc.top == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
            {
                num5 = 4;
                num3 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
            }
            Point location;
            if (UserSettings.Default.DockPreview && UserSettings.Default.PreviewGridAlign == "None")
            {
                if (UserSettings.Default.startincenter)
                {
                    int right = Screen.PrimaryScreen.Bounds.Right;
                    Rectangle bounds = Screen.PrimaryScreen.Bounds;
                    int num8 = bounds.Right / 2;
                    int x2 = right - num8 - Size.Width / 2;
                    bounds = Screen.PrimaryScreen.Bounds;
                    int bottom = bounds.Bottom;
                    bounds = Screen.PrimaryScreen.Bounds;
                    int num9 = bounds.Bottom / 2;
                    int y2 = bottom - num9 - Size.Height / 2;
                    Location = new Point(x2, y2);
                }
                else if (UserSettings.Default.startbottomleft)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                }
                else if (UserSettings.Default.startbottomright)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    int right = Screen.PrimaryScreen.Bounds.Right;
                    Size size = Size;
                    int width = size.Width;
                    int x2 = right - width + num3 + num6;
                    int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                    size = Size;
                    int height = size.Height;
                    int y2 = bottom - height + num4 + num7;
                    Location = new Point(x2, y2);
                }
                else if (UserSettings.Default.startright)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y < y1)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, y1);
                        }
                        else
                        {
                            int num8 = Location.Y + Size.Height;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num9 = size.Height + num7;
                            if (num8 > num9)
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                int x2 = right - width + num3 + num6;
                                size = SystemInformation.PrimaryMonitorSize;
                                int num10 = size.Height + num7;
                                size = Size;
                                int height = size.Height;
                                int y2 = num10 - height;
                                Location = new Point(x2, y2);
                            }
                            else
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                Location = new Point(right - width + num3 + num6, Location.Y);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 3)
                            num3 = 0;
                        Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Location.Y);
                    }
                }
                else if (UserSettings.Default.startleft)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y < y1)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, y1);
                        }
                        else
                        {
                            int num8 = Location.Y + Size.Height;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num9 = size.Height + num7;
                            if (num8 > num9)
                            {
                                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                                size = SystemInformation.PrimaryMonitorSize;
                                int num10 = size.Height + num7;
                                size = Size;
                                int height = size.Height;
                                int y2 = num10 - height;
                                Location = new Point(x2, y2);
                            }
                            else
                                Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Location.Y);
                        }
                    }
                    else
                    {
                        if (num5 == 4)
                            num3 = 0;
                        int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                        location = Location;
                        int y2 = location.Y;
                        Location = new Point(x2, y2);
                    }
                }
                else if (UserSettings.Default.startbottommiddle)
                {
                    if (num5 == 2)
                        num4 = 0;
                    int num8 = Screen.PrimaryScreen.Bounds.Right / 2;
                    Size size = Size;
                    int num9 = size.Width / 2;
                    int x2 = num8 - num9 + num3;
                    int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                    size = Size;
                    int height = size.Height;
                    int y2 = bottom - height + num4 + num7;
                    Location = new Point(x2, y2);
                }
                else if (UserSettings.Default.startbottom)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X < x1)
                        {
                            Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                        }
                        else
                        {
                            int num8 = Location.X + Size.Width;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num9 = size.Width + num6;
                            if (num8 > num9)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num10 = size.Width + num6;
                                size = Size;
                                int width = size.Width;
                                int x2 = num10 - width;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y2 = bottom - height + num4 + num7;
                                Location = new Point(x2, y2);
                            }
                            else
                            {
                                int x2 = Location.X;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y2 = bottom - height + num4 + num7;
                                Location = new Point(x2, y2);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 2)
                            num4 = 0;
                        Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                    }
                }
                else if (UserSettings.Default.starttopleft)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                }
                else if (UserSettings.Default.starttopright)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                }
                else if (UserSettings.Default.starttopmiddle)
                {
                    if (num5 == 1)
                        num4 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Right / 2 - Size.Width / 2 + num3, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                }
                else if (UserSettings.Default.starttop)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X < x1)
                        {
                            Location = new Point(x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                        }
                        else
                        {
                            int num8 = Location.X + Size.Width;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num9 = size.Width + num6;
                            if (num8 > num9)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num10 = size.Width + num6;
                                size = Size;
                                int width = size.Width;
                                Location = new Point(num10 - width, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                            }
                            else
                                Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                        }
                    }
                    else
                    {
                        if (num5 == 1)
                            num4 = 0;
                        Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                    }
                }
                else if (UserSettings.Default.StartCustomLocation)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        int x2 = UserSettings.Default.startpositionx;
                        int y2 = UserSettings.Default.startpositiony;
                        Size size;
                        if (x2 < x1)
                        {
                            x2 = x1;
                        }
                        else
                        {
                            int num8 = x2 + Size.Width;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num9 = size.Width + num6;
                            if (num8 > num9)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num10 = size.Width + num6;
                                size = Size;
                                int width = size.Width;
                                x2 = num10 - width;
                            }
                        }
                        if (y2 < y1)
                        {
                            y2 = y1;
                        }
                        else
                        {
                            int num8 = y2;
                            size = Size;
                            int height = size.Height;
                            int num9 = num8 + height;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Height + num7;
                            if (num9 > num10)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num11 = size.Height + num7;
                                size = Size;
                                int width = size.Width;
                                y2 = num11 - width;
                            }
                        }
                        Location = new Point(x2, y2);
                    }
                    else
                        Location = new Point(UserSettings.Default.startpositionx, UserSettings.Default.startpositiony);
                }
                else
                {
                    int x2 = Location.X;
                    location = Location;
                    int y2 = location.Y;
                    Size size;
                    if (x2 < x1)
                    {
                        x2 = x1;
                    }
                    else
                    {
                        int num8 = x2 + Size.Width;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num9 = size.Width + num6;
                        if (num8 > num9)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Width + num6;
                            size = Size;
                            int width = size.Width;
                            x2 = num10 - width;
                        }
                    }
                    if (y2 < y1)
                    {
                        y2 = y1;
                    }
                    else
                    {
                        int num8 = y2;
                        size = Size;
                        int height1 = size.Height;
                        int num9 = num8 + height1;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num10 = size.Height + num7;
                        if (num9 > num10)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Height + num7;
                            size = Size;
                            int height2 = size.Height;
                            y2 = num11 - height2;
                        }
                    }
                    Location = new Point(x2, y2);
                }
            }
            else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormEdgeDocking && (DockinW || DockinE || DockinN || DockinS))
            {
                if (DockinW && DockinS)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                }
                else if (DockinE && DockinS)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    int right = Screen.PrimaryScreen.Bounds.Right;
                    Size size = Size;
                    int width = size.Width;
                    int x2 = right - width + num3 + num6;
                    int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                    size = Size;
                    int height = size.Height;
                    int y2 = bottom - height + num4 + num7;
                    Location = new Point(x2, y2);
                }
                else if (DockinW && DockinN)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                }
                else if (DockinE && DockinN)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                }
                else if (DockinE)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y < y1)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Location.Y);
                        }
                        else
                        {
                            int num8 = Location.Y + Size.Height;
                            Size size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                            int num9 = size.Height + num7;
                            if (num8 > num9)
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                int x2 = right - width + num3 + num6;
                                size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                                int height1 = size.Height;
                                size = Size;
                                int height2 = size.Height;
                                int y2 = height1 - height2 + num7;
                                Location = new Point(x2, y2);
                            }
                            else
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                Location = new Point(right - width + num3 + num6, Location.Y);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 3)
                            num3 = 0;
                        int x2 = Screen.PrimaryScreen.Bounds.Right - Size.Width + num3;
                        location = Location;
                        int y2 = location.Y;
                        Location = new Point(x2, y2);
                    }
                }
                else if (DockinW)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y < y1)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Location.Y);
                        }
                        else
                        {
                            int num8 = Location.Y + Size.Height;
                            Size size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                            int num9 = size.Height + num7;
                            if (num8 > num9)
                            {
                                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                                size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                                int height1 = size.Height;
                                size = Size;
                                int height2 = size.Height;
                                int y2 = height1 - height2 + num7;
                                Location = new Point(x2, y2);
                            }
                            else
                            {
                                if (num5 == 3)
                                    num3 = 0;
                                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                                location = Location;
                                int y2 = location.Y;
                                Location = new Point(x2, y2);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 4)
                            num3 = 0;
                        int x2 = Screen.PrimaryScreen.Bounds.Left + num3;
                        location = Location;
                        int y2 = location.Y;
                        Location = new Point(x2, y2);
                    }
                }
                else if (DockinS)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X < x1)
                        {
                            Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                        }
                        else
                        {
                            int num8 = Location.X - Size.Width;
                            Size size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                            int num9 = size.Width + num6;
                            if (num8 > num9)
                            {
                                size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                                int num10 = size.Width + num6;
                                size = Size;
                                int width = size.Width;
                                int x2 = num10 - width;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y2 = bottom - height + num4 + num7;
                                Location = new Point(x2, y2);
                            }
                            else
                            {
                                int x2 = Location.X;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y2 = bottom - height + num4 + num7;
                                Location = new Point(x2, y2);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 2)
                            num4 = 0;
                        Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                    }
                }
                else if (DockinN)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X < x1)
                        {
                            Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                        }
                        else
                        {
                            int num8 = Location.X - Size.Width;
                            Size size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                            int num9 = size.Width + num6;
                            if (num8 > num9)
                            {
                                size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                                int num10 = size.Width + num6;
                                size = Size;
                                int width = size.Width;
                                Location = new Point(num10 - width, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                            }
                            else
                                Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                        }
                    }
                    else
                    {
                        if (num5 == 1)
                            num4 = 0;
                        Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                    }
                }
            }
            else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormCentreDocking && DockinCentre)
            {
                int right = Screen.PrimaryScreen.Bounds.Right;
                Rectangle bounds = Screen.PrimaryScreen.Bounds;
                int num8 = bounds.Right / 2;
                int x2 = right - num8 - Size.Width / 2;
                bounds = Screen.PrimaryScreen.Bounds;
                int bottom = bounds.Bottom;
                bounds = Screen.PrimaryScreen.Bounds;
                int num9 = bounds.Bottom / 2;
                int y2 = bottom - num9 - Size.Height / 2;
                Location = new Point(x2, y2);
            }
            else
            {
                int x2 = Location.X;
                location = Location;
                int y2 = location.Y;
                location = Location;
                Size size;
                if (location.X < x1)
                {
                    x2 = x1;
                }
                else
                {
                    location = Location;
                    int num8 = location.X + Size.Width;
                    size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                    int num9 = size.Width + num6;
                    if (num8 > num9)
                    {
                        size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                        int num10 = size.Width + num6;
                        size = Size;
                        int width = size.Width;
                        x2 = num10 - width;
                    }
                }
                location = Location;
                if (location.Y < y1)
                {
                    y2 = y1;
                }
                else
                {
                    location = Location;
                    int y3 = location.Y;
                    size = Size;
                    int height1 = size.Height;
                    int num8 = y3 + height1;
                    size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                    int num9 = size.Height + num7;
                    if (num8 > num9)
                    {
                        size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                        int num10 = size.Height + num7;
                        size = Size;
                        int height2 = size.Height;
                        y2 = num10 - height2;
                    }
                }
                Location = new Point(x2, y2);
            }
            location = Location;
            storedwindowlocationx = location.X;
            location = Location;
            storedwindowlocationy = location.Y;
        }

        private void Position_Screen_With_Offset(int Forceoffsetx, int Forceoffsety)
        {
            if (initializeparam == 1)
                return;
            APPBARDATA pData = new APPBARDATA();
            int num1 = (int)SHAppBarMessage(5, ref pData);
            int num2 = (int)SHAppBarMessage(4, ref pData);
            int num3 = 0;
            int num4 = 0;
            int num5 = 0;
            int x1 = 0;  //this and below: margins?
            int num6 = 0;
            int num7 = 0;
            int num8 = 0;
            if (UserSettings.Default.CustomTaskbarMargins)
            {
                x1 = UserSettings.Default.CustomTaskbarMarginsLeft;
                num8 = -UserSettings.Default.CustomTaskbarMarginsBottom;
                num7 = UserSettings.Default.CustomTaskbarMarginsTop;
                num6 = -UserSettings.Default.CustomTaskbarMarginsRight;
            }
            else if (pData.rc.bottom == SystemInformation.VirtualScreen.Height && pData.rc.left == 0 && pData.rc.right == SystemInformation.VirtualScreen.Width)
            {
                num5 = 1;
                num4 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
            }
            else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.right == SystemInformation.VirtualScreen.Width)
            {
                num5 = 2;
                num4 = pData.rc.bottom;
            }
            else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.bottom == new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height).Height)
            {
                num5 = 3;
                num3 = pData.rc.right;
            }
            else if (pData.rc.top == 0 && pData.rc.right == new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height).Width && pData.rc.bottom == new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height).Height)
            {
                num5 = 4;
                num3 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
            }
            Point location;
            if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.DockPreview)
            {
                if (UserSettings.Default.startincenter)
                {
                    int right = Screen.PrimaryScreen.Bounds.Right;
                    Rectangle bounds = Screen.PrimaryScreen.Bounds;
                    int num9 = bounds.Right / 2;
                    int x2 = right - num9 - Size.Width / 2;
                    bounds = Screen.PrimaryScreen.Bounds;
                    int bottom = bounds.Bottom;
                    bounds = Screen.PrimaryScreen.Bounds;
                    int num10 = bounds.Bottom / 2;
                    int y = bottom - num10 - Size.Height / 2;
                    Location = new Point(x2, y);
                }
                else if (UserSettings.Default.startbottomleft)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num8);
                }
                else if (UserSettings.Default.startbottomright)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    int right = Screen.PrimaryScreen.Bounds.Right;
                    Size size = Size;
                    int width = size.Width;
                    int x2 = right - width + num3 + num6;
                    int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                    size = Size;
                    int height = size.Height;
                    int y = bottom - height + num4 + num8;
                    Location = new Point(x2, y);
                }
                else if (UserSettings.Default.startright)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y < num7)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, num7 + Forceoffsety);
                        }
                        else
                        {
                            int num9 = Location.Y + Size.Height;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Height + num8;
                            if (num9 > num10)
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                int x2 = right - width + num3 + num6;
                                size = SystemInformation.PrimaryMonitorSize;
                                int num11 = size.Height + num8;
                                size = Size;
                                int height = size.Height;
                                int y = num11 - height + Forceoffsety;
                                Location = new Point(x2, y);
                            }
                            else
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                Location = new Point(right - width + num3 + num6, Location.Y + Forceoffsety);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 3)
                            num3 = 0;
                        Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Location.Y + Forceoffsety);
                    }
                }
                else if (UserSettings.Default.startleft)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y < num7)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, num7 + Forceoffsety);
                        }
                        else
                        {
                            int num9 = Location.Y + Size.Height;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Height + num8;
                            if (num9 > num10)
                            {
                                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                                size = SystemInformation.PrimaryMonitorSize;
                                int num11 = size.Height + num8;
                                size = Size;
                                int height = size.Height;
                                int y = num11 - height + Forceoffsety;
                                Location = new Point(x2, y);
                            }
                            else
                                Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Location.Y + Forceoffsety);
                        }
                    }
                    else
                    {
                        if (num5 == 4)
                            num3 = 0;
                        int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                        location = Location;
                        int y = location.Y + Forceoffsety;
                        Location = new Point(x2, y);
                    }
                }
                else if (UserSettings.Default.startbottommiddle)
                {
                    if (num5 == 2)
                        num4 = 0;
                    int num9 = Screen.PrimaryScreen.Bounds.Right / 2;
                    Size size = Size;
                    int num10 = size.Width / 2;
                    int x2 = num9 - num10 + num3;
                    int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                    size = Size;
                    int height = size.Height;
                    int y = bottom - height + num4 + num8;
                    Location = new Point(x2, y);
                }
                else if (UserSettings.Default.startbottom)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X < x1)
                        {
                            Location = new Point(x1 + Forceoffsetx, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num8);
                        }
                        else
                        {
                            int num9 = Location.X + Size.Width;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Width + num6;
                            if (num9 > num10)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num11 = size.Width + num6;
                                size = Size;
                                int width = size.Width;
                                int x2 = num11 - width + Forceoffsetx;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y = bottom - height + num4 + num8;
                                Location = new Point(x2, y);
                            }
                            else
                            {
                                int x2 = Location.X + Forceoffsetx;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y = bottom - height + num4 + num8;
                                Location = new Point(x2, y);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 2)
                            num4 = 0;
                        Location = new Point(Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num8);
                    }
                }
                else if (UserSettings.Default.starttopleft)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
                }
                else if (UserSettings.Default.starttopright)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
                }
                else if (UserSettings.Default.starttopmiddle)
                {
                    if (num5 == 1)
                        num4 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Right / 2 - Size.Width / 2 + num3, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
                }
                else if (UserSettings.Default.starttop)
                {
                    if (num5 == 1)
                        num4 = 0;
                    int x2 = Location.X + Forceoffsetx;
                    Rectangle bounds = Screen.PrimaryScreen.Bounds;
                    int y1 = bounds.Top + num4;
                    Location = new Point(x2, y1);
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X < x1)
                        {
                            int x3 = x1 + Forceoffsetx;
                            bounds = Screen.PrimaryScreen.Bounds;
                            int y2 = bounds.Top + num4 + num7;
                            Location = new Point(x3, y2);
                        }
                        else
                        {
                            int num9 = Location.X + Size.Width;
                            Size size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Width + num6;
                            if (num9 > num10)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num11 = size.Width + num6;
                                size = Size;
                                int width = size.Width;
                                int x3 = num11 - width + Forceoffsetx;
                                bounds = Screen.PrimaryScreen.Bounds;
                                int y2 = bounds.Top + num4 + num7;
                                Location = new Point(x3, y2);
                            }
                            else
                            {
                                int x3 = Location.X + Forceoffsetx;
                                bounds = Screen.PrimaryScreen.Bounds;
                                int y2 = bounds.Top + num4 + num7;
                                Location = new Point(x3, y2);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 1)
                            num4 = 0;
                        int x3 = Location.X + Forceoffsetx;
                        bounds = Screen.PrimaryScreen.Bounds;
                        int y2 = bounds.Top + num4 + num7;
                        Location = new Point(x3, y2);
                    }
                }
                else if (UserSettings.Default.StartCustomLocation)
                {
                    int x2 = UserSettings.Default.startpositionx + Forceoffsetx;
                    int y = UserSettings.Default.startpositiony + Forceoffsety;
                    Size size;
                    if (x2 < x1)
                    {
                        x2 = x1;
                    }
                    else
                    {
                        int num9 = x2 + Size.Width;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num10 = size.Width + num6;
                        if (num9 > num10)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Width + num6;
                            size = Size;
                            int width = size.Width;
                            x2 = num11 - width;
                        }
                    }
                    if (y < num7)
                    {
                        y = num7;
                    }
                    else
                    {
                        int num9 = y;
                        size = Size;
                        int height1 = size.Height;
                        int num10 = num9 + height1;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num11 = size.Height + num8;
                        if (num10 > num11)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num12 = size.Height + num8;
                            size = Size;
                            int height2 = size.Height;
                            y = num12 - height2;
                        }
                    }
                    Location = new Point(x2, y);
                }
                else
                {
                    int x2 = Location.X + Forceoffsetx;
                    int y = Location.Y + Forceoffsety;
                    Size size;
                    if (x2 < x1)
                    {
                        x2 = x1;
                    }
                    else
                    {
                        int num9 = x2 + Size.Width;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num10 = size.Width + num6;
                        if (num9 > num10)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Width + num6;
                            size = Size;
                            int width = size.Width;
                            x2 = num11 - width;
                        }
                    }
                    if (y < num7)
                    {
                        y = num7;
                    }
                    else
                    {
                        int num9 = y;
                        size = Size;
                        int height1 = size.Height;
                        int num10 = num9 + height1;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num11 = size.Height + num8;
                        if (num10 > num11)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num12 = size.Height + num8;
                            size = Size;
                            int height2 = size.Height;
                            y = num12 - height2;
                        }
                    }
                    Location = new Point(x2, y);
                }
            }
            else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormEdgeDocking && (DockinW || DockinE || DockinN || DockinS))
            {
                if (DockinW && DockinS)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num8);
                }
                else if (DockinE && DockinS)
                {
                    if (num5 == 2)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    int right = Screen.PrimaryScreen.Bounds.Right;
                    Size size = Size;
                    int width = size.Width;
                    int x2 = right - width + num3 + num6;
                    int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                    size = Size;
                    int height = size.Height;
                    int y = bottom - height + num4 + num8;
                    Location = new Point(x2, y);
                }
                else if (DockinW && DockinN)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 4)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
                }
                else if (DockinE && DockinN)
                {
                    if (num5 == 1)
                        num4 = 0;
                    if (num5 == 3)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
                }
                else if (DockinE)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y + Forceoffsety < num7)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Location.Y + Forceoffsety);
                        }
                        else
                        {
                            int num9 = Location.Y + Forceoffsety;
                            Size size = Size;
                            int height1 = size.Height;
                            int num10 = num9 + height1;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Height + num8;
                            if (num10 > num11)
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                int x2 = right - width + num3 + num6;
                                size = SystemInformation.PrimaryMonitorSize;
                                int height2 = size.Height;
                                size = Size;
                                int height3 = size.Height;
                                int y = height2 - height3 + num8;
                                Location = new Point(x2, y);
                            }
                            else
                            {
                                int right = Screen.PrimaryScreen.Bounds.Right;
                                size = Size;
                                int width = size.Width;
                                Location = new Point(right - width + num6 + num3, Location.Y + Forceoffsety);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 3)
                            num3 = 0;
                        int x2 = Screen.PrimaryScreen.Bounds.Right - Size.Width + num3;
                        location = Location;
                        int y = location.Y + Forceoffsety;
                        Location = new Point(x2, y);
                    }
                }
                else if (DockinW)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.Y + Forceoffsety < num7)
                        {
                            Location = new Point(Screen.PrimaryScreen.Bounds.Left + x1 + num3, Location.Y + Forceoffsety);
                        }
                        else
                        {
                            int num9 = Location.Y + Forceoffsety;
                            Size size = Size;
                            int height1 = size.Height;
                            int num10 = num9 + height1;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Height + num8;
                            if (num10 > num11)
                            {
                                int x2 = Screen.PrimaryScreen.Bounds.Left + x1 + num3;
                                size = SystemInformation.PrimaryMonitorSize;
                                int height2 = size.Height;
                                size = Size;
                                int height3 = size.Height;
                                int y = height2 - height3 + num8;
                                Location = new Point(x2, y);
                            }
                            else
                            {
                                if (num5 == 3)
                                    num3 = 0;
                                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                                location = Location;
                                int y = location.Y + Forceoffsety;
                                Location = new Point(x2, y);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 4)
                            num3 = 0;
                        int x2 = Screen.PrimaryScreen.Bounds.Left + num3;
                        location = Location;
                        int y = location.Y + Forceoffsety;
                        Location = new Point(x2, y);
                    }
                }
                else if (DockinS)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X + Forceoffsetx < x1)
                        {
                            Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num8 + num4);
                        }
                        else
                        {
                            int num9 = Location.X + Forceoffsetx;
                            Size size = Size;
                            int width1 = size.Width;
                            int num10 = num9 - width1;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Width + num6;
                            if (num10 > num11)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num12 = size.Width + num6;
                                size = Size;
                                int width2 = size.Width;
                                int x2 = num12 - width2;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y = bottom - height + num8 + num4;
                                Location = new Point(x2, y);
                            }
                            else
                            {
                                int x2 = Location.X + Forceoffsetx;
                                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                                size = Size;
                                int height = size.Height;
                                int y = bottom - height + num8 + num4;
                                Location = new Point(x2, y);
                            }
                        }
                    }
                    else
                    {
                        if (num5 == 2)
                            num4 = 0;
                        Location = new Point(Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4);
                    }
                }
                else if (DockinN)
                {
                    if (UserSettings.Default.CustomTaskbarMargins)
                    {
                        if (Location.X + Forceoffsetx < x1)
                        {
                            Location = new Point(Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Top + num7 + num4);
                        }
                        else
                        {
                            int num9 = Location.X + Forceoffsetx;
                            Size size = Size;
                            int width1 = size.Width;
                            int num10 = num9 - width1;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Width + num6;
                            if (num10 > num11)
                            {
                                size = SystemInformation.PrimaryMonitorSize;
                                int num12 = size.Width + num6;
                                size = Size;
                                int width2 = size.Width;
                                Location = new Point(num12 - width2, Screen.PrimaryScreen.Bounds.Top + num7 + num4);
                            }
                            else
                                Location = new Point(Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Top + num7 + num4);
                        }
                    }
                    else
                    {
                        if (num5 == 1)
                            num4 = 0;
                        Location = new Point(Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Top + num4);
                    }
                }
            }
            else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormCentreDocking && DockinCentre)
            {
                int right = Screen.PrimaryScreen.Bounds.Right;
                Rectangle bounds = Screen.PrimaryScreen.Bounds;
                int num9 = bounds.Right / 2;
                int x2 = right - num9 - Size.Width / 2;
                bounds = Screen.PrimaryScreen.Bounds;
                int bottom = bounds.Bottom;
                bounds = Screen.PrimaryScreen.Bounds;
                int num10 = bounds.Bottom / 2;
                int y = bottom - num10 - Size.Height / 2;
                Location = new Point(x2, y);
            }
            else
            {
                int x2 = Location.X + Forceoffsetx;
                int y = Location.Y + Forceoffsety;
                Size size;
                if (x2 < x1)
                {
                    x2 = x1;
                }
                else
                {
                    int num9 = x2 + Size.Width;
                    size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                    int num10 = size.Width + num6;
                    if (num9 > num10)
                    {
                        size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                        int num11 = size.Width + num6;
                        size = Size;
                        int width = size.Width;
                        x2 = num11 - width;
                    }
                }
                if (y < num7)
                {
                    y = num7;
                }
                else
                {
                    int num9 = y;
                    size = Size;
                    int height1 = size.Height;
                    int num10 = num9 + height1;
                    size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                    int num11 = size.Height + num8;
                    if (num10 > num11)
                    {
                        size = new Size(SystemInformation.VirtualScreen.Width, SystemInformation.VirtualScreen.Height);
                        int num12 = size.Height + num8;
                        size = Size;
                        int height2 = size.Height;
                        y = num12 - height2;
                    }
                }
                Location = new Point(x2, y);
            }
            location = Location;
            storedwindowlocationx = location.X;
            location = Location;
            storedwindowlocationy = location.Y;
        }

        public void Position_Screen_Start()
        {
            if (initializeparam == 1)
                return;
            APPBARDATA pData = new APPBARDATA();
            int num1 = (int)SHAppBarMessage(5, ref pData);
            int num2 = (int)SHAppBarMessage(4, ref pData);
            int num3 = 0;
            int num4 = 0;
            int num5 = 0;
            int x1 = 0;
            int num6 = 0;
            int y1 = 0;
            int num7 = 0;
            if (UserSettings.Default.CustomTaskbarMargins)
            {
                x1 = UserSettings.Default.CustomTaskbarMarginsLeft;
                num7 = -UserSettings.Default.CustomTaskbarMarginsBottom;
                y1 = UserSettings.Default.CustomTaskbarMarginsTop;
                num6 = -UserSettings.Default.CustomTaskbarMarginsRight;
            }
            else if (pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
            {
                num5 = 1;
                num4 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
            }
            else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
            {
                num5 = 2;
                num4 = pData.rc.bottom;
            }
            else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
            {
                num5 = 3;
                num3 = pData.rc.right;
            }
            else if (pData.rc.top == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
            {
                num5 = 4;
                num3 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
            }
            Point location;
            if (UserSettings.Default.startincenter)
            {
                int right = Screen.PrimaryScreen.Bounds.Right;
                Rectangle bounds = Screen.PrimaryScreen.Bounds;
                int num8 = bounds.Right / 2;
                int x2 = right - num8 - Size.Width / 2;
                bounds = Screen.PrimaryScreen.Bounds;
                int bottom = bounds.Bottom;
                bounds = Screen.PrimaryScreen.Bounds;
                int num9 = bounds.Bottom / 2;
                int y2 = bottom - num9 - Size.Height / 2;
                Location = new Point(x2, y2);
            }
            else if (UserSettings.Default.startbottomleft)
            {
                if (num5 == 2)
                    num4 = 0;
                if (num5 == 4)
                    num3 = 0;
                Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
            }
            else if (UserSettings.Default.startbottomright)
            {
                if (num5 == 2)
                    num4 = 0;
                if (num5 == 3)
                    num3 = 0;
                int right = Screen.PrimaryScreen.Bounds.Right;
                Size size = Size;
                int width = size.Width;
                int x2 = right - width + num3 + num6;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = Size;
                int height = size.Height;
                int y2 = bottom - height + num4 + num7;
                Location = new Point(x2, y2);
            }
            else if (UserSettings.Default.startright)
            {
                if (UserSettings.Default.CustomTaskbarMargins)
                {
                    if (Location.Y < y1)
                    {
                        Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, y1);
                    }
                    else
                    {
                        int num8 = Location.Y + Size.Height;
                        Size size = SystemInformation.PrimaryMonitorSize;
                        int num9 = size.Height + num7;
                        if (num8 > num9)
                        {
                            int right = Screen.PrimaryScreen.Bounds.Right;
                            size = Size;
                            int width = size.Width;
                            int x2 = right - width + num3 + num6;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Height + num7;
                            size = Size;
                            int height = size.Height;
                            int y2 = num10 - height;
                            Location = new Point(x2, y2);
                        }
                        else
                        {
                            int right = Screen.PrimaryScreen.Bounds.Right;
                            size = Size;
                            int width = size.Width;
                            Location = new Point(right - width + num3 + num6, Location.Y);
                        }
                    }
                }
                else
                {
                    if (num5 == 3)
                        num3 = 0;
                    Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Location.Y);
                }
            }
            else if (UserSettings.Default.startleft)
            {
                if (UserSettings.Default.CustomTaskbarMargins)
                {
                    if (Location.Y < y1)
                    {
                        Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, y1);
                    }
                    else
                    {
                        int num8 = Location.Y + Size.Height;
                        Size size = SystemInformation.PrimaryMonitorSize;
                        int num9 = size.Height + num7;
                        if (num8 > num9)
                        {
                            int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Height + num7;
                            size = Size;
                            int height = size.Height;
                            int y2 = num10 - height;
                            Location = new Point(x2, y2);
                        }
                        else
                            Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Location.Y);
                    }
                }
                else
                {
                    if (num5 == 4)
                        num3 = 0;
                    int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                    location = Location;
                    int y2 = location.Y;
                    Location = new Point(x2, y2);
                }
            }
            else if (UserSettings.Default.startbottommiddle)
            {
                if (num5 == 2)
                    num4 = 0;
                int num8 = Screen.PrimaryScreen.Bounds.Right / 2;
                Size size = Size;
                int num9 = size.Width / 2;
                int x2 = num8 - num9 + num3;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = Size;
                int height = size.Height;
                int y2 = bottom - height + num4 + num7;
                Location = new Point(x2, y2);
            }
            else if (UserSettings.Default.startbottom)
            {
                if (UserSettings.Default.CustomTaskbarMargins)
                {
                    if (Location.X < x1)
                    {
                        Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                    }
                    else
                    {
                        int num8 = Location.X + Size.Width;
                        Size size = SystemInformation.PrimaryMonitorSize;
                        int num9 = size.Width + num6;
                        if (num8 > num9)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Width + num6;
                            size = Size;
                            int width = size.Width;
                            int x2 = num10 - width;
                            int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                            size = Size;
                            int height = size.Height;
                            int y2 = bottom - height + num4 + num7;
                            Location = new Point(x2, y2);
                        }
                        else
                        {
                            int x2 = Location.X;
                            int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                            size = Size;
                            int height = size.Height;
                            int y2 = bottom - height + num4 + num7;
                            Location = new Point(x2, y2);
                        }
                    }
                }
                else
                {
                    if (num5 == 2)
                        num4 = 0;
                    Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Bottom - Size.Height + num4 + num7);
                }
            }
            else if (UserSettings.Default.starttopleft)
            {
                if (num5 == 1)
                    num4 = 0;
                if (num5 == 4)
                    num3 = 0;
                Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
            else if (UserSettings.Default.starttopright)
            {
                if (num5 == 1)
                    num4 = 0;
                if (num5 == 3)
                    num3 = 0;
                Location = new Point(Screen.PrimaryScreen.Bounds.Right - Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
            else if (UserSettings.Default.starttopmiddle)
            {
                if (num5 == 1)
                    num4 = 0;
                Location = new Point(Screen.PrimaryScreen.Bounds.Right / 2 - Size.Width / 2 + num3, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
            else if (UserSettings.Default.starttop)
            {
                if (UserSettings.Default.CustomTaskbarMargins)
                {
                    if (Location.X < x1)
                    {
                        Location = new Point(x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                    }
                    else
                    {
                        int num8 = Location.X + Size.Width;
                        Size size = SystemInformation.PrimaryMonitorSize;
                        int num9 = size.Width + num6;
                        if (num8 > num9)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Width + num6;
                            size = Size;
                            int width = size.Width;
                            Location = new Point(num10 - width, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                        }
                        else
                            Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                    }
                }
                else
                {
                    if (num5 == 1)
                        num4 = 0;
                    Location = new Point(Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
                }
            }
            else if (UserSettings.Default.StartCustomLocation)
            {
                if (UserSettings.Default.CustomTaskbarMargins)
                {
                    int x2 = UserSettings.Default.startpositionx;
                    int y2 = UserSettings.Default.startpositiony;
                    location = Location;
                    Size size;
                    if (location.X < x1)
                    {
                        x2 = x1;
                    }
                    else
                    {
                        location = Location;
                        int num8 = location.X + Size.Width;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num9 = size.Width + num6;
                        if (num8 > num9)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Width + num6;
                            size = Size;
                            int width = size.Width;
                            x2 = num10 - width;
                        }
                    }
                    location = Location;
                    if (location.Y < y1)
                    {
                        y2 = y1;
                    }
                    else
                    {
                        location = Location;
                        int y3 = location.Y;
                        size = Size;
                        int height = size.Height;
                        int num8 = y3 + height;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num9 = size.Height + num7;
                        if (num8 > num9)
                        {
                            size = SystemInformation.PrimaryMonitorSize;
                            int num10 = size.Height + num7;
                            size = Size;
                            int width = size.Width;
                            y2 = num10 - width;
                        }
                    }
                    Location = new Point(x2, y2);
                }
                else
                    Location = new Point(UserSettings.Default.startpositionx, UserSettings.Default.startpositiony);
            }
            else
            {
                int x2 = Location.X;
                location = Location;
                int y2 = location.Y;
                location = Location;
                Size size;
                if (location.X < x1)
                {
                    x2 = x1;
                }
                else
                {
                    location = Location;
                    int num8 = location.X + Size.Width;
                    size = SystemInformation.PrimaryMonitorSize;
                    int num9 = size.Width + num6;
                    if (num8 > num9)
                    {
                        size = SystemInformation.PrimaryMonitorSize;
                        int num10 = size.Width + num6;
                        size = Size;
                        int width = size.Width;
                        x2 = num10 - width;
                    }
                }
                location = Location;
                if (location.Y < y1)
                {
                    y2 = y1;
                }
                else
                {
                    location = Location;
                    int y3 = location.Y;
                    size = Size;
                    int height1 = size.Height;
                    int num8 = y3 + height1;
                    size = SystemInformation.PrimaryMonitorSize;
                    int num9 = size.Height + num7;
                    if (num8 > num9)
                    {
                        size = SystemInformation.PrimaryMonitorSize;
                        int num10 = size.Height + num7;
                        size = Size;
                        int height2 = size.Height;
                        y2 = num10 - height2;
                    }
                }
                Location = new Point(x2, y2);
            }
            location = Location;
            storedwindowlocationx = location.X;
            location = Location;
            storedwindowlocationy = location.Y;
        }

        private void GetWindows()
        {
            windows.Clear();
            EnumWindows(Callback, 0);
            if (!UserSettings.Default.ShowWindowListInContextMenu)
                return;
            int num = 0;
            foreach (Window window in windows)
            {
                ++num;
                contextMenuStrip1.Items.Add(num + ".  " + window);
            }
        }

        private bool Callback(IntPtr hwnd, int lParam)
        {
            if (Handle != hwnd && ((long)GetWindowLongA(hwnd, GWL_STYLE) & (long)TARGETWINDOW) == (long)TARGETWINDOW)
            {
                StringBuilder lpString = new StringBuilder(100);
                GetWindowText(hwnd, lpString, lpString.Capacity);
                windows.Add(new Window
                {
                    Handle = hwnd,
                    Title = lpString.ToString()
                });
            }
            return true;
        }

        public void SizeWindow(int state)
        {
            if (triploop || minimizing || initializeparam == 1)
                return;
            FindFrameBorderSize(ref FrameBorderSizeWidth, ref FrameBorderSizeHeight);
            PSIZE size1;
            DwmQueryThumbnailSourceSize(thumb, out size1);
            DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
            props.fVisible = true;
            props.dwFlags = DWM_TNP_VISIBLE | DWM_TNP_RECTDESTINATION | DWM_TNP_OPACITY | DWM_TNP_RECTSOURCE;
            props.opacity = (byte)(byte.MaxValue - UserSettings.Default.ThumbTransparency);
            props.rcSource = new Rect(-FrameBorderSizeWidth, -FrameBorderSizeHeight, size1.x + FrameBorderSizeWidth, size1.y + FrameBorderSizeHeight);
            currentwindow = thumb.ToString();
            int index = 0;
            foreach (Window window in windows)
            {
                if (currentwindowhandle == windows[index].Handle)
                {
                    currentwindowname = window.Title;
                    break;
                }
                ++index;
            }
            if (state == 1)
            {
                ResetSettings();
                if (UseCacheParams)
                {
                    LoadPreviewCacheParams();
                    TargetShown = true;
                    hideTargetWindowToolStripMenuItem_Click(this, null);
                    croppedratioxleft = cropleftoffset / (double)xsizethumb;
                    croppedratioytop = croptopoffset / (double)ysizethumb;
                    croppedratioxright = croprightoffset / (double)xsizethumb;
                    croppedratioybottom = cropbottomoffset / (double)ysizethumb;
                    xsizeform = xsizethumb + TotalBorderThicknessX();
                    ysizeform = ysizethumb + TotalBorderThicknessY();
                    ResizeFormP(xsizeform, ysizeform);
                    props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                    DwmUpdateThumbnailProperties(thumb, ref props);
                    PreviousXSizeThumb = xsizethumb;
                    PreviousYSizeThumb = ysizethumb;
                    PreviousXImageSize = image.Size.Width;
                    Size size2 = image.Size;
                    PreviousYImageSize = size2.Height;
                    size2 = Size;
                    PreviousXFormSize = size2.Width;
                    size2 = Size;
                    PreviousYFormSize = size2.Height;
                    Point location = Location;
                    storedwindowlocationx = location.X;
                    location = Location;
                    storedwindowlocationy = location.Y;
                }
                else
                {
                    TargetShown = true;
                    hideTargetWindowToolStripMenuItem_Click(this, null);
                    if (!UserSettings.Default.MoveTargetOffscreen)
                        ;
                    aspectratio = size1.x / (double)size1.y;
                    xsizethumb = (int)(size1.x * UserSettings.Default.scalingfactor);
                    ysizethumb = (int)(size1.y * UserSettings.Default.scalingfactor);
                    if (xsizethumb < UserSettings.Default.minpixelsx || ysizethumb < UserSettings.Default.minpixelsy)
                    {
                        xsizethumb = (int)(UserSettings.Default.minpixelsy * aspectratio);
                        ysizethumb = UserSettings.Default.minpixelsy;
                    }
                    else if (xsizethumb > UserSettings.Default.maxpixelsx || ysizethumb > UserSettings.Default.maxpixelsy)
                    {
                        xsizethumb = (int)(UserSettings.Default.maxpixelsy * aspectratio);
                        ysizethumb = UserSettings.Default.maxpixelsy;
                    }
                    props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                    DwmUpdateThumbnailProperties(thumb, ref props);
                    xsizeform = xsizethumb + TotalBorderThicknessX();
                    ysizeform = ysizethumb + TotalBorderThicknessY();
                    ResizeFormP(xsizeform, ysizeform);
                    PreviousXSizeThumb = xsizethumb;
                    PreviousYSizeThumb = ysizethumb;
                    Size size2 = image.Size;
                    PreviousXImageSize = size2.Width;
                    size2 = image.Size;
                    PreviousYImageSize = size2.Height;
                    size2 = Size;
                    PreviousXFormSize = size2.Width;
                    size2 = Size;
                    PreviousYFormSize = size2.Height;
                    Point location = Location;
                    storedwindowlocationx = location.X;
                    location = Location;
                    storedwindowlocationy = location.Y;
                }
            }
            else if (state == 2)
            {
                cropleftoffset = (int)(PreviousXSizeThumb * croppedratioxleft);
                croptopoffset = (int)(PreviousYSizeThumb * croppedratioytop);
                croprightoffset = (int)(PreviousXSizeThumb * croppedratioxright);
                cropbottomoffset = (int)(PreviousYSizeThumb * croppedratioybottom);
                ResizeFormP(PreviousXFormSize, PreviousYFormSize);
                xsizethumb = image.Size.Width;
                Size size2 = image.Size;
                ysizethumb = size2.Height;
                if (!isbrowsermode)
                {
                    props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                    DwmUpdateThumbnailProperties(thumb, ref props);
                }
                storedwindowlocationx = Location.X;
                storedwindowlocationy = Location.Y;
                PreviousXSizeThumb = xsizethumb;
                PreviousYSizeThumb = ysizethumb;
                size2 = image.Size;
                PreviousXImageSize = size2.Width;
                size2 = image.Size;
                PreviousYImageSize = size2.Height;
                size2 = Size;
                PreviousXFormSize = size2.Width;
                size2 = Size;
                PreviousYFormSize = size2.Height;
            }
            else if (state == 3)
            {
                double num1 = image.Size.Height * aspectratio * resizeratio;
                Size size2 = image.Size;
                double num2 = size2.Height * resizeratio;
                xsizethumb = (int)num1;
                ysizethumb = (int)num2;
                if (ysizethumb == PreviousYSizeThumb && resizeratio > 1.0)
                {
                    size2 = image.Size;
                    num1 = (size2.Height + 1.0) * aspectratio * resizeratio;
                    size2 = image.Size;
                    num2 = (size2.Height + 1.0) * resizeratio;
                    xsizethumb = (int)num1;
                    ysizethumb = (int)num2;
                }
                cropleftoffset = (int)(num1 * croppedratioxleft);
                croptopoffset = (int)(num2 * croppedratioytop);
                croprightoffset = (int)(num1 * croppedratioxright);
                cropbottomoffset = (int)(num2 * croppedratioybottom);
                xsizeform = xsizethumb + TotalBorderThicknessX();
                ysizeform = ysizethumb + TotalBorderThicknessY();
                ResizeFormP(xsizeform, ysizeform);
                if (!isbrowsermode)
                {
                    props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                    DwmUpdateThumbnailProperties(thumb, ref props);
                }
                storedwindowlocationx = Location.X;
                storedwindowlocationy = Location.Y;
                PreviousXSizeThumb = xsizethumb;
                PreviousYSizeThumb = ysizethumb;
                size2 = image.Size;
                PreviousXImageSize = size2.Width;
                size2 = image.Size;
                PreviousYImageSize = size2.Height;
                size2 = Size;
                PreviousXFormSize = size2.Width;
                size2 = Size;
                PreviousYFormSize = size2.Height;
            }
            else if (state == 4)
            {
                croppedratioxleft = croppedratioxleft * resizeratio;
                croppedratioytop = croppedratioytop * resizeratio;
                croppedratioxright = croppedratioxright * resizeratio;
                croppedratioybottom = croppedratioybottom * resizeratio;
                cropleftoffset = (int)(xsizethumb * croppedratioxleft);
                croptopoffset = (int)(ysizethumb * croppedratioytop);
                croprightoffset = (int)(xsizethumb * croppedratioxright);
                cropbottomoffset = (int)(ysizethumb * croppedratioybottom);
                props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                DwmUpdateThumbnailProperties(thumb, ref props);
            }
            else
            {
                if (state != 5)
                    return;
                resizeratio = SystemInformation.PrimaryMonitorMaximizedWindowSize.Height / (double)image.Size.Height;
                xsizethumb = (int)(image.Size.Height * aspectratio * resizeratio);
                ysizethumb = (int)(image.Size.Height * resizeratio);
                cropleftoffset = (int)(PreviousXSizeThumb * croppedratioxleft * resizeratio);
                croptopoffset = (int)(PreviousYSizeThumb * croppedratioytop * resizeratio);
                croprightoffset = (int)(PreviousXSizeThumb * croppedratioxright * resizeratio);
                cropbottomoffset = (int)(PreviousYSizeThumb * croppedratioybottom * resizeratio);
                Size size2;
                if (ysizethumb == PreviousYSizeThumb && resizeratio > 1.0)
                {
                    size2 = image.Size;
                    xsizethumb = (int)((size2.Height + 1.0) * aspectratio * resizeratio);
                    size2 = image.Size;
                    ysizethumb = (int)((size2.Height + 1.0) * resizeratio);
                }
                xsizeform = xsizethumb + TotalBorderThicknessX();
                ysizeform = ysizethumb + TotalBorderThicknessY();
                ResizeFormP(xsizeform, ysizeform);
                if (!isbrowsermode)
                {
                    props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                    DwmUpdateThumbnailProperties(thumb, ref props);
                }
                storedwindowlocationx = Location.X;
                storedwindowlocationy = Location.Y;
                PreviousXSizeThumb = xsizethumb;
                PreviousYSizeThumb = ysizethumb;
                size2 = image.Size;
                PreviousXImageSize = size2.Width;
                size2 = image.Size;
                PreviousYImageSize = size2.Height;
                size2 = Size;
                PreviousXFormSize = size2.Width;
                size2 = Size;
                PreviousYFormSize = size2.Height;
                Location = new Point(0, 0);
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (!controlkeyisdown)
                return;
            FindFrameBorderSize(ref FrameBorderSizeWidth, ref FrameBorderSizeHeight);
            PSIZE size1;
            DwmQueryThumbnailSourceSize(thumb, out size1);
            DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
            props.fVisible = true;
            props.dwFlags = DWM_TNP_VISIBLE | DWM_TNP_RECTDESTINATION | DWM_TNP_OPACITY | DWM_TNP_RECTSOURCE;
            props.opacity = (byte)(byte.MaxValue - UserSettings.Default.ThumbTransparency);
            props.rcSource = new Rect(-FrameBorderSizeWidth, -FrameBorderSizeHeight, size1.x + FrameBorderSizeWidth, size1.y + FrameBorderSizeHeight);
            currentwindow = thumb.ToString();
            Point location;
            Size size2;
            if (Location.X > storedwindowlocationx)
            {
                int num1 = cropleftoffset;
                location = Location;
                int num2 = location.X - storedwindowlocationx;
                cropleftoffset = num1 - num2;
                int num3 = xsizethumb;
                location = Location;
                int num4 = location.X - storedwindowlocationx;
                xsizethumb = num3 - num4;
                croppedratioxleft = cropleftoffset / (double)xsizethumb;
            }
            else if (Location.X < storedwindowlocationx)
            {
                int num1 = cropleftoffset;
                location = Location;
                int num2 = location.X - storedwindowlocationx;
                cropleftoffset = num1 - num2;
                int num3 = xsizethumb;
                location = Location;
                int num4 = location.X - storedwindowlocationx;
                xsizethumb = num3 - num4;
                croppedratioxleft = cropleftoffset / (double)xsizethumb;
            }
            else if (Size.Width > PreviousXFormSize)
            {
                int num1 = croprightoffset;
                size2 = Size;
                int num2 = size2.Width - PreviousXFormSize;
                croprightoffset = num1 - num2;
                int num3 = xsizethumb;
                size2 = Size;
                int num4 = size2.Width - PreviousXFormSize;
                xsizethumb = num3 + num4;
                croppedratioxright = croprightoffset / (double)xsizethumb;
            }
            else if (Size.Width < PreviousXFormSize)
            {
                int num1 = croprightoffset;
                size2 = Size;
                int num2 = size2.Width - PreviousXFormSize;
                croprightoffset = num1 - num2;
                int num3 = xsizethumb;
                size2 = Size;
                int num4 = size2.Width - PreviousXFormSize;
                xsizethumb = num3 + num4;
                croppedratioxright = croprightoffset / (double)xsizethumb;
            }
            location = Location;
            if (location.Y > storedwindowlocationy)
            {
                int num1 = croptopoffset;
                location = Location;
                int num2 = location.Y - storedwindowlocationy;
                croptopoffset = num1 - num2;
                int num3 = ysizethumb;
                location = Location;
                int num4 = location.Y - storedwindowlocationy;
                ysizethumb = num3 - num4;
                croppedratioytop = croptopoffset / (double)ysizethumb;
            }
            else
            {
                location = Location;
                if (location.Y < storedwindowlocationy)
                {
                    int num1 = croptopoffset;
                    location = Location;
                    int num2 = location.Y - storedwindowlocationy;
                    croptopoffset = num1 - num2;
                    int num3 = ysizethumb;
                    location = Location;
                    int num4 = location.Y - storedwindowlocationy;
                    ysizethumb = num3 - num4;
                    croppedratioytop = croptopoffset / (double)ysizethumb;
                }
                else
                {
                    size2 = Size;
                    if (size2.Height > PreviousYFormSize)
                    {
                        int num1 = cropbottomoffset;
                        size2 = Size;
                        int num2 = size2.Height - PreviousYFormSize;
                        cropbottomoffset = num1 - num2;
                        int num3 = ysizethumb;
                        size2 = Size;
                        int num4 = size2.Height - PreviousYFormSize;
                        ysizethumb = num3 + num4;
                        croppedratioybottom = cropbottomoffset / (double)ysizethumb;
                    }
                    else
                    {
                        size2 = Size;
                        if (size2.Height < PreviousYFormSize)
                        {
                            int num1 = cropbottomoffset;
                            size2 = Size;
                            int num2 = size2.Height - PreviousYFormSize;
                            cropbottomoffset = num1 - num2;
                            int num3 = ysizethumb;
                            int num4 = PreviousYFormSize;
                            size2 = Size;
                            int height = size2.Height;
                            int num5 = num4 - height;
                            ysizethumb = num3 - num5;
                            croppedratioybottom = cropbottomoffset / (double)ysizethumb;
                        }
                    }
                }
            }
            croppedratioxleft = cropleftoffset / (double)xsizethumb;
            croppedratioxright = croprightoffset / (double)xsizethumb;
            croppedratioytop = croptopoffset / (double)ysizethumb;
            croppedratioybottom = cropbottomoffset / (double)ysizethumb;
            if (!isbrowsermode)
            {
                props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                DwmUpdateThumbnailProperties(thumb, ref props);
            }
            aspectratio = image.Width / (double)image.Height;
            location = Location;
            storedwindowlocationx = location.X;
            location = Location;
            storedwindowlocationy = location.Y;
            PreviousXSizeThumb = xsizethumb;
            PreviousYSizeThumb = ysizethumb;
            size2 = image.Size;
            PreviousXImageSize = size2.Width;
            size2 = image.Size;
            PreviousYImageSize = size2.Height;
            size2 = Size;
            PreviousXFormSize = size2.Width;
            size2 = Size;
            PreviousYFormSize = size2.Height;
        }

        public void ResizeFormP(int NewX, int NewY)
        {
            if (triploop || minimizing || isbrowsermode || initializeparam == 1)
                Console.WriteLine("trip loop");
            if (controlkeyisdown)
                return;
            FindFrameBorderSize(ref FrameBorderSizeWidth, ref FrameBorderSizeHeight);
            PSIZE size1;
            DwmQueryThumbnailSourceSize(thumb, out size1);
            DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
            props.fVisible = true;
            props.dwFlags = DWM_TNP_VISIBLE | DWM_TNP_RECTDESTINATION | DWM_TNP_OPACITY | DWM_TNP_RECTSOURCE;
            props.opacity = (byte)(byte.MaxValue - UserSettings.Default.ThumbTransparency);
            props.rcSource = new Rect(-FrameBorderSizeWidth, -FrameBorderSizeHeight, size1.x + FrameBorderSizeWidth, size1.y + FrameBorderSizeHeight);
            currentwindow = thumb.ToString();
            xsizethumb = (int)(NewY * aspectratio);
            ysizethumb = NewY;
            cropleftoffset = (int)(xsizethumb * croppedratioxleft);
            croptopoffset = (int)(ysizethumb * croppedratioytop);
            croprightoffset = (int)(xsizethumb * croppedratioxright);
            cropbottomoffset = (int)(ysizethumb * croppedratioybottom);
            if (!isbrowsermode)
            {
                props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
                DwmUpdateThumbnailProperties(thumb, ref props);
            }
            int width = xsizethumb + TotalBorderThicknessX();
            int height = ysizethumb + TotalBorderThicknessY();
            Size size2 = Size;
            int num;
            if (size2.Width == width)
            {
                size2 = Size;
                num = size2.Height == height ? 1 : 0;
            }
            else
                num = 0;
            if (num == 0)
                Size = new Size(width, height);
            storedwindowlocationx = Location.X;
            storedwindowlocationy = Location.Y;
            PreviousXSizeThumb = xsizethumb;
            PreviousYSizeThumb = ysizethumb;
            size2 = image.Size;
            PreviousXImageSize = size2.Width;
            size2 = image.Size;
            PreviousYImageSize = size2.Height;
            size2 = Size;
            PreviousXFormSize = size2.Width;
            size2 = Size;
            PreviousYFormSize = size2.Height;
        }

        public int TotalBorderThicknessX()
        {
            if (FormBorderStyle.ToString() == "None")
                return 0;
            return 2 * SystemInformation.FrameBorderSize.Width;
        }

        public int TotalBorderThicknessY()
        {
            if (FormBorderStyle.ToString() == "None")
                return 0;
            return 2 * SystemInformation.FrameBorderSize.Height;
        }

        public void ChangeTransparency(int thumbtransparencyvalue, int backtransparencyvalue)
        {
            FindFrameBorderSize(ref FrameBorderSizeWidth, ref FrameBorderSizeHeight);
            PSIZE size;
            DwmQueryThumbnailSourceSize(thumb, out size);
            DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
            props.fVisible = true;
            props.dwFlags = DWM_TNP_VISIBLE | DWM_TNP_RECTDESTINATION | DWM_TNP_OPACITY | DWM_TNP_RECTSOURCE;
            props.opacity = (byte)(byte.MaxValue - thumbtransparencyvalue);
            props.rcSource = new Rect(-FrameBorderSizeWidth, -FrameBorderSizeHeight, size.x + FrameBorderSizeWidth, size.y + FrameBorderSizeHeight);
            SetLayeredWindowAttributes(Handle, 0, (byte)(byte.MaxValue - backtransparencyvalue), 2);
            props.rcDestination = new Rect(cropleftoffset, croptopoffset, xsizethumb + croprightoffset, ysizethumb + cropbottomoffset);
            DwmUpdateThumbnailProperties(thumb, ref props);
        }

        public void ToggleBackTitles(bool TurnOn)
        {
            if (TurnOn)
            {
                TitleLabel.Visible = true;
                AuthorLabel.Visible = true;
            }
            else
            {
                TitleLabel.Visible = false;
                AuthorLabel.Visible = false;
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            contextMenuStrip1.Items.Clear();
            contextMenuStrip1.Items.Add(showTargetWindowToolStripMenuItem);
            contextMenuStrip1.Items.Add(hideTargetWindowToolStripMenuItem);
            contextMenuStrip1.Items.Add(minimizeToolStripMenuItem);
            contextMenuStrip1.Items.Add(alwaysOnTopNoMoveToolStripMenuItem);
            alwaysOnTopNoMoveToolStripMenuItem.Checked = (bool)qparent.AlwaysOnTop[qparent.TargettedWindows.IndexOf(currentwindowhandle)];
            contextMenuStrip1.Visible = true;
            contextMenuStrip1.Show();
            contextMenuStrip1.BringToFront();
        }

        public void alwaysOnTopNoMoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int index = qparent.TargettedWindows.IndexOf(currentwindowhandle);
            if (index < 0)
                return;
            if (alwaysOnTopNoMoveToolStripMenuItem.Checked)
            {
                qparent.AlwaysOnTop[index] = true;
                qparent.NoMove[index] = true;
                ThumbnailWindow thumbnailWindow = (ThumbnailWindow)qparent.PreviewControls[index];
                thumbnailWindow.TopMost = true;
                thumbnailWindow.BringToFront();
            }
            else
            {
                qparent.AlwaysOnTop[index] = false;
                qparent.NoMove[index] = false;
                ThumbnailWindow thumbnailWindow = (ThumbnailWindow)qparent.PreviewControls[index];
                if (UserSettings.Default.PinToTop)
                {
                    thumbnailWindow.TopMost = true;
                    thumbnailWindow.BringToFront();
                }
                else if (UserSettings.Default.PinToNormal)
                    thumbnailWindow.TopMost = false;
                else if (UserSettings.Default.PinToBottom)
                {
                    thumbnailWindow.TopMost = false;
                    thumbnailWindow.SendToBack();
                }
            }
        }

        public void SetAlwaysTopNoMove()
        {
            alwaysOnTopNoMoveToolStripMenuItem.Checked = true;
            alwaysOnTopNoMoveToolStripMenuItem_Click(this, null);
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void minimizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetForegroundWindow(GetDesktopWindow());
            isProcessing = true;
            Hide();
            hideTargetWindowToolStripMenuItem_Click(this, null);
            isProcessing = false;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (!(e.ClickedItem.Text != "Close") || !(e.ClickedItem.Text != "Quit") || (!(e.ClickedItem.Text != "Minimize") || !(e.ClickedItem.Text != "Settings")) || (!(e.ClickedItem.Text != "Learn...") || !(e.ClickedItem.Text != "Hide Target Window") || !(e.ClickedItem.Text != "Show Target Window")) || e.ClickedItem == alwaysOnTopNoMoveToolStripMenuItem)
                return;
            if (thumb != IntPtr.Zero)
                DwmUnregisterThumbnail(thumb);
            currentwindowname = e.ClickedItem.Text;
            userwindowindexnum = int.Parse(currentwindowname.Substring(0, currentwindowname.IndexOf('.')));
            int num = DwmRegisterThumbnail(Handle, windows[userwindowindexnum - 1].Handle, out thumb);
            MoveWindow(currentwindowhandle, winfo2.rcWindow.left, winfo2.rcWindow.top, winfo2.rcWindow.right - winfo2.rcWindow.left, winfo2.rcWindow.bottom - winfo2.rcWindow.top, true);
            currentwindowhandle = windows[userwindowindexnum - 1].Handle;
            GetWindowInfo(currentwindowhandle, ref winfo);
            winfo2 = winfo;
            if (winfo.rcWindow.right < -30000)
            {
                SendMessage(currentwindowhandle, WM_SYSCOMMAND, SC_RESTORE, 0);
                GetWindowInfo(currentwindowhandle, ref winfo);
                winfo2 = winfo;
            }
            PSIZE size;
            DwmQueryThumbnailSourceSize(thumb, out size);
            currentwindowsize.x = size.x;
            currentwindowsize.y = size.y;
            if (num == 0)
            {
                var test = "sadf";
                // this.SetDefaultView();
                //     if (UserSettings.Default.MoveTargetOffscreen)
                //    ThumbnailWindow.MoveWindow(this.currentwindowhandle, SystemInformation.PrimaryMonitorSize.Width, 0, this.winfo.rcWindow.right - this.winfo.rcWindow.left, this.winfo.rcWindow.bottom - this.winfo.rcWindow.top, true);
            }
        }

        private void contextMenuStrip1_MouseUp(object sender, MouseEventArgs e)
        {
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void contextMenuStrip1_MouseLeave(object sender, EventArgs e)
        {
        }

        private void learnToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

        [DllImport("user32")]
        private static extern uint GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        public static int GetWindowProcessID(int hwnd)
        {
            int lpdwProcessId = 1;
            int num = (int)GetWindowThreadProcessId(hwnd, out lpdwProcessId);
            return lpdwProcessId;
        }

        public void hideTargetWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isProcessing = true;
            qparent.ToggleShowTaskbarTab(currentwindowhandle, false);
            SetWindowLong(currentwindowhandle, -20, winLong | 524288L | 134217728L | 32L);
            SetLayeredWindowAttributes(currentwindowhandle, 0, 0, 2);
            bool flag1;
            if (IsIconic(currentwindowhandle))
            {
                bool flag2 = false;
                if (XPAppearance.MinAnimate)
                {
                    XPAppearance.MinAnimate = false;
                    flag2 = true;
                }
                ShowWindow(currentwindowhandle, 9);
                if (flag2)
                {
                    XPAppearance.MinAnimate = true;
                    flag1 = false;
                }
            }
            StringBuilder lpString = new StringBuilder(100);
            GetWindowText(currentwindowhandle, lpString, lpString.Capacity);
            if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(GetWindowProcessID((int)currentwindowhandle)).ProcessName.Equals("Photoshop"))
            {
                bool flag2 = false;
                if (XPAppearance.MinAnimate)
                {
                    XPAppearance.MinAnimate = false;
                    flag2 = true;
                }
                placement = new WINDOWPLACEMENT();
                placement.length = Marshal.SizeOf(placement);
                GetWindowPlacement(currentwindowhandle, ref placement);
                placement.showCmd = placement.flags != 2 ? 1 : 3;
                SetWindowPos(currentwindowhandle, HWND_BOTTOM, SystemInformation.PrimaryMonitorSize.Width - 1, SystemInformation.PrimaryMonitorSize.Height - 1, 0, 0, 17U);
                if (flag2)
                {
                    XPAppearance.MinAnimate = true;
                    flag1 = false;
                }
            }
            qparent.HideTaskbarTab();
            TargetShown = false;
            isProcessing = false;
        }

        public void showTargetWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool flag1 = false;
            GetWindows();
            foreach (Window window in windows)
            {
                if (window.Handle == currentwindowhandle)
                {
                    flag1 = true;
                    break;
                }
            }
            if (!flag1)
                return;
            isProcessing = true;
            if (TargetShown)
            {
                SetForegroundWindow((int)currentwindowhandle);
            }
            else
            {
                bool flag2;
                if (IsIconic(currentwindowhandle))
                {
                    bool flag3 = false;
                    if (XPAppearance.MinAnimate)
                    {
                        XPAppearance.MinAnimate = false;
                        flag3 = true;
                    }
                    ShowWindow(currentwindowhandle, 9);
                    if (flag3)
                    {
                        XPAppearance.MinAnimate = true;
                        flag2 = false;
                    }
                }
                StringBuilder lpString = new StringBuilder(100);
                GetWindowText(currentwindowhandle, lpString, lpString.Capacity);
                if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(GetWindowProcessID((int)currentwindowhandle)).ProcessName.Equals("Photoshop"))
                {
                    bool flag3 = false;
                    if (XPAppearance.MinAnimate)
                    {
                        XPAppearance.MinAnimate = false;
                        flag3 = true;
                    }
                    SetWindowPos(currentwindowhandle, IntPtr.Zero, 0, 0, 0, 0, 19U);
                    SetWindowPlacement(currentwindowhandle, ref placement);
                    if (flag3)
                    {
                        XPAppearance.MinAnimate = true;
                        flag2 = false;
                    }
                }
                SetWindowLong(currentwindowhandle, -20, winLong);
                qparent.ToggleShowTaskbarTab(currentwindowhandle, true);
                qparent.ShowTaskbarTab(currentwindowhandle);
                SetForegroundWindow((int)currentwindowhandle);
                TargetShown = true;
                isProcessing = false;
            }
        }

        public void showTargetWindowMinimized(object sender, EventArgs e)
        {
            bool flag1 = false;
            GetWindows();
            foreach (Window window in windows)
            {
                if (window.Handle == currentwindowhandle)
                {
                    flag1 = true;
                    break;
                }
            }
            if (!flag1)
                return;
            isProcessing = true;
            if (TargetShown)
                return;
            StringBuilder lpString = new StringBuilder(100);
            GetWindowText(currentwindowhandle, lpString, lpString.Capacity);
            bool flag2;
            if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(GetWindowProcessID((int)currentwindowhandle)).ProcessName.Equals("Photoshop"))
            {
                bool flag3 = false;
                if (XPAppearance.MinAnimate)
                {
                    XPAppearance.MinAnimate = false;
                    flag3 = true;
                }
                SetWindowPos(currentwindowhandle, IntPtr.Zero, 0, 0, 0, 0, 19U);
                SetWindowPlacement(currentwindowhandle, ref placement);
                if (flag3)
                {
                    XPAppearance.MinAnimate = true;
                    flag2 = false;
                }
            }
            qparent.ToggleShowTaskbarTab(currentwindowhandle, true);
            qparent.ShowTaskbarTab(currentwindowhandle);
            SetWindowLong(currentwindowhandle, -20, winLong);
            if (!IsIconic(currentwindowhandle))
            {
                bool flag3 = false;
                if (XPAppearance.MinAnimate)
                {
                    XPAppearance.MinAnimate = false;
                    flag3 = true;
                }
                ShowWindow(currentwindowhandle, 7);
                if (flag3)
                {
                    XPAppearance.MinAnimate = true;
                    flag2 = false;
                }
            }
            TargetShown = true;
            isProcessing = false;
        }

        public bool GetProcessingState()
        {
            return isProcessing;
        }

        public bool GetShownState()
        {
            return TargetShown;
        }

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window, EnumWindowProc callback, IntPtr i);

        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> list = new List<IntPtr>();
            GCHandle gcHandle = GCHandle.Alloc(list);
            try
            {
                EnumWindowProc callback = EnumWindow;
                EnumChildWindows(parent, callback, GCHandle.ToIntPtr(gcHandle));
            }
            finally
            {
                if (gcHandle.IsAllocated)
                    gcHandle.Free();
            }
            return list;
        }

        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            List<IntPtr> list = GCHandle.FromIntPtr(pointer).Target as List<IntPtr>;
            if (list == null)
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            list.Add(handle);
            return true;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            click_timer.Stop();
            click_timer.Enabled = false;
            Show();
            WindowState = FormWindowState.Normal;
            minimizing = false;
            SizeWindow(2);
            try
            {
                notifyIcon1.Visible = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NotifyIcon already disposed");
            }
            Position_Screen();
            restorewindow = false;
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
                return;
            click_timer.Start();
        }

        private void click_timer_Tick(object sender, EventArgs e)
        {
            click_timer.Stop();
            notifyIcon1.ContextMenuStrip = contextMenuStrip1;
            INPUT[] pInputs = new INPUT[2];
            MOUSEINPUT mouseinput1 = new MOUSEINPUT();
            mouseinput1.dwFlags = 32776U;
            mouseinput1.dx = 0;
            mouseinput1.dy = 0;
            mouseinput1.time = 0U;
            mouseinput1.mouseData = 0U;
            MOUSEINPUT mouseinput2 = new MOUSEINPUT();
            mouseinput2.dwFlags = 32784U;
            mouseinput2.dx = 0;
            mouseinput2.dy = 0;
            mouseinput2.time = 0U;
            mouseinput2.mouseData = 0U;
            pInputs[0] = new INPUT();
            pInputs[0].type = 0;
            pInputs[0].mi = mouseinput1;
            pInputs[1] = new INPUT();
            pInputs[1].type = 0;
            pInputs[1].mi = mouseinput2;
            int num = (int)Win32.SendInput(2U, pInputs, Marshal.SizeOf(pInputs[0]));
        }

        private void image_MouseDown(object sender, MouseEventArgs e)
        {
            image.Focus();
            if (inRegion && e.Button == MouseButtons.Left && !controlkeyisdown)
            {
                clicked = true;
                if (e.Button != MouseButtons.Left)
                    return;
                int x1 = e.X;
                Point location = Location;
                int x2 = location.X;
                int x3 = x1 + x2;
                int y1 = e.Y;
                location = Location;
                int y2 = location.Y;
                int y3 = y1 + y2;
                ptOffset = new Point(x3, y3);
            }
            else
            {
                if (controlkeyisdown || e.Button != MouseButtons.Left || e.Button != MouseButtons.Left)
                    return;
                int x;
                int y;
                if (FormBorderStyle == FormBorderStyle.Sizable)
                {
                    x = -e.X - SystemInformation.FrameBorderSize.Width;
                    y = -e.Y - SystemInformation.FrameBorderSize.Height;
                }
                else
                {
                    x = -e.X;
                    y = -e.Y;
                }
                mouseOffset = new Point(x, y);
                isMouseDown = true;
                panoffsetx1 = panoffsetx2;
                panoffsetx2 = x;
                panoffsety1 = panoffsety2;
                panoffsety2 = y;
            }
        }

        private void image_MouseMove(object sender, MouseEventArgs e)
        {
            if (!controlkeyisdown)
            {
                if (e.X <= West)
                    inW = true;
                if (e.X >= East)
                    inE = true;
                if (e.Y <= North)
                    inN = true;
                if (e.Y >= South)
                    inS = true;
                if (!clicked)
                {
                    if (inN && inE)
                    {
                        Cursor = Cursors.PanNE;
                        inRegion = true;
                    }
                    else if (inN && inW)
                    {
                        Cursor = Cursors.PanNW;
                        inRegion = true;
                    }
                    else if (inS && inE)
                    {
                        Cursor = Cursors.PanSE;
                        inRegion = true;
                    }
                    else if (inS && inW)
                    {
                        Cursor = Cursors.PanSW;
                        inRegion = true;
                    }
                    else if (inW)
                    {
                        Cursor = Cursors.PanWest;
                        inRegion = true;
                    }
                    else if (inE)
                    {
                        Cursor = Cursors.PanEast;
                        inRegion = true;
                    }
                    else if (inN)
                    {
                        Cursor = Cursors.PanNorth;
                        inRegion = true;
                    }
                    else if (inS)
                    {
                        Cursor = Cursors.PanSouth;
                        inRegion = true;
                    }
                    else if (!inN && !inS && !inE && !inW)
                    {
                        Cursor = Cursors.Default;
                        inRegion = false;
                    }
                }
            }
            if (clicked && inRegion && !controlkeyisdown)
            {
                PreviousSize = Size;
                LockedPoint = Location;
                qparent.TargettedWindows.IndexOf(currentwindowhandle);
                Point point;
                if (inN && inE)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int num1 = point.Y - ptOffset.Y;
                    point = MousePosition;
                    int num2 = point.X - ptOffset.X;
                    int num3 = num1 - num2;
                    int NewY = height - num3;
                    ResizeFormP(width, NewY);
                    point = Location;
                    int x = point.X;
                    int y1 = LockedPoint.Y;
                    size = Size;
                    int num4 = size.Height - PreviousSize.Height;
                    int y2 = y1 - num4;
                    Location = new Point(x, y2);
                }
                else if (inN && inW)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int num1 = point.Y - ptOffset.Y;
                    int num2 = height - num1;
                    point = MousePosition;
                    int num3 = point.X - ptOffset.X;
                    int NewY = num2 - num3;
                    ResizeFormP(width, NewY);
                    int x1 = LockedPoint.X;
                    size = Size;
                    int num4 = size.Width - PreviousSize.Width;
                    int x2 = x1 - num4;
                    int y1 = LockedPoint.Y;
                    size = Size;
                    int num5 = size.Height - PreviousSize.Height;
                    int y2 = y1 - num5;
                    Location = new Point(x2, y2);
                }
                else if (inS && inE)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int num1 = point.Y - ptOffset.Y;
                    int num2 = height + num1;
                    point = MousePosition;
                    int num3 = point.X - ptOffset.X;
                    int NewY = num2 + num3;
                    ResizeFormP(width, NewY);
                }
                else if (inS && inW)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int num1 = point.Y - ptOffset.Y;
                    int num2 = height + num1;
                    point = MousePosition;
                    int num3 = point.X - ptOffset.X;
                    int NewY = num2 - num3;
                    ResizeFormP(width, NewY);
                    int x1 = LockedPoint.X;
                    size = Size;
                    int num4 = size.Width - PreviousSize.Width;
                    int x2 = x1 - num4;
                    point = Location;
                    int y = point.Y;
                    Location = new Point(x2, y);
                }
                else if (inW)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int num1 = point.X - ptOffset.X;
                    int NewY = height - num1;
                    ResizeFormP(width, NewY);
                    int x1 = LockedPoint.X;
                    size = Size;
                    int num2 = size.Width - PreviousSize.Width;
                    int x2 = x1 - num2;
                    int y1 = LockedPoint.Y;
                    size = Size;
                    int num3 = (size.Height - PreviousSize.Height) / 2;
                    int y2 = y1 - num3;
                    Location = new Point(x2, y2);
                }
                else if (inE)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int x1 = point.X;
                    int NewY = height + x1 - ptOffset.X;
                    ResizeFormP(width, NewY);
                    point = Location;
                    int x2 = point.X;
                    int y1 = LockedPoint.Y;
                    size = Size;
                    int num = (size.Height - PreviousSize.Height) / 2;
                    int y2 = y1 - num;
                    Location = new Point(x2, y2);
                }
                else if (inN)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int num1 = point.Y - ptOffset.Y;
                    int NewY = height - num1;
                    ResizeFormP(width, NewY);
                    int x1 = LockedPoint.X;
                    size = Size;
                    int num2 = (size.Width - PreviousSize.Width) / 2;
                    int x2 = x1 - num2;
                    int y1 = LockedPoint.Y;
                    size = Size;
                    int num3 = size.Height - PreviousSize.Height;
                    int y2 = y1 - num3;
                    Location = new Point(x2, y2);
                }
                else if (inS)
                {
                    Size size = Size;
                    int width = size.Width;
                    size = Size;
                    int height = size.Height;
                    point = MousePosition;
                    int y1 = point.Y;
                    int NewY = height + y1 - ptOffset.Y;
                    ResizeFormP(width, NewY);
                    int x1 = LockedPoint.X;
                    size = Size;
                    int num = (size.Width - PreviousSize.Width) / 2;
                    int x2 = x1 - num;
                    point = Location;
                    int y2 = point.Y;
                    Location = new Point(x2, y2);
                }
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                Point local1 = ptOffset;
                point = MousePosition;
                int x3 = point.X;
                // ISSUE: explicit reference operation
                (local1).X = x3;
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                Point local2 = ptOffset;
                point = MousePosition;
                int y3 = point.Y;
                // ISSUE: explicit reference operation
                (local2).Y = y3;
            }
            else if (isMouseDown && !controlkeyisdown)
            {
                Point mousePosition = MousePosition;
                mousePosition.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePosition;
            }
            else
            {
                inN = false;
                inS = false;
                inE = false;
                inW = false;
            }
        }

        private void image_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = false;
                clicked = false;
                inRegion = false;
                Cursor = Cursors.Default;
            }
            if (Moved && UserSettings.Default.PreviewGridAlign == "Snap" && !(bool)qparent.AlwaysOnTop[qparent.TargettedWindows.IndexOf(currentwindowhandle)])
            {

                Point mousePosition = MousePosition;
                int x = mousePosition.X;
                mousePosition = MousePosition;
                int y = mousePosition.Y;
                // ISSUE: explicit reference operation
                Point p = new Point(x, y);
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                Point local = p;
                qparent.CheckAndInsert(Handle, p);
            }
            Moved = false;
        }

        private void image_MouseClick(object sender, MouseEventArgs e)
        {
            image.Focus();
        }

        private void image_DoubleClick(object sender, EventArgs e)
        {
            if (zoomin())
            {
                resizeratio = UserSettings.Default.ZoomScalingFactor * UserSettings.Default.ZoomClickSensitivity;
                SizeWindow(3);
                Position_Screen();
            }
            else if (zoomout())
            {
                resizeratio = 1.0 / (UserSettings.Default.ZoomScalingFactor * UserSettings.Default.ZoomClickSensitivity);
                SizeWindow(3);
                Position_Screen();
            }
            else if (defaultviewbuttonclicked())
                SetDefaultView();
            else
                Close();
        }

        private void ThumbnailWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            isProcessing = true;
            SavePreviewCacheParams();
            if (thumb != IntPtr.Zero)
                DwmUnregisterThumbnail(thumb);
            try
            {
                if (qparent.quitting)
                    showTargetWindowMinimized(this, null);
                else
                    showTargetWindowToolStripMenuItem_Click(this, null);
            }
            catch (Exception ex)
            {
            }
            try
            {
                notifyIcon1.Visible = false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NotifyIcon already disposed");
            }
            isProcessing = false;
        }

        private void image_Click(object sender, EventArgs e)
        {
        }

        private void image_MouseEnter(object sender, EventArgs e)
        {
        }

        private void image_MouseLeave(object sender, EventArgs e)
        {
            if (controlkeyisdown)
                return;
            Position_Screen();
            int index = qparent.TargettedWindows.IndexOf(currentwindowhandle);
            if (UserSettings.Default.PreviewGridAlign == "Snap")
                qparent.ReAlign();
            if (!(bool)qparent.AlwaysOnTop[index] && !contextMenuStrip1.Visible && (!UserSettings.Default.PinToTop && !UserSettings.Default.PinToNormal) && UserSettings.Default.PinToBottom)
                SendToBack();
            if (!contextMenuStrip1.Visible)
                SetForegroundWindow(GetDesktopWindow());
        }

        private void image_MouseHover(object sender, EventArgs e)
        {
            if (contextMenuStrip1.Visible)
                return;
            Activate();
            image.Focus();
            image.BringToFront();
        }

        public bool zoomin()
        {
            Point point1 = MousePosition;
            int x1 = point1.X;
            point1 = Location;
            int num1 = point1.X + (int)((1.0 - UserSettings.Default.ZoomButtonsSizeFactorX) * image.Size.Width);
            int num2;
            if (x1 >= num1)
            {
                Point point2 = MousePosition;
                int x2 = point2.X;
                point2 = Location;
                int num3 = point2.X + Size.Width;
                if (x2 <= num3)
                {
                    point2 = MousePosition;
                    int y1 = point2.Y;
                    point2 = Location;
                    int num4 = point2.Y + (int)((1.0 - UserSettings.Default.ZoomButtonsSizeFactorY) * image.Size.Height);
                    if (y1 >= num4)
                    {
                        point2 = MousePosition;
                        int y2 = point2.Y;
                        point2 = Location;
                        int num5 = point2.Y + Size.Height;
                        num2 = y2 <= num5 ? 1 : 0;
                        goto label_5;
                    }
                }
            }
            num2 = 0;
        label_5:
            return num2 != 0;
        }

        public bool zoomout()
        {
            Point point1 = MousePosition;
            int x1 = point1.X;
            point1 = Location;
            int x2 = point1.X;
            int num1;
            if (x1 >= x2)
            {
                Point point2 = MousePosition;
                int x3 = point2.X;
                point2 = Location;
                int x4 = point2.X;
                Size size = Size;
                int width = size.Width;
                int num2 = x4 + width;
                double num3 = 1.0 - UserSettings.Default.ZoomButtonsSizeFactorX;
                size = image.Size;
                double num4 = size.Width;
                int num5 = (int)(num3 * num4);
                int num6 = num2 - num5;
                if (x3 <= num6)
                {
                    point2 = MousePosition;
                    int y1 = point2.Y;
                    point2 = Location;
                    int y2 = point2.Y;
                    double num7 = 1.0 - UserSettings.Default.ZoomButtonsSizeFactorY;
                    size = image.Size;
                    double num8 = size.Height;
                    int num9 = (int)(num7 * num8);
                    int num10 = y2 + num9;
                    if (y1 >= num10)
                    {
                        point2 = MousePosition;
                        int y3 = point2.Y;
                        point2 = Location;
                        int y4 = point2.Y;
                        size = Size;
                        int height = size.Height;
                        int num11 = y4 + height;
                        num1 = y3 <= num11 ? 1 : 0;
                        goto label_5;
                    }
                }
            }
            num1 = 0;
        label_5:
            return num1 != 0;
        }

        public bool defaultviewbuttonclicked()
        {
            Point point1 = MousePosition;
            int x1 = point1.X;
            point1 = Location;
            int num1 = point1.X + (int)(UserSettings.Default.ZoomButtonsSizeFactorX * image.Size.Width);
            int num2;
            if (x1 >= num1)
            {
                Point point2 = MousePosition;
                int x2 = point2.X;
                point2 = Location;
                int num3 = point2.X + Size.Width - (int)(UserSettings.Default.ZoomButtonsSizeFactorX * image.Size.Width);
                if (x2 <= num3)
                {
                    point2 = MousePosition;
                    int y1 = point2.Y;
                    point2 = Location;
                    int num4 = point2.Y + (int)((1.0 - UserSettings.Default.ZoomButtonsSizeFactorY) * image.Size.Height);
                    if (y1 >= num4)
                    {
                        point2 = MousePosition;
                        int y2 = point2.Y;
                        point2 = Location;
                        int num5 = point2.Y + Size.Height;
                        num2 = y2 <= num5 ? 1 : 0;
                        goto label_5;
                    }
                }
            }
            num2 = 0;
        label_5:
            return num2 != 0;
        }

        public void Zoom(string UpDown)
        {
            if (UpDown == "Down")
            {
                resizeratio = 1.0 / UserSettings.Default.MouseScrollZoomSensitivity;
                SizeWindow(3);
            }
            else
            {
                resizeratio = UserSettings.Default.MouseScrollZoomSensitivity;
                SizeWindow(3);
            }
        }

        public void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (initializeparam == 1)
                return;
            int count = windows.Count;
            Size size = Size;
            int width1 = size.Width;
            size = Size;
            int height1 = size.Height;
            int index = qparent.TargettedWindows.IndexOf(currentwindowhandle);
            switch (e.KeyCode.ToString())
            {
                case "Escape":
                    Close();
                    break;
                case "Down":
                    if (ShiftisDown)
                        qparent.ZoomAll("Down");
                    resizeratio = 1.0 / UserSettings.Default.ZoomScalingFactor;
                    SizeWindow(3);
                    if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool)qparent.AlwaysOnTop[index])
                    {
                        qparent.ReAlign();
                        break;
                    }
                    size = Size;
                    int Forceoffsetx1 = -(size.Width - width1) / 2;
                    size = Size;
                    int Forceoffsety1 = -(size.Height - height1) / 2;
                    Position_Screen_With_Offset(Forceoffsetx1, Forceoffsety1);
                    break;
                case "Up":
                    if (ShiftisDown)
                        qparent.ZoomAll("Up");
                    resizeratio = UserSettings.Default.ZoomScalingFactor;
                    SizeWindow(3);
                    if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool)qparent.AlwaysOnTop[index])
                    {
                        qparent.ReAlign();
                        break;
                    }
                    size = Size;
                    int Forceoffsetx2 = -(size.Width - width1) / 2;
                    size = Size;
                    int Forceoffsety2 = -(size.Height - height1) / 2;
                    Position_Screen_With_Offset(Forceoffsetx2, Forceoffsety2);
                    break;
                case "Left":
                    if (!(UserSettings.Default.PreviewGridAlign == "Snap"))
                        break;
                    int OldIndex1 = qparent.PreviewControls.IndexOf(this);
                    if (OldIndex1 - 1 < 0)
                        break;
                    if (OldIndex1 - 1 == 0)
                    {
                        Point location = ((Form)qparent.PreviewControls[OldIndex1 - 1]).Location;
                        int x = location.X;
                        location = Location;
                        int y = location.Y;
                        Location = new Point(x, y);
                    }
                    else if (OldIndex1 == 0)
                    {
                        ThumbnailWindow thumbnailWindow1 = (ThumbnailWindow)qparent.PreviewControls[OldIndex1 + 1];
                        ThumbnailWindow thumbnailWindow2 = thumbnailWindow1;
                        Point location = Location;
                        int x = location.X;
                        location = thumbnailWindow1.Location;
                        int y = location.Y;
                        Point point = new Point(x, y);
                        thumbnailWindow2.Location = point;
                    }
                    qparent.SwapAndRemove(OldIndex1 - 1, OldIndex1);
                    qparent.ReAlign();
                    break;
                case "Right":
                    if (!(UserSettings.Default.PreviewGridAlign == "Snap"))
                        break;
                    int OldIndex2 = qparent.PreviewControls.IndexOf(this);
                    if (OldIndex2 + 1 >= qparent.PreviewControls.Count)
                        break;
                    if (OldIndex2 == 0)
                    {
                        ThumbnailWindow thumbnailWindow1 = (ThumbnailWindow)qparent.PreviewControls[OldIndex2 + 1];
                        ThumbnailWindow thumbnailWindow2 = thumbnailWindow1;
                        Point location = Location;
                        int x = location.X;
                        location = thumbnailWindow1.Location;
                        int y = location.Y;
                        Point point = new Point(x, y);
                        thumbnailWindow2.Location = point;
                    }
                    qparent.SwapAndRemove(OldIndex2 + 1, OldIndex2);
                    qparent.ReAlign();
                    break;
                case "ControlKey":
                    size = Size;
                    int width2 = size.Width;
                    size = SystemInformation.MinimumWindowSize;
                    int width3 = size.Width;
                    if (width2 < width3)
                    {
                        controlkeyisdown = false;
                        FormToolTip.SetToolTip(image, "Preview is too small to crop. Please enlarge.");
                        FormToolTip.Active = true;
                        break;
                    }
                    size = Size;
                    int height2 = size.Height;
                    size = SystemInformation.MinimumWindowSize;
                    int height3 = size.Height;
                    if (height2 < height3)
                    {
                        controlkeyisdown = false;
                        FormToolTip.SetToolTip(image, "Preview is too small to crop. Please enlarge.");
                        FormToolTip.Active = true;
                        break;
                    }
                    triploop = true;
                    if (FormBorderStyle.ToString() == "None")
                        FormBorderStyle = FormBorderStyle.Sizable;
                    triploop = false;
                    controlkeyisdown = true;
                    Point location1 = Location;
                    storedwindowlocationx = location1.X;
                    location1 = Location;
                    storedwindowlocationy = location1.Y;
                    PreviousXSizeThumb = xsizethumb;
                    PreviousYSizeThumb = ysizethumb;
                    size = image.Size;
                    PreviousXImageSize = size.Width;
                    size = image.Size;
                    PreviousYImageSize = size.Height;
                    size = Size;
                    PreviousXFormSize = size.Width;
                    size = Size;
                    PreviousYFormSize = size.Height;
                    break;
                case "ShiftKey":
                    ShiftisDown = true;
                    break;
            }
        }

        private IntPtr GetDesktopListViewHandle()
        {
            IntPtr num1 = IntPtr.Zero;
            IntPtr hwndParent = (IntPtr)FindWindow("Progman", null);
            IntPtr num2 = IntPtr.Zero;
            if (hwndParent != IntPtr.Zero)
            {
                IntPtr windowEx = FindWindowEx(hwndParent, IntPtr.Zero, "SHELLDLL_DefView", null);
                if (windowEx != IntPtr.Zero)
                    num1 = FindWindowEx(windowEx, IntPtr.Zero, "SysListView32", null);
            }
            return num1;
        }

        private IntPtr GetDesktopListViewHandle2()
        {
            IntPtr num1 = IntPtr.Zero;
            IntPtr hwndParent = (IntPtr)FindWindow("Progman", null);
            IntPtr num2 = IntPtr.Zero;
            if (hwndParent != IntPtr.Zero)
            {
                IntPtr windowEx = FindWindowEx(hwndParent, IntPtr.Zero, "SHELLDLL_DefView", null);
                if (windowEx != IntPtr.Zero)
                    num1 = FindWindowEx(windowEx, IntPtr.Zero, "SysListView32", null);
            }
            return hwndParent;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            FormToolTip.Active = false;
            controlkeyisdown = false;
            ShiftisDown = false;
            if (FormBorderStyle == FormBorderStyle.Sizable)
                FormBorderStyle = FormBorderStyle.None;
            Point location = Location;
            storedwindowlocationx = location.X;
            location = Location;
            storedwindowlocationy = location.Y;
            PreviousXSizeThumb = xsizethumb;
            PreviousYSizeThumb = ysizethumb;
            PreviousXImageSize = image.Size.Width;
            PreviousYImageSize = image.Size.Height;
            PreviousXFormSize = Size.Width;
            PreviousYFormSize = Size.Height;
            Position_Screen();
        }

        private void ThumbnailWindow_Move(object sender, EventArgs e)
        {
            DockinN = false;
            DockinS = false;
            DockinE = false;
            DockinW = false;
            DockinCentre = false;
            if (UserSettings.Default.FreeFormEdgeDocking || UserSettings.Default.FreeFormCentreDocking)
            {
                APPBARDATA pData = new APPBARDATA();
                int num1 = (int)SHAppBarMessage(5, ref pData);
                int num2 = 0;
                int num3 = 0;

                int x1 = SystemInformation.PrimaryMonitorSize.Width / 2;
                Size size = SystemInformation.PrimaryMonitorSize;
                int y1 = size.Height / 2;
                Point point = new Point(x1, y1);
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                Point local = point;
                // ISSUE: explicit reference operation
                if (UserSettings.Default.CustomTaskbarMargins)
                {
                    int taskbarMarginsLeft = UserSettings.Default.CustomTaskbarMarginsLeft;
                    int num4 = -UserSettings.Default.CustomTaskbarMarginsBottom;
                    int taskbarMarginsTop = UserSettings.Default.CustomTaskbarMarginsTop;
                    int num5 = -UserSettings.Default.CustomTaskbarMarginsRight;
                    Point location = Location;
                    if (location.X <= taskbarMarginsLeft + UserSettings.Default.FreeFormEdgeDockThickness)
                        DockinW = true;
                    location = Location;
                    int x2 = location.X;
                    size = Size;
                    int width1 = size.Width;
                    int num6 = x2 + width1;
                    size = SystemInformation.PrimaryMonitorSize;
                    int num7 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness + num5;
                    if (num6 >= num7)
                        DockinE = true;
                    location = Location;
                    if (location.Y <= taskbarMarginsTop + UserSettings.Default.FreeFormEdgeDockThickness)
                        DockinN = true;
                    location = Location;
                    int y2 = location.Y;
                    size = Size;
                    int height1 = size.Height;
                    int num8 = y2 + height1;
                    size = SystemInformation.PrimaryMonitorSize;
                    int num9 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness + num2 + num4;
                    if (num8 >= num9)
                        DockinS = true;
                    location = Location;
                    int x3 = location.X;
                    size = Size;
                    int width2 = size.Width;
                    int num10;
                    if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
                    {
                        location = Location;
                        if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                        {
                            location = Location;
                            int y3 = location.Y;
                            size = Size;
                            int height2 = size.Height;
                            if (y3 + height2 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                            {
                                location = Location;
                                num10 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                                goto label_15;
                            }
                        }
                    }
                    num10 = 1;
                label_15:
                    if (num10 == 0)
                        DockinCentre = true;
                }
                else
                {
                    int num4 = pData.rc.bottom;
                    size = SystemInformation.PrimaryMonitorSize;
                    int height1 = size.Height;
                    int num5;
                    if (num4 == height1 && pData.rc.left == 0)
                    {
                        int num6 = pData.rc.right;
                        size = SystemInformation.PrimaryMonitorSize;
                        int width = size.Width;
                        num5 = num6 != width ? 1 : 0;
                    }
                    else
                        num5 = 1;
                    if (num5 == 0)
                    {
                        num3 = 1;
                        int num6 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
                        if (Location.X <= UserSettings.Default.FreeFormEdgeDockThickness)
                            DockinW = true;
                        Point location = Location;
                        int x2 = location.X;
                        size = Size;
                        int width1 = size.Width;
                        int num7 = x2 + width1;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num8 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness;
                        if (num7 >= num8)
                            DockinE = true;
                        location = Location;
                        if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness)
                            DockinN = true;
                        location = Location;
                        int y2 = location.Y;
                        size = Size;
                        int height2 = size.Height;
                        int num9 = y2 + height2;
                        size = SystemInformation.PrimaryMonitorSize;
                        int num10 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness + num6;
                        if (num9 >= num10)
                            DockinS = true;
                        location = Location;
                        int x3 = location.X;
                        size = Size;
                        int width2 = size.Width;
                        int num11;
                        if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
                        {
                            location = Location;
                            if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                            {
                                location = Location;
                                int y3 = location.Y;
                                size = Size;
                                int height3 = size.Height;
                                if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                                {
                                    location = Location;
                                    num11 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                                    goto label_34;
                                }
                            }
                        }
                        num11 = 1;
                    label_34:
                        if (num11 == 0)
                            DockinCentre = true;
                    }
                    else
                    {
                        int num6;
                        if (pData.rc.top == 0 && pData.rc.left == 0)
                        {
                            int num7 = pData.rc.right;
                            size = SystemInformation.PrimaryMonitorSize;
                            int width = size.Width;
                            num6 = num7 != width ? 1 : 0;
                        }
                        else
                            num6 = 1;
                        if (num6 == 0)
                        {
                            num3 = 2;
                            int num7 = pData.rc.bottom;
                            if (Location.X <= UserSettings.Default.FreeFormEdgeDockThickness)
                                DockinW = true;
                            Point location = Location;
                            int x2 = location.X;
                            size = Size;
                            int width1 = size.Width;
                            int num8 = x2 + width1;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num9 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness;
                            if (num8 >= num9)
                                DockinE = true;
                            location = Location;
                            if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness + num7)
                                DockinN = true;
                            location = Location;
                            int y2 = location.Y;
                            size = Size;
                            int height2 = size.Height;
                            int num10 = y2 + height2;
                            size = SystemInformation.PrimaryMonitorSize;
                            int num11 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness;
                            if (num10 >= num11)
                                DockinS = true;
                            location = Location;
                            int x3 = location.X;
                            size = Size;
                            int width2 = size.Width;
                            int num12;
                            if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
                            {
                                location = Location;
                                if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                                {
                                    location = Location;
                                    int y3 = location.Y;
                                    size = Size;
                                    int height3 = size.Height;
                                    if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                                    {
                                        location = Location;
                                        num12 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                                        goto label_53;
                                    }
                                }
                            }
                            num12 = 1;
                        label_53:
                            if (num12 == 0)
                                DockinCentre = true;
                        }
                        else
                        {
                            int num7;
                            if (pData.rc.top == 0 && pData.rc.left == 0)
                            {
                                int num8 = pData.rc.bottom;
                                size = SystemInformation.PrimaryMonitorSize;
                                int height2 = size.Height;
                                num7 = num8 != height2 ? 1 : 0;
                            }
                            else
                                num7 = 1;
                            if (num7 == 0)
                            {
                                num3 = 3;
                                if (Location.X <= UserSettings.Default.FreeFormEdgeDockThickness + pData.rc.right)
                                    DockinW = true;
                                Point location = Location;
                                int x2 = location.X;
                                size = Size;
                                int width1 = size.Width;
                                int num8 = x2 + width1;
                                size = SystemInformation.PrimaryMonitorSize;
                                int num9 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness;
                                if (num8 >= num9)
                                    DockinE = true;
                                location = Location;
                                if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness)
                                    DockinN = true;
                                location = Location;
                                int y2 = location.Y;
                                size = Size;
                                int height2 = size.Height;
                                int num10 = y2 + height2;
                                size = SystemInformation.PrimaryMonitorSize;
                                int num11 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness;
                                if (num10 >= num11)
                                    DockinS = true;
                                location = Location;
                                int x3 = location.X;
                                size = Size;
                                int width2 = size.Width;
                                int num12;
                                if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
                                {
                                    location = Location;
                                    if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                                    {
                                        location = Location;
                                        int y3 = location.Y;
                                        size = Size;
                                        int height3 = size.Height;
                                        if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                                        {
                                            location = Location;
                                            num12 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                                            goto label_72;
                                        }
                                    }
                                }
                                num12 = 1;
                            label_72:
                                if (num12 == 0)
                                    DockinCentre = true;
                            }
                            else
                            {
                                int num8;
                                if (pData.rc.top == 0)
                                {
                                    int num9 = pData.rc.right;
                                    size = SystemInformation.PrimaryMonitorSize;
                                    int width = size.Width;
                                    if (num9 == width)
                                    {
                                        int num10 = pData.rc.bottom;
                                        size = SystemInformation.PrimaryMonitorSize;
                                        int height2 = size.Height;
                                        num8 = num10 != height2 ? 1 : 0;
                                        goto label_78;
                                    }
                                }
                                num8 = 1;
                            label_78:
                                if (num8 == 0)
                                {
                                    num3 = 4;
                                    int num9 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
                                    if (Location.X <= UserSettings.Default.FreeFormEdgeDockThickness)
                                        DockinW = true;
                                    Point location = Location;
                                    int x2 = location.X;
                                    size = Size;
                                    int width1 = size.Width;
                                    int num10 = x2 + width1;
                                    size = SystemInformation.PrimaryMonitorSize;
                                    int num11 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness + num9;
                                    if (num10 >= num11)
                                        DockinE = true;
                                    location = Location;
                                    if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness)
                                        DockinN = true;
                                    location = Location;
                                    int y2 = location.Y;
                                    size = Size;
                                    int height2 = size.Height;
                                    int num12 = y2 + height2;
                                    size = SystemInformation.PrimaryMonitorSize;
                                    int num13 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness;
                                    if (num12 >= num13)
                                        DockinS = true;
                                    location = Location;
                                    int x3 = location.X;
                                    size = Size;
                                    int width2 = size.Width;
                                    int num14;
                                    if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
                                    {
                                        location = Location;
                                        if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                                        {
                                            location = Location;
                                            int y3 = location.Y;
                                            size = Size;
                                            int height3 = size.Height;
                                            if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                                            {
                                                location = Location;
                                                num14 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                                                goto label_92;
                                            }
                                        }
                                    }
                                    num14 = 1;
                                label_92:
                                    if (num14 == 0)
                                        DockinCentre = true;
                                }
                            }
                        }
                    }
                }
            }
            if (qparent.FirstPreviewNotAlwaysOnTop() == Handle && UserSettings.Default.PreviewGridAlign == "Snap")
                qparent.ReAlign();
            Moved = true;
        }

        private void ThumbnailWindow_Leave(object sender, EventArgs e)
        {
            storedwindowlocationx = Location.X;
            storedwindowlocationy = Location.Y;
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_MOUSEWHEEL)
            {
                int num = (Int64)m.WParam <= 0 ? -1 : 1;
                if (!controlkeyisdown && !ShiftisDown)
                {
                    if (num > 0)
                    {
                        resizeratio = UserSettings.Default.MouseScrollZoomSensitivity;
                        int width = Size.Width;
                        int height = Size.Height;
                        SizeWindow(3);
                        if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool)qparent.AlwaysOnTop[qparent.TargettedWindows.IndexOf(currentwindowhandle)])
                            qparent.ReAlign();
                        else
                            Position_Screen_With_Offset(-(Size.Width - width) / 2, -(Size.Height - height) / 2);
                    }
                    else
                    {
                        if (num >= 0)
                            return;
                        resizeratio = 1.0 / UserSettings.Default.MouseScrollZoomSensitivity;
                        Size size = Size;
                        int width = size.Width;
                        size = Size;
                        int height = size.Height;
                        SizeWindow(3);
                        if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool)qparent.AlwaysOnTop[qparent.TargettedWindows.IndexOf(currentwindowhandle)])
                        {
                            qparent.ReAlign();
                        }
                        else
                        {
                            size = Size;
                            int Forceoffsetx = -(size.Width - width) / 2;
                            size = Size;
                            int Forceoffsety = -(size.Height - height) / 2;
                            Position_Screen_With_Offset(Forceoffsetx, Forceoffsety);
                        }
                    }
                }
                else
                {
                    if (!ShiftisDown)
                        return;
                    if (num < 0)
                        qparent.ZoomAll("Up");
                    else if (num > 0)
                        qparent.ZoomAll("Down");
                }
            }
            else if (m.Msg == WM_SYSCOMMAND && m.WParam == (IntPtr)SC_RESTORE && m.LParam == (IntPtr)0)
            {
                Debug.WriteLine("Restoring preview");
                if (notifyIcon1.Visible)
                {
                    Show();
                    WindowState = FormWindowState.Normal;
                    minimizing = false;
                    SizeWindow(2);
                    try
                    {
                        notifyIcon1.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("NotifyIcon already disposed");
                    }
                    Position_Screen();
                    restorewindow = false;
                }
                Show();
                WindowState = FormWindowState.Normal;
                BringToFront();
                Activate();
            }
            else
                base.WndProc(ref m);
        }

        public bool CompareTitle(string CTtitle)
        {
            return false;
        }

        private void ThumbnailWindow_Validated(object sender, EventArgs e)
        {
            storedwindowlocationx = Location.X;
            storedwindowlocationy = Location.Y;
        }

        private void ThumbnailWindow_Deactivate(object sender, EventArgs e)
        {
            controlkeyisdown = false;
            triploop = true;
            if (FormBorderStyle == FormBorderStyle.Sizable)
                FormBorderStyle = FormBorderStyle.None;
            triploop = false;
            Point location = Location;
            storedwindowlocationx = location.X;
            location = Location;
            storedwindowlocationy = location.Y;
            PreviousXSizeThumb = xsizethumb;
            PreviousYSizeThumb = ysizethumb;
            PreviousXImageSize = image.Size.Width;
            Size size = image.Size;
            PreviousYImageSize = size.Height;
            size = Size;
            PreviousXFormSize = size.Width;
            size = Size;
            PreviousYFormSize = size.Height;
            isMouseDown = false;
            clicked = false;
            inRegion = false;
            Cursor = Cursors.Default;
        }

        private void contextMenuStrip1_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            contextMenuStrip1.Hide();
            contextMenuStrip1.Visible = false;
        }

        private void ThumbnailWindow_Shown(object sender, EventArgs e)
        {
        }

        [DllImport("Gdi32.dll")]
        private static extern IntPtr CreateRoundRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, int nWidthEllipse, int nHeightEllipse);

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern void DwmExtendFrameIntoClientArea(IntPtr hwnd, ref MARGINS margins);

        [DllImport("dwmapi.dll", PreserveSig = false)]
        public static extern bool DwmIsCompositionEnabled();

        public void FindFrameBorderSize(ref int width, ref int height)
        {
            if (UserSettings.Default.EnableAeroBackground && DwmIsCompositionEnabled())
            {
                Size frameBorderSize = SystemInformation.FrameBorderSize;
                FrameBorderSizeWidth = frameBorderSize.Width;
                frameBorderSize = SystemInformation.FrameBorderSize;
                FrameBorderSizeHeight = frameBorderSize.Height;
            }
            else
            {
                FrameBorderSizeWidth = 0;
                FrameBorderSizeHeight = 0;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (UserSettings.Default.EnableAeroBackground && DwmIsCompositionEnabled())
            {
                margins = new MARGINS();
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                MARGINS local1 = margins;
                Size size = Size;
                int height1 = size.Height;
                // ISSUE: explicit reference operation
                (local1).Top = height1;
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                MARGINS local2 = margins;
                size = Size;
                int width1 = size.Width;
                // ISSUE: explicit reference operation
                (local2).Left = width1;
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                MARGINS local3 = margins;
                size = Size;
                int height2 = size.Height;
                // ISSUE: explicit reference operation
                (local3).Bottom = height2;
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                MARGINS local4 = margins;
                size = Size;
                int width2 = size.Width;
                // ISSUE: explicit reference operation
                (local4).Right = width2;
                DwmExtendFrameIntoClientArea(Handle, ref margins);
            }
            base.OnLoad(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            e.Graphics.Clear(Color.Transparent);
            if (UserSettings.Default.EnableAeroBackground && DwmIsCompositionEnabled())
            {
                margins.Top = Size.Height;
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                MARGINS local1 = margins;
                Size size = Size;
                int width1 = size.Width;
                // ISSUE: explicit reference operation
                (local1).Left = width1;
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                MARGINS local2 = margins;
                size = Size;
                int height = size.Height;
                // ISSUE: explicit reference operation
                (local2).Bottom = height;
                // ISSUE: explicit reference operation
                // ISSUE: variable of a reference type
                MARGINS local3 = margins;
                size = Size;
                int width2 = size.Width;
                // ISSUE: explicit reference operation
                (local3).Right = width2;
                DwmExtendFrameIntoClientArea(Handle, ref margins);
            }
            else if (!UserSettings.Default.EnableAeroBackground && DwmIsCompositionEnabled())
            {
                margins.Top = 0;
                margins.Left = 0;
                margins.Bottom = 0;
                margins.Right = 0;
                DwmExtendFrameIntoClientArea(Handle, ref margins);
            }
            OnPaint(e);
        }

        public struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public uint mouseData;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        public struct KEYBDINPUT
        {
            public ushort wVk;
            public ushort wScan;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        public struct HARDWAREINPUT
        {
            public uint uMsg;
            public ushort wParamL;
            public ushort wParamH;
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct INPUT
        {
            [FieldOffset(0)]
            public int type;
            [FieldOffset(4)]
            public MOUSEINPUT mi;
            [FieldOffset(4)]
            public KEYBDINPUT ki;
            [FieldOffset(4)]
            public HARDWAREINPUT hi;
        }

        public static class Win32
        {
            public const int INPUT_MOUSE = 0;
            public const int INPUT_KEYBOARD = 1;
            public const int INPUT_HARDWARE = 2;
            public const uint KEYEVENTF_EXTENDEDKEY = 1U;
            public const uint KEYEVENTF_KEYUP = 2U;
            public const uint KEYEVENTF_UNICODE = 4U;
            public const uint KEYEVENTF_SCANCODE = 8U;
            public const uint XBUTTON1 = 1U;
            public const uint XBUTTON2 = 2U;
            public const uint MOUSEEVENTF_MOVE = 1U;
            public const uint MOUSEEVENTF_LEFTDOWN = 2U;
            public const uint MOUSEEVENTF_LEFTUP = 4U;
            public const uint MOUSEEVENTF_RIGHTDOWN = 8U;
            public const uint MOUSEEVENTF_RIGHTUP = 16U;
            public const uint MOUSEEVENTF_MIDDLEDOWN = 32U;
            public const uint MOUSEEVENTF_MIDDLEUP = 64U;
            public const uint MOUSEEVENTF_XDOWN = 128U;
            public const uint MOUSEEVENTF_XUP = 256U;
            public const uint MOUSEEVENTF_WHEEL = 2048U;
            public const uint MOUSEEVENTF_VIRTUALDESK = 16384U;
            public const uint MOUSEEVENTF_ABSOLUTE = 32768U;

            [DllImport("user32.dll", SetLastError = true)]
            public static extern uint SendInput(uint nInputs, INPUT[] pInputs, int cbSize);

            [DllImport("user32.dll")]
            public static extern IntPtr GetMessageExtraInfo();
        }

        private delegate bool EnumWindowsCallback(IntPtr hwnd, int lParam);

        public enum MouseEventType
        {
            LeftDown = 2,
            LeftUp = 4,
            RightDown = 8,
            RightUp = 16
        }

        public struct POINTAPI
        {
            public int x;
            public int y;
        }

        public struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public POINTAPI ptMinPosition;
            public POINTAPI ptMaxPosition;
            public RECT rcNormalPosition;
        }

        private enum ShowWindowEnum
        {
            Hide = 0,
            ShowNormal = 1,
            ShowMinimized = 2,
            Maximize = 3,
            ShowMaximized = 3,
            ShowNormalNoActivate = 4,
            Show = 5,
            Minimize = 6,
            ShowMinNoActivate = 7,
            ShowNoActivate = 8,
            Restore = 9,
            ShowDefault = 10,
            ForceMinimized = 11
        }

        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        public struct MARGINS
        {
            public int Left;
            public int Right;
            public int Top;
            public int Bottom;
        }
    }
}
