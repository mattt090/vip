﻿// Decompiled with JetBrains decompiler
// Type: MinimizeCapture.XPAppearance
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Runtime.InteropServices;

namespace MinimizeCapture
{
  internal static class XPAppearance
  {
    private const uint SPI_GETBEEP = 1U;
    private const uint SPI_SETBEEP = 2U;
    private const uint SPI_GETMOUSE = 3U;
    private const uint SPI_SETMOUSE = 4U;
    private const uint SPI_GETBORDER = 5U;
    private const uint SPI_SETBORDER = 6U;
    private const uint SPI_GETKEYBOARDSPEED = 10U;
    private const uint SPI_SETKEYBOARDSPEED = 11U;
    private const uint SPI_LANGDRIVER = 12U;
    private const uint SPI_ICONHORIZONTALSPACING = 13U;
    private const uint SPI_GETSCREENSAVETIMEOUT = 14U;
    private const uint SPI_SETSCREENSAVETIMEOUT = 15U;
    private const uint SPI_GETSCREENSAVEACTIVE = 16U;
    private const uint SPI_SETSCREENSAVEACTIVE = 17U;
    private const uint SPI_GETGRIDGRANULARITY = 18U;
    private const uint SPI_SETGRIDGRANULARITY = 19U;
    private const uint SPI_SETDESKWALLPAPER = 20U;
    private const uint SPI_SETDESKPATTERN = 21U;
    private const uint SPI_GETKEYBOARDDELAY = 22U;
    private const uint SPI_SETKEYBOARDDELAY = 23U;
    private const uint SPI_ICONVERTICALSPACING = 24U;
    private const uint SPI_GETICONTITLEWRAP = 25U;
    private const uint SPI_SETICONTITLEWRAP = 26U;
    private const uint SPI_GETMENUDROPALIGNMENT = 27U;
    private const uint SPI_SETMENUDROPALIGNMENT = 28U;
    private const uint SPI_SETDOUBLECLKWIDTH = 29U;
    private const uint SPI_SETDOUBLECLKHEIGHT = 30U;
    private const uint SPI_GETICONTITLELOGFONT = 31U;
    private const uint SPI_SETDOUBLECLICKTIME = 32U;
    private const uint SPI_SETMOUSEBUTTONSWAP = 33U;
    private const uint SPI_SETICONTITLELOGFONT = 34U;
    private const uint SPI_GETFASTTASKSWITCH = 35U;
    private const uint SPI_SETFASTTASKSWITCH = 36U;
    private const uint SPI_SETDRAGFULLWINDOWS = 37U;
    private const uint SPI_GETDRAGFULLWINDOWS = 38U;
    private const uint SPI_GETNONCLIENTMETRICS = 41U;
    private const uint SPI_SETNONCLIENTMETRICS = 42U;
    private const uint SPI_GETMINIMIZEDMETRICS = 43U;
    private const uint SPI_SETMINIMIZEDMETRICS = 44U;
    private const uint SPI_GETICONMETRICS = 45U;
    private const uint SPI_SETICONMETRICS = 46U;
    private const uint SPI_SETWORKAREA = 47U;
    private const uint SPI_GETWORKAREA = 48U;
    private const uint SPI_SETPENWINDOWS = 49U;
    private const uint SPI_GETHIGHCONTRAST = 66U;
    private const uint SPI_SETHIGHCONTRAST = 67U;
    private const uint SPI_GETKEYBOARDPREF = 68U;
    private const uint SPI_SETKEYBOARDPREF = 69U;
    private const uint SPI_GETSCREENREADER = 70U;
    private const uint SPI_SETSCREENREADER = 71U;
    private const uint SPI_GETANIMATION = 72U;
    private const uint SPI_SETANIMATION = 73U;
    private const uint SPI_GETFONTSMOOTHING = 74U;
    private const uint SPI_SETFONTSMOOTHING = 75U;
    private const uint SPI_SETDRAGWIDTH = 76U;
    private const uint SPI_SETDRAGHEIGHT = 77U;
    private const uint SPI_SETHANDHELD = 78U;
    private const uint SPI_GETLOWPOWERTIMEOUT = 79U;
    private const uint SPI_GETPOWEROFFTIMEOUT = 80U;
    private const uint SPI_SETLOWPOWERTIMEOUT = 81U;
    private const uint SPI_SETPOWEROFFTIMEOUT = 82U;
    private const uint SPI_GETLOWPOWERACTIVE = 83U;
    private const uint SPI_GETPOWEROFFACTIVE = 84U;
    private const uint SPI_SETLOWPOWERACTIVE = 85U;
    private const uint SPI_SETPOWEROFFACTIVE = 86U;
    private const uint SPI_SETICONS = 88U;
    private const uint SPI_GETDEFAULTINPUTLANG = 89U;
    private const uint SPI_SETDEFAULTINPUTLANG = 90U;
    private const uint SPI_SETLANGTOGGLE = 91U;
    private const uint SPI_GETWINDOWSEXTENSION = 92U;
    private const uint SPI_SETMOUSETRAILS = 93U;
    private const uint SPI_GETMOUSETRAILS = 94U;
    private const uint SPI_SCREENSAVERRUNNING = 97U;
    private const uint SPI_GETFILTERKEYS = 50U;
    private const uint SPI_SETFILTERKEYS = 51U;
    private const uint SPI_GETTOGGLEKEYS = 52U;
    private const uint SPI_SETTOGGLEKEYS = 53U;
    private const uint SPI_GETMOUSEKEYS = 54U;
    private const uint SPI_SETMOUSEKEYS = 55U;
    private const uint SPI_GETSHOWSOUNDS = 56U;
    private const uint SPI_SETSHOWSOUNDS = 57U;
    private const uint SPI_GETSTICKYKEYS = 58U;
    private const uint SPI_SETSTICKYKEYS = 59U;
    private const uint SPI_GETACCESSTIMEOUT = 60U;
    private const uint SPI_SETACCESSTIMEOUT = 61U;
    private const uint SPI_GETSERIALKEYS = 62U;
    private const uint SPI_SETSERIALKEYS = 63U;
    private const uint SPI_GETSOUNDSENTRY = 64U;
    private const uint SPI_SETSOUNDSENTRY = 65U;
    private const uint SPI_GETSNAPTODEFBUTTON = 95U;
    private const uint SPI_SETSNAPTODEFBUTTON = 96U;
    private const uint SPI_GETMOUSEHOVERWIDTH = 98U;
    private const uint SPI_SETMOUSEHOVERWIDTH = 99U;
    private const uint SPI_GETMOUSEHOVERHEIGHT = 100U;
    private const uint SPI_SETMOUSEHOVERHEIGHT = 101U;
    private const uint SPI_GETMOUSEHOVERTIME = 102U;
    private const uint SPI_SETMOUSEHOVERTIME = 103U;
    private const uint SPI_GETWHEELSCROLLLINES = 104U;
    private const uint SPI_SETWHEELSCROLLLINES = 105U;
    private const uint SPI_GETMENUSHOWDELAY = 106U;
    private const uint SPI_SETMENUSHOWDELAY = 107U;
    private const uint SPI_GETSHOWIMEUI = 110U;
    private const uint SPI_SETSHOWIMEUI = 111U;
    private const uint SPI_GETMOUSESPEED = 112U;
    private const uint SPI_SETMOUSESPEED = 113U;
    private const uint SPI_GETSCREENSAVERRUNNING = 114U;
    private const uint SPI_GETDESKWALLPAPER = 115U;
    private const uint SPI_GETACTIVEWINDOWTRACKING = 4096U;
    private const uint SPI_SETACTIVEWINDOWTRACKING = 4097U;
    private const uint SPI_GETMENUANIMATION = 4098U;
    private const uint SPI_SETMENUANIMATION = 4099U;
    private const uint SPI_GETCOMBOBOXANIMATION = 4100U;
    private const uint SPI_SETCOMBOBOXANIMATION = 4101U;
    private const uint SPI_GETLISTBOXSMOOTHSCROLLING = 4102U;
    private const uint SPI_SETLISTBOXSMOOTHSCROLLING = 4103U;
    private const uint SPI_GETGRADIENTCAPTIONS = 4104U;
    private const uint SPI_SETGRADIENTCAPTIONS = 4105U;
    private const uint SPI_GETKEYBOARDCUES = 4106U;
    private const uint SPI_SETKEYBOARDCUES = 4107U;
    private const uint SPI_GETMENUUNDERLINES = 4106U;
    private const uint SPI_SETMENUUNDERLINES = 4107U;
    private const uint SPI_GETACTIVEWNDTRKZORDER = 4108U;
    private const uint SPI_SETACTIVEWNDTRKZORDER = 4109U;
    private const uint SPI_GETHOTTRACKING = 4110U;
    private const uint SPI_SETHOTTRACKING = 4111U;
    private const uint SPI_GETMENUFADE = 4114U;
    private const uint SPI_SETMENUFADE = 4115U;
    private const uint SPI_GETSELECTIONFADE = 4116U;
    private const uint SPI_SETSELECTIONFADE = 4117U;
    private const uint SPI_GETTOOLTIPANIMATION = 4118U;
    private const uint SPI_SETTOOLTIPANIMATION = 4119U;
    private const uint SPI_GETTOOLTIPFADE = 4120U;
    private const uint SPI_SETTOOLTIPFADE = 4121U;
    private const uint SPI_GETCURSORSHADOW = 4122U;
    private const uint SPI_SETCURSORSHADOW = 4123U;
    private const uint SPI_GETMOUSESONAR = 4124U;
    private const uint SPI_SETMOUSESONAR = 4125U;
    private const uint SPI_GETMOUSECLICKLOCK = 4126U;
    private const uint SPI_SETMOUSECLICKLOCK = 4127U;
    private const uint SPI_GETMOUSEVANISH = 4128U;
    private const uint SPI_SETMOUSEVANISH = 4129U;
    private const uint SPI_GETFLATMENU = 4130U;
    private const uint SPI_SETFLATMENU = 4131U;
    private const uint SPI_GETDROPSHADOW = 4132U;
    private const uint SPI_SETDROPSHADOW = 4133U;
    private const uint SPI_GETBLOCKSENDINPUTRESETS = 4134U;
    private const uint SPI_SETBLOCKSENDINPUTRESETS = 4135U;
    private const uint SPI_GETUIEFFECTS = 4158U;
    private const uint SPI_SETUIEFFECTS = 4159U;
    private const uint SPI_GETFOREGROUNDLOCKTIMEOUT = 8192U;
    private const uint SPI_SETFOREGROUNDLOCKTIMEOUT = 8193U;
    private const uint SPI_GETACTIVEWNDTRKTIMEOUT = 8194U;
    private const uint SPI_SETACTIVEWNDTRKTIMEOUT = 8195U;
    private const uint SPI_GETFOREGROUNDFLASHCOUNT = 8196U;
    private const uint SPI_SETFOREGROUNDFLASHCOUNT = 8197U;
    private const uint SPI_GETCARETWIDTH = 8198U;
    private const uint SPI_SETCARETWIDTH = 8199U;
    private const uint SPI_GETMOUSECLICKLOCKTIME = 8200U;
    private const uint SPI_SETMOUSECLICKLOCKTIME = 8201U;
    private const uint SPI_GETFONTSMOOTHINGTYPE = 8202U;
    private const uint SPI_SETFONTSMOOTHINGTYPE = 8203U;
    private const uint SPI_GETFONTSMOOTHINGCONTRAST = 8204U;
    private const uint SPI_SETFONTSMOOTHINGCONTRAST = 8205U;
    private const uint SPI_GETFOCUSBORDERWIDTH = 8206U;
    private const uint SPI_SETFOCUSBORDERWIDTH = 8207U;
    private const uint SPI_GETFOCUSBORDERHEIGHT = 8208U;
    private const uint SPI_SETFOCUSBORDERHEIGHT = 8209U;
    private const uint SPI_GETFONTSMOOTHINGORIENTATION = 8210U;
    private const uint SPI_SETFONTSMOOTHINGORIENTATION = 8211U;

    public static bool MinAnimate
    {
      get
      {
        XPAppearance.ANIMATIONINFO pvParam = new XPAppearance.ANIMATIONINFO(false);
        XPAppearance.SystemParametersInfo(XPAppearance.SPI.SPI_GETANIMATION, XPAppearance.ANIMATIONINFO.GetSize(), ref pvParam, XPAppearance.SPIF.None);
        return pvParam.IMinAnimate;
      }
      set
      {
        XPAppearance.ANIMATIONINFO pvParam = new XPAppearance.ANIMATIONINFO(value);
        XPAppearance.SystemParametersInfo(XPAppearance.SPI.SPI_SETANIMATION, XPAppearance.ANIMATIONINFO.GetSize(), ref pvParam, XPAppearance.SPIF.SPIF_SENDCHANGE);
      }
    }

    [DllImport("user32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool SystemParametersInfo(XPAppearance.SPI uiAction, uint uiParam, ref XPAppearance.ANIMATIONINFO pvParam, XPAppearance.SPIF fWinIni);

    [Flags]
    private enum SPIF
    {
      None = 0,
      SPIF_UPDATEINIFILE = 1,
      SPIF_SENDCHANGE = 2,
      SPIF_SENDWININICHANGE = SPIF_SENDCHANGE,
    }

    private struct ANIMATIONINFO
    {
      public uint cbSize;
      private int iMinAnimate;

      public bool IMinAnimate
      {
        get
        {
          return this.iMinAnimate != 0;
        }
        set
        {
          if (value)
            this.iMinAnimate = 1;
          else
            this.iMinAnimate = 0;
        }
      }

      public ANIMATIONINFO(bool iMinAnimate)
      {
        this.cbSize = XPAppearance.ANIMATIONINFO.GetSize();
        if (iMinAnimate)
          this.iMinAnimate = 1;
        else
          this.iMinAnimate = 0;
      }

      public static uint GetSize()
      {
        return (uint) Marshal.SizeOf(typeof (XPAppearance.ANIMATIONINFO));
      }
    }

    private enum SPI : uint
    {
      SPI_GETBEEP = 1U,
      SPI_SETBEEP = 2U,
      SPI_GETMOUSE = 3U,
      SPI_SETMOUSE = 4U,
      SPI_GETBORDER = 5U,
      SPI_SETBORDER = 6U,
      SPI_GETKEYBOARDSPEED = 10U,
      SPI_SETKEYBOARDSPEED = 11U,
      SPI_LANGDRIVER = 12U,
      SPI_ICONHORIZONTALSPACING = 13U,
      SPI_GETSCREENSAVETIMEOUT = 14U,
      SPI_SETSCREENSAVETIMEOUT = 15U,
      SPI_GETSCREENSAVEACTIVE = 16U,
      SPI_SETSCREENSAVEACTIVE = 17U,
      SPI_GETGRIDGRANULARITY = 18U,
      SPI_SETGRIDGRANULARITY = 19U,
      SPI_SETDESKWALLPAPER = 20U,
      SPI_SETDESKPATTERN = 21U,
      SPI_GETKEYBOARDDELAY = 22U,
      SPI_SETKEYBOARDDELAY = 23U,
      SPI_ICONVERTICALSPACING = 24U,
      SPI_GETICONTITLEWRAP = 25U,
      SPI_SETICONTITLEWRAP = 26U,
      SPI_GETMENUDROPALIGNMENT = 27U,
      SPI_SETMENUDROPALIGNMENT = 28U,
      SPI_SETDOUBLECLKWIDTH = 29U,
      SPI_SETDOUBLECLKHEIGHT = 30U,
      SPI_GETICONTITLELOGFONT = 31U,
      SPI_SETDOUBLECLICKTIME = 32U,
      SPI_SETMOUSEBUTTONSWAP = 33U,
      SPI_SETICONTITLELOGFONT = 34U,
      SPI_GETFASTTASKSWITCH = 35U,
      SPI_SETFASTTASKSWITCH = 36U,
      SPI_SETDRAGFULLWINDOWS = 37U,
      SPI_GETDRAGFULLWINDOWS = 38U,
      SPI_GETNONCLIENTMETRICS = 41U,
      SPI_SETNONCLIENTMETRICS = 42U,
      SPI_GETMINIMIZEDMETRICS = 43U,
      SPI_SETMINIMIZEDMETRICS = 44U,
      SPI_GETICONMETRICS = 45U,
      SPI_SETICONMETRICS = 46U,
      SPI_SETWORKAREA = 47U,
      SPI_GETWORKAREA = 48U,
      SPI_SETPENWINDOWS = 49U,
      SPI_GETFILTERKEYS = 50U,
      SPI_SETFILTERKEYS = 51U,
      SPI_GETTOGGLEKEYS = 52U,
      SPI_SETTOGGLEKEYS = 53U,
      SPI_GETMOUSEKEYS = 54U,
      SPI_SETMOUSEKEYS = 55U,
      SPI_GETSHOWSOUNDS = 56U,
      SPI_SETSHOWSOUNDS = 57U,
      SPI_GETSTICKYKEYS = 58U,
      SPI_SETSTICKYKEYS = 59U,
      SPI_GETACCESSTIMEOUT = 60U,
      SPI_SETACCESSTIMEOUT = 61U,
      SPI_GETSERIALKEYS = 62U,
      SPI_SETSERIALKEYS = 63U,
      SPI_GETSOUNDSENTRY = 64U,
      SPI_SETSOUNDSENTRY = 65U,
      SPI_GETHIGHCONTRAST = 66U,
      SPI_SETHIGHCONTRAST = 67U,
      SPI_GETKEYBOARDPREF = 68U,
      SPI_SETKEYBOARDPREF = 69U,
      SPI_GETSCREENREADER = 70U,
      SPI_SETSCREENREADER = 71U,
      SPI_GETANIMATION = 72U,
      SPI_SETANIMATION = 73U,
      SPI_GETFONTSMOOTHING = 74U,
      SPI_SETFONTSMOOTHING = 75U,
      SPI_SETDRAGWIDTH = 76U,
      SPI_SETDRAGHEIGHT = 77U,
      SPI_SETHANDHELD = 78U,
      SPI_GETLOWPOWERTIMEOUT = 79U,
      SPI_GETPOWEROFFTIMEOUT = 80U,
      SPI_SETLOWPOWERTIMEOUT = 81U,
      SPI_SETPOWEROFFTIMEOUT = 82U,
      SPI_GETLOWPOWERACTIVE = 83U,
      SPI_GETPOWEROFFACTIVE = 84U,
      SPI_SETLOWPOWERACTIVE = 85U,
      SPI_SETPOWEROFFACTIVE = 86U,
      SPI_SETCURSORS = 87U,
      SPI_SETICONS = 88U,
      SPI_GETDEFAULTINPUTLANG = 89U,
      SPI_SETDEFAULTINPUTLANG = 90U,
      SPI_SETLANGTOGGLE = 91U,
      SPI_GETWINDOWSEXTENSION = 92U,
      SPI_SETMOUSETRAILS = 93U,
      SPI_GETMOUSETRAILS = 94U,
      SPI_GETSNAPTODEFBUTTON = 95U,
      SPI_SETSNAPTODEFBUTTON = 96U,
      SPI_SCREENSAVERRUNNING = 97U,
      SPI_SETSCREENSAVERRUNNING = 97U,
      SPI_GETMOUSEHOVERWIDTH = 98U,
      SPI_SETMOUSEHOVERWIDTH = 99U,
      SPI_GETMOUSEHOVERHEIGHT = 100U,
      SPI_SETMOUSEHOVERHEIGHT = 101U,
      SPI_GETMOUSEHOVERTIME = 102U,
      SPI_SETMOUSEHOVERTIME = 103U,
      SPI_GETWHEELSCROLLLINES = 104U,
      SPI_SETWHEELSCROLLLINES = 105U,
      SPI_GETMENUSHOWDELAY = 106U,
      SPI_SETMENUSHOWDELAY = 107U,
      SPI_GETSHOWIMEUI = 110U,
      SPI_SETSHOWIMEUI = 111U,
      SPI_GETMOUSESPEED = 112U,
      SPI_SETMOUSESPEED = 113U,
      SPI_GETSCREENSAVERRUNNING = 114U,
      SPI_GETDESKWALLPAPER = 115U,
      SPI_GETACTIVEWINDOWTRACKING = 4096U,
      SPI_SETACTIVEWINDOWTRACKING = 4097U,
      SPI_GETMENUANIMATION = 4098U,
      SPI_SETMENUANIMATION = 4099U,
      SPI_GETCOMBOBOXANIMATION = 4100U,
      SPI_SETCOMBOBOXANIMATION = 4101U,
      SPI_GETLISTBOXSMOOTHSCROLLING = 4102U,
      SPI_SETLISTBOXSMOOTHSCROLLING = 4103U,
      SPI_GETGRADIENTCAPTIONS = 4104U,
      SPI_SETGRADIENTCAPTIONS = 4105U,
      SPI_GETKEYBOARDCUES = 4106U,
      SPI_GETMENUUNDERLINES = 4106U,
      SPI_SETKEYBOARDCUES = 4107U,
      SPI_SETMENUUNDERLINES = 4107U,
      SPI_GETACTIVEWNDTRKZORDER = 4108U,
      SPI_SETACTIVEWNDTRKZORDER = 4109U,
      SPI_GETHOTTRACKING = 4110U,
      SPI_SETHOTTRACKING = 4111U,
      SPI_GETMENUFADE = 4114U,
      SPI_SETMENUFADE = 4115U,
      SPI_GETSELECTIONFADE = 4116U,
      SPI_SETSELECTIONFADE = 4117U,
      SPI_GETTOOLTIPANIMATION = 4118U,
      SPI_SETTOOLTIPANIMATION = 4119U,
      SPI_GETTOOLTIPFADE = 4120U,
      SPI_SETTOOLTIPFADE = 4121U,
      SPI_GETCURSORSHADOW = 4122U,
      SPI_SETCURSORSHADOW = 4123U,
      SPI_GETMOUSESONAR = 4124U,
      SPI_SETMOUSESONAR = 4125U,
      SPI_GETMOUSECLICKLOCK = 4126U,
      SPI_SETMOUSECLICKLOCK = 4127U,
      SPI_GETMOUSEVANISH = 4128U,
      SPI_SETMOUSEVANISH = 4129U,
      SPI_GETFLATMENU = 4130U,
      SPI_SETFLATMENU = 4131U,
      SPI_GETDROPSHADOW = 4132U,
      SPI_SETDROPSHADOW = 4133U,
      SPI_GETBLOCKSENDINPUTRESETS = 4134U,
      SPI_SETBLOCKSENDINPUTRESETS = 4135U,
      SPI_GETUIEFFECTS = 4158U,
      SPI_SETUIEFFECTS = 4159U,
      SPI_GETFOREGROUNDLOCKTIMEOUT = 8192U,
      SPI_SETFOREGROUNDLOCKTIMEOUT = 8193U,
      SPI_GETACTIVEWNDTRKTIMEOUT = 8194U,
      SPI_SETACTIVEWNDTRKTIMEOUT = 8195U,
      SPI_GETFOREGROUNDFLASHCOUNT = 8196U,
      SPI_SETFOREGROUNDFLASHCOUNT = 8197U,
      SPI_GETCARETWIDTH = 8198U,
      SPI_SETCARETWIDTH = 8199U,
      SPI_GETMOUSECLICKLOCKTIME = 8200U,
      SPI_SETMOUSECLICKLOCKTIME = 8201U,
      SPI_GETFONTSMOOTHINGTYPE = 8202U,
      SPI_SETFONTSMOOTHINGTYPE = 8203U,
      SPI_GETFONTSMOOTHINGCONTRAST = 8204U,
      SPI_SETFONTSMOOTHINGCONTRAST = 8205U,
      SPI_GETFOCUSBORDERWIDTH = 8206U,
      SPI_SETFOCUSBORDERWIDTH = 8207U,
      SPI_GETFOCUSBORDERHEIGHT = 8208U,
      SPI_SETFOCUSBORDERHEIGHT = 8209U,
      SPI_GETFONTSMOOTHINGORIENTATION = 8210U,
      SPI_SETFONTSMOOTHINGORIENTATION = 8211U,
    }
  }
}
