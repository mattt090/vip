﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyProduct("Thumbnail Persist")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyCompany("Eric Wong")]
[assembly: AssemblyCopyright("Copyright ©  2008")]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("Thumbnail Persist")]
[assembly: AssemblyDescription("Creates Persistant Taskbar Thumbnail Previews in Vista")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
