﻿// Decompiled with JetBrains decompiler
// Type: TrayPreviewSpace.CacheWindowSize
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

namespace TrayPreviewSpace
{
  public struct CacheWindowSize
  {
    public double aspectratio;
    public double resizeratio;
    public int xsizethumb;
    public int ysizethumb;
    public int cropbottomoffset;
    public int croprightoffset;
    public int croptopoffset;
    public int cropleftoffset;
    public bool alwaysontop;
    public bool nomove;
    public int locationx;
    public int locationy;
  }
}
