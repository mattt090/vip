﻿// Decompiled with JetBrains decompiler
// Type: TrayPreviewSpace.DWM_THUMBNAIL_PROPERTIES
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

namespace TrayPreviewSpace
{
  internal struct DWM_THUMBNAIL_PROPERTIES
  {
    public int dwFlags;
    public Rect rcDestination;
    public Rect rcSource;
    public byte opacity;
    public bool fVisible;
    public bool fSourceClientAreaOnly;
  }
}
