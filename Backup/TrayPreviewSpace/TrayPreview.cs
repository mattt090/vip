﻿// Decompiled with JetBrains decompiler
// Type: TrayPreviewSpace.TrayPreview
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using gma.System.Windows;
using Hook;
using MinimizeCapture;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using TaskbarList;
using ThumbnailPersist;

namespace TrayPreviewSpace
{
  public class TrayPreview : Form
  {
    private static readonly int GWL_STYLE = -16;
    private static readonly int DWM_TNP_VISIBLE = 8;
    private static readonly int DWM_TNP_OPACITY = 4;
    private static readonly int DWM_TNP_RECTDESTINATION = 1;
    private static readonly int DWM_TNP_RECTSOURCE = 2;
    private static readonly int DWM_TNP_SOURCECLIENTAREAONLY = 16;
    private static readonly ulong WS_VISIBLE = 268435456UL;
    private static readonly ulong WS_BORDER = 8388608UL;
    private static readonly ulong TARGETWINDOW = TrayPreview.WS_BORDER | TrayPreview.WS_VISIBLE;
    private static Hooks hook = new Hooks();
    private static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
    private IContainer components = (IContainer) null;
    public WINDOWINFO winfo = new WINDOWINFO();
    public WINDOWINFO winfo2 = new WINDOWINFO();
    public WINDOWINFO winfo3 = new WINDOWINFO();
    public bool window_Destroyed_Processing = false;
    public bool NonTarget = false;
    public bool emptydesktop = false;
    public ArrayList CacheTargetHandles = new ArrayList();
    public ArrayList CacheGridOrder = new ArrayList();
    public ArrayList CachePreviewSize = new ArrayList();
    private bool triploop = false;
    private int LastUsedX = 0;
    private int LastUsedY = 0;
    public WINDOWINFO TargetWindowProperties = new WINDOWINFO();
    public ArrayList TargetProperties = new ArrayList();
    public ArrayList TaskbarTabShown = new ArrayList();
    public ArrayList AlwaysOnTop = new ArrayList();
    public ArrayList NoMove = new ArrayList();
    public bool TrayPreviewProcessing = false;
    private bool window_MinimizeStart_Processing = false;
    private int ArrangeLargestPreviewHeight = 0;
    private int ArrangeLargestPreviewWidth = 0;
    public ArrayList FormCache = new ArrayList();
    private List<Window> windows = new List<Window>();
    public ArrayList PreviewDataLeft = new ArrayList();
    public ArrayList PreviewDataTop = new ArrayList();
    public ArrayList PreviewDataRight = new ArrayList();
    public ArrayList PreviewDataBottom = new ArrayList();
    public ArrayList PreviewDataHandle = new ArrayList();
    public ArrayList ThumbList = new ArrayList();
    public ArrayList TargettedWindows = new ArrayList();
    public ArrayList ViPPreviewHandles = new ArrayList();
    public ArrayList WindowLongStyles = new ArrayList();
    public ArrayList PreviewControls = new ArrayList();
    private int PreviewStartofMinimized = 0;
    private bool MinimizingPreviews = false;
    private bool AllMinimized = false;
    public bool MinimizeHotkeyDown = false;
    public bool Modifier1 = false;
    public const int WM_NCLBUTTONDOWN = 161;
    public const int HTCAPTION = 2;
    private const int SC_RESTORE = 61728;
    private const int WM_SYSCOMMAND = 274;
    private const int SC_MINIMIZE = 61472;
    public const int SC_MAXIMIZE = 61488;
    private const int WM_CLOSE = 16;
    private const int SW_SHOWNORMAL = 1;
    private const int SW_SHOWMINIMIZED = 2;
    private const int SW_SHOWMAXIMIZED = 3;
    public const int WM_LBUTTONDOWN = 513;
    public const int WM_LBUTTONUP = 514;
    public const int WM_RBUTTONDOWN = 516;
    public const int WM_RBUTTONUP = 517;
    public const int WM_COMMAND2 = 274;
    public const int WM_CLOSE2 = 61536;
    private const uint WM_PAINT = 15U;
    private const int WS_EX_LAYERED = 524288;
    private const int LWA_ALPHA = 2;
    private const int SW_SHOWNOACTIVATE = 4;
    private const int HWND_TOPMOST = -1;
    private const uint SWP_NOACTIVATE = 16U;
    private const uint SWP_NOSIZE = 1U;
    private const uint SWP_NOMOVE = 2U;
    private const uint SWP_SHOWWINDOW = 64U;
    private const int GWL_EXSTYLE = -20;
    private const int WS_EX_TOOLWINDOW = 128;
    private const int WS_EX_APPWINDOW = 262144;
    private const int RDW_INVALIDATE = 1;
    private const int RDW_INTERNALPAINT = 2;
    private const int RDW_ERASE = 4;
    private const int RDW_VALIDATE = 8;
    private const int RDW_NOINTERNALPAINT = 16;
    private const int RDW_NOERASE = 32;
    private const int RDW_NOCHILDREN = 64;
    private const int RDW_ALLCHILDREN = 128;
    private const int RDW_UPDATENOW = 256;
    private const int RDW_ERASENOW = 512;
    private const int RDW_FRAME = 1024;
    private const int RDW_NOFRAME = 2048;
    private const int WM_MOUSEWHEEL = 522;
    private const int WM_SIZE = 5;
    private const int SIZE_MINIMIZED = 1;
    public const int LWA_COLORKEY = 1;
    private NotifyIcon notifyIcon1;
    private PictureBox pictureBox1;
    private ContextMenuStrip contextMenuStrip1;
    private ToolStripMenuItem quitToolStripMenuItem;
    private ToolStripMenuItem makePreviewsIgnoreInputToolStripMenuItem;
    private ToolStripMenuItem makePreviewsRespondToInputToolStripMenuItem;
    private ToolStripMenuItem settingsToolStripMenuItem;
    private ToolStripMenuItem restoreAllTargetWindowsToolStripMenuItem;
    private ToolStripMenuItem CreatePreviewsOnMinimize;
    private ToolStripMenuItem PreviewAllOpenWindows;
    private ToolStripMenuItem MinimizePreviews;
    private ToolStripMenuItem ShowPreviews;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripSeparator toolStripSeparator4;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripMenuItem RecoverLostWindows;
    private ToolStripMenuItem requireCTRLKeyToCreatePreviewToolStripMenuItem;
    private ToolStripMenuItem pinPreviewsToToolStripMenuItem;
    private ToolStripMenuItem topmostToolStripMenuItem;
    private ToolStripMenuItem normalToolStripMenuItem;
    private ToolStripMenuItem desktopToolStripMenuItem;
    private ToolStripMenuItem refreshLayoutToolStripMenuItem;
    private ToolStripMenuItem alignmentToolStripMenuItem;
    private ToolStripMenuItem noneToolStripMenuItem;
    private ToolStripMenuItem snapToGridToolStripMenuItem;
    private Timer click_timer;
    private ContextMenuStrip contextMenuStrip2;
    private IntPtr thumb;
    public static long WS_Transparent_Reverse;
    public bool quitting;
    private static UserActivityHook actHook;
    private TaskbarList tblc;
    private bool TripResizeLoop;
    public Point TrayClickPoint;
    private TrayPreview.MARGINS margins;

    public TrayPreview()
    {
      this.quitting = false;
      this.TripResizeLoop = true;
      this.cleardata();
      this.Hide();
      this.InitializeComponent();
      TrayPreview.hook.OnWindowDestroy += new OnWindowDestroyDelegate(this.window_Destroyed);
      TrayPreview.hook.OnWindowMinimizeStart += new OnWindowMinimizeStartDelegate(this.window_MinimizeStart);
      TrayPreview.hook.OnForegroundWindowChanged += new OnForegroundWindowChangedDelegate(this.window_ForegroundChanged);
      GC.KeepAlive((object) TrayPreview.hook);
      this.tblc = (TaskbarList) new TaskbarListClass();
      this.tblc.HrInit();
      TrayPreview.actHook = new UserActivityHook();
      TrayPreview.actHook.KeyDown += new KeyEventHandler(this.MyKeyDown);
      TrayPreview.actHook.KeyUp += new KeyEventHandler(this.MyKeyUp);
      TrayPreview.actHook.Start();
      GC.KeepAlive((object) TrayPreview.actHook);
      this.click_timer.Interval = SystemInformation.DoubleClickTime;
      this.click_timer.Enabled = true;
      TrayPreview.SetWindowLong(this.Handle, -20, TrayPreview.GetWindowLong(this.Handle, -20) | 128L);
      this.GetWindows();
      UserSettings.Default.FirstPreview = true;
      this.TripResizeLoop = false;
      GC.KeepAlive((object) this);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (TrayPreview));
      this.notifyIcon1 = new NotifyIcon(this.components);
      this.contextMenuStrip1 = new ContextMenuStrip(this.components);
      this.quitToolStripMenuItem = new ToolStripMenuItem();
      this.settingsToolStripMenuItem = new ToolStripMenuItem();
      this.RecoverLostWindows = new ToolStripMenuItem();
      this.toolStripSeparator2 = new ToolStripSeparator();
      this.makePreviewsIgnoreInputToolStripMenuItem = new ToolStripMenuItem();
      this.makePreviewsRespondToInputToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator4 = new ToolStripSeparator();
      this.PreviewAllOpenWindows = new ToolStripMenuItem();
      this.restoreAllTargetWindowsToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator3 = new ToolStripSeparator();
      this.MinimizePreviews = new ToolStripMenuItem();
      this.ShowPreviews = new ToolStripMenuItem();
      this.refreshLayoutToolStripMenuItem = new ToolStripMenuItem();
      this.alignmentToolStripMenuItem = new ToolStripMenuItem();
      this.noneToolStripMenuItem = new ToolStripMenuItem();
      this.snapToGridToolStripMenuItem = new ToolStripMenuItem();
      this.pinPreviewsToToolStripMenuItem = new ToolStripMenuItem();
      this.topmostToolStripMenuItem = new ToolStripMenuItem();
      this.normalToolStripMenuItem = new ToolStripMenuItem();
      this.desktopToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.CreatePreviewsOnMinimize = new ToolStripMenuItem();
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem = new ToolStripMenuItem();
      this.pictureBox1 = new PictureBox();
      this.click_timer = new Timer(this.components);
      this.contextMenuStrip2 = new ContextMenuStrip(this.components);
      this.contextMenuStrip1.SuspendLayout();
      ((ISupportInitialize) this.pictureBox1).BeginInit();
      this.SuspendLayout();
      this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
      this.notifyIcon1.Icon = (Icon) componentResourceManager.GetObject("notifyIcon1.Icon");
      this.notifyIcon1.Text = "Video in Picture";
      this.notifyIcon1.Visible = true;
      this.notifyIcon1.MouseMove += new MouseEventHandler(this.notifyIcon1_MouseMove);
      this.notifyIcon1.MouseClick += new MouseEventHandler(this.notifyIcon1_MouseClick);
      this.notifyIcon1.MouseDoubleClick += new MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
      this.contextMenuStrip1.Items.AddRange(new ToolStripItem[18]
      {
        (ToolStripItem) this.quitToolStripMenuItem,
        (ToolStripItem) this.settingsToolStripMenuItem,
        (ToolStripItem) this.RecoverLostWindows,
        (ToolStripItem) this.toolStripSeparator2,
        (ToolStripItem) this.makePreviewsIgnoreInputToolStripMenuItem,
        (ToolStripItem) this.makePreviewsRespondToInputToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator4,
        (ToolStripItem) this.PreviewAllOpenWindows,
        (ToolStripItem) this.restoreAllTargetWindowsToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.MinimizePreviews,
        (ToolStripItem) this.ShowPreviews,
        (ToolStripItem) this.refreshLayoutToolStripMenuItem,
        (ToolStripItem) this.alignmentToolStripMenuItem,
        (ToolStripItem) this.pinPreviewsToToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.CreatePreviewsOnMinimize,
        (ToolStripItem) this.requireCTRLKeyToCreatePreviewToolStripMenuItem
      });
      this.contextMenuStrip1.Name = "contextMenuStrip1";
      this.contextMenuStrip1.ShowCheckMargin = true;
      this.contextMenuStrip1.ShowImageMargin = false;
      this.contextMenuStrip1.Size = new Size(327, 358);
      this.contextMenuStrip1.Opened += new EventHandler(this.contextMenuStrip1_Opened);
      this.contextMenuStrip1.Opening += new CancelEventHandler(this.contextMenuStrip1_Opening);
      this.contextMenuStrip1.Closing += new ToolStripDropDownClosingEventHandler(this.contextMenuStrip1_Closing);
      this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
      this.quitToolStripMenuItem.Size = new Size(326, 22);
      this.quitToolStripMenuItem.Text = "Quit";
      this.quitToolStripMenuItem.Click += new EventHandler(this.quitToolStripMenuItem_Click);
      this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
      this.settingsToolStripMenuItem.Size = new Size(326, 22);
      this.settingsToolStripMenuItem.Text = "Settings";
      this.settingsToolStripMenuItem.Click += new EventHandler(this.settingsToolStripMenuItem_Click);
      this.RecoverLostWindows.Name = "RecoverLostWindows";
      this.RecoverLostWindows.Size = new Size(326, 22);
      this.RecoverLostWindows.Text = "Recover Lost Windows";
      this.RecoverLostWindows.Click += new EventHandler(this.RecoverLostWindows_Click);
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new Size(323, 6);
      this.makePreviewsIgnoreInputToolStripMenuItem.Name = "makePreviewsIgnoreInputToolStripMenuItem";
      this.makePreviewsIgnoreInputToolStripMenuItem.ShortcutKeys = Keys.W | Keys.Control;
      this.makePreviewsIgnoreInputToolStripMenuItem.Size = new Size(326, 22);
      this.makePreviewsIgnoreInputToolStripMenuItem.Text = "Make Previews Ignore Input";
      this.makePreviewsIgnoreInputToolStripMenuItem.Click += new EventHandler(this.makePreviewsIgnoreInputToolStripMenuItem_Click);
      this.makePreviewsRespondToInputToolStripMenuItem.Name = "makePreviewsRespondToInputToolStripMenuItem";
      this.makePreviewsRespondToInputToolStripMenuItem.ShortcutKeys = Keys.Q | Keys.Control;
      this.makePreviewsRespondToInputToolStripMenuItem.Size = new Size(326, 22);
      this.makePreviewsRespondToInputToolStripMenuItem.Text = "Make Previews Respond to Input";
      this.makePreviewsRespondToInputToolStripMenuItem.Click += new EventHandler(this.makePreviewsRespondToInputToolStripMenuItem_Click);
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      this.toolStripSeparator4.Size = new Size(323, 6);
      this.PreviewAllOpenWindows.Name = "PreviewAllOpenWindows";
      this.PreviewAllOpenWindows.ShortcutKeys = Keys.A | Keys.Control;
      this.PreviewAllOpenWindows.Size = new Size(326, 22);
      this.PreviewAllOpenWindows.Text = "Preview All Open Windows";
      this.PreviewAllOpenWindows.Click += new EventHandler(this.PreviewAllOpenWindows_Click);
      this.restoreAllTargetWindowsToolStripMenuItem.Name = "restoreAllTargetWindowsToolStripMenuItem";
      this.restoreAllTargetWindowsToolStripMenuItem.ShortcutKeys = Keys.X | Keys.Control;
      this.restoreAllTargetWindowsToolStripMenuItem.Size = new Size(326, 22);
      this.restoreAllTargetWindowsToolStripMenuItem.Text = "Restore All Target Windows";
      this.restoreAllTargetWindowsToolStripMenuItem.Click += new EventHandler(this.restoreAllTargetWindowsToolStripMenuItem_Click);
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new Size(323, 6);
      this.MinimizePreviews.Name = "MinimizePreviews";
      this.MinimizePreviews.ShortcutKeys = Keys.Z | Keys.Control;
      this.MinimizePreviews.Size = new Size(326, 22);
      this.MinimizePreviews.Text = "Minimize All Previews";
      this.MinimizePreviews.Click += new EventHandler(this.MinimizePreviews_Click);
      this.ShowPreviews.Name = "ShowPreviews";
      this.ShowPreviews.ShortcutKeys = Keys.S | Keys.Control;
      this.ShowPreviews.Size = new Size(326, 22);
      this.ShowPreviews.Text = "Show All Previews";
      this.ShowPreviews.Click += new EventHandler(this.ShowPreviews_Click);
      this.refreshLayoutToolStripMenuItem.Name = "refreshLayoutToolStripMenuItem";
      this.refreshLayoutToolStripMenuItem.ShortcutKeys = Keys.R | Keys.Control;
      this.refreshLayoutToolStripMenuItem.Size = new Size(326, 22);
      this.refreshLayoutToolStripMenuItem.Text = "Refresh Layout";
      this.refreshLayoutToolStripMenuItem.Click += new EventHandler(this.refreshLayoutToolStripMenuItem_Click);
      this.alignmentToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.noneToolStripMenuItem,
        (ToolStripItem) this.snapToGridToolStripMenuItem
      });
      this.alignmentToolStripMenuItem.Name = "alignmentToolStripMenuItem";
      this.alignmentToolStripMenuItem.Size = new Size(326, 22);
      this.alignmentToolStripMenuItem.Text = "Alignment";
      this.noneToolStripMenuItem.CheckOnClick = true;
      this.noneToolStripMenuItem.Name = "noneToolStripMenuItem";
      this.noneToolStripMenuItem.Size = new Size(139, 22);
      this.noneToolStripMenuItem.Text = "None";
      this.noneToolStripMenuItem.Click += new EventHandler(this.noneToolStripMenuItem_Click);
      this.snapToGridToolStripMenuItem.CheckOnClick = true;
      this.snapToGridToolStripMenuItem.Name = "snapToGridToolStripMenuItem";
      this.snapToGridToolStripMenuItem.Size = new Size(139, 22);
      this.snapToGridToolStripMenuItem.Text = "Snap to Grid";
      this.snapToGridToolStripMenuItem.Click += new EventHandler(this.snapToGridToolStripMenuItem_Click);
      this.pinPreviewsToToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
      {
        (ToolStripItem) this.topmostToolStripMenuItem,
        (ToolStripItem) this.normalToolStripMenuItem,
        (ToolStripItem) this.desktopToolStripMenuItem
      });
      this.pinPreviewsToToolStripMenuItem.Name = "pinPreviewsToToolStripMenuItem";
      this.pinPreviewsToToolStripMenuItem.Size = new Size(326, 22);
      this.pinPreviewsToToolStripMenuItem.Text = "Pin Previews To......";
      this.topmostToolStripMenuItem.CheckOnClick = true;
      this.topmostToolStripMenuItem.Name = "topmostToolStripMenuItem";
      this.topmostToolStripMenuItem.Size = new Size(122, 22);
      this.topmostToolStripMenuItem.Text = "Topmost";
      this.topmostToolStripMenuItem.Click += new EventHandler(this.topmostToolStripMenuItem_Click);
      this.normalToolStripMenuItem.CheckOnClick = true;
      this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
      this.normalToolStripMenuItem.Size = new Size(122, 22);
      this.normalToolStripMenuItem.Text = "Normal";
      this.normalToolStripMenuItem.Click += new EventHandler(this.normalToolStripMenuItem_Click);
      this.desktopToolStripMenuItem.CheckOnClick = true;
      this.desktopToolStripMenuItem.Name = "desktopToolStripMenuItem";
      this.desktopToolStripMenuItem.Size = new Size(122, 22);
      this.desktopToolStripMenuItem.Text = "Desktop";
      this.desktopToolStripMenuItem.Click += new EventHandler(this.desktopToolStripMenuItem_Click);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(323, 6);
      this.CreatePreviewsOnMinimize.CheckOnClick = true;
      this.CreatePreviewsOnMinimize.Name = "CreatePreviewsOnMinimize";
      this.CreatePreviewsOnMinimize.ShortcutKeys = Keys.Space | Keys.Control;
      this.CreatePreviewsOnMinimize.Size = new Size(326, 22);
      this.CreatePreviewsOnMinimize.Text = "Create Previews on Minimize";
      this.CreatePreviewsOnMinimize.Click += new EventHandler(this.CreatePreviewsOnMinimize_Click);
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem.CheckOnClick = true;
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem.Name = "requireCTRLKeyToCreatePreviewToolStripMenuItem";
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem.ShortcutKeys = Keys.Space | Keys.Alt;
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem.Size = new Size(326, 22);
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem.Text = "Require SHIFT Key to Create Preview";
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem.Click += new EventHandler(this.requireCTRLKeyToCreatePreviewToolStripMenuItem_Click);
      this.pictureBox1.AccessibleRole = AccessibleRole.Window;
      this.pictureBox1.BackColor = Color.Transparent;
      this.pictureBox1.Cursor = Cursors.Hand;
      this.pictureBox1.Dock = DockStyle.Fill;
      this.pictureBox1.Location = new Point(0, 0);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new Size(91, 24);
      this.pictureBox1.TabIndex = 0;
      this.pictureBox1.TabStop = false;
      this.pictureBox1.MouseLeave += new EventHandler(this.pictureBox1_MouseLeave);
      this.pictureBox1.MouseClick += new MouseEventHandler(this.pictureBox1_MouseClick);
      this.pictureBox1.MouseHover += new EventHandler(this.pictureBox1_MouseHover);
      this.click_timer.Tick += new EventHandler(this.click_timer_Tick);
      this.contextMenuStrip2.Name = "contextMenuStrip2";
      this.contextMenuStrip2.Size = new Size(61, 4);
      this.AccessibleRole = AccessibleRole.Window;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoScroll = true;
      this.BackColor = SystemColors.GradientActiveCaption;
      this.ClientSize = new Size(108, 20);
      this.ControlBox = false;
      this.Controls.Add((Control) this.pictureBox1);
      this.KeyPreview = true;
      this.Name = "TrayPreview";
      this.Opacity = 0.0;
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.Manual;
      this.TopMost = true;
      this.TransparencyKey = Color.Transparent;
      this.Deactivate += new EventHandler(this.TrayPreview_Deactivate);
      this.Load += new EventHandler(this.TrayPreview_Load);
      this.Shown += new EventHandler(this.TrayPreview_Shown);
      this.FormClosed += new FormClosedEventHandler(this.TrayPreview_FormClosed);
      this.FormClosing += new FormClosingEventHandler(this.TrayPreview_FormClosing);
      this.Resize += new EventHandler(this.TrayPreview_Resize);
      this.KeyDown += new KeyEventHandler(this.TrayPreview_KeyDown);
      this.MouseHover += new EventHandler(this.TrayPreview_MouseHover);
      this.contextMenuStrip1.ResumeLayout(false);
      ((ISupportInitialize) this.pictureBox1).EndInit();
      this.ResumeLayout(false);
    }

    [DllImport("dwmapi.dll")]
    private static extern int DwmRegisterThumbnail(IntPtr dest, IntPtr src, out IntPtr thumb);

    [DllImport("dwmapi.dll")]
    private static extern int DwmUnregisterThumbnail(IntPtr thumb);

    [DllImport("dwmapi.dll")]
    private static extern int DwmQueryThumbnailSourceSize(IntPtr thumb, out PSIZE size);

    [DllImport("dwmapi.dll")]
    private static extern int DwmUpdateThumbnailProperties(IntPtr hThumb, ref DWM_THUMBNAIL_PROPERTIES props);

    [DllImport("SHELL32", CallingConvention = CallingConvention.StdCall)]
    private static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);

    [DllImport("USER32")]
    private static extern int GetSystemMetrics(int Index);

    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    private static extern bool MoveWindow(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);

    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    private static extern int RegisterWindowMessage(string msg);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr GetParent(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern ulong GetWindowLongA(IntPtr hWnd, int nIndex);

    [DllImport("user32.dll")]
    private static extern int EnumWindows(TrayPreview.EnumWindowsCallback lpEnumFunc, int lParam);

    [DllImport("user32.dll")]
    public static extern void GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

    [DllImport("user32.dll")]
    private static extern bool SetForegroundWindow(int hwnd);

    [DllImport("User32.dll")]
    private static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern int GetDesktopWindow();

    [DllImport("user32.dll")]
    private static extern int FindWindow(string className, string windowText);

    [DllImport("user32")]
    public static extern int MoveWindow(int hwnd, int x, int y, int nWidth, int nHeight, int bRepaint);

    [DllImport("user32.dll")]
    private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);

    [DllImport("user32")]
    private static extern int SetLayeredWindowAttributes(IntPtr hWnd, byte crey, byte alpha, int flags);

    [DllImport("user32.dll")]
    private static extern uint SendMessage(IntPtr hWnd, uint msg, uint wParam, uint lParam);

    [DllImport("user32.dll")]
    public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

    [DllImport("user32.dll")]
    private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPos(int hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

    [DllImport("user32")]
    public static extern int SetCursorPos(int x, int y);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

    [DllImport("user32.dll")]
    private static extern long GetWindowLong(IntPtr hWnd, int nIndex);

    [DllImport("user32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool ShowWindow(IntPtr hWnd, TrayPreview.ShowWindowEnum flags);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

    [DllImport("user32.dll")]
    private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, [In] ref RECT lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool IsIconic(IntPtr hWnd);

    [DllImport("user32")]
    private static extern uint GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

    public static int GetWindowProcessID(int hwnd)
    {
      int lpdwProcessId = 1;
      int num = (int) TrayPreview.GetWindowThreadProcessId(hwnd, out lpdwProcessId);
      return lpdwProcessId;
    }

    private void window_Destroyed(IntPtr hWnd)
    {
      GC.KeepAlive((object) TrayPreview.hook);
      if (this.quitting)
        return;
      this.NonTarget = false;
      this.window_Destroyed_Processing = false;
      if (!this.TargettedWindows.Contains((object) hWnd) && !this.ViPPreviewHandles.Contains((object) hWnd))
      {
        if (this.CacheTargetHandles.Contains((object) hWnd))
        {
          int index = this.CacheTargetHandles.IndexOf((object) hWnd);
          this.CacheTargetHandles.RemoveAt(index);
          this.CacheGridOrder.RemoveAt(index);
          this.CachePreviewSize.RemoveAt(index);
          if (this.CacheTargetHandles.Count > 500)
            Debug.WriteLine("Number of caches is over 500");
        }
        foreach (Window window in this.windows)
        {
          if (window.Handle == hWnd)
          {
            this.NonTarget = true;
            this.HideTaskbarTab();
            return;
          }
        }
        this.GetWindows();
        if (this.TargettedWindows.Count >= this.windows.Count)
        {
          this.NonTarget = true;
          this.emptydesktop = true;
          this.HideTaskbarTab();
          return;
        }
      }
      else if (this.TargettedWindows.Contains((object) hWnd))
      {
        int index1 = this.TargettedWindows.IndexOf((object) hWnd);
        this.window_Destroyed_Processing = true;
        TrayPreview.SendMessage((IntPtr) long.Parse(this.ViPPreviewHandles[index1].ToString()), 274, 61536, 0);
        if (this.CacheTargetHandles.Contains((object) hWnd))
        {
          int index2 = this.CacheTargetHandles.IndexOf((object) hWnd);
          this.CacheTargetHandles.RemoveAt(index2);
          this.CacheGridOrder.RemoveAt(index2);
          this.CachePreviewSize.RemoveAt(index2);
          if (this.CacheTargetHandles.Count > 500)
            Debug.WriteLine("Number of caches is over 500");
        }
      }
      else if (this.ViPPreviewHandles.Contains((object) hWnd))
      {
        int index = this.ViPPreviewHandles.IndexOf((object) hWnd);
        this.window_Destroyed_Processing = true;
        this.TaskbarTabShown.RemoveAt(index);
        this.ViPPreviewHandles.RemoveAt(index);
        this.TargettedWindows.RemoveAt(index);
        this.PreviewControls.RemoveAt(index);
        this.AlwaysOnTop.RemoveAt(index);
        this.NoMove.RemoveAt(index);
        if (UserSettings.Default.PreviewGridAlign == "Snap")
          this.ReAlign();
      }
      this.HideTaskbarTab();
    }

    public void CheckAndInsert(IntPtr hWnd, Point p)
    {
      if (UserSettings.Default.PreviewGridAlign != "Snap")
        return;
      foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
      {
        if (thumbnailWindow.Handle != hWnd)
        {
          Point location = thumbnailWindow.Location;
          int x1 = location.X;
          location = thumbnailWindow.Location;
          int y1 = location.Y;
          Point point1 = new Point(x1, y1);
          location = thumbnailWindow.Location;
          int x2 = location.X;
          Size size = thumbnailWindow.Size;
          int width = size.Width;
          int x3 = x2 + width;
          location = thumbnailWindow.Location;
          int y2 = location.Y;
          size = thumbnailWindow.Size;
          int height = size.Height;
          int y3 = y2 + height;
          Point point2 = new Point(x3, y3);
          if (p.X >= point1.X && p.X <= point2.X && p.Y >= point1.Y && p.Y <= point2.Y)
          {
            this.SwapAndRemove(this.PreviewControls.IndexOf((object) thumbnailWindow), this.ViPPreviewHandles.IndexOf((object) hWnd));
            this.ReAlign();
            break;
          }
        }
      }
    }

    public void SwapAndRemove(int NewIndex, int OldIndex)
    {
      if (NewIndex < OldIndex)
      {
        this.TaskbarTabShown.Insert(NewIndex, (object) (bool) ((bool) this.TaskbarTabShown[OldIndex] ? 1 : 0));
        this.TargettedWindows.Insert(NewIndex, (object) (IntPtr) this.TargettedWindows[OldIndex]);
        this.AlwaysOnTop.Insert(NewIndex, (object) (bool) ((bool) this.AlwaysOnTop[OldIndex] ? 1 : 0));
        this.NoMove.Insert(NewIndex, (object) (bool) ((bool) this.NoMove[OldIndex] ? 1 : 0));
        this.PreviewControls.Insert(NewIndex, (object) (ThumbnailWindow) this.PreviewControls[OldIndex]);
        this.ViPPreviewHandles.Insert(NewIndex, (object) (IntPtr) this.ViPPreviewHandles[OldIndex]);
        this.TaskbarTabShown.RemoveAt(OldIndex + 1);
        this.TargettedWindows.RemoveAt(OldIndex + 1);
        this.AlwaysOnTop.RemoveAt(OldIndex + 1);
        this.NoMove.RemoveAt(OldIndex + 1);
        this.PreviewControls.RemoveAt(OldIndex + 1);
        this.ViPPreviewHandles.RemoveAt(OldIndex + 1);
      }
      else
      {
        this.TaskbarTabShown.Insert(NewIndex + 1, (object) (bool) ((bool) this.TaskbarTabShown[OldIndex] ? 1 : 0));
        this.TargettedWindows.Insert(NewIndex + 1, (object) (IntPtr) this.TargettedWindows[OldIndex]);
        this.AlwaysOnTop.Insert(NewIndex + 1, (object) (bool) ((bool) this.AlwaysOnTop[OldIndex] ? 1 : 0));
        this.NoMove.Insert(NewIndex + 1, (object) (bool) ((bool) this.NoMove[OldIndex] ? 1 : 0));
        this.PreviewControls.Insert(NewIndex + 1, (object) (ThumbnailWindow) this.PreviewControls[OldIndex]);
        this.ViPPreviewHandles.Insert(NewIndex + 1, (object) (IntPtr) this.ViPPreviewHandles[OldIndex]);
        this.TaskbarTabShown.RemoveAt(OldIndex);
        this.TargettedWindows.RemoveAt(OldIndex);
        this.AlwaysOnTop.RemoveAt(OldIndex);
        this.NoMove.RemoveAt(OldIndex);
        this.PreviewControls.RemoveAt(OldIndex);
        this.ViPPreviewHandles.RemoveAt(OldIndex);
      }
      this.RenumberCache();
    }

    public IntPtr FirstPreviewNotAlwaysOnTop()
    {
      int index = 0;
      IntPtr num1 = IntPtr.Zero;
      foreach (IntPtr num2 in this.ViPPreviewHandles)
      {
        if (!(bool) this.AlwaysOnTop[index])
        {
          num1 = num2;
          break;
        }
        ++index;
      }
      return num1;
    }

    public void RenumberCache()
    {
      foreach (IntPtr num in this.TargettedWindows)
      {
        int index = this.CacheTargetHandles.IndexOf((object) num);
        if (index >= 0)
          this.CacheGridOrder[index] = (object) this.TargettedWindows.IndexOf((object) num);
      }
    }

    public void ZoomAll(string UpDown)
    {
      KeyEventArgs keyEventArgs;
      if (UpDown == "Down")
      {
        keyEventArgs = new KeyEventArgs(Keys.Down);
        foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
          thumbnailWindow.Zoom(UpDown);
      }
      else
      {
        keyEventArgs = new KeyEventArgs(Keys.Up);
        foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
          thumbnailWindow.Zoom(UpDown);
      }
      this.ReAlign();
    }

    private void window_MinimizeEnd(IntPtr hWnd)
    {
      GC.KeepAlive((object) TrayPreview.hook);
      this.GetWindows();
      this.HideTaskbarTab();
    }

    private void window_ForegroundChanged(IntPtr hWnd)
    {
      GC.KeepAlive((object) TrayPreview.hook);
      this.GetWindows();
      if (this.triploop)
        return;
      if (this.emptydesktop)
        this.emptydesktop = false;
      if (hWnd == this.Handle && !this.TargettedWindows.Contains((object) hWnd))
      {
        this.window_MinimizeStart_Processing = false;
        this.window_Destroyed_Processing = false;
        this.NonTarget = false;
        this.HideTaskbarTab();
      }
      else if (this.NonTarget || this.window_MinimizeStart_Processing || this.window_Destroyed_Processing || this.TrayPreviewProcessing)
      {
        this.window_MinimizeStart_Processing = false;
        this.window_Destroyed_Processing = false;
        this.NonTarget = false;
        this.TrayPreviewProcessing = false;
        this.HideTaskbarTab();
      }
      else
      {
        this.window_MinimizeStart_Processing = false;
        this.window_Destroyed_Processing = false;
        this.NonTarget = false;
        int index = this.TargettedWindows.IndexOf((object) hWnd);
        if (index >= 0 && !this.TrayPreviewProcessing && !this.MinimizingPreviews && this.TargettedWindows.Contains((object) hWnd))
        {
          ThumbnailWindow thumbnailWindow = (ThumbnailWindow) this.PreviewControls[index];
          if (!thumbnailWindow.GetProcessingState() && !thumbnailWindow.GetShownState())
          {
            this.triploop = true;
            TrayPreview.ShowWindow((IntPtr) this.ViPPreviewHandles[index], 1);
            TrayPreview.SetForegroundWindow((IntPtr) this.ViPPreviewHandles[index]);
            this.triploop = false;
          }
        }
        this.HideTaskbarTab();
      }
    }

    private void window_MinimizeStart(IntPtr hWnd)
    {
      GC.KeepAlive((object) TrayPreview.hook);
      this.GetWindows();
      this.TrayPreviewProcessing = true;
      this.window_MinimizeStart_Processing = true;
      bool flag1;
      if (UserSettings.Default.KeepTargettedFromMinimizing && UserSettings.Default.MoveTargetOffscreen && this.TargettedWindows.Contains((object) hWnd))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        ((ThumbnailWindow) this.PreviewControls[this.TargettedWindows.IndexOf((object) hWnd)]).hideTargetWindowToolStripMenuItem_Click((object) this, (EventArgs) null);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      else if (UserSettings.Default.KeepTargettedFromMinimizing && this.TargettedWindows.Contains((object) hWnd))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        ((ThumbnailWindow) this.PreviewControls[this.TargettedWindows.IndexOf((object) hWnd)]).hideTargetWindowToolStripMenuItem_Click((object) this, (EventArgs) null);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      else if (this.ViPPreviewHandles.Contains((object) hWnd))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        Debug.WriteLine("preview minimized");
        TrayPreview.SendMessage(hWnd, 274, 61728, 0);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      else if (UserSettings.Default.CreatePreviewsOnMinimize)
      {
        if (UserSettings.Default.CreatePreviewOnlyOnHotkey && !this.MinimizeHotkeyDown)
          return;
        bool flag2 = false;
        foreach (Window window in this.windows)
        {
          if (window.Handle == hWnd)
          {
            flag2 = true;
            break;
          }
        }
        if (flag2 && hWnd != this.Handle && !this.TargettedWindows.Contains((object) hWnd))
        {
          if (TrayPreview.IsIconic(hWnd))
            this.CreatePreview(hWnd, true);
          else
            this.CreatePreview(hWnd, false);
        }
      }
      this.HideTaskbarTab();
      this.TrayPreviewProcessing = false;
    }

    private void restoreAllTargetWindowsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (IntPtr hWnd in this.ViPPreviewHandles)
      {
        this.TrayPreviewProcessing = true;
        TrayPreview.SendMessage(hWnd, 274, 61536, 0);
        this.TrayPreviewProcessing = false;
      }
    }

    public void ClosePreview(IntPtr hWnd)
    {
      TrayPreview.SendMessage(hWnd, 274, 61536, 0);
    }

    private void CreatePreview(IntPtr hWnd, bool NeedtoRestore)
    {
      this.TrayPreviewProcessing = true;
      long windowLong = TrayPreview.GetWindowLong(hWnd, -20);
      bool WindowIsMaximized = false;
      this.TaskbarTabShown.Add((object) false);
      this.TargettedWindows.Add((object) hWnd);
      this.AlwaysOnTop.Add((object) false);
      this.NoMove.Add((object) false);
      ThumbnailWindow newpreview;
      if (this.CacheTargetHandles.Contains((object) hWnd))
      {
        CacheWindowSize cacheWindowSize = new CacheWindowSize();
        CacheWindowSize OverRidePreviewParams = (CacheWindowSize) this.CachePreviewSize[this.CacheTargetHandles.IndexOf((object) hWnd)];
        newpreview = new ThumbnailWindow(4, hWnd, windowLong, WindowIsMaximized, this, OverRidePreviewParams);
      }
      else
        newpreview = new ThumbnailWindow(3, hWnd, windowLong, WindowIsMaximized, this);
      newpreview.Show();
      newpreview.BringToFront();
      newpreview.TopMost = true;
      this.PreviewControls.Add((object) newpreview);
      this.ViPPreviewHandles.Add((object) newpreview.Handle);
      bool flag1 = false;
      if (this.CacheTargetHandles.Contains((object) hWnd))
      {
        int index1 = (int) this.CacheGridOrder[this.CacheTargetHandles.IndexOf((object) hWnd)];
        int index2 = this.TargettedWindows.IndexOf((object) hWnd);
        CacheWindowSize cacheWindowSize1 = new CacheWindowSize();
        CacheWindowSize cacheWindowSize2 = (CacheWindowSize) this.CachePreviewSize[index1];
        bool flag2 = (bool) this.TaskbarTabShown[index2];
        bool flag3 = cacheWindowSize2.alwaysontop;
        bool flag4 = cacheWindowSize2.nomove;
        if (index1 > this.TargettedWindows.Count - 1)
          index1 = this.TargettedWindows.Count - 1;
        if (index1 != index2)
        {
          this.TaskbarTabShown.RemoveAt(index2);
          this.TargettedWindows.RemoveAt(index2);
          this.AlwaysOnTop.RemoveAt(index2);
          this.NoMove.RemoveAt(index2);
          this.PreviewControls.RemoveAt(index2);
          this.ViPPreviewHandles.RemoveAt(index2);
          this.TaskbarTabShown.Insert(index1, (object) (bool) (flag2 ? 1 : 0));
          this.TargettedWindows.Insert(index1, (object) hWnd);
          this.AlwaysOnTop.Insert(index1, (object) (bool) (flag3 ? 1 : 0));
          this.NoMove.Insert(index1, (object) (bool) (flag4 ? 1 : 0));
          this.PreviewControls.Insert(index1, (object) newpreview);
          this.ViPPreviewHandles.Insert(index1, (object) newpreview.Handle);
        }
        newpreview.SavePreviewCacheParams();
      }
      else
      {
        this.CacheTargetHandles.Add((object) hWnd);
        this.CacheGridOrder.Add((object) this.TargettedWindows.IndexOf((object) hWnd));
        this.CachePreviewSize.Add((object) new CacheWindowSize());
        newpreview.SavePreviewCacheParams();
        flag1 = true;
      }
      this.TrayPreviewProcessing = false;
      int index = this.PreviewControls.IndexOf((object) newpreview);
      if ((bool) this.AlwaysOnTop[index] && (bool) this.NoMove[index])
      {
        newpreview.SetAlwaysTopNoMove();
        newpreview.Show();
        newpreview.RestoreTransparency();
      }
      else if (!(UserSettings.Default.PreviewGridAlign == "Snap") && !flag1)
      {
        newpreview.Show();
        newpreview.RestoreTransparency();
      }
      else if (UserSettings.Default.PreviewGridAlign == "Snap")
        this.ReAlign(newpreview);
      else
        this.arrangepreviews(newpreview, 0);
    }

    public void ReAlign(ThumbnailWindow newpreview)
    {
      UserSettings.Default.FirstPreview = true;
      int index = 0;
      foreach (ThumbnailWindow newpreview1 in this.PreviewControls)
      {
        if (newpreview1 == newpreview)
          this.arrangepreviews(newpreview, 0);
        else if (!(bool) this.AlwaysOnTop[index] && !(bool) this.NoMove[index])
          this.arrangepreviews(newpreview1, 1);
        ++index;
      }
    }

    public void ReAlign()
    {
      UserSettings.Default.FirstPreview = true;
      int index = 0;
      foreach (ThumbnailWindow newpreview in this.PreviewControls)
      {
        if (!(bool) this.AlwaysOnTop[index] && !(bool) this.NoMove[index])
          this.arrangepreviews(newpreview, 1);
        ++index;
      }
    }

    private void refreshLayoutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.FirstPreview = true;
      int index = 0;
      foreach (ThumbnailWindow newpreview in this.PreviewControls)
      {
        if (!(bool) this.AlwaysOnTop[index] && !(bool) this.NoMove[index])
        {
          newpreview.Position_Screen_Start();
          this.arrangepreviews(newpreview, 0);
        }
        ++index;
      }
    }

    private void arrangepreviews(ThumbnailWindow newpreview, int state)
    {
      APPBARDATA pData = new APPBARDATA();
      int num1 = (int) TrayPreview.SHAppBarMessage(5, ref pData);
      int num2 = (int) TrayPreview.SHAppBarMessage(4, ref pData);
      int num3 = 0;
      int num4 = 0;
      int y1 = 0;
      int x1 = SystemInformation.PrimaryMonitorSize.Width;
      int num5 = SystemInformation.PrimaryMonitorSize.Height;
      Size size;
      if (!UserSettings.Default.CustomTaskbarMargins)
      {
        int num6 = pData.rc.bottom;
        size = SystemInformation.PrimaryMonitorSize;
        int height1 = size.Height;
        int num7;
        if (num6 == height1 && pData.rc.left == 0)
        {
          int num8 = pData.rc.right;
          size = SystemInformation.PrimaryMonitorSize;
          int width = size.Width;
          num7 = num8 != width ? 1 : 0;
        }
        else
          num7 = 1;
        if (num7 == 0)
        {
          num3 = 1;
          int num8 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
          num5 += num8;
        }
        else
        {
          int num8;
          if (pData.rc.top == 0 && pData.rc.left == 0)
          {
            int num9 = pData.rc.right;
            size = SystemInformation.PrimaryMonitorSize;
            int width = size.Width;
            num8 = num9 != width ? 1 : 0;
          }
          else
            num8 = 1;
          if (num8 == 0)
          {
            num3 = 2;
            int num9 = pData.rc.bottom;
            y1 += num9;
          }
          else
          {
            int num9;
            if (pData.rc.top == 0 && pData.rc.left == 0)
            {
              int num10 = pData.rc.bottom;
              size = SystemInformation.PrimaryMonitorSize;
              int height2 = size.Height;
              num9 = num10 != height2 ? 1 : 0;
            }
            else
              num9 = 1;
            if (num9 == 0)
            {
              num3 = 3;
              int num10 = pData.rc.right;
              num4 += num10;
            }
            else
            {
              int num10;
              if (pData.rc.top == 0)
              {
                int num11 = pData.rc.right;
                size = SystemInformation.PrimaryMonitorSize;
                int width = size.Width;
                if (num11 == width)
                {
                  int num12 = pData.rc.bottom;
                  size = SystemInformation.PrimaryMonitorSize;
                  int height2 = size.Height;
                  num10 = num12 != height2 ? 1 : 0;
                  goto label_20;
                }
              }
              num10 = 1;
label_20:
              if (num10 == 0)
              {
                num3 = 4;
                int num11 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
                x1 += num11;
              }
            }
          }
        }
      }
      else
      {
        num4 = UserSettings.Default.CustomTaskbarMarginsLeft;
        y1 = UserSettings.Default.CustomTaskbarMarginsTop;
        x1 = SystemInformation.PrimaryMonitorSize.Width - UserSettings.Default.CustomTaskbarMarginsRight;
        num5 = SystemInformation.PrimaryMonitorSize.Height - UserSettings.Default.CustomTaskbarMarginsBottom;
      }
      if (UserSettings.Default.DockPreview && (UserSettings.Default.startbottom || UserSettings.Default.starttop))
      {
        if (UserSettings.Default.FirstPreview)
        {
          int x2 = newpreview.Location.X;
          size = newpreview.Size;
          int width = size.Width;
          this.LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
          UserSettings.Default.FirstPreview = false;
        }
        else
        {
          int num6 = this.LastUsedX;
          size = newpreview.Size;
          int width1 = size.Width;
          if (num6 + width1 > x1)
          {
            this.LastUsedX = num4;
          }
          else
          {
            if (newpreview.Location.X != this.LastUsedX)
              newpreview.Location = new Point(this.LastUsedX, newpreview.Location.Y);
            int x2 = newpreview.Location.X;
            size = newpreview.Size;
            int width2 = size.Width;
            this.LastUsedX = x2 + width2 + UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
        }
      }
      else if (UserSettings.Default.DockPreview && (UserSettings.Default.startleft || UserSettings.Default.startright))
      {
        if (UserSettings.Default.FirstPreview)
        {
          int y2 = newpreview.Location.Y;
          size = newpreview.Size;
          int height = size.Height;
          this.LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
          UserSettings.Default.FirstPreview = false;
        }
        else
        {
          int num6 = this.LastUsedY;
          size = newpreview.Size;
          int height1 = size.Height;
          if (num6 + height1 > num5)
          {
            this.LastUsedY = num4;
          }
          else
          {
            if (newpreview.Location.Y != this.LastUsedY)
              newpreview.Location = new Point(newpreview.Location.X, this.LastUsedY);
            int y2 = newpreview.Location.Y;
            size = newpreview.Size;
            int height2 = size.Height;
            this.LastUsedY = y2 + height2 + UserSettings.Default.PreviewLayoutVerticalSpacing;
          }
        }
      }
      else if (this.ViPPreviewHandles.Count == 0 || UserSettings.Default.FirstPreview)
      {
        if (UserSettings.Default.RowsFirst)
        {
          if (UserSettings.Default.ArrangePreviewXDirection == "Right")
          {
            int x2 = newpreview.Location.X;
            size = newpreview.Size;
            int width = size.Width;
            this.LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int y2 = newpreview.Location.Y;
              size = newpreview.Size;
              int height = size.Height;
              this.LastUsedY = y2 + height;
            }
            else
              this.LastUsedY = newpreview.Location.Y;
          }
          else
          {
            this.LastUsedX = newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int y2 = newpreview.Location.Y;
              size = newpreview.Size;
              int height = size.Height;
              this.LastUsedY = y2 + height;
            }
            else
              this.LastUsedY = newpreview.Location.Y;
          }
          if (this.LastUsedX < num4 || this.LastUsedX > x1)
          {
            this.LastUsedX = !(UserSettings.Default.ArrangePreviewXDirection == "Right") ? x1 : num4;
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              this.LastUsedY = newpreview.Location.Y - UserSettings.Default.PreviewLayoutVerticalSpacing;
            }
            else
            {
              int y2 = newpreview.Location.Y;
              size = newpreview.Size;
              int height = size.Height;
              this.LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
            }
          }
        }
        else
        {
          if (UserSettings.Default.ArrangePreviewYDirection == "Down")
          {
            int y2 = newpreview.Location.Y;
            size = newpreview.Size;
            int height = size.Height;
            this.LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
            this.LastUsedX = !(UserSettings.Default.ArrangePreviewXDirection == "Left") ? newpreview.Location.X : newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
          else
          {
            this.LastUsedY = newpreview.Location.Y - UserSettings.Default.PreviewLayoutVerticalSpacing;
            this.LastUsedX = !(UserSettings.Default.ArrangePreviewXDirection == "Left") ? newpreview.Location.X : newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
          if (this.LastUsedY < y1 || this.LastUsedY > num5)
          {
            this.LastUsedY = !(UserSettings.Default.ArrangePreviewYDirection == "Down") ? num5 : y1;
            if (UserSettings.Default.ArrangePreviewXDirection == "Left")
            {
              this.LastUsedX = newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
            }
            else
            {
              int x2 = newpreview.Location.X;
              size = newpreview.Size;
              int width = size.Width;
              this.LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
            }
          }
        }
        size = newpreview.Size;
        this.ArrangeLargestPreviewHeight = size.Height;
        size = newpreview.Size;
        this.ArrangeLargestPreviewWidth = size.Width;
        UserSettings.Default.FirstPreview = false;
      }
      else
      {
        size = newpreview.Size;
        if (size.Height > this.ArrangeLargestPreviewHeight)
        {
          size = newpreview.Size;
          this.ArrangeLargestPreviewHeight = size.Height;
        }
        size = newpreview.Size;
        if (size.Width > this.ArrangeLargestPreviewWidth)
        {
          size = newpreview.Size;
          this.ArrangeLargestPreviewWidth = size.Width;
        }
        if (UserSettings.Default.RowsFirst)
        {
          if (UserSettings.Default.ArrangePreviewXDirection == "Right")
          {
            int num6 = this.LastUsedX;
            size = newpreview.Size;
            int width = size.Width;
            if (num6 + width > x1)
            {
              this.LastUsedX = num4;
              if (UserSettings.Default.ArrangePreviewYDirection == "Up")
              {
                this.LastUsedY = this.LastUsedY - (this.ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing);
                int num7 = this.LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 - height < y1)
                  this.LastUsedY = num5;
              }
              else
              {
                this.LastUsedY = this.LastUsedY + this.ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing;
                int num7 = this.LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 + height > num5)
                  this.LastUsedY = y1;
              }
            }
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int num7 = this.LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 - height1 < y1)
              {
                if (newpreview.Location.X != this.LastUsedX || newpreview.Location.Y != y1)
                  newpreview.Location = new Point(this.LastUsedX, y1);
              }
              else
              {
                int num8;
                if (newpreview.Location.X == this.LastUsedX)
                {
                  int y2 = newpreview.Location.Y;
                  int num9 = this.LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num10 = num9 - height2;
                  num8 = y2 == num10 ? 1 : 0;
                }
                else
                  num8 = 0;
                if (num8 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int x2 = this.LastUsedX;
                  int num9 = this.LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num9 - height2;
                  Point point = new Point(x2, y2);
                  thumbnailWindow.Location = point;
                }
              }
            }
            else
            {
              int num7 = this.LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 + height1 > num5)
              {
                int num8;
                if (newpreview.Location.X == this.LastUsedX)
                {
                  int y2 = newpreview.Location.Y;
                  int num9 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num10 = num9 - height2;
                  num8 = y2 == num10 ? 1 : 0;
                }
                else
                  num8 = 0;
                if (num8 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int x2 = this.LastUsedX;
                  int num9 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num9 - height2;
                  Point point = new Point(x2, y2);
                  thumbnailWindow.Location = point;
                }
              }
              else if (newpreview.Location.X != this.LastUsedX || newpreview.Location.Y != this.LastUsedY)
                newpreview.Location = new Point(this.LastUsedX, this.LastUsedY);
            }
          }
          else
          {
            int num6 = this.LastUsedX;
            size = newpreview.Size;
            int width1 = size.Width;
            if (num6 - width1 < num4)
            {
              this.LastUsedX = x1;
              if (UserSettings.Default.ArrangePreviewYDirection == "Up")
              {
                this.LastUsedY = this.LastUsedY - (this.ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing);
                int num7 = this.LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 - height < y1)
                  this.LastUsedY = num5;
              }
              else
              {
                this.LastUsedY = this.LastUsedY + this.ArrangeLargestPreviewHeight + UserSettings.Default.PreviewLayoutVerticalSpacing;
                int num7 = this.LastUsedY;
                size = newpreview.Size;
                int height = size.Height;
                if (num7 + height > num5)
                  this.LastUsedY = y1;
              }
            }
            if (UserSettings.Default.ArrangePreviewYDirection == "Up")
            {
              int num7 = this.LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 - height1 < y1)
              {
                int x2 = newpreview.Location.X;
                int num8 = this.LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != y1)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = this.LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, y1);
                  thumbnailWindow.Location = point;
                }
              }
              else
              {
                int x2 = newpreview.Location.X;
                int num8 = this.LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                int num10;
                if (x2 == num9)
                {
                  int y2 = newpreview.Location.Y;
                  int num11 = this.LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num12 = num11 - height2;
                  num10 = y2 == num12 ? 1 : 0;
                }
                else
                  num10 = 0;
                if (num10 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num11 = this.LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  int x3 = num11 - width3;
                  int num12 = this.LastUsedY;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num12 - height2;
                  Point point = new Point(x3, y2);
                  thumbnailWindow.Location = point;
                }
              }
            }
            else
            {
              int num7 = this.LastUsedY;
              size = newpreview.Size;
              int height1 = size.Height;
              if (num7 + height1 > num5)
              {
                int x2 = newpreview.Location.X;
                int num8 = this.LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                int num10;
                if (x2 == num9)
                {
                  int y2 = newpreview.Location.Y;
                  int num11 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int num12 = num11 - height2;
                  num10 = y2 == num12 ? 1 : 0;
                }
                else
                  num10 = 0;
                if (num10 == 0)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num11 = this.LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  int x3 = num11 - width3;
                  int num12 = num5;
                  size = newpreview.Size;
                  int height2 = size.Height;
                  int y2 = num12 - height2;
                  Point point = new Point(x3, y2);
                  thumbnailWindow.Location = point;
                }
              }
              else
              {
                int x2 = newpreview.Location.X;
                int num8 = this.LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != this.LastUsedY)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = this.LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, this.LastUsedY);
                  thumbnailWindow.Location = point;
                }
              }
            }
          }
          if (UserSettings.Default.ArrangePreviewXDirection == "Right")
          {
            int x2 = newpreview.Location.X;
            size = newpreview.Size;
            int width = size.Width;
            this.LastUsedX = x2 + width + UserSettings.Default.PreviewLayoutHorizontalSpacing;
          }
          else
            this.LastUsedX = newpreview.Location.X - UserSettings.Default.PreviewLayoutHorizontalSpacing;
        }
        else
        {
          if (UserSettings.Default.ArrangePreviewYDirection == "Down")
          {
            int num6 = this.LastUsedY;
            size = newpreview.Size;
            int height = size.Height;
            if (num6 + height > num5)
            {
              this.LastUsedY = y1;
              if (UserSettings.Default.ArrangePreviewXDirection == "Left")
              {
                this.LastUsedX = this.LastUsedX - (this.ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing);
                int num7 = this.LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 - width < num4)
                  this.LastUsedX = x1;
              }
              else
              {
                this.LastUsedX = this.LastUsedX + this.ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing;
                int num7 = this.LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 + width > x1)
                  this.LastUsedX = num4;
              }
            }
            if (UserSettings.Default.ArrangePreviewXDirection == "Left")
            {
              int num7 = this.LastUsedX;
              size = newpreview.Size;
              int width1 = size.Width;
              if (num7 - width1 < x1)
              {
                if (newpreview.Location.X != x1 || newpreview.Location.Y != this.LastUsedY)
                  newpreview.Location = new Point(x1, this.LastUsedY);
              }
              else
              {
                int x2 = newpreview.Location.X;
                int num8 = this.LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != this.LastUsedY)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = this.LastUsedX;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, this.LastUsedY);
                  thumbnailWindow.Location = point;
                }
              }
            }
            else
            {
              int num7 = this.LastUsedX;
              size = newpreview.Size;
              int width1 = size.Width;
              if (num7 + width1 > x1)
              {
                int x2 = newpreview.Location.X;
                int num8 = x1;
                size = newpreview.Size;
                int width2 = size.Width;
                int num9 = num8 - width2;
                if (x2 != num9 || newpreview.Location.Y != this.LastUsedY)
                {
                  ThumbnailWindow thumbnailWindow = newpreview;
                  int num10 = x1;
                  size = newpreview.Size;
                  int width3 = size.Width;
                  Point point = new Point(num10 - width3, this.LastUsedY);
                  thumbnailWindow.Location = point;
                }
              }
              else if (newpreview.Location.X != this.LastUsedX || newpreview.Location.Y != this.LastUsedY)
                newpreview.Location = new Point(this.LastUsedX, this.LastUsedY);
            }
          }
          else
          {
            int num6 = this.LastUsedY;
            size = newpreview.Size;
            int height1 = size.Height;
            if (num6 - height1 < y1)
            {
              this.LastUsedY = num5;
              if (UserSettings.Default.ArrangePreviewXDirection == "Left")
              {
                this.LastUsedX = this.LastUsedX - (this.ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing);
                int num7 = this.LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 - width < num4)
                  this.LastUsedX = x1;
              }
              else
              {
                this.LastUsedX = this.LastUsedX + this.ArrangeLargestPreviewWidth + UserSettings.Default.PreviewLayoutHorizontalSpacing;
                int num7 = this.LastUsedX;
                size = newpreview.Size;
                int width = size.Width;
                if (num7 + width > x1)
                  this.LastUsedX = num4;
              }
            }
            if (UserSettings.Default.ArrangePreviewXDirection == "Left")
            {
              int x2 = newpreview.Location.X;
              int num7 = this.LastUsedX;
              size = newpreview.Size;
              int width1 = size.Width;
              int num8 = num7 - width1;
              int num9;
              if (x2 == num8)
              {
                int y2 = newpreview.Location.Y;
                int num10 = this.LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int num11 = num10 - height2;
                num9 = y2 == num11 ? 1 : 0;
              }
              else
                num9 = 0;
              if (num9 == 0)
              {
                ThumbnailWindow thumbnailWindow = newpreview;
                int num10 = this.LastUsedX;
                size = newpreview.Size;
                int width2 = size.Width;
                int x3 = num10 - width2;
                int num11 = this.LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int y2 = num11 - height2;
                Point point = new Point(x3, y2);
                thumbnailWindow.Location = point;
              }
            }
            else
            {
              int num7;
              if (newpreview.Location.X == this.LastUsedX)
              {
                int y2 = newpreview.Location.Y;
                int num8 = this.LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int num9 = num8 - height2;
                num7 = y2 == num9 ? 1 : 0;
              }
              else
                num7 = 0;
              if (num7 == 0)
              {
                ThumbnailWindow thumbnailWindow = newpreview;
                int x2 = this.LastUsedX;
                int num8 = this.LastUsedY;
                size = newpreview.Size;
                int height2 = size.Height;
                int y2 = num8 - height2;
                Point point = new Point(x2, y2);
                thumbnailWindow.Location = point;
              }
            }
          }
          if (UserSettings.Default.ArrangePreviewYDirection == "Down")
          {
            int y2 = newpreview.Location.Y;
            size = newpreview.Size;
            int height = size.Height;
            this.LastUsedY = y2 + height + UserSettings.Default.PreviewLayoutVerticalSpacing;
          }
          else
            this.LastUsedY = newpreview.Location.Y - UserSettings.Default.PreviewLayoutVerticalSpacing;
        }
      }
      if (state != 0)
        return;
      if (UserSettings.Default.PinToTop)
        newpreview.TopMost = true;
      else if (UserSettings.Default.PinToNormal)
        newpreview.TopMost = false;
      else if (UserSettings.Default.PinToBottom)
      {
        newpreview.TopMost = false;
        newpreview.SendToBack();
      }
      newpreview.Show();
      newpreview.RestoreTransparency();
    }

    public void HideTaskbarTab(IntPtr hWnd)
    {
      if (!UserSettings.Default.HideTaskBarButtons || (bool) this.TaskbarTabShown[this.TargettedWindows.IndexOf((object) hWnd)])
        return;
      this.tblc.DeleteTab((int) hWnd);
    }

    public void HideTaskbarTab()
    {
      if (!UserSettings.Default.HideTaskBarButtons)
        return;
      int index = 0;
      foreach (IntPtr num in this.TargettedWindows)
      {
        if (!(bool) this.TaskbarTabShown[index])
          this.tblc.DeleteTab((int) num);
        ++index;
      }
    }

    public void ShowTaskbarTab(IntPtr hWnd)
    {
      this.tblc.AddTab((int) hWnd);
    }

    public void ShowTaskbarTab()
    {
      foreach (IntPtr num in this.TargettedWindows)
        this.tblc.AddTab((int) num);
    }

    public void ToggleShowTaskbarTab(IntPtr hWnd, bool Show)
    {
      this.TaskbarTabShown[this.TargettedWindows.IndexOf((object) hWnd)] = (object) (bool) (Show ? 1 : 0);
    }

    private void PositionPreviewMenu()
    {
      this.TripResizeLoop = true;
      APPBARDATA pData = new APPBARDATA();
      int num1 = (int) TrayPreview.SHAppBarMessage(5, ref pData);
      int num2 = (int) TrayPreview.SHAppBarMessage(4, ref pData);
      int num3 = 0;
      if (UserSettings.Default.CustomTaskbarMargins)
      {
        int taskbarMarginsLeft = UserSettings.Default.CustomTaskbarMarginsLeft;
        int num4 = -UserSettings.Default.CustomTaskbarMarginsBottom;
        int taskbarMarginsTop = UserSettings.Default.CustomTaskbarMarginsTop;
        int num5 = -UserSettings.Default.CustomTaskbarMarginsRight;
        Point mousePosition = Control.MousePosition;
        int x = mousePosition.X - this.Size.Width / 2;
        mousePosition = Control.MousePosition;
        int y = mousePosition.Y;
        int num6 = x + this.Size.Width;
        Size size = SystemInformation.PrimaryMonitorSize;
        int num7 = size.Width + num5;
        if (num6 > num7)
        {
          size = SystemInformation.PrimaryMonitorSize;
          int num8 = size.Width + num5;
          size = this.Size;
          int width = size.Width;
          x = num8 - width;
        }
        if (x < taskbarMarginsLeft)
          x = taskbarMarginsLeft;
        size = this.Size;
        int height1 = size.Height;
        size = SystemInformation.PrimaryMonitorSize;
        int num9 = size.Height - taskbarMarginsTop + num4;
        if (height1 > num9)
        {
          size = this.Size;
          int width = size.Width;
          size = SystemInformation.PrimaryMonitorSize;
          int height2 = size.Height - taskbarMarginsTop + num4;
          this.Size = new Size(width, height2);
        }
        int num10 = y;
        size = this.Size;
        int height3 = size.Height;
        int num11 = num10 + height3;
        size = SystemInformation.PrimaryMonitorSize;
        int num12 = size.Height - taskbarMarginsTop + num4;
        if (num11 > num12)
        {
          size = SystemInformation.PrimaryMonitorSize;
          int num8 = size.Height - taskbarMarginsTop + num4;
          size = this.Size;
          int height2 = size.Height;
          y = num8 - height2;
        }
        if (y < taskbarMarginsTop)
          y = taskbarMarginsTop;
        this.Location = new Point(x, y);
      }
      else
      {
        int num4 = pData.rc.bottom;
        Size size = SystemInformation.PrimaryMonitorSize;
        int height1 = size.Height;
        int num5;
        if (num4 == height1 && pData.rc.left == 0)
        {
          int num6 = pData.rc.right;
          size = SystemInformation.PrimaryMonitorSize;
          int width = size.Width;
          num5 = num6 != width ? 1 : 0;
        }
        else
          num5 = 1;
        if (num5 == 0)
        {
          num3 = 1;
          int num6 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
          int x1 = Control.MousePosition.X;
          size = this.Size;
          int num7 = size.Width / 2;
          int num8 = x1 - num7;
          int num9 = num8;
          size = this.Size;
          int width1 = size.Width;
          int num10 = num9 + width1;
          size = SystemInformation.PrimaryMonitorSize;
          int width2 = size.Width;
          if (num10 > width2)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int width3 = size.Width;
            size = this.Size;
            int width4 = size.Width;
            num8 = width3 - width4;
          }
          size = this.Size;
          int num11 = size.Height - num6;
          size = SystemInformation.PrimaryMonitorSize;
          int height2 = size.Height;
          if (num11 > height2)
          {
            size = this.Size;
            int width3 = size.Width;
            size = SystemInformation.PrimaryMonitorSize;
            int height3 = size.Height + num6;
            this.Size = new Size(width3, height3);
          }
          int x2 = num8;
          size = SystemInformation.PrimaryMonitorSize;
          int height4 = size.Height;
          size = this.Size;
          int height5 = size.Height;
          int y = height4 - height5 + num6;
          this.Location = new Point(x2, y);
        }
        else
        {
          int num6;
          if (pData.rc.top == 0 && pData.rc.left == 0)
          {
            int num7 = pData.rc.right;
            size = SystemInformation.PrimaryMonitorSize;
            int width = size.Width;
            num6 = num7 != width ? 1 : 0;
          }
          else
            num6 = 1;
          if (num6 == 0)
          {
            num3 = 2;
            int y = pData.rc.bottom;
            int x1 = Control.MousePosition.X;
            size = this.Size;
            int num7 = size.Width / 2;
            int x2 = x1 - num7;
            int num8 = x2;
            size = this.Size;
            int width1 = size.Width;
            int num9 = num8 + width1;
            size = SystemInformation.PrimaryMonitorSize;
            int width2 = size.Width;
            if (num9 > width2)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int width3 = size.Width;
              size = this.Size;
              int width4 = size.Width;
              x2 = width3 - width4;
            }
            size = this.Size;
            int num10 = size.Height + y;
            size = SystemInformation.PrimaryMonitorSize;
            int height2 = size.Height;
            if (num10 > height2)
            {
              size = this.Size;
              int width3 = size.Width;
              size = SystemInformation.PrimaryMonitorSize;
              int height3 = size.Height - y;
              this.Size = new Size(width3, height3);
            }
            this.Location = new Point(x2, y);
          }
          else
          {
            int num7;
            if (pData.rc.top == 0 && pData.rc.left == 0)
            {
              int num8 = pData.rc.bottom;
              size = SystemInformation.PrimaryMonitorSize;
              int height2 = size.Height;
              num7 = num8 != height2 ? 1 : 0;
            }
            else
              num7 = 1;
            if (num7 == 0)
            {
              num3 = 3;
              int x = pData.rc.right;
              int y = Control.MousePosition.Y;
              size = this.Size;
              Rectangle bounds;
              if (size.Height > Screen.PrimaryScreen.Bounds.Height)
              {
                size = this.Size;
                int width = size.Width;
                bounds = Screen.PrimaryScreen.Bounds;
                int height2 = bounds.Height;
                this.Size = new Size(width, height2);
              }
              int num8 = y;
              size = this.Size;
              int height3 = size.Height;
              int num9 = num8 + height3;
              bounds = Screen.PrimaryScreen.Bounds;
              int height4 = bounds.Height;
              if (num9 > height4)
              {
                bounds = Screen.PrimaryScreen.Bounds;
                int bottom = bounds.Bottom;
                size = this.Size;
                int height2 = size.Height;
                y = bottom - height2;
              }
              this.Location = new Point(x, y);
            }
            else
            {
              int num8;
              if (pData.rc.top == 0)
              {
                int num9 = pData.rc.right;
                size = SystemInformation.PrimaryMonitorSize;
                int width = size.Width;
                if (num9 == width)
                {
                  int num10 = pData.rc.bottom;
                  size = SystemInformation.PrimaryMonitorSize;
                  int height2 = size.Height;
                  num8 = num10 != height2 ? 1 : 0;
                  goto label_43;
                }
              }
              num8 = 1;
label_43:
              if (num8 == 0)
              {
                num3 = 4;
                size = SystemInformation.PrimaryMonitorSize;
                int num9 = size.Width - (pData.rc.right - pData.rc.left);
                size = this.Size;
                int width1 = size.Width;
                int x = num9 - width1;
                int y = Control.MousePosition.Y;
                size = this.Size;
                Rectangle bounds;
                if (size.Height > Screen.PrimaryScreen.Bounds.Height)
                {
                  size = this.Size;
                  int width2 = size.Width;
                  bounds = Screen.PrimaryScreen.Bounds;
                  int height2 = bounds.Height;
                  this.Size = new Size(width2, height2);
                }
                int num10 = y;
                size = this.Size;
                int height3 = size.Height;
                int num11 = num10 + height3;
                bounds = Screen.PrimaryScreen.Bounds;
                int height4 = bounds.Height;
                if (num11 > height4)
                {
                  bounds = Screen.PrimaryScreen.Bounds;
                  int bottom = bounds.Bottom;
                  size = this.Size;
                  int height2 = size.Height;
                  y = bottom - height2;
                }
                this.Location = new Point(x, y);
              }
            }
          }
        }
      }
      this.TripResizeLoop = false;
    }

    private void GetWindows()
    {
      this.windows.Clear();
      TrayPreview.EnumWindows(new TrayPreview.EnumWindowsCallback(this.Callback), 0);
    }

    private bool Callback(IntPtr hwnd, int lParam)
    {
      if (this.Handle != hwnd && ((long) TrayPreview.GetWindowLongA(hwnd, TrayPreview.GWL_STYLE) & (long) TrayPreview.TARGETWINDOW) == (long) TrayPreview.TARGETWINDOW)
      {
        StringBuilder lpString = new StringBuilder(100);
        TrayPreview.GetWindowText(hwnd, lpString, lpString.Capacity);
        this.windows.Add(new Window()
        {
          Handle = hwnd,
          Title = lpString.ToString()
        });
      }
      return true;
    }

    private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      this.cleardata();
      this.Hide();
      if (!this.AllMinimized)
        this.MinimizePreviews_Click((object) this, (EventArgs) null);
      else
        this.ShowPreviews_Click((object) this, (EventArgs) null);
    }

    private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left && this.MinimizeHotkeyDown)
      {
        this.ShowPreviews_Click((object) this, (EventArgs) null);
      }
      else
      {
        if (e.Button == MouseButtons.Right)
          return;
        if (e.Button == MouseButtons.Middle)
        {
          this.ShowPreviews_Click((object) this, (EventArgs) null);
        }
        else
        {
          this.TripResizeLoop = true;
          this.cleardata();
          Point mousePosition = Control.MousePosition;
          int x = mousePosition.X;
          mousePosition = Control.MousePosition;
          int y = mousePosition.Y;
          this.TrayClickPoint = new Point(x, y);
          this.GetWindows();
          int index = 0;
          int top1 = 0;
          if (this.thumb != IntPtr.Zero)
            TrayPreview.DwmUnregisterThumbnail(this.thumb);
          DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
          foreach (Window window in this.windows)
          {
            TrayPreview.DwmRegisterThumbnail(this.Handle, this.windows[index].Handle, out this.thumb);
            this.ThumbList.Add((object) this.thumb);
            PSIZE size1;
            TrayPreview.DwmQueryThumbnailSourceSize(this.thumb, out size1);
            int previewMenuSizeX = UserSettings.Default.PreviewMenuSizeX;
            Size size2 = this.Size;
            int height1 = size2.Height;
            this.Size = new Size(previewMenuSizeX, height1);
            props.fVisible = true;
            props.dwFlags = TrayPreview.DWM_TNP_VISIBLE | TrayPreview.DWM_TNP_RECTDESTINATION | TrayPreview.DWM_TNP_OPACITY | TrayPreview.DWM_TNP_RECTSOURCE;
            props.opacity = byte.MaxValue;
            // ISSUE: explicit reference operation
            // ISSUE: variable of a reference type
            DWM_THUMBNAIL_PROPERTIES& local = @props;
            size2 = SystemInformation.FrameBorderSize;
            int left = -size2.Width;
            size2 = SystemInformation.FrameBorderSize;
            int top2 = -size2.Height;
            int num1 = size1.x;
            size2 = SystemInformation.FrameBorderSize;
            int width = size2.Width;
            int right = num1 + width;
            int num2 = size1.y;
            size2 = SystemInformation.FrameBorderSize;
            int height2 = size2.Height;
            int bottom = num2 + height2;
            Rect rect = new Rect(left, top2, right, bottom);
            // ISSUE: explicit reference operation
            (^local).rcSource = rect;
            this.Opacity = 1.0;
            props.rcDestination = new Rect(0, top1, this.pictureBox1.Width, top1 + (int) ((double) this.pictureBox1.Width * (double) size1.y / (double) size1.x) + UserSettings.Default.VerticalSpaceBetweenPreviews);
            this.PreviewDataLeft.Add((object) 0);
            this.PreviewDataTop.Add((object) top1);
            this.PreviewDataRight.Add((object) this.pictureBox1.Width);
            this.PreviewDataBottom.Add((object) (top1 + (int) ((double) this.pictureBox1.Width * (double) size1.y / (double) size1.x)));
            this.PreviewDataHandle.Add((object) this.windows[index].Handle);
            top1 = top1 + (int) ((double) this.pictureBox1.Width * (double) size1.y / (double) size1.x) + UserSettings.Default.VerticalSpaceBetweenPreviews;
            ++index;
            TrayPreview.DwmUpdateThumbnailProperties(this.thumb, ref props);
          }
          int num3 = top1 - UserSettings.Default.VerticalSpaceBetweenPreviews;
          int previewMenuSizeX1 = UserSettings.Default.PreviewMenuSizeX;
          int num4 = num3;
          Size size = this.Size;
          int height3 = size.Height;
          size = this.pictureBox1.Size;
          int height4 = size.Height;
          int num5 = height3 - height4;
          int height5 = num4 + num5;
          this.Size = new Size(previewMenuSizeX1, height5);
          this.PositionPreviewMenu();
          this.TopMost = true;
          this.Show();
          this.BringToFront();
          this.Activate();
          this.TripResizeLoop = false;
        }
      }
    }

    private void click_timer_Tick(object sender, EventArgs e)
    {
      this.click_timer.Stop();
    }

    private void TrayPreview_Load(object sender, EventArgs e)
    {
      this.cleardata();
      this.Hide();
    }

    private void TrayPreview_Shown(object sender, EventArgs e)
    {
      this.cleardata();
      this.Hide();
    }

    private void cleardata()
    {
      foreach (IntPtr thumb in this.ThumbList)
      {
        try
        {
          if (thumb != IntPtr.Zero)
            TrayPreview.DwmUnregisterThumbnail(thumb);
        }
        catch (Exception ex)
        {
        }
      }
      this.ThumbList.Clear();
      this.thumb = IntPtr.Zero;
      this.PreviewDataLeft.Clear();
      this.PreviewDataTop.Clear();
      this.PreviewDataRight.Clear();
      this.PreviewDataBottom.Clear();
      this.PreviewDataHandle.Clear();
    }

    private void pictureBox1_MouseLeave(object sender, EventArgs e)
    {
    }

    private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
    {
      Point p;
      // ISSUE: explicit reference operation
      // ISSUE: variable of a reference type
      Point& local = @p;
      Point mousePosition = Control.MousePosition;
      int x1 = mousePosition.X;
      mousePosition = Control.MousePosition;
      int y1 = mousePosition.Y;
      // ISSUE: explicit reference operation
      ^local = new Point(x1, y1);
      Point point = this.PointToClient(p);
      int x2 = point.X;
      int y2 = point.Y;
      int index = 0;
      if (!(e.Button.ToString() == "Left"))
        return;
      try
      {
        foreach (IntPtr hWnd in this.PreviewDataHandle)
        {
          if (x2 >= int.Parse(this.PreviewDataLeft[index].ToString()) && x2 <= int.Parse(this.PreviewDataRight[index].ToString()) && y2 >= int.Parse(this.PreviewDataTop[index].ToString()) && y2 <= int.Parse(this.PreviewDataBottom[index].ToString()))
          {
            if (!this.TargettedWindows.Contains((object) hWnd))
            {
              if (TrayPreview.IsIconic(hWnd))
              {
                this.CreatePreview(hWnd, true);
                break;
              }
              this.CreatePreview(hWnd, false);
              break;
            }
            TrayPreview.SendMessage((IntPtr) long.Parse(this.ViPPreviewHandles[this.TargettedWindows.IndexOf((object) hWnd)].ToString()), 274, 61728, 0);
            break;
          }
          ++index;
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine("Array Out of Bounds");
      }
    }

    private void notifyIcon1_MouseMove(object sender, MouseEventArgs e)
    {
    }

    protected override void WndProc(ref Message m)
    {
      if (m.Msg == 522)
      {
        int num = (int) m.WParam <= 0 ? -1 : 1;
        if (num > 0)
        {
          this.myscroll(UserSettings.Default.PreviewMenuScrollSensitivity);
        }
        else
        {
          if (num >= 0)
            return;
          this.myscroll(-UserSettings.Default.PreviewMenuScrollSensitivity);
        }
      }
      else
        base.WndProc(ref m);
    }

    private void myscroll(int updown)
    {
      if (this.PreviewDataTop.Count <= 0)
        return;
      this.Activate();
      int num1 = int.Parse(this.PreviewDataBottom[this.PreviewDataBottom.Count - 1].ToString());
      Size size1 = this.pictureBox1.Size;
      int height1 = size1.Height;
      if (num1 <= height1 && updown < 0 || int.Parse(this.PreviewDataTop[0].ToString()) >= 0 && updown > 0)
        return;
      int num2 = int.Parse(this.PreviewDataBottom[this.PreviewDataBottom.Count - 1].ToString()) + updown;
      size1 = this.pictureBox1.Size;
      int height2 = size1.Height;
      if (num2 < height2 && updown < 0)
      {
        size1 = this.pictureBox1.Size;
        updown = size1.Height - int.Parse(this.PreviewDataBottom[this.PreviewDataBottom.Count - 1].ToString());
      }
      else if (int.Parse(this.PreviewDataTop[0].ToString()) + updown > 0 && updown > 0)
        updown = -int.Parse(this.PreviewDataTop[0].ToString());
      int index = 0;
      DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
      foreach (IntPtr thumb in this.ThumbList)
      {
        PSIZE size2;
        TrayPreview.DwmQueryThumbnailSourceSize(thumb, out size2);
        props.fVisible = true;
        props.dwFlags = TrayPreview.DWM_TNP_VISIBLE | TrayPreview.DWM_TNP_RECTDESTINATION | TrayPreview.DWM_TNP_OPACITY | TrayPreview.DWM_TNP_RECTSOURCE;
        props.opacity = byte.MaxValue;
        this.Opacity = 1.0;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        DWM_THUMBNAIL_PROPERTIES& local = @props;
        size1 = SystemInformation.FrameBorderSize;
        int left = -size1.Width;
        size1 = SystemInformation.FrameBorderSize;
        int top = -size1.Height;
        int num3 = size2.x;
        size1 = SystemInformation.FrameBorderSize;
        int width = size1.Width;
        int right = num3 + width;
        int num4 = size2.y;
        size1 = SystemInformation.FrameBorderSize;
        int height3 = size1.Height;
        int bottom = num4 + height3;
        Rect rect = new Rect(left, top, right, bottom);
        // ISSUE: explicit reference operation
        (^local).rcSource = rect;
        props.rcDestination = new Rect(0, int.Parse(this.PreviewDataTop[index].ToString()) + updown, this.pictureBox1.Width, int.Parse(this.PreviewDataBottom[index].ToString()) + updown);
        this.PreviewDataTop[index] = (object) (int.Parse(this.PreviewDataTop[index].ToString()) + updown);
        this.PreviewDataBottom[index] = (object) (int.Parse(this.PreviewDataBottom[index].ToString()) + updown);
        TrayPreview.DwmUpdateThumbnailProperties((IntPtr) this.ThumbList[index], ref props);
        ++index;
      }
    }

    private void TrayPreview_FormClosing(object sender, FormClosingEventArgs e)
    {
    }

    private void TrayPreview_FormClosed(object sender, FormClosedEventArgs e)
    {
    }

    private void pictureBox1_MouseHover(object sender, EventArgs e)
    {
    }

    private void TrayPreview_MouseHover(object sender, EventArgs e)
    {
    }

    private void TrayPreview_Deactivate(object sender, EventArgs e)
    {
      this.TripResizeLoop = true;
      this.cleardata();
      this.Hide();
      GC.KeepAlive((object) this);
      this.TripResizeLoop = false;
    }

    private void quitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.quitting = true;
      foreach (IntPtr hWnd in this.ViPPreviewHandles)
      {
        try
        {
          if (this.ViPPreviewHandles.Contains((object) hWnd))
            TrayPreview.SendMessage(hWnd, 274, 61536, 0);
        }
        catch (Exception ex)
        {
          Console.WriteLine("closing exception");
        }
      }
      this.notifyIcon1.Dispose();
      Application.Exit();
    }

    private void contextMenuStrip1_Opened(object sender, EventArgs e)
    {
      this.contextMenuStrip1.BringToFront();
    }

    private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
    {
      this.CreatePreviewsOnMinimize.Checked = UserSettings.Default.CreatePreviewsOnMinimize;
      this.requireCTRLKeyToCreatePreviewToolStripMenuItem.Checked = UserSettings.Default.CreatePreviewOnlyOnHotkey;
      if (UserSettings.Default.PinToTop)
      {
        this.topmostToolStripMenuItem.Checked = true;
        this.desktopToolStripMenuItem.Checked = false;
        this.normalToolStripMenuItem.Checked = false;
      }
      else if (UserSettings.Default.PinToNormal)
      {
        this.topmostToolStripMenuItem.Checked = false;
        this.desktopToolStripMenuItem.Checked = false;
        this.normalToolStripMenuItem.Checked = true;
      }
      else if (UserSettings.Default.PinToBottom)
      {
        this.topmostToolStripMenuItem.Checked = false;
        this.desktopToolStripMenuItem.Checked = true;
        this.normalToolStripMenuItem.Checked = false;
      }
      if (UserSettings.Default.PreviewGridAlign == "Snap")
      {
        this.snapToGridToolStripMenuItem.Checked = true;
        this.noneToolStripMenuItem.Checked = false;
      }
      else
      {
        this.snapToGridToolStripMenuItem.Checked = false;
        this.noneToolStripMenuItem.Checked = true;
      }
      this.contextMenuStrip1.Visible = true;
      this.contextMenuStrip1.BringToFront();
    }

    private void makePreviewsIgnoreInputToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (IntPtr hWnd in this.ViPPreviewHandles)
        TrayPreview.SetWindowLong(hWnd, -20, TrayPreview.GetWindowLong(hWnd, -20) | 32L);
    }

    private void makePreviewsRespondToInputToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (IntPtr hWnd in this.ViPPreviewHandles)
        TrayPreview.SetWindowLong(hWnd, -20, TrayPreview.GetWindowLong(hWnd, -20) + -32L);
    }

    private void TrayPreview_KeyDown(object sender, KeyEventArgs e)
    {
      switch (e.KeyCode.ToString())
      {
        case "Down":
          this.myscroll(UserSettings.Default.PreviewMenuScrollSensitivity);
          break;
        case "Up":
          this.myscroll(-UserSettings.Default.PreviewMenuScrollSensitivity);
          break;
      }
    }

    private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Form form = (Form) new SettingsPanel(this);
      form.StartPosition = FormStartPosition.CenterScreen;
      form.Show();
      form.TopMost = true;
      form.BringToFront();
    }

    public void ChangeTransparency(int thumbtransparencyvalue, int backtransparencyvalue)
    {
      foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
        thumbnailWindow.ChangeTransparency(thumbtransparencyvalue, backtransparencyvalue);
    }

    private void TrayPreview_Resize(object sender, EventArgs e)
    {
      if (this.TripResizeLoop)
        return;
      UserSettings.Default.PreviewMenuSizeX = this.Size.Width;
      UserSettings.Default.Save();
      int width1 = this.Size.Width;
      Size size1 = SystemInformation.MinimizedWindowSize;
      int width2 = size1.Width;
      if (width1 <= width2)
      {
        // ISSUE: variable of a compiler-generated type
        UserSettings @default = UserSettings.Default;
        size1 = SystemInformation.MinimizedWindowSize;
        int width3 = size1.Width;
        @default.PreviewMenuSizeX = width3;
        UserSettings.Default.Save();
      }
      else
      {
        int index = 0;
        int top1 = 0;
        DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
        this.PreviewDataLeft.Clear();
        this.PreviewDataTop.Clear();
        this.PreviewDataRight.Clear();
        this.PreviewDataBottom.Clear();
        this.PreviewDataHandle.Clear();
        foreach (IntPtr num1 in this.ThumbList)
        {
          PSIZE size2;
          TrayPreview.DwmQueryThumbnailSourceSize(num1, out size2);
          props.fVisible = true;
          props.dwFlags = TrayPreview.DWM_TNP_VISIBLE | TrayPreview.DWM_TNP_RECTDESTINATION | TrayPreview.DWM_TNP_OPACITY | TrayPreview.DWM_TNP_RECTSOURCE;
          // ISSUE: explicit reference operation
          // ISSUE: variable of a reference type
          DWM_THUMBNAIL_PROPERTIES& local = @props;
          size1 = SystemInformation.FrameBorderSize;
          int left = -size1.Width;
          size1 = SystemInformation.FrameBorderSize;
          int top2 = -size1.Height;
          int num2 = size2.x;
          size1 = SystemInformation.FrameBorderSize;
          int width3 = size1.Width;
          int right = num2 + width3;
          int num3 = size2.y;
          size1 = SystemInformation.FrameBorderSize;
          int height = size1.Height;
          int bottom = num3 + height;
          Rect rect = new Rect(left, top2, right, bottom);
          // ISSUE: explicit reference operation
          (^local).rcSource = rect;
          props.opacity = byte.MaxValue;
          this.Opacity = 1.0;
          props.rcDestination = new Rect(0, top1, this.pictureBox1.Width, top1 + (int) ((double) this.pictureBox1.Width * (double) size2.y / (double) size2.x) + UserSettings.Default.VerticalSpaceBetweenPreviews);
          TrayPreview.DwmUpdateThumbnailProperties(num1, ref props);
          this.PreviewDataLeft.Add((object) 0);
          this.PreviewDataTop.Add((object) top1);
          this.PreviewDataRight.Add((object) this.pictureBox1.Width);
          this.PreviewDataBottom.Add((object) (top1 + (int) ((double) this.pictureBox1.Width * (double) size2.y / (double) size2.x)));
          this.PreviewDataHandle.Add((object) this.windows[index].Handle);
          top1 = top1 + (int) ((double) this.pictureBox1.Width * (double) size2.y / (double) size2.x) + UserSettings.Default.VerticalSpaceBetweenPreviews;
          ++index;
        }
        Size size3 = new Size(UserSettings.Default.PreviewMenuSizeX, top1 - UserSettings.Default.VerticalSpaceBetweenPreviews + (this.Size.Height - this.pictureBox1.Size.Height));
      }
    }

    private void CreatePreviewsOnMinimize_Click(object sender, EventArgs e)
    {
      UserSettings.Default.CreatePreviewsOnMinimize = this.CreatePreviewsOnMinimize.Checked;
      UserSettings.Default.Save();
    }

    private void PreviewAllOpenWindows_Click(object sender, EventArgs e)
    {
      this.GetWindows();
      try
      {
        foreach (Window window in this.windows)
        {
          if (!this.TargettedWindows.Contains((object) window.Handle))
          {
            if (TrayPreview.IsIconic(window.Handle))
              this.CreatePreview(window.Handle, true);
            else
              this.CreatePreview(window.Handle, false);
          }
        }
      }
      catch (Exception ex)
      {
      }
    }

    private void MinimizePreviews_Click(object sender, EventArgs e)
    {
      this.MinimizingPreviews = true;
      foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
        thumbnailWindow.minimizeToolStripMenuItem_Click((object) this, (EventArgs) null);
      this.MinimizingPreviews = false;
      this.AllMinimized = true;
    }

    private void ShowPreviews_Click(object sender, EventArgs e)
    {
      foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
      {
        if (!thumbnailWindow.TopMost)
        {
          thumbnailWindow.TopMost = true;
          thumbnailWindow.TopMost = false;
        }
        thumbnailWindow.Show();
        thumbnailWindow.BringToFront();
      }
      this.AllMinimized = false;
    }

    public void AeroBackGroundRefresh()
    {
      foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
      {
        thumbnailWindow.ResizeFormP(thumbnailWindow.Size.Width, thumbnailWindow.Size.Height);
        thumbnailWindow.Invalidate();
      }
    }

    private void RecoverLostWindows_Click(object sender, EventArgs e)
    {
      this.restoreAllTargetWindowsToolStripMenuItem_Click((object) this, (EventArgs) null);
      this.GetWindows();
      foreach (Window window in this.windows)
      {
        if (window.Handle != this.Handle)
        {
          TrayPreview.SetWindowLong(window.Handle, -20, 0);
          this.ShowTaskbarTab(window.Handle);
          StringBuilder lpString = new StringBuilder(100);
          TrayPreview.GetWindowText(window.Handle, lpString, lpString.Capacity);
          if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(TrayPreview.GetWindowProcessID((int) window.Handle)).ProcessName.Equals("Photoshop"))
          {
            bool flag = false;
            if (XPAppearance.MinAnimate)
            {
              XPAppearance.MinAnimate = false;
              flag = true;
            }
            TrayPreview.SetWindowPos(window.Handle, IntPtr.Zero, 0, 0, 0, 0, 17U);
            if (flag)
              XPAppearance.MinAnimate = true;
          }
        }
      }
    }

    public void MyKeyDown(object sender, KeyEventArgs e)
    {
      GC.KeepAlive((object) TrayPreview.actHook);
      if (e.KeyData.ToString() == "LShiftKey" || e.KeyData.ToString() == "RShiftKey")
        this.MinimizeHotkeyDown = true;
      else if (e.KeyData.ToString() == "LWin" || e.KeyData.ToString() == "RWin")
        this.Modifier1 = true;
      else if (e.KeyData.ToString() == "F5")
      {
        if (this.Modifier1)
          this.PreviewAllOpenWindows_Click((object) this, (EventArgs) null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
      else if (e.KeyData.ToString() == "F2")
      {
        if (this.Modifier1)
          this.ShowPreviews_Click((object) this, (EventArgs) null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
      else if (e.KeyData.ToString() == "F3")
      {
        if (this.Modifier1)
          this.MinimizePreviews_Click((object) this, (EventArgs) null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
      else
      {
        if (!(e.KeyData.ToString() == "F4"))
          return;
        if (this.Modifier1)
          this.restoreAllTargetWindowsToolStripMenuItem_Click((object) this, (EventArgs) null);
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
    }

    public void MyKeyUp(object sender, KeyEventArgs e)
    {
      GC.KeepAlive((object) TrayPreview.actHook);
      if (e.KeyData.ToString() == "LShiftKey" || e.KeyData.ToString() == "RShiftKey")
      {
        this.MinimizeHotkeyDown = false;
      }
      else
      {
        if (!(e.KeyData.ToString() == "LWin") && !(e.KeyData.ToString() == "RWin"))
          return;
        this.Modifier1 = false;
      }
    }

    private void requireCTRLKeyToCreatePreviewToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.CreatePreviewOnlyOnHotkey = this.requireCTRLKeyToCreatePreviewToolStripMenuItem.Checked;
      UserSettings.Default.Save();
    }

    private void topmostToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.PinToTop = true;
      UserSettings.Default.PinToNormal = false;
      UserSettings.Default.PinToBottom = false;
      UserSettings.Default.Save();
      this.topmostToolStripMenuItem.Checked = true;
      this.desktopToolStripMenuItem.Checked = false;
      this.normalToolStripMenuItem.Checked = false;
      foreach (Form form in this.PreviewControls)
        form.TopMost = true;
    }

    private void desktopToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.PinToTop = false;
      UserSettings.Default.PinToNormal = false;
      UserSettings.Default.PinToBottom = true;
      UserSettings.Default.Save();
      this.topmostToolStripMenuItem.Checked = false;
      this.desktopToolStripMenuItem.Checked = true;
      this.normalToolStripMenuItem.Checked = false;
      int index = 0;
      foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
      {
        if (!(bool) this.AlwaysOnTop[index])
        {
          thumbnailWindow.TopMost = false;
          thumbnailWindow.SendToBack();
        }
        ++index;
      }
    }

    private void normalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      UserSettings.Default.PinToTop = false;
      UserSettings.Default.PinToNormal = true;
      UserSettings.Default.PinToBottom = false;
      UserSettings.Default.Save();
      this.topmostToolStripMenuItem.Checked = false;
      this.desktopToolStripMenuItem.Checked = false;
      this.normalToolStripMenuItem.Checked = true;
      int index = 0;
      foreach (ThumbnailWindow thumbnailWindow in this.PreviewControls)
      {
        if (!(bool) this.AlwaysOnTop[index])
          thumbnailWindow.TopMost = false;
        ++index;
      }
    }

    private void contextMenuStrip1_Closing(object sender, ToolStripDropDownClosingEventArgs e)
    {
    }

    private void noneToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.snapToGridToolStripMenuItem.Checked = false;
      this.noneToolStripMenuItem.Checked = true;
      UserSettings.Default.PreviewGridAlign = "None";
      UserSettings.Default.Save();
    }

    private void snapToGridToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.snapToGridToolStripMenuItem.Checked = true;
      this.noneToolStripMenuItem.Checked = false;
      UserSettings.Default.PreviewGridAlign = "Snap";
      UserSettings.Default.Save();
      this.ReAlign();
    }

    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern void DwmExtendFrameIntoClientArea(IntPtr hwnd, ref TrayPreview.MARGINS margins);

    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern bool DwmIsCompositionEnabled();

    private delegate bool EnumWindowsCallback(IntPtr hwnd, int lParam);

    public enum MouseEventType
    {
      LeftDown = 2,
      LeftUp = 4,
      RightDown = 8,
      RightUp = 16,
    }

    private enum ShowWindowEnum
    {
      Hide = 0,
      ShowNormal = 1,
      ShowMinimized = 2,
      Maximize = 3,
      ShowMaximized = 3,
      ShowNormalNoActivate = 4,
      Show = 5,
      Minimize = 6,
      ShowMinNoActivate = 7,
      ShowNoActivate = 8,
      Restore = 9,
      ShowDefault = 10,
      ForceMinimized = 11,
    }

    public struct MARGINS
    {
      public int Left;
      public int Right;
      public int Top;
      public int Bottom;
    }
  }
}
