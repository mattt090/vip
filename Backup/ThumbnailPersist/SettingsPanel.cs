﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.SettingsPanel
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ThumbnailPersist.Properties;
using TrayPreviewSpace;

namespace ThumbnailPersist
{
  public class SettingsPanel : Form
  {
    private IContainer components = (IContainer) null;
    public bool TB1changed = false;
    public bool TB2changed = false;
    public bool TB3changed = false;
    public bool TB4changed = false;
    public bool TB5changed = false;
    public bool TB6changed = false;
    private bool OriginalEnableAeroBack = true;
    private TabControl tabControl1;
    private TabPage tabPage1;
    private TabPage tabPage2;
    private TrackBar trackBar5;
    private TrackBar trackBar4;
    private TrackBar trackBar3;
    private TrackBar trackBar2;
    private TrackBar trackBar1;
    private Button MainOK;
    private Button MainCancel;
    private Button ApplyButton;
    private CheckBox DockPreviewCB;
    private TabPage tabPage3;
    private TabPage tabPage4;
    private GroupBox groupBox1;
    private CheckBox CustomCB;
    private CheckBox WindowsDefaultCB;
    private CheckBox TopRightCB;
    private CheckBox BottomRightCB;
    private CheckBox TopMiddleCB;
    private CheckBox BottomMiddleCB;
    private CheckBox TopLeftCB;
    private CheckBox BottomLeftCB;
    private TabPage tabPage5;
    private Label label1;
    private Label label2;
    private Label label3;
    private Label label4;
    private Label label5;
    private NumericUpDown CustomLocationX;
    private Label label7;
    private Label label6;
    private NumericUpDown CustomLocationY;
    private CheckBox CentreCB;
    private Label MinPreviewSize;
    private Label label8;
    private Label label9;
    private NumericUpDown MinPreviewSizeY;
    private NumericUpDown MinPreviewSizeX;
    private GroupBox groupBox2;
    private Label MaxPreviewSize;
    private Label label11;
    private Label label12;
    private NumericUpDown MaxPreviewSizeY;
    private NumericUpDown MaxPreviewSizeX;
    private Label label10;
    private CheckBox ShowBordersByDefault;
    private Label TransparencyLabel;
    private TrackBar ThumbTransparencyTB;
    private Label label13;
    private TrackBar BackTransparency;
    private CheckBox ShowBackText;
    private CheckBox RightCB;
    private CheckBox LeftCB;
    private CheckBox TopCB;
    private CheckBox BottomCB;
    private Label ZoomMouseScroll;
    private TrackBar trackBar6;
    private CheckBox ShowListofWindows;
    private GroupBox groupBox3;
    private Label label14;
    private NumericUpDown PreviewLaunchWidth;
    private Label label15;
    private NumericUpDown PreviewLaunchVerticalSpace;
    private Label label16;
    private TrackBar PreviewLauncherScrollSensitivity;
    private CheckBox HideTargetByDefault;
    private CheckBox KeepTargetFromMinimize;
    private Label label17;
    private CheckBox FreeFormEdgeDocking;
    private GroupBox groupBox4;
    private CheckBox FreeFormCentreDocking;
    private NumericUpDown CentreDockingThickness;
    private NumericUpDown EdgeDockingThickness;
    private ToolTip EdgeDockingThicknessToolTip;
    private ToolTip CentreDockingThicknessToolTip;
    private CheckBox HideTaskbarButtons;
    private CheckBox CreatePreviewsOnMinimize;
    private Label label18;
    private CheckBox RequireHotkeyOnMinimize;
    private Label label19;
    private TabPage Layout;
    private GroupBox groupBox8;
    private GroupBox groupBox5;
    private GroupBox groupBox9;
    private CheckBox ColumnsFirst;
    private CheckBox RowsFirst;
    private GroupBox groupBox7;
    private CheckBox DownDirectionCB;
    private CheckBox UpDirectionCB;
    private GroupBox groupBox6;
    private CheckBox RightDirectionCB;
    private CheckBox LeftDirectionCB;
    private CheckBox PinDesktop;
    private CheckBox PinNormal;
    private CheckBox PinTopMost;
    private Label label20;
    private Label VerticalSpacingLabel;
    private NumericUpDown PreviewHorizontalSpacing;
    private NumericUpDown PreviewVerticalSpacing;
    private GroupBox groupBox10;
    private GroupBox groupBox11;
    private NumericUpDown MarginBottom;
    private NumericUpDown MarginTop;
    private PictureBox pictureBox1;
    private NumericUpDown MarginLeft;
    private NumericUpDown MarginRight;
    private CheckBox CustomTaskbarMargins;
    private CheckBox AutomaticTaskbarMargins;
    private CheckBox EnableAeroBack;
    private Label label21;
    private Label label22;
    private TrayPreview qparent;

    public SettingsPanel(TrayPreview frm1)
    {
      this.InitializeComponent();
      this.qparent = frm1;
      this.AcceptButton = (IButtonControl) this.MainOK;
      this.CancelButton = (IButtonControl) this.MainCancel;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (SettingsPanel));
      this.tabControl1 = new TabControl();
      this.tabPage2 = new TabPage();
      this.groupBox4 = new GroupBox();
      this.CentreDockingThickness = new NumericUpDown();
      this.EdgeDockingThickness = new NumericUpDown();
      this.FreeFormCentreDocking = new CheckBox();
      this.FreeFormEdgeDocking = new CheckBox();
      this.DockPreviewCB = new CheckBox();
      this.groupBox1 = new GroupBox();
      this.RightCB = new CheckBox();
      this.LeftCB = new CheckBox();
      this.TopCB = new CheckBox();
      this.BottomCB = new CheckBox();
      this.CentreCB = new CheckBox();
      this.label7 = new Label();
      this.label6 = new Label();
      this.CustomLocationY = new NumericUpDown();
      this.CustomLocationX = new NumericUpDown();
      this.CustomCB = new CheckBox();
      this.WindowsDefaultCB = new CheckBox();
      this.TopRightCB = new CheckBox();
      this.BottomRightCB = new CheckBox();
      this.TopMiddleCB = new CheckBox();
      this.BottomMiddleCB = new CheckBox();
      this.TopLeftCB = new CheckBox();
      this.BottomLeftCB = new CheckBox();
      this.Layout = new TabPage();
      this.groupBox11 = new GroupBox();
      this.CustomTaskbarMargins = new CheckBox();
      this.AutomaticTaskbarMargins = new CheckBox();
      this.MarginLeft = new NumericUpDown();
      this.MarginRight = new NumericUpDown();
      this.pictureBox1 = new PictureBox();
      this.MarginBottom = new NumericUpDown();
      this.MarginTop = new NumericUpDown();
      this.groupBox10 = new GroupBox();
      this.label20 = new Label();
      this.VerticalSpacingLabel = new Label();
      this.PreviewHorizontalSpacing = new NumericUpDown();
      this.PreviewVerticalSpacing = new NumericUpDown();
      this.groupBox8 = new GroupBox();
      this.PinDesktop = new CheckBox();
      this.PinNormal = new CheckBox();
      this.PinTopMost = new CheckBox();
      this.groupBox5 = new GroupBox();
      this.groupBox9 = new GroupBox();
      this.ColumnsFirst = new CheckBox();
      this.RowsFirst = new CheckBox();
      this.groupBox7 = new GroupBox();
      this.DownDirectionCB = new CheckBox();
      this.UpDirectionCB = new CheckBox();
      this.groupBox6 = new GroupBox();
      this.RightDirectionCB = new CheckBox();
      this.LeftDirectionCB = new CheckBox();
      this.tabPage3 = new TabPage();
      this.label22 = new Label();
      this.label21 = new Label();
      this.EnableAeroBack = new CheckBox();
      this.ShowBackText = new CheckBox();
      this.label13 = new Label();
      this.BackTransparency = new TrackBar();
      this.TransparencyLabel = new Label();
      this.ThumbTransparencyTB = new TrackBar();
      this.ShowBordersByDefault = new CheckBox();
      this.tabPage1 = new TabPage();
      this.label16 = new Label();
      this.PreviewLauncherScrollSensitivity = new TrackBar();
      this.ZoomMouseScroll = new Label();
      this.label5 = new Label();
      this.label4 = new Label();
      this.label3 = new Label();
      this.label2 = new Label();
      this.label1 = new Label();
      this.trackBar5 = new TrackBar();
      this.trackBar4 = new TrackBar();
      this.trackBar3 = new TrackBar();
      this.trackBar2 = new TrackBar();
      this.trackBar1 = new TrackBar();
      this.trackBar6 = new TrackBar();
      this.tabPage4 = new TabPage();
      this.groupBox3 = new GroupBox();
      this.label15 = new Label();
      this.PreviewLaunchVerticalSpace = new NumericUpDown();
      this.label14 = new Label();
      this.PreviewLaunchWidth = new NumericUpDown();
      this.groupBox2 = new GroupBox();
      this.label10 = new Label();
      this.MaxPreviewSize = new Label();
      this.label11 = new Label();
      this.label12 = new Label();
      this.MaxPreviewSizeY = new NumericUpDown();
      this.MaxPreviewSizeX = new NumericUpDown();
      this.MinPreviewSize = new Label();
      this.label8 = new Label();
      this.label9 = new Label();
      this.MinPreviewSizeY = new NumericUpDown();
      this.MinPreviewSizeX = new NumericUpDown();
      this.tabPage5 = new TabPage();
      this.label19 = new Label();
      this.RequireHotkeyOnMinimize = new CheckBox();
      this.label18 = new Label();
      this.HideTaskbarButtons = new CheckBox();
      this.CreatePreviewsOnMinimize = new CheckBox();
      this.label17 = new Label();
      this.KeepTargetFromMinimize = new CheckBox();
      this.HideTargetByDefault = new CheckBox();
      this.ShowListofWindows = new CheckBox();
      this.MainOK = new Button();
      this.MainCancel = new Button();
      this.ApplyButton = new Button();
      this.EdgeDockingThicknessToolTip = new ToolTip(this.components);
      this.CentreDockingThicknessToolTip = new ToolTip(this.components);
      this.tabControl1.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.CentreDockingThickness.BeginInit();
      this.EdgeDockingThickness.BeginInit();
      this.groupBox1.SuspendLayout();
      this.CustomLocationY.BeginInit();
      this.CustomLocationX.BeginInit();
      this.Layout.SuspendLayout();
      this.groupBox11.SuspendLayout();
      this.MarginLeft.BeginInit();
      this.MarginRight.BeginInit();
      ((ISupportInitialize) this.pictureBox1).BeginInit();
      this.MarginBottom.BeginInit();
      this.MarginTop.BeginInit();
      this.groupBox10.SuspendLayout();
      this.PreviewHorizontalSpacing.BeginInit();
      this.PreviewVerticalSpacing.BeginInit();
      this.groupBox8.SuspendLayout();
      this.groupBox5.SuspendLayout();
      this.groupBox9.SuspendLayout();
      this.groupBox7.SuspendLayout();
      this.groupBox6.SuspendLayout();
      this.tabPage3.SuspendLayout();
      this.BackTransparency.BeginInit();
      this.ThumbTransparencyTB.BeginInit();
      this.tabPage1.SuspendLayout();
      this.PreviewLauncherScrollSensitivity.BeginInit();
      this.trackBar5.BeginInit();
      this.trackBar4.BeginInit();
      this.trackBar3.BeginInit();
      this.trackBar2.BeginInit();
      this.trackBar1.BeginInit();
      this.trackBar6.BeginInit();
      this.tabPage4.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.PreviewLaunchVerticalSpace.BeginInit();
      this.PreviewLaunchWidth.BeginInit();
      this.groupBox2.SuspendLayout();
      this.MaxPreviewSizeY.BeginInit();
      this.MaxPreviewSizeX.BeginInit();
      this.MinPreviewSizeY.BeginInit();
      this.MinPreviewSizeX.BeginInit();
      this.tabPage5.SuspendLayout();
      this.SuspendLayout();
      this.tabControl1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.tabControl1.Controls.Add((Control) this.tabPage2);
      this.tabControl1.Controls.Add((Control) this.Layout);
      this.tabControl1.Controls.Add((Control) this.tabPage3);
      this.tabControl1.Controls.Add((Control) this.tabPage1);
      this.tabControl1.Controls.Add((Control) this.tabPage4);
      this.tabControl1.Controls.Add((Control) this.tabPage5);
      this.tabControl1.Location = new Point(5, 5);
      this.tabControl1.Margin = new Padding(0);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new Size(309, 417);
      this.tabControl1.TabIndex = 0;
      this.tabPage2.Controls.Add((Control) this.groupBox4);
      this.tabPage2.Controls.Add((Control) this.groupBox1);
      this.tabPage2.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.tabPage2.Location = new Point(4, 22);
      this.tabPage2.Margin = new Padding(0);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Size = new Size(301, 391);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Location";
      this.tabPage2.UseVisualStyleBackColor = true;
      this.groupBox4.Controls.Add((Control) this.CentreDockingThickness);
      this.groupBox4.Controls.Add((Control) this.EdgeDockingThickness);
      this.groupBox4.Controls.Add((Control) this.FreeFormCentreDocking);
      this.groupBox4.Controls.Add((Control) this.FreeFormEdgeDocking);
      this.groupBox4.Controls.Add((Control) this.DockPreviewCB);
      this.groupBox4.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox4.Location = new Point(6, 0);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new Size(289, 116);
      this.groupBox4.TabIndex = 3;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Docking";
      this.CentreDockingThickness.Location = new Point(167, 77);
      NumericUpDown numericUpDown1 = this.CentreDockingThickness;
      int[] bits1 = new int[4];
      bits1[0] = 10000;
      Decimal num1 = new Decimal(bits1);
      numericUpDown1.Maximum = num1;
      this.CentreDockingThickness.Name = "CentreDockingThickness";
      this.CentreDockingThickness.Size = new Size(60, 27);
      this.CentreDockingThickness.TabIndex = 13;
      this.EdgeDockingThickness.Location = new Point(157, 45);
      NumericUpDown numericUpDown2 = this.EdgeDockingThickness;
      int[] bits2 = new int[4];
      bits2[0] = 10000;
      Decimal num2 = new Decimal(bits2);
      numericUpDown2.Maximum = num2;
      this.EdgeDockingThickness.Name = "EdgeDockingThickness";
      this.EdgeDockingThickness.Size = new Size(60, 27);
      this.EdgeDockingThickness.TabIndex = 12;
      this.FreeFormCentreDocking.AutoSize = true;
      this.FreeFormCentreDocking.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.FreeFormCentreDocking.Location = new Point(7, 82);
      this.FreeFormCentreDocking.Name = "FreeFormCentreDocking";
      this.FreeFormCentreDocking.Size = new Size(160, 19);
      this.FreeFormCentreDocking.TabIndex = 3;
      this.FreeFormCentreDocking.Text = "Freeform Centre Docking";
      this.FreeFormCentreDocking.UseVisualStyleBackColor = true;
      this.FreeFormCentreDocking.CheckedChanged += new EventHandler(this.FreeFormCentreDocking_CheckedChanged);
      this.FreeFormEdgeDocking.AutoSize = true;
      this.FreeFormEdgeDocking.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.FreeFormEdgeDocking.Location = new Point(7, 48);
      this.FreeFormEdgeDocking.Name = "FreeFormEdgeDocking";
      this.FreeFormEdgeDocking.Size = new Size(150, 19);
      this.FreeFormEdgeDocking.TabIndex = 2;
      this.FreeFormEdgeDocking.Text = "Freeform Edge Docking";
      this.FreeFormEdgeDocking.UseVisualStyleBackColor = true;
      this.FreeFormEdgeDocking.CheckedChanged += new EventHandler(this.FreeFormEdgeDocking_CheckedChanged);
      this.DockPreviewCB.AutoSize = true;
      this.DockPreviewCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.DockPreviewCB.Location = new Point(6, 20);
      this.DockPreviewCB.Name = "DockPreviewCB";
      this.DockPreviewCB.Size = new Size(191, 19);
      this.DockPreviewCB.TabIndex = 0;
      this.DockPreviewCB.Text = "Dock Preview to Start Location";
      this.DockPreviewCB.UseVisualStyleBackColor = true;
      this.DockPreviewCB.CheckedChanged += new EventHandler(this.DockPreviewCB_CheckedChanged);
      this.groupBox1.Controls.Add((Control) this.RightCB);
      this.groupBox1.Controls.Add((Control) this.LeftCB);
      this.groupBox1.Controls.Add((Control) this.TopCB);
      this.groupBox1.Controls.Add((Control) this.BottomCB);
      this.groupBox1.Controls.Add((Control) this.CentreCB);
      this.groupBox1.Controls.Add((Control) this.label7);
      this.groupBox1.Controls.Add((Control) this.label6);
      this.groupBox1.Controls.Add((Control) this.CustomLocationY);
      this.groupBox1.Controls.Add((Control) this.CustomLocationX);
      this.groupBox1.Controls.Add((Control) this.CustomCB);
      this.groupBox1.Controls.Add((Control) this.WindowsDefaultCB);
      this.groupBox1.Controls.Add((Control) this.TopRightCB);
      this.groupBox1.Controls.Add((Control) this.BottomRightCB);
      this.groupBox1.Controls.Add((Control) this.TopMiddleCB);
      this.groupBox1.Controls.Add((Control) this.BottomMiddleCB);
      this.groupBox1.Controls.Add((Control) this.TopLeftCB);
      this.groupBox1.Controls.Add((Control) this.BottomLeftCB);
      this.groupBox1.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox1.Location = new Point(22, 117);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(256, 270);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Preview Start Location";
      this.RightCB.AutoSize = true;
      this.RightCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.RightCB.Location = new Point(142, 143);
      this.RightCB.Name = "RightCB";
      this.RightCB.Size = new Size(54, 19);
      this.RightCB.TabIndex = 16;
      this.RightCB.Text = "Right";
      this.RightCB.UseVisualStyleBackColor = true;
      this.RightCB.CheckedChanged += new EventHandler(this.RightCB_CheckedChanged);
      this.LeftCB.AutoSize = true;
      this.LeftCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.LeftCB.Location = new Point(16, 145);
      this.LeftCB.Name = "LeftCB";
      this.LeftCB.Size = new Size(45, 19);
      this.LeftCB.TabIndex = 15;
      this.LeftCB.Text = "Left";
      this.LeftCB.UseVisualStyleBackColor = true;
      this.LeftCB.CheckedChanged += new EventHandler(this.LeftCB_CheckedChanged);
      this.TopCB.AutoSize = true;
      this.TopCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.TopCB.Location = new Point(142, 113);
      this.TopCB.Name = "TopCB";
      this.TopCB.Size = new Size(45, 19);
      this.TopCB.TabIndex = 14;
      this.TopCB.Text = "Top";
      this.TopCB.UseVisualStyleBackColor = true;
      this.TopCB.CheckedChanged += new EventHandler(this.TopCB_CheckedChanged);
      this.BottomCB.AutoSize = true;
      this.BottomCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.BottomCB.Location = new Point(16, 119);
      this.BottomCB.Name = "BottomCB";
      this.BottomCB.Size = new Size(65, 19);
      this.BottomCB.TabIndex = 13;
      this.BottomCB.Text = "Bottom";
      this.BottomCB.UseVisualStyleBackColor = true;
      this.BottomCB.CheckedChanged += new EventHandler(this.BottomCB_CheckedChanged);
      this.CentreCB.AutoSize = true;
      this.CentreCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.CentreCB.Location = new Point(143, 170);
      this.CentreCB.Name = "CentreCB";
      this.CentreCB.Size = new Size(61, 19);
      this.CentreCB.TabIndex = 12;
      this.CentreCB.Text = "Centre";
      this.CentreCB.UseVisualStyleBackColor = true;
      this.CentreCB.CheckedChanged += new EventHandler(this.CentreCB_CheckedChanged);
      this.label7.AutoSize = true;
      this.label7.Location = new Point(111, 235);
      this.label7.Name = "label7";
      this.label7.Size = new Size(17, 19);
      this.label7.TabIndex = 11;
      this.label7.Text = "Y";
      this.label6.AutoSize = true;
      this.label6.Location = new Point(111, 203);
      this.label6.Name = "label6";
      this.label6.Size = new Size(18, 19);
      this.label6.TabIndex = 10;
      this.label6.Text = "X";
      this.CustomLocationY.Location = new Point(135, 235);
      NumericUpDown numericUpDown3 = this.CustomLocationY;
      int[] bits3 = new int[4];
      bits3[0] = 10000;
      Decimal num3 = new Decimal(bits3);
      numericUpDown3.Maximum = num3;
      this.CustomLocationY.Name = "CustomLocationY";
      this.CustomLocationY.Size = new Size(84, 27);
      this.CustomLocationY.TabIndex = 9;
      this.CustomLocationX.Location = new Point(135, 201);
      NumericUpDown numericUpDown4 = this.CustomLocationX;
      int[] bits4 = new int[4];
      bits4[0] = 10000;
      Decimal num4 = new Decimal(bits4);
      numericUpDown4.Maximum = num4;
      this.CustomLocationX.Name = "CustomLocationX";
      this.CustomLocationX.Size = new Size(84, 27);
      this.CustomLocationX.TabIndex = 8;
      this.CustomCB.AutoSize = true;
      this.CustomCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.CustomCB.Location = new Point(39, 221);
      this.CustomCB.Name = "CustomCB";
      this.CustomCB.Size = new Size(67, 19);
      this.CustomCB.TabIndex = 7;
      this.CustomCB.Text = "Custom";
      this.CustomCB.UseVisualStyleBackColor = true;
      this.CustomCB.CheckedChanged += new EventHandler(this.CustomCB_CheckedChanged);
      this.WindowsDefaultCB.AutoSize = true;
      this.WindowsDefaultCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.WindowsDefaultCB.Location = new Point(17, 170);
      this.WindowsDefaultCB.Name = "WindowsDefaultCB";
      this.WindowsDefaultCB.Size = new Size(121, 19);
      this.WindowsDefaultCB.TabIndex = 6;
      this.WindowsDefaultCB.Text = "Windows Default";
      this.WindowsDefaultCB.UseVisualStyleBackColor = true;
      this.WindowsDefaultCB.CheckedChanged += new EventHandler(this.WindowsDefaultCB_CheckedChanged);
      this.TopRightCB.AutoSize = true;
      this.TopRightCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.TopRightCB.Location = new Point(143, 87);
      this.TopRightCB.Name = "TopRightCB";
      this.TopRightCB.Size = new Size(76, 19);
      this.TopRightCB.TabIndex = 5;
      this.TopRightCB.Text = "Top Right";
      this.TopRightCB.UseVisualStyleBackColor = true;
      this.TopRightCB.CheckedChanged += new EventHandler(this.TopRightCB_CheckedChanged);
      this.BottomRightCB.AutoSize = true;
      this.BottomRightCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.BottomRightCB.Location = new Point(17, 90);
      this.BottomRightCB.Name = "BottomRightCB";
      this.BottomRightCB.Size = new Size(96, 19);
      this.BottomRightCB.TabIndex = 4;
      this.BottomRightCB.Text = "Bottom Right";
      this.BottomRightCB.UseVisualStyleBackColor = true;
      this.BottomRightCB.CheckedChanged += new EventHandler(this.BottomRightCB_CheckedChanged);
      this.TopMiddleCB.AutoSize = true;
      this.TopMiddleCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.TopMiddleCB.Location = new Point(143, 60);
      this.TopMiddleCB.Name = "TopMiddleCB";
      this.TopMiddleCB.Size = new Size(87, 19);
      this.TopMiddleCB.TabIndex = 3;
      this.TopMiddleCB.Text = "Top Middle";
      this.TopMiddleCB.UseVisualStyleBackColor = true;
      this.TopMiddleCB.CheckedChanged += new EventHandler(this.TopMiddleCB_CheckedChanged);
      this.BottomMiddleCB.AutoSize = true;
      this.BottomMiddleCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.BottomMiddleCB.Location = new Point(17, 61);
      this.BottomMiddleCB.Name = "BottomMiddleCB";
      this.BottomMiddleCB.Size = new Size(107, 19);
      this.BottomMiddleCB.TabIndex = 2;
      this.BottomMiddleCB.Text = "Bottom Middle";
      this.BottomMiddleCB.UseVisualStyleBackColor = true;
      this.BottomMiddleCB.CheckedChanged += new EventHandler(this.BottomMiddleCB_CheckedChanged);
      this.TopLeftCB.AutoSize = true;
      this.TopLeftCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.TopLeftCB.Location = new Point(144, 32);
      this.TopLeftCB.Name = "TopLeftCB";
      this.TopLeftCB.Size = new Size(67, 19);
      this.TopLeftCB.TabIndex = 1;
      this.TopLeftCB.Text = "Top Left";
      this.TopLeftCB.UseVisualStyleBackColor = true;
      this.TopLeftCB.CheckedChanged += new EventHandler(this.TopLeftCB_CheckedChanged);
      this.BottomLeftCB.AutoSize = true;
      this.BottomLeftCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.BottomLeftCB.Location = new Point(17, 28);
      this.BottomLeftCB.Name = "BottomLeftCB";
      this.BottomLeftCB.Size = new Size(87, 19);
      this.BottomLeftCB.TabIndex = 0;
      this.BottomLeftCB.Text = "Bottom Left";
      this.BottomLeftCB.UseVisualStyleBackColor = true;
      this.BottomLeftCB.CheckedChanged += new EventHandler(this.BottomLeftCB_CheckedChanged);
      this.Layout.Controls.Add((Control) this.groupBox11);
      this.Layout.Controls.Add((Control) this.groupBox10);
      this.Layout.Controls.Add((Control) this.groupBox8);
      this.Layout.Controls.Add((Control) this.groupBox5);
      this.Layout.Location = new Point(4, 22);
      this.Layout.Margin = new Padding(0);
      this.Layout.Name = "Layout";
      this.Layout.Size = new Size(301, 391);
      this.Layout.TabIndex = 5;
      this.Layout.Text = "Layout";
      this.Layout.UseVisualStyleBackColor = true;
      this.groupBox11.Controls.Add((Control) this.CustomTaskbarMargins);
      this.groupBox11.Controls.Add((Control) this.AutomaticTaskbarMargins);
      this.groupBox11.Controls.Add((Control) this.MarginLeft);
      this.groupBox11.Controls.Add((Control) this.MarginRight);
      this.groupBox11.Controls.Add((Control) this.pictureBox1);
      this.groupBox11.Controls.Add((Control) this.MarginBottom);
      this.groupBox11.Controls.Add((Control) this.MarginTop);
      this.groupBox11.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox11.Location = new Point(9, 215);
      this.groupBox11.Name = "groupBox11";
      this.groupBox11.Size = new Size(283, 173);
      this.groupBox11.TabIndex = 39;
      this.groupBox11.TabStop = false;
      this.groupBox11.Text = "Account for Vista Taskbar Margins";
      this.CustomTaskbarMargins.AutoSize = true;
      this.CustomTaskbarMargins.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.CustomTaskbarMargins.Location = new Point(217, 26);
      this.CustomTaskbarMargins.Name = "CustomTaskbarMargins";
      this.CustomTaskbarMargins.Size = new Size(67, 19);
      this.CustomTaskbarMargins.TabIndex = 40;
      this.CustomTaskbarMargins.Text = "Custom";
      this.CustomTaskbarMargins.UseVisualStyleBackColor = true;
      this.CustomTaskbarMargins.CheckedChanged += new EventHandler(this.CustomTaskbarMargins_CheckedChanged);
      this.AutomaticTaskbarMargins.AutoSize = true;
      this.AutomaticTaskbarMargins.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.AutomaticTaskbarMargins.Location = new Point(6, 26);
      this.AutomaticTaskbarMargins.Name = "AutomaticTaskbarMargins";
      this.AutomaticTaskbarMargins.Size = new Size(81, 19);
      this.AutomaticTaskbarMargins.TabIndex = 39;
      this.AutomaticTaskbarMargins.Text = "Automatic";
      this.AutomaticTaskbarMargins.UseVisualStyleBackColor = true;
      this.AutomaticTaskbarMargins.CheckedChanged += new EventHandler(this.AutomaticTaskbarMargins_CheckedChanged);
      this.MarginLeft.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MarginLeft.Location = new Point(4, 81);
      this.MarginLeft.Maximum = new Decimal(new int[4]);
      this.MarginLeft.Name = "MarginLeft";
      this.MarginLeft.Size = new Size(60, 23);
      this.MarginLeft.TabIndex = 38;
      this.MarginRight.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MarginRight.Location = new Point(218, 81);
      this.MarginRight.Maximum = new Decimal(new int[4]);
      this.MarginRight.Name = "MarginRight";
      this.MarginRight.Size = new Size(60, 23);
      this.MarginRight.TabIndex = 37;
      this.pictureBox1.Image = (Image) Resources.Desktop;
      this.pictureBox1.Location = new Point(68, 53);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new Size(146, 86);
      this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
      this.pictureBox1.TabIndex = 36;
      this.pictureBox1.TabStop = false;
      this.MarginBottom.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MarginBottom.Location = new Point(109, 143);
      this.MarginBottom.Maximum = new Decimal(new int[4]);
      this.MarginBottom.Name = "MarginBottom";
      this.MarginBottom.Size = new Size(60, 23);
      this.MarginBottom.TabIndex = 35;
      this.MarginTop.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MarginTop.Location = new Point(110, 26);
      this.MarginTop.Maximum = new Decimal(new int[4]);
      this.MarginTop.Name = "MarginTop";
      this.MarginTop.Size = new Size(60, 23);
      this.MarginTop.TabIndex = 34;
      this.groupBox10.Controls.Add((Control) this.label20);
      this.groupBox10.Controls.Add((Control) this.VerticalSpacingLabel);
      this.groupBox10.Controls.Add((Control) this.PreviewHorizontalSpacing);
      this.groupBox10.Controls.Add((Control) this.PreviewVerticalSpacing);
      this.groupBox10.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox10.Location = new Point(8, 155);
      this.groupBox10.Name = "groupBox10";
      this.groupBox10.Size = new Size(283, 56);
      this.groupBox10.TabIndex = 38;
      this.groupBox10.TabStop = false;
      this.groupBox10.Text = "Spacing Between Previews";
      this.label20.AutoSize = true;
      this.label20.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label20.Location = new Point(132, 24);
      this.label20.Name = "label20";
      this.label20.Size = new Size(65, 15);
      this.label20.TabIndex = 37;
      this.label20.Text = "Horizontal";
      this.VerticalSpacingLabel.AutoSize = true;
      this.VerticalSpacingLabel.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.VerticalSpacingLabel.Location = new Point(13, 24);
      this.VerticalSpacingLabel.Name = "VerticalSpacingLabel";
      this.VerticalSpacingLabel.Size = new Size(48, 15);
      this.VerticalSpacingLabel.TabIndex = 36;
      this.VerticalSpacingLabel.Text = "Vertical";
      this.PreviewHorizontalSpacing.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.PreviewHorizontalSpacing.Location = new Point(198, 22);
      this.PreviewHorizontalSpacing.Minimum = new Decimal(new int[4]
      {
        100,
        0,
        0,
        int.MinValue
      });
      this.PreviewHorizontalSpacing.Name = "PreviewHorizontalSpacing";
      this.PreviewHorizontalSpacing.Size = new Size(60, 23);
      this.PreviewHorizontalSpacing.TabIndex = 35;
      this.PreviewVerticalSpacing.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.PreviewVerticalSpacing.Location = new Point(66, 22);
      this.PreviewVerticalSpacing.Minimum = new Decimal(new int[4]
      {
        100,
        0,
        0,
        int.MinValue
      });
      this.PreviewVerticalSpacing.Name = "PreviewVerticalSpacing";
      this.PreviewVerticalSpacing.Size = new Size(60, 23);
      this.PreviewVerticalSpacing.TabIndex = 34;
      this.groupBox8.Controls.Add((Control) this.PinDesktop);
      this.groupBox8.Controls.Add((Control) this.PinNormal);
      this.groupBox8.Controls.Add((Control) this.PinTopMost);
      this.groupBox8.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox8.Location = new Point(8, 95);
      this.groupBox8.Name = "groupBox8";
      this.groupBox8.Size = new Size(284, 55);
      this.groupBox8.TabIndex = 33;
      this.groupBox8.TabStop = false;
      this.groupBox8.Text = "Pin Previews To.....";
      this.PinDesktop.AutoSize = true;
      this.PinDesktop.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.PinDesktop.Location = new Point(205, 26);
      this.PinDesktop.Name = "PinDesktop";
      this.PinDesktop.Size = new Size(70, 19);
      this.PinDesktop.TabIndex = 29;
      this.PinDesktop.Text = "Desktop";
      this.PinDesktop.UseVisualStyleBackColor = true;
      this.PinDesktop.CheckedChanged += new EventHandler(this.PinDesktop_CheckedChanged);
      this.PinNormal.AutoSize = true;
      this.PinNormal.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.PinNormal.Location = new Point(113, 26);
      this.PinNormal.Name = "PinNormal";
      this.PinNormal.Size = new Size(67, 19);
      this.PinNormal.TabIndex = 28;
      this.PinNormal.Text = "Normal";
      this.PinNormal.UseVisualStyleBackColor = true;
      this.PinNormal.CheckedChanged += new EventHandler(this.PinNormal_CheckedChanged);
      this.PinTopMost.AutoSize = true;
      this.PinTopMost.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.PinTopMost.Location = new Point(15, 26);
      this.PinTopMost.Name = "PinTopMost";
      this.PinTopMost.Size = new Size(72, 19);
      this.PinTopMost.TabIndex = 27;
      this.PinTopMost.Text = "Topmost";
      this.PinTopMost.UseVisualStyleBackColor = true;
      this.PinTopMost.CheckedChanged += new EventHandler(this.PinTopMost_CheckedChanged);
      this.groupBox5.Controls.Add((Control) this.groupBox9);
      this.groupBox5.Controls.Add((Control) this.groupBox7);
      this.groupBox5.Controls.Add((Control) this.groupBox6);
      this.groupBox5.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox5.Location = new Point(8, 3);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new Size(284, 91);
      this.groupBox5.TabIndex = 31;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Preview Layout Direction";
      this.groupBox9.Controls.Add((Control) this.ColumnsFirst);
      this.groupBox9.Controls.Add((Control) this.RowsFirst);
      this.groupBox9.Location = new Point(159, 13);
      this.groupBox9.Name = "groupBox9";
      this.groupBox9.Size = new Size(119, 68);
      this.groupBox9.TabIndex = 32;
      this.groupBox9.TabStop = false;
      this.ColumnsFirst.AutoSize = true;
      this.ColumnsFirst.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.ColumnsFirst.Location = new Point(14, 41);
      this.ColumnsFirst.Name = "ColumnsFirst";
      this.ColumnsFirst.Size = new Size(102, 19);
      this.ColumnsFirst.TabIndex = 29;
      this.ColumnsFirst.Text = "Columns First";
      this.ColumnsFirst.UseVisualStyleBackColor = true;
      this.ColumnsFirst.CheckedChanged += new EventHandler(this.ColumnsFirst_CheckedChanged);
      this.RowsFirst.AutoSize = true;
      this.RowsFirst.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.RowsFirst.Location = new Point(14, 16);
      this.RowsFirst.Name = "RowsFirst";
      this.RowsFirst.Size = new Size(83, 19);
      this.RowsFirst.TabIndex = 28;
      this.RowsFirst.Text = "Rows First";
      this.RowsFirst.UseVisualStyleBackColor = true;
      this.RowsFirst.CheckedChanged += new EventHandler(this.RowsFirst_CheckedChanged);
      this.groupBox7.Controls.Add((Control) this.DownDirectionCB);
      this.groupBox7.Controls.Add((Control) this.UpDirectionCB);
      this.groupBox7.Location = new Point(78, 14);
      this.groupBox7.Name = "groupBox7";
      this.groupBox7.Size = new Size(75, 68);
      this.groupBox7.TabIndex = 31;
      this.groupBox7.TabStop = false;
      this.DownDirectionCB.AutoSize = true;
      this.DownDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.DownDirectionCB.Location = new Point(11, 41);
      this.DownDirectionCB.Name = "DownDirectionCB";
      this.DownDirectionCB.Size = new Size(57, 19);
      this.DownDirectionCB.TabIndex = 29;
      this.DownDirectionCB.Text = "Down";
      this.DownDirectionCB.UseVisualStyleBackColor = true;
      this.DownDirectionCB.CheckedChanged += new EventHandler(this.DownDirectionCB_CheckedChanged);
      this.UpDirectionCB.AutoSize = true;
      this.UpDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.UpDirectionCB.Location = new Point(11, 16);
      this.UpDirectionCB.Name = "UpDirectionCB";
      this.UpDirectionCB.Size = new Size(41, 19);
      this.UpDirectionCB.TabIndex = 28;
      this.UpDirectionCB.Text = "Up";
      this.UpDirectionCB.UseVisualStyleBackColor = true;
      this.UpDirectionCB.CheckedChanged += new EventHandler(this.UpDirectionCB_CheckedChanged);
      this.groupBox6.Controls.Add((Control) this.RightDirectionCB);
      this.groupBox6.Controls.Add((Control) this.LeftDirectionCB);
      this.groupBox6.Location = new Point(7, 14);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new Size(65, 68);
      this.groupBox6.TabIndex = 30;
      this.groupBox6.TabStop = false;
      this.RightDirectionCB.AutoSize = true;
      this.RightDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.RightDirectionCB.Location = new Point(9, 42);
      this.RightDirectionCB.Name = "RightDirectionCB";
      this.RightDirectionCB.Size = new Size(54, 19);
      this.RightDirectionCB.TabIndex = 27;
      this.RightDirectionCB.Text = "Right";
      this.RightDirectionCB.UseVisualStyleBackColor = true;
      this.RightDirectionCB.CheckedChanged += new EventHandler(this.RightDirectionCB_CheckedChanged);
      this.LeftDirectionCB.AutoSize = true;
      this.LeftDirectionCB.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.LeftDirectionCB.Location = new Point(9, 17);
      this.LeftDirectionCB.Name = "LeftDirectionCB";
      this.LeftDirectionCB.Size = new Size(45, 19);
      this.LeftDirectionCB.TabIndex = 26;
      this.LeftDirectionCB.Text = "Left";
      this.LeftDirectionCB.UseVisualStyleBackColor = true;
      this.LeftDirectionCB.CheckedChanged += new EventHandler(this.LeftDirectionCB_CheckedChanged);
      this.tabPage3.Controls.Add((Control) this.label22);
      this.tabPage3.Controls.Add((Control) this.label21);
      this.tabPage3.Controls.Add((Control) this.EnableAeroBack);
      this.tabPage3.Controls.Add((Control) this.ShowBackText);
      this.tabPage3.Controls.Add((Control) this.label13);
      this.tabPage3.Controls.Add((Control) this.BackTransparency);
      this.tabPage3.Controls.Add((Control) this.TransparencyLabel);
      this.tabPage3.Controls.Add((Control) this.ThumbTransparencyTB);
      this.tabPage3.Controls.Add((Control) this.ShowBordersByDefault);
      this.tabPage3.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.tabPage3.Location = new Point(4, 22);
      this.tabPage3.Margin = new Padding(0);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Size = new Size(301, 391);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "Appearance";
      this.tabPage3.UseVisualStyleBackColor = true;
      this.label22.AutoSize = true;
      this.label22.Location = new Point(76, 171);
      this.label22.Name = "label22";
      this.label22.Size = new Size(126, 13);
      this.label22.TabIndex = 29;
      this.label22.Text = "with Aero Background on.";
      this.label21.AutoSize = true;
      this.label21.Location = new Point(18, 157);
      this.label21.Name = "label21";
      this.label21.Size = new Size(251, 13);
      this.label21.TabIndex = 28;
      this.label21.Text = "Note: Context menu may appear behind the preview";
      this.EnableAeroBack.AutoSize = true;
      this.EnableAeroBack.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.EnableAeroBack.Location = new Point(27, 134);
      this.EnableAeroBack.Name = "EnableAeroBack";
      this.EnableAeroBack.Size = new Size(225, 19);
      this.EnableAeroBack.TabIndex = 27;
      this.EnableAeroBack.Text = "Enable Aero Background in Previews";
      this.EnableAeroBack.UseVisualStyleBackColor = true;
      this.EnableAeroBack.CheckedChanged += new EventHandler(this.EnableAeroBack_CheckedChanged);
      this.ShowBackText.AutoSize = true;
      this.ShowBackText.Enabled = false;
      this.ShowBackText.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.ShowBackText.Location = new Point(64, 329);
      this.ShowBackText.Name = "ShowBackText";
      this.ShowBackText.Size = new Size(147, 19);
      this.ShowBackText.TabIndex = 18;
      this.ShowBackText.Text = "Show Background Text";
      this.ShowBackText.UseVisualStyleBackColor = true;
      this.ShowBackText.Visible = false;
      this.ShowBackText.CheckedChanged += new EventHandler(this.ShowBackText_CheckedChanged);
      this.label13.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label13.AutoSize = true;
      this.label13.Location = new Point(78, 67);
      this.label13.Name = "label13";
      this.label13.Size = new Size(106, 13);
      this.label13.TabIndex = 17;
      this.label13.Text = "Overall Transparency";
      this.BackTransparency.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.BackTransparency.BackColor = SystemColors.Window;
      this.BackTransparency.Location = new Point(18, 80);
      this.BackTransparency.Margin = new Padding(0);
      this.BackTransparency.Maximum = 254;
      this.BackTransparency.Minimum = 1;
      this.BackTransparency.Name = "BackTransparency";
      this.BackTransparency.Size = new Size(269, 45);
      this.BackTransparency.TabIndex = 16;
      this.BackTransparency.TickStyle = TickStyle.None;
      this.BackTransparency.Value = 1;
      this.BackTransparency.Scroll += new EventHandler(this.TransparencyTB_Scroll);
      this.TransparencyLabel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.TransparencyLabel.AutoSize = true;
      this.TransparencyLabel.Location = new Point(78, 22);
      this.TransparencyLabel.Name = "TransparencyLabel";
      this.TransparencyLabel.Size = new Size(110, 13);
      this.TransparencyLabel.TabIndex = 15;
      this.TransparencyLabel.Text = "Preview Transparency";
      this.ThumbTransparencyTB.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ThumbTransparencyTB.BackColor = SystemColors.Window;
      this.ThumbTransparencyTB.Location = new Point(18, 35);
      this.ThumbTransparencyTB.Margin = new Padding(0);
      this.ThumbTransparencyTB.Maximum = (int) byte.MaxValue;
      this.ThumbTransparencyTB.Name = "ThumbTransparencyTB";
      this.ThumbTransparencyTB.Size = new Size(269, 45);
      this.ThumbTransparencyTB.TabIndex = 14;
      this.ThumbTransparencyTB.TickStyle = TickStyle.None;
      this.ThumbTransparencyTB.Value = 1;
      this.ThumbTransparencyTB.Scroll += new EventHandler(this.TransparencyTB_Scroll);
      this.ShowBordersByDefault.AutoSize = true;
      this.ShowBordersByDefault.Enabled = false;
      this.ShowBordersByDefault.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.ShowBordersByDefault.Location = new Point(64, 363);
      this.ShowBordersByDefault.Name = "ShowBordersByDefault";
      this.ShowBordersByDefault.Size = new Size(160, 19);
      this.ShowBordersByDefault.TabIndex = 13;
      this.ShowBordersByDefault.Text = "Show Borders By Default";
      this.ShowBordersByDefault.UseVisualStyleBackColor = true;
      this.ShowBordersByDefault.Visible = false;
      this.tabPage1.Controls.Add((Control) this.label16);
      this.tabPage1.Controls.Add((Control) this.PreviewLauncherScrollSensitivity);
      this.tabPage1.Controls.Add((Control) this.ZoomMouseScroll);
      this.tabPage1.Controls.Add((Control) this.label5);
      this.tabPage1.Controls.Add((Control) this.label4);
      this.tabPage1.Controls.Add((Control) this.label3);
      this.tabPage1.Controls.Add((Control) this.label2);
      this.tabPage1.Controls.Add((Control) this.label1);
      this.tabPage1.Controls.Add((Control) this.trackBar5);
      this.tabPage1.Controls.Add((Control) this.trackBar4);
      this.tabPage1.Controls.Add((Control) this.trackBar3);
      this.tabPage1.Controls.Add((Control) this.trackBar2);
      this.tabPage1.Controls.Add((Control) this.trackBar1);
      this.tabPage1.Controls.Add((Control) this.trackBar6);
      this.tabPage1.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.tabPage1.Location = new Point(4, 22);
      this.tabPage1.Margin = new Padding(0);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Size = new Size(301, 391);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Sensitivity";
      this.tabPage1.UseVisualStyleBackColor = true;
      this.label16.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label16.AutoSize = true;
      this.label16.Location = new Point(65, 251);
      this.label16.Name = "label16";
      this.label16.Size = new Size(169, 13);
      this.label16.TabIndex = 13;
      this.label16.Text = "Preview Launcher Scroll Sensitivity";
      this.PreviewLauncherScrollSensitivity.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.PreviewLauncherScrollSensitivity.BackColor = SystemColors.Window;
      this.PreviewLauncherScrollSensitivity.Location = new Point(23, 260);
      this.PreviewLauncherScrollSensitivity.Maximum = 200;
      this.PreviewLauncherScrollSensitivity.Minimum = 1;
      this.PreviewLauncherScrollSensitivity.Name = "PreviewLauncherScrollSensitivity";
      this.PreviewLauncherScrollSensitivity.Size = new Size(271, 45);
      this.PreviewLauncherScrollSensitivity.TabIndex = 12;
      this.PreviewLauncherScrollSensitivity.TickStyle = TickStyle.None;
      this.PreviewLauncherScrollSensitivity.Value = 50;
      this.ZoomMouseScroll.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ZoomMouseScroll.AutoSize = true;
      this.ZoomMouseScroll.Location = new Point(65, 174);
      this.ZoomMouseScroll.Name = "ZoomMouseScroll";
      this.ZoomMouseScroll.Size = new Size(175, 13);
      this.ZoomMouseScroll.TabIndex = 11;
      this.ZoomMouseScroll.Text = "Zoom Mouse Scroll Wheel Senstivity";
      this.label5.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label5.AutoSize = true;
      this.label5.Location = new Point(81, 136);
      this.label5.Name = "label5";
      this.label5.Size = new Size(134, 13);
      this.label5.TabIndex = 9;
      this.label5.Text = "Zoom Arrow Keys Senstivity";
      this.label4.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(87, 210);
      this.label4.Name = "label4";
      this.label4.Size = new Size(125, 13);
      this.label4.TabIndex = 8;
      this.label4.Text = "Window Resize Senstivity";
      this.label3.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label3.AutoSize = true;
      this.label3.Location = new Point(93, 94);
      this.label3.Name = "label3";
      this.label3.Size = new Size(104, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Zoom Click Senstivity";
      this.label2.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(70, 52);
      this.label2.Name = "label2";
      this.label2.Size = new Size(165, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Zoom Divisions (Affects All Zooms)";
      this.label1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(87, 13);
      this.label1.Name = "label1";
      this.label1.Size = new Size(109, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "Default Scaling Factor";
      this.trackBar5.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.trackBar5.BackColor = SystemColors.Window;
      this.trackBar5.Location = new Point(23, 219);
      this.trackBar5.Maximum = 150;
      this.trackBar5.Minimum = 50;
      this.trackBar5.Name = "trackBar5";
      this.trackBar5.Size = new Size(271, 45);
      this.trackBar5.TabIndex = 4;
      this.trackBar5.TickStyle = TickStyle.None;
      this.trackBar5.Value = 50;
      this.trackBar5.Scroll += new EventHandler(this.trackBar5_Scroll);
      this.trackBar4.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.trackBar4.BackColor = SystemColors.Window;
      this.trackBar4.Location = new Point(23, 143);
      this.trackBar4.Margin = new Padding(0);
      this.trackBar4.Maximum = 25;
      this.trackBar4.Minimum = 1;
      this.trackBar4.Name = "trackBar4";
      this.trackBar4.Size = new Size(269, 45);
      this.trackBar4.TabIndex = 3;
      this.trackBar4.TickStyle = TickStyle.None;
      this.trackBar4.Value = 1;
      this.trackBar4.Scroll += new EventHandler(this.trackBar4_Scroll);
      this.trackBar3.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.trackBar3.BackColor = SystemColors.Window;
      this.trackBar3.Location = new Point(23, 102);
      this.trackBar3.Margin = new Padding(0);
      this.trackBar3.Maximum = 40;
      this.trackBar3.Minimum = 1;
      this.trackBar3.Name = "trackBar3";
      this.trackBar3.Size = new Size(269, 45);
      this.trackBar3.TabIndex = 2;
      this.trackBar3.TickStyle = TickStyle.None;
      this.trackBar3.Value = 1;
      this.trackBar3.Scroll += new EventHandler(this.trackBar3_Scroll);
      this.trackBar2.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.trackBar2.BackColor = SystemColors.Window;
      this.trackBar2.Location = new Point(25, 59);
      this.trackBar2.Maximum = 40;
      this.trackBar2.Minimum = 1;
      this.trackBar2.Name = "trackBar2";
      this.trackBar2.Size = new Size(269, 45);
      this.trackBar2.TabIndex = 1;
      this.trackBar2.TickStyle = TickStyle.None;
      this.trackBar2.Value = 1;
      this.trackBar2.Scroll += new EventHandler(this.trackBar2_Scroll);
      this.trackBar1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.trackBar1.BackColor = SystemColors.Window;
      this.trackBar1.Location = new Point(23, 20);
      this.trackBar1.Margin = new Padding(0);
      this.trackBar1.Maximum = 80;
      this.trackBar1.Minimum = 1;
      this.trackBar1.Name = "trackBar1";
      this.trackBar1.Size = new Size(269, 45);
      this.trackBar1.TabIndex = 0;
      this.trackBar1.TickStyle = TickStyle.None;
      this.trackBar1.Value = 1;
      this.trackBar1.Scroll += new EventHandler(this.trackBar1_Scroll);
      this.trackBar6.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.trackBar6.BackColor = SystemColors.Window;
      this.trackBar6.Location = new Point(28, 185);
      this.trackBar6.Margin = new Padding(0);
      this.trackBar6.Maximum = 140;
      this.trackBar6.Minimum = 100;
      this.trackBar6.Name = "trackBar6";
      this.trackBar6.Size = new Size(269, 45);
      this.trackBar6.TabIndex = 10;
      this.trackBar6.TickStyle = TickStyle.None;
      this.trackBar6.Value = 130;
      this.trackBar6.Scroll += new EventHandler(this.trackBar6_Scroll);
      this.tabPage4.Controls.Add((Control) this.groupBox3);
      this.tabPage4.Controls.Add((Control) this.groupBox2);
      this.tabPage4.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.tabPage4.Location = new Point(4, 22);
      this.tabPage4.Margin = new Padding(0);
      this.tabPage4.Name = "tabPage4";
      this.tabPage4.Size = new Size(301, 391);
      this.tabPage4.TabIndex = 3;
      this.tabPage4.Text = "Size";
      this.tabPage4.UseVisualStyleBackColor = true;
      this.groupBox3.Controls.Add((Control) this.label15);
      this.groupBox3.Controls.Add((Control) this.PreviewLaunchVerticalSpace);
      this.groupBox3.Controls.Add((Control) this.label14);
      this.groupBox3.Controls.Add((Control) this.PreviewLaunchWidth);
      this.groupBox3.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox3.Location = new Point(17, 141);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new Size(271, 93);
      this.groupBox3.TabIndex = 23;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Preview Launch Menu";
      this.label15.AutoSize = true;
      this.label15.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label15.Location = new Point(8, 55);
      this.label15.Name = "label15";
      this.label15.Size = new Size(94, 15);
      this.label15.TabIndex = 18;
      this.label15.Text = "Vertical Spacing";
      this.PreviewLaunchVerticalSpace.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.PreviewLaunchVerticalSpace.Location = new Point(108, 53);
      NumericUpDown numericUpDown5 = this.PreviewLaunchVerticalSpace;
      int[] bits5 = new int[4];
      bits5[0] = 30000;
      Decimal num5 = new Decimal(bits5);
      numericUpDown5.Maximum = num5;
      NumericUpDown numericUpDown6 = this.PreviewLaunchVerticalSpace;
      int[] bits6 = new int[4];
      bits6[0] = 1;
      Decimal num6 = new Decimal(bits6);
      numericUpDown6.Minimum = num6;
      this.PreviewLaunchVerticalSpace.Name = "PreviewLaunchVerticalSpace";
      this.PreviewLaunchVerticalSpace.Size = new Size(84, 23);
      this.PreviewLaunchVerticalSpace.TabIndex = 17;
      NumericUpDown numericUpDown7 = this.PreviewLaunchVerticalSpace;
      int[] bits7 = new int[4];
      bits7[0] = 1;
      Decimal num7 = new Decimal(bits7);
      numericUpDown7.Value = num7;
      this.label14.AutoSize = true;
      this.label14.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label14.Location = new Point(8, 26);
      this.label14.Name = "label14";
      this.label14.Size = new Size(41, 15);
      this.label14.TabIndex = 16;
      this.label14.Text = "Width";
      this.PreviewLaunchWidth.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.PreviewLaunchWidth.Location = new Point(55, 24);
      NumericUpDown numericUpDown8 = this.PreviewLaunchWidth;
      int[] bits8 = new int[4];
      bits8[0] = 30000;
      Decimal num8 = new Decimal(bits8);
      numericUpDown8.Maximum = num8;
      NumericUpDown numericUpDown9 = this.PreviewLaunchWidth;
      int[] bits9 = new int[4];
      bits9[0] = 1;
      Decimal num9 = new Decimal(bits9);
      numericUpDown9.Minimum = num9;
      this.PreviewLaunchWidth.Name = "PreviewLaunchWidth";
      this.PreviewLaunchWidth.Size = new Size(84, 23);
      this.PreviewLaunchWidth.TabIndex = 15;
      NumericUpDown numericUpDown10 = this.PreviewLaunchWidth;
      int[] bits10 = new int[4];
      bits10[0] = 1;
      Decimal num10 = new Decimal(bits10);
      numericUpDown10.Value = num10;
      this.groupBox2.Controls.Add((Control) this.label10);
      this.groupBox2.Controls.Add((Control) this.MaxPreviewSize);
      this.groupBox2.Controls.Add((Control) this.label11);
      this.groupBox2.Controls.Add((Control) this.label12);
      this.groupBox2.Controls.Add((Control) this.MaxPreviewSizeY);
      this.groupBox2.Controls.Add((Control) this.MaxPreviewSizeX);
      this.groupBox2.Controls.Add((Control) this.MinPreviewSize);
      this.groupBox2.Controls.Add((Control) this.label8);
      this.groupBox2.Controls.Add((Control) this.label9);
      this.groupBox2.Controls.Add((Control) this.MinPreviewSizeY);
      this.groupBox2.Controls.Add((Control) this.MinPreviewSizeX);
      this.groupBox2.Font = new Font("Calibri", 12f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.groupBox2.Location = new Point(14, 5);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(274, 120);
      this.groupBox2.TabIndex = 22;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Default Initial Preview Size Limits";
      this.label10.AutoSize = true;
      this.label10.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label10.Location = new Point(40, 98);
      this.label10.Name = "label10";
      this.label10.Size = new Size(203, 15);
      this.label10.TabIndex = 22;
      this.label10.Text = "Limits used if none others specified.";
      this.MaxPreviewSize.AutoSize = true;
      this.MaxPreviewSize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MaxPreviewSize.Location = new Point(141, 21);
      this.MaxPreviewSize.Name = "MaxPreviewSize";
      this.MaxPreviewSize.Size = new Size(132, 15);
      this.MaxPreviewSize.TabIndex = 21;
      this.MaxPreviewSize.Text = "Maximum Preview Size";
      this.label11.AutoSize = true;
      this.label11.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label11.Location = new Point(148, 68);
      this.label11.Name = "label11";
      this.label11.Size = new Size(13, 15);
      this.label11.TabIndex = 20;
      this.label11.Text = "Y";
      this.label12.AutoSize = true;
      this.label12.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label12.Location = new Point(147, 41);
      this.label12.Name = "label12";
      this.label12.Size = new Size(14, 15);
      this.label12.TabIndex = 19;
      this.label12.Text = "X";
      this.MaxPreviewSizeY.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MaxPreviewSizeY.Location = new Point(171, 66);
      NumericUpDown numericUpDown11 = this.MaxPreviewSizeY;
      int[] bits11 = new int[4];
      bits11[0] = 30000;
      Decimal num11 = new Decimal(bits11);
      numericUpDown11.Maximum = num11;
      this.MaxPreviewSizeY.Name = "MaxPreviewSizeY";
      this.MaxPreviewSizeY.Size = new Size(84, 23);
      this.MaxPreviewSizeY.TabIndex = 18;
      this.MaxPreviewSizeY.ValueChanged += new EventHandler(this.MaxPreviewSizeY_ValueChanged);
      this.MaxPreviewSizeX.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MaxPreviewSizeX.Location = new Point(171, 39);
      NumericUpDown numericUpDown12 = this.MaxPreviewSizeX;
      int[] bits12 = new int[4];
      bits12[0] = 30000;
      Decimal num12 = new Decimal(bits12);
      numericUpDown12.Maximum = num12;
      this.MaxPreviewSizeX.Name = "MaxPreviewSizeX";
      this.MaxPreviewSizeX.Size = new Size(84, 23);
      this.MaxPreviewSizeX.TabIndex = 17;
      this.MaxPreviewSizeX.ValueChanged += new EventHandler(this.MaxPreviewSizeX_ValueChanged);
      this.MinPreviewSize.AutoSize = true;
      this.MinPreviewSize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MinPreviewSize.Location = new Point(4, 21);
      this.MinPreviewSize.Name = "MinPreviewSize";
      this.MinPreviewSize.Size = new Size(130, 15);
      this.MinPreviewSize.TabIndex = 16;
      this.MinPreviewSize.Text = "Minimum Preview Size";
      this.label8.AutoSize = true;
      this.label8.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label8.Location = new Point(11, 68);
      this.label8.Name = "label8";
      this.label8.Size = new Size(13, 15);
      this.label8.TabIndex = 15;
      this.label8.Text = "Y";
      this.label9.AutoSize = true;
      this.label9.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label9.Location = new Point(10, 41);
      this.label9.Name = "label9";
      this.label9.Size = new Size(14, 15);
      this.label9.TabIndex = 14;
      this.label9.Text = "X";
      this.MinPreviewSizeY.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MinPreviewSizeY.Location = new Point(34, 66);
      NumericUpDown numericUpDown13 = this.MinPreviewSizeY;
      int[] bits13 = new int[4];
      bits13[0] = 30000;
      Decimal num13 = new Decimal(bits13);
      numericUpDown13.Maximum = num13;
      this.MinPreviewSizeY.Name = "MinPreviewSizeY";
      this.MinPreviewSizeY.Size = new Size(84, 23);
      this.MinPreviewSizeY.TabIndex = 13;
      this.MinPreviewSizeY.ValueChanged += new EventHandler(this.MinPreviewSizeY_ValueChanged);
      this.MinPreviewSizeX.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MinPreviewSizeX.Location = new Point(34, 39);
      NumericUpDown numericUpDown14 = this.MinPreviewSizeX;
      int[] bits14 = new int[4];
      bits14[0] = 30000;
      Decimal num14 = new Decimal(bits14);
      numericUpDown14.Maximum = num14;
      this.MinPreviewSizeX.Name = "MinPreviewSizeX";
      this.MinPreviewSizeX.Size = new Size(84, 23);
      this.MinPreviewSizeX.TabIndex = 12;
      this.MinPreviewSizeX.ValueChanged += new EventHandler(this.MinPreviewSizeX_ValueChanged);
      this.tabPage5.Controls.Add((Control) this.label19);
      this.tabPage5.Controls.Add((Control) this.RequireHotkeyOnMinimize);
      this.tabPage5.Controls.Add((Control) this.label18);
      this.tabPage5.Controls.Add((Control) this.HideTaskbarButtons);
      this.tabPage5.Controls.Add((Control) this.CreatePreviewsOnMinimize);
      this.tabPage5.Controls.Add((Control) this.label17);
      this.tabPage5.Controls.Add((Control) this.KeepTargetFromMinimize);
      this.tabPage5.Controls.Add((Control) this.HideTargetByDefault);
      this.tabPage5.Controls.Add((Control) this.ShowListofWindows);
      this.tabPage5.Font = new Font("Calibri", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.tabPage5.Location = new Point(4, 22);
      this.tabPage5.Margin = new Padding(0);
      this.tabPage5.Name = "tabPage5";
      this.tabPage5.Size = new Size(301, 391);
      this.tabPage5.TabIndex = 4;
      this.tabPage5.Text = "Behavior";
      this.tabPage5.UseVisualStyleBackColor = true;
      this.label19.AutoSize = true;
      this.label19.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.label19.Location = new Point(34, 104);
      this.label19.Name = "label19";
      this.label19.Size = new Size((int) sbyte.MaxValue, 15);
      this.label19.TabIndex = 31;
      this.label19.Text = "Previews on Minimize";
      this.RequireHotkeyOnMinimize.AutoSize = true;
      this.RequireHotkeyOnMinimize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.RequireHotkeyOnMinimize.Location = new Point(18, 85);
      this.RequireHotkeyOnMinimize.Name = "RequireHotkeyOnMinimize";
      this.RequireHotkeyOnMinimize.Size = new Size(260, 19);
      this.RequireHotkeyOnMinimize.TabIndex = 28;
      this.RequireHotkeyOnMinimize.Text = "Require SHIFT to be Pressed When Creating";
      this.RequireHotkeyOnMinimize.UseVisualStyleBackColor = true;
      this.label18.AutoSize = true;
      this.label18.Location = new Point(6, 178);
      this.label18.Name = "label18";
      this.label18.Size = new Size(278, 13);
      this.label18.TabIndex = 25;
      this.label18.Text = "Note: Some options have been disabled for further coding.";
      this.HideTaskbarButtons.AutoSize = true;
      this.HideTaskbarButtons.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.HideTaskbarButtons.Location = new Point(18, (int) sbyte.MaxValue);
      this.HideTaskbarButtons.Name = "HideTaskbarButtons";
      this.HideTaskbarButtons.Size = new Size(241, 19);
      this.HideTaskbarButtons.TabIndex = 24;
      this.HideTaskbarButtons.Text = "Hide Taskbar Button of Target Windows";
      this.HideTaskbarButtons.UseVisualStyleBackColor = true;
      this.HideTaskbarButtons.CheckedChanged += new EventHandler(this.HideTaskbarButtons_CheckedChanged);
      this.CreatePreviewsOnMinimize.AutoSize = true;
      this.CreatePreviewsOnMinimize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.CreatePreviewsOnMinimize.Location = new Point(18, 62);
      this.CreatePreviewsOnMinimize.Name = "CreatePreviewsOnMinimize";
      this.CreatePreviewsOnMinimize.Size = new Size(184, 19);
      this.CreatePreviewsOnMinimize.TabIndex = 23;
      this.CreatePreviewsOnMinimize.Text = "Create Previews on Minimize";
      this.CreatePreviewsOnMinimize.UseVisualStyleBackColor = true;
      this.label17.AutoSize = true;
      this.label17.Location = new Point(6, 160);
      this.label17.Name = "label17";
      this.label17.Size = new Size(286, 13);
      this.label17.TabIndex = 22;
      this.label17.Text = "Note: Live previews require non-minimized target windows.";
      this.KeepTargetFromMinimize.AutoSize = true;
      this.KeepTargetFromMinimize.Enabled = false;
      this.KeepTargetFromMinimize.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.KeepTargetFromMinimize.Location = new Point(18, 37);
      this.KeepTargetFromMinimize.Name = "KeepTargetFromMinimize";
      this.KeepTargetFromMinimize.Size = new Size(237, 19);
      this.KeepTargetFromMinimize.TabIndex = 21;
      this.KeepTargetFromMinimize.Text = "Keep Target Windows from Minimizing";
      this.KeepTargetFromMinimize.UseVisualStyleBackColor = true;
      this.HideTargetByDefault.AutoSize = true;
      this.HideTargetByDefault.Enabled = false;
      this.HideTargetByDefault.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.HideTargetByDefault.Location = new Point(18, 12);
      this.HideTargetByDefault.Name = "HideTargetByDefault";
      this.HideTargetByDefault.Size = new Size(201, 19);
      this.HideTargetByDefault.TabIndex = 20;
      this.HideTargetByDefault.Text = "Hide Target Windows by Default";
      this.HideTargetByDefault.UseVisualStyleBackColor = true;
      this.ShowListofWindows.AutoSize = true;
      this.ShowListofWindows.Enabled = false;
      this.ShowListofWindows.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.ShowListofWindows.Location = new Point(18, 363);
      this.ShowListofWindows.Name = "ShowListofWindows";
      this.ShowListofWindows.Size = new Size(256, 19);
      this.ShowListofWindows.TabIndex = 19;
      this.ShowListofWindows.Text = "Show List of Windows in Right-Click Menu";
      this.ShowListofWindows.UseVisualStyleBackColor = true;
      this.ShowListofWindows.Visible = false;
      this.MainOK.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.MainOK.Font = new Font("Calibri", 14.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0);
      this.MainOK.Location = new Point(108, 429);
      this.MainOK.Name = "MainOK";
      this.MainOK.Size = new Size(100, 34);
      this.MainOK.TabIndex = 1;
      this.MainOK.Text = "OK";
      this.MainOK.UseVisualStyleBackColor = true;
      this.MainOK.Click += new EventHandler(this.MainOK_Click);
      this.MainCancel.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.MainCancel.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.MainCancel.Location = new Point(214, 433);
      this.MainCancel.Name = "MainCancel";
      this.MainCancel.Size = new Size(89, 25);
      this.MainCancel.TabIndex = 2;
      this.MainCancel.Text = "Cancel";
      this.MainCancel.UseVisualStyleBackColor = true;
      this.MainCancel.Click += new EventHandler(this.MainCancel_Click);
      this.ApplyButton.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ApplyButton.Font = new Font("Calibri", 9.75f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.ApplyButton.Location = new Point(13, 433);
      this.ApplyButton.Margin = new Padding(0);
      this.ApplyButton.Name = "ApplyButton";
      this.ApplyButton.Size = new Size(89, 25);
      this.ApplyButton.TabIndex = 3;
      this.ApplyButton.Text = "Apply";
      this.ApplyButton.UseVisualStyleBackColor = true;
      this.ApplyButton.MouseClick += new MouseEventHandler(this.ApplyButton_MouseClick);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(316, 471);
      this.Controls.Add((Control) this.ApplyButton);
      this.Controls.Add((Control) this.MainCancel);
      this.Controls.Add((Control) this.MainOK);
      this.Controls.Add((Control) this.tabControl1);
      this.FormBorderStyle = FormBorderStyle.Fixed3D;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = "SettingsPanel";
      this.Text = "ViP 0.2 (Alpha) - Settings Panel";
      this.TopMost = true;
      this.Load += new EventHandler(this.SettingsPanel_Load);
      this.FormClosing += new FormClosingEventHandler(this.SettingsPanel_FormClosing);
      this.tabControl1.ResumeLayout(false);
      this.tabPage2.ResumeLayout(false);
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.CentreDockingThickness.EndInit();
      this.EdgeDockingThickness.EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.CustomLocationY.EndInit();
      this.CustomLocationX.EndInit();
      this.Layout.ResumeLayout(false);
      this.groupBox11.ResumeLayout(false);
      this.groupBox11.PerformLayout();
      this.MarginLeft.EndInit();
      this.MarginRight.EndInit();
      ((ISupportInitialize) this.pictureBox1).EndInit();
      this.MarginBottom.EndInit();
      this.MarginTop.EndInit();
      this.groupBox10.ResumeLayout(false);
      this.groupBox10.PerformLayout();
      this.PreviewHorizontalSpacing.EndInit();
      this.PreviewVerticalSpacing.EndInit();
      this.groupBox8.ResumeLayout(false);
      this.groupBox8.PerformLayout();
      this.groupBox5.ResumeLayout(false);
      this.groupBox9.ResumeLayout(false);
      this.groupBox9.PerformLayout();
      this.groupBox7.ResumeLayout(false);
      this.groupBox7.PerformLayout();
      this.groupBox6.ResumeLayout(false);
      this.groupBox6.PerformLayout();
      this.tabPage3.ResumeLayout(false);
      this.tabPage3.PerformLayout();
      this.BackTransparency.EndInit();
      this.ThumbTransparencyTB.EndInit();
      this.tabPage1.ResumeLayout(false);
      this.tabPage1.PerformLayout();
      this.PreviewLauncherScrollSensitivity.EndInit();
      this.trackBar5.EndInit();
      this.trackBar4.EndInit();
      this.trackBar3.EndInit();
      this.trackBar2.EndInit();
      this.trackBar1.EndInit();
      this.trackBar6.EndInit();
      this.tabPage4.ResumeLayout(false);
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.PreviewLaunchVerticalSpace.EndInit();
      this.PreviewLaunchWidth.EndInit();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.MaxPreviewSizeY.EndInit();
      this.MaxPreviewSizeX.EndInit();
      this.MinPreviewSizeY.EndInit();
      this.MinPreviewSizeX.EndInit();
      this.tabPage5.ResumeLayout(false);
      this.tabPage5.PerformLayout();
      this.ResumeLayout(false);
    }

    private void SettingsPanel_Load(object sender, EventArgs e)
    {
      this.trackBar1.Value = (int) (UserSettings.Default.scalingfactor * 100.0);
      this.trackBar2.Value = (int) UserSettings.Default.ZoomDivisions;
      this.trackBar3.Value = (int) (UserSettings.Default.ZoomClickSensitivity * 10.0);
      this.trackBar4.Value = (int) (UserSettings.Default.ZoomArrowSensitivity * 10.0);
      this.trackBar5.Value = (int) (UserSettings.Default.ZoomScalingFactor * 100.0);
      this.trackBar6.Value = (int) (UserSettings.Default.MouseScrollZoomSensitivity * 100.0);
      this.PreviewLauncherScrollSensitivity.Value = UserSettings.Default.PreviewMenuScrollSensitivity;
      this.PreviewVerticalSpacing.Value = (Decimal) UserSettings.Default.PreviewLayoutVerticalSpacing;
      this.PreviewHorizontalSpacing.Value = (Decimal) UserSettings.Default.PreviewLayoutHorizontalSpacing;
      this.RowsFirst.Checked = UserSettings.Default.RowsFirst;
      this.ColumnsFirst.Checked = UserSettings.Default.ColumnsFirst;
      this.MarginTop.Maximum = (Decimal) SystemInformation.PrimaryMonitorSize.Height;
      this.MarginBottom.Maximum = (Decimal) SystemInformation.PrimaryMonitorSize.Height;
      this.MarginLeft.Maximum = (Decimal) SystemInformation.PrimaryMonitorSize.Width;
      this.MarginRight.Maximum = (Decimal) SystemInformation.PrimaryMonitorSize.Width;
      this.MarginTop.Value = UserSettings.Default.CustomTaskbarMarginsTop <= SystemInformation.PrimaryMonitorSize.Height ? (Decimal) UserSettings.Default.CustomTaskbarMarginsTop : (Decimal) SystemInformation.PrimaryMonitorSize.Height;
      this.MarginBottom.Value = UserSettings.Default.CustomTaskbarMarginsBottom <= SystemInformation.PrimaryMonitorSize.Height ? (Decimal) UserSettings.Default.CustomTaskbarMarginsBottom : (Decimal) SystemInformation.PrimaryMonitorSize.Height;
      this.MarginLeft.Value = UserSettings.Default.CustomTaskbarMarginsLeft <= SystemInformation.PrimaryMonitorSize.Width ? (Decimal) UserSettings.Default.CustomTaskbarMarginsLeft : (Decimal) SystemInformation.PrimaryMonitorSize.Width;
      this.MarginRight.Value = UserSettings.Default.CustomTaskbarMarginsRight <= SystemInformation.PrimaryMonitorSize.Width ? (Decimal) UserSettings.Default.CustomTaskbarMarginsRight : (Decimal) SystemInformation.PrimaryMonitorSize.Width;
      if (UserSettings.Default.AutomaticTaskbarMargin)
      {
        this.AutomaticTaskbarMargins.Checked = true;
        this.CustomTaskbarMargins.Checked = false;
        this.MarginTop.Enabled = false;
        this.MarginBottom.Enabled = false;
        this.MarginLeft.Enabled = false;
        this.MarginRight.Enabled = false;
      }
      else
      {
        this.AutomaticTaskbarMargins.Checked = false;
        this.CustomTaskbarMargins.Checked = true;
        this.MarginTop.Enabled = true;
        this.MarginBottom.Enabled = true;
        this.MarginLeft.Enabled = true;
        this.MarginRight.Enabled = true;
      }
      this.DockPreviewCB.Checked = UserSettings.Default.DockPreview;
      this.FreeFormEdgeDocking.Checked = UserSettings.Default.FreeFormEdgeDocking;
      this.FreeFormCentreDocking.Checked = UserSettings.Default.FreeFormCentreDocking;
      this.CentreDockingThickness.Value = (Decimal) UserSettings.Default.FreeFormCentreDockThickness;
      this.EdgeDockingThickness.Value = (Decimal) UserSettings.Default.FreeFormEdgeDockThickness;
      this.CentreDockingThicknessToolTip.SetToolTip((Control) this.CentreDockingThickness, "Thickness of Border Around Centre");
      this.EdgeDockingThicknessToolTip.SetToolTip((Control) this.EdgeDockingThickness, "Thickness of Edge Border");
      this.BottomLeftCB.Checked = UserSettings.Default.startbottomleft;
      this.BottomRightCB.Checked = UserSettings.Default.startbottomright;
      this.BottomMiddleCB.Checked = UserSettings.Default.startbottommiddle;
      this.CentreCB.Checked = UserSettings.Default.startincenter;
      this.TopLeftCB.Checked = UserSettings.Default.starttopleft;
      this.TopRightCB.Checked = UserSettings.Default.starttopright;
      this.TopMiddleCB.Checked = UserSettings.Default.starttopmiddle;
      this.BottomCB.Checked = UserSettings.Default.startbottom;
      this.TopCB.Checked = UserSettings.Default.starttop;
      this.LeftCB.Checked = UserSettings.Default.startleft;
      this.RightCB.Checked = UserSettings.Default.startright;
      this.WindowsDefaultCB.Checked = UserSettings.Default.StartWindowsDefault;
      this.CustomCB.Checked = UserSettings.Default.StartCustomLocation;
      this.CustomLocationX.Value = (Decimal) UserSettings.Default.startpositionx;
      this.CustomLocationY.Value = (Decimal) UserSettings.Default.startpositiony;
      if (!this.CustomCB.Checked)
      {
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        this.CustomLocationX.ReadOnly = false;
        this.CustomLocationY.ReadOnly = false;
        this.CustomLocationX.Increment = new Decimal(1);
        this.CustomLocationY.Increment = new Decimal(1);
      }
      this.ShowBordersByDefault.Checked = UserSettings.Default.ShowBordersByDefault;
      this.ThumbTransparencyTB.Value = UserSettings.Default.ThumbTransparency;
      this.BackTransparency.Value = UserSettings.Default.BackTransparency;
      this.ShowBackText.Checked = UserSettings.Default.ShowBackText;
      this.EnableAeroBack.Checked = UserSettings.Default.EnableAeroBackground;
      this.OriginalEnableAeroBack = UserSettings.Default.EnableAeroBackground;
      this.MinPreviewSizeX.Value = (Decimal) UserSettings.Default.minpixelsx;
      this.MinPreviewSizeY.Value = (Decimal) UserSettings.Default.minpixelsy;
      this.MaxPreviewSizeX.Value = (Decimal) UserSettings.Default.maxpixelsx;
      this.MaxPreviewSizeY.Value = (Decimal) UserSettings.Default.maxpixelsy;
      this.PreviewLaunchWidth.Value = (Decimal) UserSettings.Default.PreviewMenuSizeX;
      this.PreviewLaunchVerticalSpace.Value = (Decimal) UserSettings.Default.VerticalSpaceBetweenPreviews;
      this.ShowListofWindows.Checked = UserSettings.Default.ShowWindowListInContextMenu;
      this.HideTargetByDefault.Checked = UserSettings.Default.MoveTargetOffscreen;
      this.KeepTargetFromMinimize.Checked = UserSettings.Default.KeepTargettedFromMinimizing;
      this.CreatePreviewsOnMinimize.Checked = UserSettings.Default.CreatePreviewsOnMinimize;
      this.HideTaskbarButtons.Checked = UserSettings.Default.HideTaskBarButtons;
      if (UserSettings.Default.ArrangePreviewXDirection == "Right")
      {
        this.RightDirectionCB.Checked = true;
        this.LeftDirectionCB.Checked = false;
      }
      else
      {
        this.RightDirectionCB.Checked = false;
        this.LeftDirectionCB.Checked = true;
      }
      if (UserSettings.Default.ArrangePreviewYDirection == "Up")
      {
        this.UpDirectionCB.Checked = true;
        this.DownDirectionCB.Checked = false;
      }
      else
      {
        this.UpDirectionCB.Checked = false;
        this.DownDirectionCB.Checked = true;
      }
      this.RequireHotkeyOnMinimize.Checked = UserSettings.Default.CreatePreviewOnlyOnHotkey;
      this.PinTopMost.Checked = UserSettings.Default.PinToTop;
      this.PinNormal.Checked = UserSettings.Default.PinToNormal;
      this.PinDesktop.Checked = UserSettings.Default.PinToBottom;
    }

    private void MainOK_Click(object sender, EventArgs e)
    {
      this.ApplyButton_MouseClick((object) this, (MouseEventArgs) null);
      try
      {
        this.Dispose();
        this.Close();
      }
      catch (Exception ex)
      {
      }
    }

    private void MainCancel_Click(object sender, EventArgs e)
    {
      this.qparent.ChangeTransparency(UserSettings.Default.ThumbTransparency, UserSettings.Default.BackTransparency);
      if (this.HideTaskbarButtons.Checked != UserSettings.Default.HideTaskBarButtons)
      {
        if (UserSettings.Default.HideTaskBarButtons)
          this.qparent.HideTaskbarTab();
        else
          this.qparent.ShowTaskbarTab();
      }
      if (this.EnableAeroBack.Checked != this.OriginalEnableAeroBack)
      {
        UserSettings.Default.EnableAeroBackground = this.OriginalEnableAeroBack;
        this.qparent.AeroBackGroundRefresh();
      }
      try
      {
        this.Dispose();
        this.Close();
      }
      catch (Exception ex)
      {
      }
    }

    private void trackBar1_Scroll(object sender, EventArgs e)
    {
      this.TB1changed = true;
    }

    private void trackBar2_Scroll(object sender, EventArgs e)
    {
      this.TB2changed = true;
    }

    private void trackBar3_Scroll(object sender, EventArgs e)
    {
      this.TB3changed = true;
    }

    private void trackBar4_Scroll(object sender, EventArgs e)
    {
      this.TB4changed = true;
    }

    private void trackBar5_Scroll(object sender, EventArgs e)
    {
      this.TB5changed = true;
    }

    private void trackBar6_Scroll(object sender, EventArgs e)
    {
      this.TB6changed = true;
    }

    private void BottomLeftCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.BottomLeftCB.Checked)
      {
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomMiddleCB.Checked || this.BottomRightCB.Checked || (this.TopLeftCB.Checked || this.TopMiddleCB.Checked) || (this.TopRightCB.Checked || this.BottomCB.Checked || (this.TopCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void TopLeftCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.TopLeftCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopMiddleCB.Checked) || (this.TopRightCB.Checked || this.BottomCB.Checked || (this.TopCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void BottomMiddleCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.BottomMiddleCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomRightCB.Checked || (this.TopLeftCB.Checked || this.TopMiddleCB.Checked) || (this.TopRightCB.Checked || this.BottomCB.Checked || (this.TopCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void TopMiddleCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.TopMiddleCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopRightCB.Checked || this.BottomCB.Checked || (this.TopCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void BottomRightCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.BottomRightCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.TopLeftCB.Checked || this.TopMiddleCB.Checked) || (this.TopRightCB.Checked || this.BottomCB.Checked || (this.TopCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void TopRightCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.TopRightCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.BottomCB.Checked || (this.TopCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void WindowsDefaultCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.WindowsDefaultCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.TopRightCB.Checked || (this.BottomCB.Checked || this.TopCB.Checked)) || (this.LeftCB.Checked || this.RightCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void CentreCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.CentreCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.TopRightCB.Checked || (this.BottomCB.Checked || this.TopCB.Checked)) || (this.LeftCB.Checked || this.RightCB.Checked || this.WindowsDefaultCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void CustomCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.CustomCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomLocationX.ReadOnly = false;
        this.CustomLocationY.ReadOnly = false;
        this.CustomLocationX.Increment = new Decimal(1);
        this.CustomLocationY.Increment = new Decimal(1);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.TopRightCB.Checked || this.WindowsDefaultCB.Checked) || this.CentreCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void BottomCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.BottomCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.TopRightCB.Checked || (this.TopCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void TopCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.TopCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.LeftCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.TopRightCB.Checked || (this.BottomCB.Checked || this.LeftCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void LeftCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.LeftCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.RightCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.TopRightCB.Checked || (this.BottomCB.Checked || this.TopCB.Checked)) || (this.RightCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void RightCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.RightCB.Checked)
      {
        this.BottomLeftCB.Checked = false;
        this.BottomMiddleCB.Checked = false;
        this.BottomRightCB.Checked = false;
        this.TopLeftCB.Checked = false;
        this.TopMiddleCB.Checked = false;
        this.TopRightCB.Checked = false;
        this.WindowsDefaultCB.Checked = false;
        this.CentreCB.Checked = false;
        this.CustomCB.Checked = false;
        this.BottomCB.Checked = false;
        this.TopCB.Checked = false;
        this.LeftCB.Checked = false;
        this.CustomLocationX.ReadOnly = true;
        this.CustomLocationY.ReadOnly = true;
        this.CustomLocationX.Increment = new Decimal(0);
        this.CustomLocationY.Increment = new Decimal(0);
      }
      else
      {
        if (this.BottomLeftCB.Checked || this.BottomMiddleCB.Checked || (this.BottomRightCB.Checked || this.TopLeftCB.Checked) || (this.TopMiddleCB.Checked || this.TopRightCB.Checked || (this.BottomCB.Checked || this.TopCB.Checked)) || (this.LeftCB.Checked || this.WindowsDefaultCB.Checked || this.CentreCB.Checked) || this.CustomCB.Checked)
          return;
        this.WindowsDefaultCB.Checked = true;
      }
    }

    private void MinPreviewSizeX_ValueChanged(object sender, EventArgs e)
    {
      if (!(this.MinPreviewSizeX.Value > this.MaxPreviewSizeX.Value))
        return;
      this.MaxPreviewSizeX.Value = this.MinPreviewSizeX.Value;
    }

    private void MaxPreviewSizeX_ValueChanged(object sender, EventArgs e)
    {
      if (!(this.MinPreviewSizeX.Value > this.MaxPreviewSizeX.Value))
        return;
      this.MinPreviewSizeX.Value = this.MaxPreviewSizeX.Value;
    }

    private void MinPreviewSizeY_ValueChanged(object sender, EventArgs e)
    {
      if (!(this.MinPreviewSizeY.Value > this.MaxPreviewSizeY.Value))
        return;
      this.MaxPreviewSizeY.Value = this.MinPreviewSizeY.Value;
    }

    private void MaxPreviewSizeY_ValueChanged(object sender, EventArgs e)
    {
      if (!(this.MinPreviewSizeY.Value > this.MaxPreviewSizeY.Value))
        return;
      this.MinPreviewSizeY.Value = this.MaxPreviewSizeY.Value;
    }

    private void TransparencyTB_Scroll(object sender, EventArgs e)
    {
      this.qparent.ChangeTransparency(this.ThumbTransparencyTB.Value, this.BackTransparency.Value);
    }

    private void SettingsPanel_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.qparent.ChangeTransparency(UserSettings.Default.ThumbTransparency, UserSettings.Default.BackTransparency);
    }

    private void ShowBackText_CheckedChanged(object sender, EventArgs e)
    {
    }

    private void FreeFormEdgeDocking_CheckedChanged(object sender, EventArgs e)
    {
      if (!this.FreeFormEdgeDocking.Checked)
        return;
      this.DockPreviewCB.Checked = false;
    }

    private void DockPreviewCB_CheckedChanged(object sender, EventArgs e)
    {
      if (!this.DockPreviewCB.Checked)
        return;
      this.FreeFormEdgeDocking.Checked = false;
    }

    private void FreeFormCentreDocking_CheckedChanged(object sender, EventArgs e)
    {
      if (!this.FreeFormCentreDocking.Checked)
        return;
      this.DockPreviewCB.Checked = false;
    }

    private void LeftDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.LeftDirectionCB.Checked)
        this.RightDirectionCB.Checked = false;
      else
        this.RightDirectionCB.Checked = true;
    }

    private void RightDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.RightDirectionCB.Checked)
        this.LeftDirectionCB.Checked = false;
      else
        this.LeftDirectionCB.Checked = true;
    }

    private void UpDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.UpDirectionCB.Checked)
        this.DownDirectionCB.Checked = false;
      else
        this.DownDirectionCB.Checked = true;
    }

    private void DownDirectionCB_CheckedChanged(object sender, EventArgs e)
    {
      if (this.DownDirectionCB.Checked)
        this.UpDirectionCB.Checked = false;
      else
        this.UpDirectionCB.Checked = true;
    }

    private void PinTopMost_CheckedChanged(object sender, EventArgs e)
    {
      if (!this.PinTopMost.Checked)
        return;
      this.PinDesktop.Checked = false;
      this.PinNormal.Checked = false;
    }

    private void PinNormal_CheckedChanged(object sender, EventArgs e)
    {
      if (!this.PinNormal.Checked)
        return;
      this.PinTopMost.Checked = false;
      this.PinDesktop.Checked = false;
    }

    private void PinDesktop_CheckedChanged(object sender, EventArgs e)
    {
      if (!this.PinDesktop.Checked)
        return;
      this.PinTopMost.Checked = false;
      this.PinNormal.Checked = false;
    }

    private void ApplyButton_MouseClick(object sender, MouseEventArgs e)
    {
      if (this.TB1changed)
        UserSettings.Default.scalingfactor = (double) this.trackBar1.Value / 100.0;
      if (this.TB2changed)
        UserSettings.Default.ZoomDivisions = (double) this.trackBar2.Value;
      if (this.TB3changed)
        UserSettings.Default.ZoomClickSensitivity = (double) this.trackBar3.Value / 10.0;
      if (this.TB4changed)
        UserSettings.Default.ZoomArrowSensitivity = (double) this.trackBar4.Value / 10.0;
      if (this.TB5changed)
        UserSettings.Default.ZoomScalingFactor = (double) this.trackBar5.Value / 100.0;
      if (this.TB6changed)
        UserSettings.Default.MouseScrollZoomSensitivity = (double) this.trackBar6.Value / 100.0;
      UserSettings.Default.PreviewMenuScrollSensitivity = this.PreviewLauncherScrollSensitivity.Value;
      UserSettings.Default.PreviewLayoutVerticalSpacing = (int) this.PreviewVerticalSpacing.Value;
      UserSettings.Default.PreviewLayoutHorizontalSpacing = (int) this.PreviewHorizontalSpacing.Value;
      UserSettings.Default.RowsFirst = this.RowsFirst.Checked;
      UserSettings.Default.ColumnsFirst = this.ColumnsFirst.Checked;
      UserSettings.Default.CustomTaskbarMarginsTop = (int) this.MarginTop.Value;
      UserSettings.Default.CustomTaskbarMarginsBottom = (int) this.MarginBottom.Value;
      UserSettings.Default.CustomTaskbarMarginsLeft = (int) this.MarginLeft.Value;
      UserSettings.Default.CustomTaskbarMarginsRight = (int) this.MarginRight.Value;
      UserSettings.Default.AutomaticTaskbarMargin = this.AutomaticTaskbarMargins.Checked;
      UserSettings.Default.CustomTaskbarMargins = this.CustomTaskbarMargins.Checked;
      UserSettings.Default.DockPreview = this.DockPreviewCB.Checked;
      UserSettings.Default.FreeFormEdgeDocking = this.FreeFormEdgeDocking.Checked;
      UserSettings.Default.FreeFormCentreDocking = this.FreeFormCentreDocking.Checked;
      UserSettings.Default.FreeFormCentreDockThickness = (int) this.CentreDockingThickness.Value;
      UserSettings.Default.FreeFormEdgeDockThickness = (int) this.EdgeDockingThickness.Value;
      UserSettings.Default.startbottomleft = this.BottomLeftCB.Checked;
      UserSettings.Default.startbottomright = this.BottomRightCB.Checked;
      UserSettings.Default.startbottommiddle = this.BottomMiddleCB.Checked;
      UserSettings.Default.startincenter = this.CentreCB.Checked;
      UserSettings.Default.starttopleft = this.TopLeftCB.Checked;
      UserSettings.Default.starttopright = this.TopRightCB.Checked;
      UserSettings.Default.starttopmiddle = this.TopMiddleCB.Checked;
      UserSettings.Default.startbottom = this.BottomCB.Checked;
      UserSettings.Default.starttop = this.TopCB.Checked;
      UserSettings.Default.startleft = this.LeftCB.Checked;
      UserSettings.Default.startright = this.RightCB.Checked;
      UserSettings.Default.StartWindowsDefault = this.WindowsDefaultCB.Checked;
      UserSettings.Default.StartCustomLocation = this.CustomCB.Checked;
      UserSettings.Default.startpositionx = (int) this.CustomLocationX.Value;
      UserSettings.Default.startpositiony = (int) this.CustomLocationY.Value;
      UserSettings.Default.FirstPreview = true;
      UserSettings.Default.ShowBordersByDefault = this.ShowBordersByDefault.Checked;
      UserSettings.Default.ThumbTransparency = this.ThumbTransparencyTB.Value;
      UserSettings.Default.BackTransparency = this.BackTransparency.Value;
      UserSettings.Default.ShowBackText = this.ShowBackText.Checked;
      UserSettings.Default.EnableAeroBackground = this.EnableAeroBack.Checked;
      UserSettings.Default.minpixelsx = (int) this.MinPreviewSizeX.Value;
      UserSettings.Default.minpixelsy = (int) this.MinPreviewSizeY.Value;
      UserSettings.Default.maxpixelsx = (int) this.MaxPreviewSizeX.Value;
      UserSettings.Default.maxpixelsy = (int) this.MaxPreviewSizeY.Value;
      UserSettings.Default.PreviewMenuSizeX = (int) this.PreviewLaunchWidth.Value;
      UserSettings.Default.VerticalSpaceBetweenPreviews = (int) this.PreviewLaunchVerticalSpace.Value;
      UserSettings.Default.ShowWindowListInContextMenu = this.ShowListofWindows.Checked;
      UserSettings.Default.MoveTargetOffscreen = this.HideTargetByDefault.Checked;
      UserSettings.Default.KeepTargettedFromMinimizing = this.KeepTargetFromMinimize.Checked;
      UserSettings.Default.CreatePreviewsOnMinimize = this.CreatePreviewsOnMinimize.Checked;
      UserSettings.Default.HideTaskBarButtons = this.HideTaskbarButtons.Checked;
      UserSettings.Default.ArrangePreviewXDirection = !this.RightDirectionCB.Checked ? "Left" : "Right";
      UserSettings.Default.ArrangePreviewYDirection = !this.UpDirectionCB.Checked ? "Down" : "Up";
      UserSettings.Default.CreatePreviewOnlyOnHotkey = this.RequireHotkeyOnMinimize.Checked;
      UserSettings.Default.PinToTop = this.PinTopMost.Checked;
      UserSettings.Default.PinToNormal = this.PinNormal.Checked;
      UserSettings.Default.PinToBottom = this.PinDesktop.Checked;
      UserSettings.Default.Save();
    }

    private void AutomaticTaskbarMargins_CheckedChanged(object sender, EventArgs e)
    {
      if (this.AutomaticTaskbarMargins.Checked)
      {
        this.CustomTaskbarMargins.Checked = false;
        this.MarginTop.Enabled = false;
        this.MarginBottom.Enabled = false;
        this.MarginLeft.Enabled = false;
        this.MarginRight.Enabled = false;
      }
      else
      {
        this.CustomTaskbarMargins.Checked = true;
        this.MarginTop.Enabled = true;
        this.MarginBottom.Enabled = true;
        this.MarginLeft.Enabled = true;
        this.MarginRight.Enabled = true;
      }
    }

    private void CustomTaskbarMargins_CheckedChanged(object sender, EventArgs e)
    {
      if (this.CustomTaskbarMargins.Checked)
      {
        this.AutomaticTaskbarMargins.Checked = false;
        this.MarginTop.Enabled = true;
        this.MarginBottom.Enabled = true;
        this.MarginLeft.Enabled = true;
        this.MarginRight.Enabled = true;
      }
      else
      {
        this.AutomaticTaskbarMargins.Checked = true;
        this.MarginTop.Enabled = false;
        this.MarginBottom.Enabled = false;
        this.MarginLeft.Enabled = false;
        this.MarginRight.Enabled = false;
      }
    }

    private void RowsFirst_CheckedChanged(object sender, EventArgs e)
    {
      if (this.RowsFirst.Checked)
        this.ColumnsFirst.Checked = false;
      else
        this.ColumnsFirst.Checked = true;
    }

    private void ColumnsFirst_CheckedChanged(object sender, EventArgs e)
    {
      if (this.ColumnsFirst.Checked)
        this.RowsFirst.Checked = false;
      else
        this.RowsFirst.Checked = true;
    }

    private void HideTaskbarButtons_CheckedChanged(object sender, EventArgs e)
    {
      if (this.HideTaskbarButtons.Checked)
        this.qparent.HideTaskbarTab();
      else
        this.qparent.ShowTaskbarTab();
    }

    private void EnableAeroBack_CheckedChanged(object sender, EventArgs e)
    {
      UserSettings.Default.EnableAeroBackground = this.EnableAeroBack.Checked;
      this.qparent.AeroBackGroundRefresh();
    }
  }
}
