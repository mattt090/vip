﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.UserSettings
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ThumbnailPersist
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  internal sealed class UserSettings : ApplicationSettingsBase
  {
    private static UserSettings defaultInstance = (UserSettings) SettingsBase.Synchronized((SettingsBase) new UserSettings());

    public static UserSettings Default
    {
      get
      {
        UserSettings userSettings = UserSettings.defaultInstance;
        return userSettings;
      }
    }

    [DefaultSettingValue("0.25")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public double scalingfactor
    {
      get
      {
        return (double) this["scalingfactor"];
      }
      set
      {
        this["scalingfactor"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("80")]
    [UserScopedSetting]
    public int minpixelsx
    {
      get
      {
        return (int) this["minpixelsx"];
      }
      set
      {
        this["minpixelsx"] = (object) value;
      }
    }

    [DefaultSettingValue("80")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public int minpixelsy
    {
      get
      {
        return (int) this["minpixelsy"];
      }
      set
      {
        this["minpixelsy"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("120")]
    public int startpositionx
    {
      get
      {
        return (int) this["startpositionx"];
      }
      set
      {
        this["startpositionx"] = (object) value;
      }
    }

    [DefaultSettingValue("200")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public int startpositiony
    {
      get
      {
        return (int) this["startpositiony"];
      }
      set
      {
        this["startpositiony"] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool startincenter
    {
      get
      {
        return (bool) this["startincenter"];
      }
      set
      {
        this["startincenter"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool startbottomright
    {
      get
      {
        return (bool) this["startbottomright"];
      }
      set
      {
        this["startbottomright"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("True")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool startbottomleft
    {
      get
      {
        return (bool) this["startbottomleft"];
      }
      set
      {
        this["startbottomleft"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool startbottommiddle
    {
      get
      {
        return (bool) this["startbottommiddle"];
      }
      set
      {
        this["startbottommiddle"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool starttopright
    {
      get
      {
        return (bool) this["starttopright"];
      }
      set
      {
        this["starttopright"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    [UserScopedSetting]
    public bool starttopleft
    {
      get
      {
        return (bool) this["starttopleft"];
      }
      set
      {
        this["starttopleft"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool starttopmiddle
    {
      get
      {
        return (bool) this["starttopmiddle"];
      }
      set
      {
        this["starttopmiddle"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("323")]
    [UserScopedSetting]
    public int emptysizex
    {
      get
      {
        return (int) this["emptysizex"];
      }
      set
      {
        this["emptysizex"] = (object) value;
      }
    }

    [DefaultSettingValue("126")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int emptysizey
    {
      get
      {
        return (int) this["emptysizey"];
      }
      set
      {
        this["emptysizey"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    public bool startwithspecifiedposition
    {
      get
      {
        return (bool) this["startwithspecifiedposition"];
      }
      set
      {
        this["startwithspecifiedposition"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool freewindowmove
    {
      get
      {
        return (bool) this["freewindowmove"];
      }
      set
      {
        this["freewindowmove"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool AutoZoomVideos
    {
      get
      {
        return (bool) this["AutoZoomVideos"];
      }
      set
      {
        this["AutoZoomVideos"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool HoverControls
    {
      get
      {
        return (bool) this["HoverControls"];
      }
      set
      {
        this["HoverControls"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("307")]
    public int ControlsWindowSizeX
    {
      get
      {
        return (int) this["ControlsWindowSizeX"];
      }
      set
      {
        this["ControlsWindowSizeX"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("45")]
    public int ControlsWindowSizeY
    {
      get
      {
        return (int) this["ControlsWindowSizeY"];
      }
      set
      {
        this["ControlsWindowSizeY"] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool ScaleControlsWindow
    {
      get
      {
        return (bool) this["ScaleControlsWindow"];
      }
      set
      {
        this["ScaleControlsWindow"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("10")]
    [DebuggerNonUserCode]
    public double ZoomDivisions
    {
      get
      {
        return (double) this["ZoomDivisions"];
      }
      set
      {
        this["ZoomDivisions"] = (object) value;
      }
    }

    [DefaultSettingValue("10")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public double ZoomMaxFactor
    {
      get
      {
        return (double) this["ZoomMaxFactor"];
      }
      set
      {
        this["ZoomMaxFactor"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("0.1")]
    [DebuggerNonUserCode]
    public double ZoomMinFactor
    {
      get
      {
        return (double) this["ZoomMinFactor"];
      }
      set
      {
        this["ZoomMinFactor"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("0.3")]
    [UserScopedSetting]
    public double ZoomButtonsSizeFactorX
    {
      get
      {
        return (double) this["ZoomButtonsSizeFactorX"];
      }
      set
      {
        this["ZoomButtonsSizeFactorX"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("0.3")]
    [UserScopedSetting]
    public double ZoomButtonsSizeFactorY
    {
      get
      {
        return (double) this["ZoomButtonsSizeFactorY"];
      }
      set
      {
        this["ZoomButtonsSizeFactorY"] = (object) value;
      }
    }

    [DefaultSettingValue("1.5")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public double ZoomClickSensitivity
    {
      get
      {
        return (double) this["ZoomClickSensitivity"];
      }
      set
      {
        this["ZoomClickSensitivity"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("1")]
    public double ZoomDefaultFactor
    {
      get
      {
        return (double) this["ZoomDefaultFactor"];
      }
      set
      {
        this["ZoomDefaultFactor"] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool zoombyhovering
    {
      get
      {
        return (bool) this["zoombyhovering"];
      }
      set
      {
        this["zoombyhovering"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("3")]
    [UserScopedSetting]
    public double DragSensitivity
    {
      get
      {
        return (double) this["DragSensitivity"];
      }
      set
      {
        this["DragSensitivity"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("3")]
    public double TotalZoom
    {
      get
      {
        return (double) this["TotalZoom"];
      }
      set
      {
        this["TotalZoom"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("1.05")]
    public double ZoomScalingFactor
    {
      get
      {
        return (double) this["ZoomScalingFactor"];
      }
      set
      {
        this["ZoomScalingFactor"] = (object) value;
      }
    }

    [DefaultSettingValue("0")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public double ResizeRatioCorrectionFactor
    {
      get
      {
        return (double) this["ResizeRatioCorrectionFactor"];
      }
      set
      {
        this["ResizeRatioCorrectionFactor"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("0")]
    [DebuggerNonUserCode]
    public double ZoomLeftCorrectionFactor
    {
      get
      {
        return (double) this["ZoomLeftCorrectionFactor"];
      }
      set
      {
        this["ZoomLeftCorrectionFactor"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("0")]
    public double ZoomTopCorrectionFactor
    {
      get
      {
        return (double) this["ZoomTopCorrectionFactor"];
      }
      set
      {
        this["ZoomTopCorrectionFactor"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("0")]
    [UserScopedSetting]
    public double ZoomRightCorrectionFactor
    {
      get
      {
        return (double) this["ZoomRightCorrectionFactor"];
      }
      set
      {
        this["ZoomRightCorrectionFactor"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("0")]
    public double ZoomBottomCorrectionFactor
    {
      get
      {
        return (double) this["ZoomBottomCorrectionFactor"];
      }
      set
      {
        this["ZoomBottomCorrectionFactor"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("0.96")]
    public double ZoomHoverSensitivity
    {
      get
      {
        return (double) this["ZoomHoverSensitivity"];
      }
      set
      {
        this["ZoomHoverSensitivity"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("1")]
    public double ZoomArrowSensitivity
    {
      get
      {
        return (double) this["ZoomArrowSensitivity"];
      }
      set
      {
        this["ZoomArrowSensitivity"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool StartCustomLocation
    {
      get
      {
        return (bool) this["StartCustomLocation"];
      }
      set
      {
        this["StartCustomLocation"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("False")]
    public bool DockPreview
    {
      get
      {
        return (bool) this["DockPreview"];
      }
      set
      {
        this["DockPreview"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    [UserScopedSetting]
    public bool SetViPLocation
    {
      get
      {
        return (bool) this["SetViPLocation"];
      }
      set
      {
        this["SetViPLocation"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool SetTargetLocationAndSize
    {
      get
      {
        return (bool) this["SetTargetLocationAndSize"];
      }
      set
      {
        this["SetTargetLocationAndSize"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("True")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool MoveTargetOffscreen
    {
      get
      {
        return (bool) this["MoveTargetOffscreen"];
      }
      set
      {
        this["MoveTargetOffscreen"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("False")]
    public bool StartWindowsDefault
    {
      get
      {
        return (bool) this["StartWindowsDefault"];
      }
      set
      {
        this["StartWindowsDefault"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("120")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int maxpixelsx
    {
      get
      {
        return (int) this["maxpixelsx"];
      }
      set
      {
        this["maxpixelsx"] = (object) value;
      }
    }

    [DefaultSettingValue("120")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int maxpixelsy
    {
      get
      {
        return (int) this["maxpixelsy"];
      }
      set
      {
        this["maxpixelsy"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool ShowBordersByDefault
    {
      get
      {
        return (bool) this["ShowBordersByDefault"];
      }
      set
      {
        this["ShowBordersByDefault"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("1")]
    [UserScopedSetting]
    public int ThumbTransparency
    {
      get
      {
        return (int) this["ThumbTransparency"];
      }
      set
      {
        this["ThumbTransparency"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("1")]
    public int BackTransparency
    {
      get
      {
        return (int) this["BackTransparency"];
      }
      set
      {
        this["BackTransparency"] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool ShowBackText
    {
      get
      {
        return (bool) this["ShowBackText"];
      }
      set
      {
        this["ShowBackText"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    [UserScopedSetting]
    public bool startbottom
    {
      get
      {
        return (bool) this["startbottom"];
      }
      set
      {
        this["startbottom"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool starttop
    {
      get
      {
        return (bool) this["starttop"];
      }
      set
      {
        this["starttop"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    public bool startleft
    {
      get
      {
        return (bool) this["startleft"];
      }
      set
      {
        this["startleft"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    public bool startright
    {
      get
      {
        return (bool) this["startright"];
      }
      set
      {
        this["startright"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("1.2")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public double MouseScrollZoomSensitivity
    {
      get
      {
        return (double) this["MouseScrollZoomSensitivity"];
      }
      set
      {
        this["MouseScrollZoomSensitivity"] = (object) value;
      }
    }

    [DefaultSettingValue("True")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool HideTaskBarButtons
    {
      get
      {
        return (bool) this["HideTaskBarButtons"];
      }
      set
      {
        this["HideTaskBarButtons"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    [UserScopedSetting]
    public bool ShowPreviewOnMinimize
    {
      get
      {
        return (bool) this["ShowPreviewOnMinimize"];
      }
      set
      {
        this["ShowPreviewOnMinimize"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool ShowWindowListInContextMenu
    {
      get
      {
        return (bool) this["ShowWindowListInContextMenu"];
      }
      set
      {
        this["ShowWindowListInContextMenu"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool AllowReplicatePreviews
    {
      get
      {
        return (bool) this["AllowReplicatePreviews"];
      }
      set
      {
        this["AllowReplicatePreviews"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool EnableNavArrowKeys
    {
      get
      {
        return (bool) this["EnableNavArrowKeys"];
      }
      set
      {
        this["EnableNavArrowKeys"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("250")]
    [UserScopedSetting]
    public int PreviewMenuSizeX
    {
      get
      {
        return (int) this["PreviewMenuSizeX"];
      }
      set
      {
        this["PreviewMenuSizeX"] = (object) value;
      }
    }

    [DefaultSettingValue("30")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int PreviewMenuScrollSensitivity
    {
      get
      {
        return (int) this["PreviewMenuScrollSensitivity"];
      }
      set
      {
        this["PreviewMenuScrollSensitivity"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("5")]
    [DebuggerNonUserCode]
    public int VerticalSpaceBetweenPreviews
    {
      get
      {
        return (int) this["VerticalSpaceBetweenPreviews"];
      }
      set
      {
        this["VerticalSpaceBetweenPreviews"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("True")]
    [DebuggerNonUserCode]
    public bool KeepTargettedFromMinimizing
    {
      get
      {
        return (bool) this["KeepTargettedFromMinimizing"];
      }
      set
      {
        this["KeepTargettedFromMinimizing"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("True")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool FreeFormEdgeDocking
    {
      get
      {
        return (bool) this["FreeFormEdgeDocking"];
      }
      set
      {
        this["FreeFormEdgeDocking"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("5")]
    [UserScopedSetting]
    public int FreeFormEdgeDockThickness
    {
      get
      {
        return (int) this["FreeFormEdgeDockThickness"];
      }
      set
      {
        this["FreeFormEdgeDockThickness"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("True")]
    public bool FreeFormCentreDocking
    {
      get
      {
        return (bool) this["FreeFormCentreDocking"];
      }
      set
      {
        this["FreeFormCentreDocking"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("50")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int FreeFormCentreDockThickness
    {
      get
      {
        return (int) this["FreeFormCentreDockThickness"];
      }
      set
      {
        this["FreeFormCentreDockThickness"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    [UserScopedSetting]
    public bool CreatePreviewsOnMinimize
    {
      get
      {
        return (bool) this["CreatePreviewsOnMinimize"];
      }
      set
      {
        this["CreatePreviewsOnMinimize"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("True")]
    [DebuggerNonUserCode]
    public bool FirstPreview
    {
      get
      {
        return (bool) this["FirstPreview"];
      }
      set
      {
        this["FirstPreview"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("Right")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public string ArrangePreviewXDirection
    {
      get
      {
        return (string) this["ArrangePreviewXDirection"];
      }
      set
      {
        this["ArrangePreviewXDirection"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("Up")]
    public string ArrangePreviewYDirection
    {
      get
      {
        return (string) this["ArrangePreviewYDirection"];
      }
      set
      {
        this["ArrangePreviewYDirection"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("X")]
    [DebuggerNonUserCode]
    public string ArrangeFirstDirection
    {
      get
      {
        return (string) this["ArrangeFirstDirection"];
      }
      set
      {
        this["ArrangeFirstDirection"] = (object) value;
      }
    }

    [DefaultSettingValue("True")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool CreatePreviewOnlyOnHotkey
    {
      get
      {
        return (bool) this["CreatePreviewOnlyOnHotkey"];
      }
      set
      {
        this["CreatePreviewOnlyOnHotkey"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    [UserScopedSetting]
    public bool PinToTop
    {
      get
      {
        return (bool) this["PinToTop"];
      }
      set
      {
        this["PinToTop"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool PinToNormal
    {
      get
      {
        return (bool) this["PinToNormal"];
      }
      set
      {
        this["PinToNormal"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    public bool PinToBottom
    {
      get
      {
        return (bool) this["PinToBottom"];
      }
      set
      {
        this["PinToBottom"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("True")]
    [DebuggerNonUserCode]
    public bool RowsFirst
    {
      get
      {
        return (bool) this["RowsFirst"];
      }
      set
      {
        this["RowsFirst"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DefaultSettingValue("False")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool ColumnsFirst
    {
      get
      {
        return (bool) this["ColumnsFirst"];
      }
      set
      {
        this["ColumnsFirst"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("10")]
    public int PreviewLayoutVerticalSpacing
    {
      get
      {
        return (int) this["PreviewLayoutVerticalSpacing"];
      }
      set
      {
        this["PreviewLayoutVerticalSpacing"] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("10")]
    public int PreviewLayoutHorizontalSpacing
    {
      get
      {
        return (int) this["PreviewLayoutHorizontalSpacing"];
      }
      set
      {
        this["PreviewLayoutHorizontalSpacing"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool AutomaticTaskbarMargin
    {
      get
      {
        return (bool) this["AutomaticTaskbarMargin"];
      }
      set
      {
        this["AutomaticTaskbarMargin"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool CustomTaskbarMargins
    {
      get
      {
        return (bool) this["CustomTaskbarMargins"];
      }
      set
      {
        this["CustomTaskbarMargins"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("0")]
    public int CustomTaskbarMarginsTop
    {
      get
      {
        return (int) this["CustomTaskbarMarginsTop"];
      }
      set
      {
        this["CustomTaskbarMarginsTop"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("0")]
    public int CustomTaskbarMarginsRight
    {
      get
      {
        return (int) this["CustomTaskbarMarginsRight"];
      }
      set
      {
        this["CustomTaskbarMarginsRight"] = (object) value;
      }
    }

    [DefaultSettingValue("0")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int CustomTaskbarMarginsBottom
    {
      get
      {
        return (int) this["CustomTaskbarMarginsBottom"];
      }
      set
      {
        this["CustomTaskbarMarginsBottom"] = (object) value;
      }
    }

    [DefaultSettingValue("0")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public int CustomTaskbarMarginsLeft
    {
      get
      {
        return (int) this["CustomTaskbarMarginsLeft"];
      }
      set
      {
        this["CustomTaskbarMarginsLeft"] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool EnableAeroBackground
    {
      get
      {
        return (bool) this["EnableAeroBackground"];
      }
      set
      {
        this["EnableAeroBackground"] = (object) (bool) (value ? 1 : 0);
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("\"None\"")]
    [UserScopedSetting]
    public string PreviewGridAlign
    {
      get
      {
        return (string) this["PreviewGridAlign"];
      }
      set
      {
        this["PreviewGridAlign"] = (object) value;
      }
    }

    private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e)
    {
    }

    private void SettingsSavingEventHandler(object sender, CancelEventArgs e)
    {
    }
  }
}
