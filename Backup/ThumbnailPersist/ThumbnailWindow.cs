﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.ThumbnailWindow
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using MinimizeCapture;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using TrayPreviewSpace;

namespace ThumbnailPersist
{
  public class ThumbnailWindow : Form
  {
    private static readonly int GWL_STYLE = -16;
    private static readonly int DWM_TNP_VISIBLE = 8;
    private static readonly int DWM_TNP_OPACITY = 4;
    private static readonly int DWM_TNP_RECTDESTINATION = 1;
    private static readonly int DWM_TNP_RECTSOURCE = 2;
    private static readonly int DWM_TNP_SOURCECLIENTAREAONLY = 16;
    private static readonly ulong WS_VISIBLE = 268435456UL;
    private static readonly ulong WS_BORDER = 8388608UL;
    private static readonly ulong TARGETWINDOW = ThumbnailWindow.WS_BORDER | ThumbnailWindow.WS_VISIBLE;
    private static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
    private IContainer components = (IContainer) null;
    public string currentwindow = (string) null;
    public bool restorewindow = false;
    public bool emptywindow = true;
    public bool minimizing = false;
    private bool isMouseDown = false;
    public bool controlkeyisdown = false;
    public int panoffsetx1 = 0;
    public int panoffsety1 = 0;
    public int panoffsetx2 = 0;
    public int panoffsety2 = 0;
    public int deltapanx = 0;
    public int deltapany = 0;
    public bool isDocked = true;
    public bool isbrowsermode = false;
    public WINDOWINFO winfo = new WINDOWINFO();
    public WINDOWINFO winfo2 = new WINDOWINFO();
    public WINDOWINFO winfo3 = new WINDOWINFO();
    public long PreviewOriginalExStyle = 0L;
    private CacheWindowSize CachedPreviewParams = new CacheWindowSize();
    private bool UseCacheParams = false;
    private List<Window> windows = new List<Window>();
    private bool TargetShown = false;
    private ArrayList ChildLongs = new ArrayList();
    private bool isProcessing = true;
    private IntPtr TempThumb = IntPtr.Zero;
    private IntPtr StoredCurrentWindowHandle = IntPtr.Zero;
    private bool clicked = false;
    private bool inRegion = false;
    private bool inN = false;
    private bool inS = false;
    private bool inE = false;
    private bool inW = false;
    public bool wallpapermode = false;
    private bool ShiftisDown = false;
    private bool DockinN = false;
    private bool DockinS = false;
    private bool DockinE = false;
    private bool DockinW = false;
    private bool DockinCentre = false;
    private bool Moved = false;
    private int FrameBorderSizeWidth = 0;
    private int FrameBorderSizeHeight = 0;
    public const int WM_NCLBUTTONDOWN = 161;
    public const int HTCAPTION = 2;
    private const int SC_RESTORE = 61728;
    private const int WM_SYSCOMMAND = 274;
    private const int SC_MINIMIZE = 61472;
    public const int SC_MAXIMIZE = 61488;
    private const int WM_CLOSE = 16;
    private const int SW_SHOWNORMAL = 1;
    private const int SW_SHOWMINIMIZED = 2;
    private const int SW_SHOWMAXIMIZED = 3;
    public const int WM_LBUTTONDOWN = 513;
    public const int WM_LBUTTONUP = 514;
    public const int WM_RBUTTONDOWN = 516;
    public const int WM_RBUTTONUP = 517;
    private const uint WM_PAINT = 15U;
    private const int SW_SHOWNOACTIVATE = 4;
    private const int HWND_TOPMOST = -1;
    private const uint SWP_NOACTIVATE = 16U;
    private const int GWL_EXSTYLE = -20;
    private const int WS_EX_TOOLWINDOW = 128;
    private const int WS_EX_APPWINDOW = 262144;
    private const int WS_EX_LAYERED = 524288;
    private const int LWA_ALPHA = 2;
    public const int LWA_COLORKEY = 1;
    private const int WS_EX_NOACTIVATE = 134217728;
    private const int WS_EX_TRANSPARENT = 32;
    private const uint SWP_NOSIZE = 1U;
    private const uint SWP_NOMOVE = 2U;
    private const uint SWP_SHOWWINDOW = 64U;
    private const int WM_MOUSEWHEEL = 522;
    private const int WM_SIZE = 5;
    private const int SIZE_MINIMIZED = 1;
    public const int WM_COMMAND2 = 274;
    public const int WM_CLOSE2 = 61536;
    private PictureBox image;
    private ContextMenuStrip contextMenuStrip1;
    private ToolStripMenuItem quitToolStripMenuItem;
    private ToolStripMenuItem minimizeToolStripMenuItem;
    private ToolStripMenuItem settingsToolStripMenuItem;
    private Timer click_timer;
    private ToolStripMenuItem learnToolStripMenuItem;
    public WebBrowser webBrowser1;
    private ToolStripMenuItem closeToolStripMenuItem;
    public NotifyIcon notifyIcon1;
    private Label AuthorLabel;
    private Label TitleLabel;
    private ToolStripMenuItem hideTargetWindowToolStripMenuItem;
    private ToolStripMenuItem showTargetWindowToolStripMenuItem;
    private ToolTip FormToolTip;
    private ToolStripMenuItem alwaysOnTopNoMoveToolStripMenuItem;
    private WindowSize currentwindowsize;
    public string currentwindowname;
    private int userwindowindexnum;
    public int zoomleftoffset;
    public int zoomtopoffset;
    public int zoomrightoffset;
    public int zoombottomoffset;
    public double zoomfactor;
    public IntPtr currentwindowhandle;
    public int zoomcenterx;
    public int zoomcentery;
    public int storedwindowlocationx;
    public int storedwindowlocationy;
    private IntPtr thumb;
    private Point mouseOffset;
    public bool isYoutube;
    public int mouseclickx;
    public int mouseclicky;
    public double aspectratio;
    public int cropleftoffset;
    public int croptopoffset;
    public int croprightoffset;
    public int cropbottomoffset;
    public int xsizethumb;
    public int ysizethumb;
    public int PreviousXSizeThumb;
    public int PreviousYSizeThumb;
    public int PreviousXImageSize;
    public int PreviousYImageSize;
    public int PreviousXFormSize;
    public int PreviousYFormSize;
    public double croppedratioxleft;
    public double croppedratioxright;
    public double croppedratioytop;
    public double croppedratioybottom;
    public double zoomscalingfactor;
    public double resizeratio;
    public bool triploop;
    public bool zoomingOP;
    public int zoombyxpixels;
    public int zoombyypixels;
    public bool isvideo;
    public int xsizeform;
    public int ysizeform;
    public bool matchedwanted;
    public bool matchednotwanted;
    public static long WS_Transparent_Reverse;
    public int initializeparam;
    public IntPtr initializetaskbarsrc;
    private TrayPreview qparent;
    private long winLong;
    private ThumbnailWindow.WINDOWPLACEMENT placement;
    private List<IntPtr> TargetChilds;
    private Point ptOffset;
    private Size PreviousSize;
    private Point LockedPoint;
    private ThumbnailWindow.MARGINS margins;

    private int North
    {
      get
      {
        return this.Height - this.Height + 10;
      }
      set
      {
        this.North = value;
      }
    }

    private int South
    {
      get
      {
        return this.Height - 10;
      }
      set
      {
        this.South = value;
      }
    }

    private int East
    {
      get
      {
        return this.Width - 10;
      }
      set
      {
        this.East = value;
      }
    }

    private int West
    {
      get
      {
        return this.Width - this.Width + 10;
      }
      set
      {
        this.West = value;
      }
    }

    public ThumbnailWindow()
    {
    }

    public ThumbnailWindow(int param, IntPtr taskbarsrc)
    {
      this.initializeparam = param;
      this.initializetaskbarsrc = taskbarsrc;
      this.InitializeComponent();
    }

    public ThumbnailWindow(int param, IntPtr taskbarsrc, long TargetExStyle, bool WindowIsMaximized, TrayPreview TrayPreviewHandle)
    {
      this.PreviewOriginalExStyle = ThumbnailWindow.GetWindowLong(this.Handle, -20);
      ThumbnailWindow.SetWindowLong(this.Handle, -20, this.PreviewOriginalExStyle | 524288L);
      ThumbnailWindow.SetLayeredWindowAttributes(this.Handle, (byte) 0, (byte) 0, 2);
      this.initializeparam = param;
      this.initializetaskbarsrc = taskbarsrc;
      this.winLong = TargetExStyle;
      this.qparent = TrayPreviewHandle;
      this.InitializeComponent();
      this.click_timer.Enabled = false;
      GC.KeepAlive((object) this);
    }

    public ThumbnailWindow(int param, IntPtr taskbarsrc, long TargetExStyle, bool WindowIsMaximized, TrayPreview TrayPreviewHandle, CacheWindowSize OverRidePreviewParams)
    {
      this.PreviewOriginalExStyle = ThumbnailWindow.GetWindowLong(this.Handle, -20);
      ThumbnailWindow.SetWindowLong(this.Handle, -20, this.PreviewOriginalExStyle | 524288L);
      ThumbnailWindow.SetLayeredWindowAttributes(this.Handle, (byte) 0, (byte) 0, 2);
      this.initializeparam = param;
      this.initializetaskbarsrc = taskbarsrc;
      this.winLong = TargetExStyle;
      this.qparent = TrayPreviewHandle;
      this.CachedPreviewParams = OverRidePreviewParams;
      this.InitializeComponent();
      this.click_timer.Enabled = false;
      GC.KeepAlive((object) this);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ThumbnailWindow));
      this.contextMenuStrip1 = new ContextMenuStrip(this.components);
      this.closeToolStripMenuItem = new ToolStripMenuItem();
      this.hideTargetWindowToolStripMenuItem = new ToolStripMenuItem();
      this.showTargetWindowToolStripMenuItem = new ToolStripMenuItem();
      this.minimizeToolStripMenuItem = new ToolStripMenuItem();
      this.learnToolStripMenuItem = new ToolStripMenuItem();
      this.quitToolStripMenuItem = new ToolStripMenuItem();
      this.settingsToolStripMenuItem = new ToolStripMenuItem();
      this.alwaysOnTopNoMoveToolStripMenuItem = new ToolStripMenuItem();
      this.notifyIcon1 = new NotifyIcon(this.components);
      this.click_timer = new Timer(this.components);
      this.image = new PictureBox();
      this.AuthorLabel = new Label();
      this.TitleLabel = new Label();
      this.FormToolTip = new ToolTip(this.components);
      this.contextMenuStrip1.SuspendLayout();
      ((ISupportInitialize) this.image).BeginInit();
      this.SuspendLayout();
      this.contextMenuStrip1.BackColor = SystemColors.ControlLightLight;
      this.contextMenuStrip1.BackgroundImageLayout = ImageLayout.None;
      this.contextMenuStrip1.Items.AddRange(new ToolStripItem[8]
      {
        (ToolStripItem) this.closeToolStripMenuItem,
        (ToolStripItem) this.hideTargetWindowToolStripMenuItem,
        (ToolStripItem) this.showTargetWindowToolStripMenuItem,
        (ToolStripItem) this.minimizeToolStripMenuItem,
        (ToolStripItem) this.learnToolStripMenuItem,
        (ToolStripItem) this.quitToolStripMenuItem,
        (ToolStripItem) this.settingsToolStripMenuItem,
        (ToolStripItem) this.alwaysOnTopNoMoveToolStripMenuItem
      });
      this.contextMenuStrip1.Name = "contextMenuStrip1";
      this.contextMenuStrip1.RenderMode = ToolStripRenderMode.System;
      this.contextMenuStrip1.ShowCheckMargin = true;
      this.contextMenuStrip1.ShowImageMargin = false;
      this.contextMenuStrip1.ShowItemToolTips = false;
      this.contextMenuStrip1.Size = new Size(188, 180);
      this.contextMenuStrip1.TabStop = true;
      this.contextMenuStrip1.MouseUp += new MouseEventHandler(this.contextMenuStrip1_MouseUp);
      this.contextMenuStrip1.ItemClicked += new ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
      this.contextMenuStrip1.Opening += new CancelEventHandler(this.contextMenuStrip1_Opening);
      this.contextMenuStrip1.Closing += new ToolStripDropDownClosingEventHandler(this.contextMenuStrip1_Closing);
      this.contextMenuStrip1.MouseLeave += new EventHandler(this.contextMenuStrip1_MouseLeave);
      this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
      this.closeToolStripMenuItem.Size = new Size(187, 22);
      this.closeToolStripMenuItem.Text = "Close";
      this.closeToolStripMenuItem.Click += new EventHandler(this.closeToolStripMenuItem_Click);
      this.hideTargetWindowToolStripMenuItem.Name = "hideTargetWindowToolStripMenuItem";
      this.hideTargetWindowToolStripMenuItem.Size = new Size(187, 22);
      this.hideTargetWindowToolStripMenuItem.Text = "Hide Target Window";
      this.hideTargetWindowToolStripMenuItem.Click += new EventHandler(this.hideTargetWindowToolStripMenuItem_Click);
      this.showTargetWindowToolStripMenuItem.Name = "showTargetWindowToolStripMenuItem";
      this.showTargetWindowToolStripMenuItem.Size = new Size(187, 22);
      this.showTargetWindowToolStripMenuItem.Text = "Show Target Window";
      this.showTargetWindowToolStripMenuItem.Click += new EventHandler(this.showTargetWindowToolStripMenuItem_Click);
      this.minimizeToolStripMenuItem.Name = "minimizeToolStripMenuItem";
      this.minimizeToolStripMenuItem.Size = new Size(187, 22);
      this.minimizeToolStripMenuItem.Text = "Minimize";
      this.minimizeToolStripMenuItem.Click += new EventHandler(this.minimizeToolStripMenuItem_Click);
      this.learnToolStripMenuItem.Name = "learnToolStripMenuItem";
      this.learnToolStripMenuItem.Size = new Size(187, 22);
      this.learnToolStripMenuItem.Text = "Learn...";
      this.learnToolStripMenuItem.Click += new EventHandler(this.learnToolStripMenuItem_Click);
      this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
      this.quitToolStripMenuItem.Size = new Size(187, 22);
      this.quitToolStripMenuItem.Text = "Quit";
      this.quitToolStripMenuItem.Click += new EventHandler(this.quitToolStripMenuItem_Click);
      this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
      this.settingsToolStripMenuItem.Size = new Size(187, 22);
      this.settingsToolStripMenuItem.Text = "Settings";
      this.settingsToolStripMenuItem.Click += new EventHandler(this.settingsToolStripMenuItem_Click);
      this.alwaysOnTopNoMoveToolStripMenuItem.CheckOnClick = true;
      this.alwaysOnTopNoMoveToolStripMenuItem.Name = "alwaysOnTopNoMoveToolStripMenuItem";
      this.alwaysOnTopNoMoveToolStripMenuItem.Size = new Size(187, 22);
      this.alwaysOnTopNoMoveToolStripMenuItem.Text = "Always on Top";
      this.alwaysOnTopNoMoveToolStripMenuItem.Click += new EventHandler(this.alwaysOnTopNoMoveToolStripMenuItem_Click);
      this.notifyIcon1.BalloonTipText = "No Name";
      this.notifyIcon1.BalloonTipTitle = "Video in Picture";
      this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
      this.notifyIcon1.Icon = (Icon) componentResourceManager.GetObject("notifyIcon1.Icon");
      this.notifyIcon1.Text = "Video in Picture";
      this.notifyIcon1.MouseClick += new MouseEventHandler(this.notifyIcon1_MouseClick);
      this.notifyIcon1.MouseDoubleClick += new MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
      this.click_timer.Tick += new EventHandler(this.click_timer_Tick);
      this.image.AccessibleDescription = "Image";
      this.image.AccessibleName = "ViP";
      this.image.AccessibleRole = AccessibleRole.Window;
      this.image.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.image.BackColor = Color.Transparent;
      this.image.BackgroundImageLayout = ImageLayout.Center;
      this.image.Location = new Point(0, 0);
      this.image.Margin = new Padding(0);
      this.image.Name = "image";
      this.image.Size = new Size(307, 110);
      this.image.SizeMode = PictureBoxSizeMode.StretchImage;
      this.image.TabIndex = 5;
      this.image.TabStop = false;
      this.image.DoubleClick += new EventHandler(this.image_DoubleClick);
      this.image.MouseLeave += new EventHandler(this.image_MouseLeave);
      this.image.MouseMove += new MouseEventHandler(this.image_MouseMove);
      this.image.Click += new EventHandler(this.image_Click);
      this.image.MouseClick += new MouseEventHandler(this.image_MouseClick);
      this.image.MouseDown += new MouseEventHandler(this.image_MouseDown);
      this.image.MouseHover += new EventHandler(this.image_MouseHover);
      this.image.MouseUp += new MouseEventHandler(this.image_MouseUp);
      this.image.MouseEnter += new EventHandler(this.image_MouseEnter);
      this.AuthorLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.AuthorLabel.AutoSize = true;
      this.AuthorLabel.BackColor = Color.Transparent;
      this.AuthorLabel.Font = new Font("Brush Script MT", 9.75f, FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.AuthorLabel.ForeColor = Color.DarkGray;
      this.AuthorLabel.Location = new Point(221, 65);
      this.AuthorLabel.Name = "AuthorLabel";
      this.AuthorLabel.Size = new Size(74, 16);
      this.AuthorLabel.TabIndex = 8;
      this.AuthorLabel.Text = "By: Eric Wong";
      this.AuthorLabel.TextAlign = ContentAlignment.MiddleCenter;
      this.TitleLabel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.TitleLabel.AutoSize = true;
      this.TitleLabel.BackColor = Color.Transparent;
      this.TitleLabel.Font = new Font("Brush Script MT", 18f, FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.TitleLabel.ForeColor = Color.Silver;
      this.TitleLabel.Location = new Point(4, 25);
      this.TitleLabel.Name = "TitleLabel";
      this.TitleLabel.Size = new Size(304, 29);
      this.TitleLabel.TabIndex = 9;
      this.TitleLabel.Text = "~ Video in Picture (0.2 Preview) ~";
      this.TitleLabel.TextAlign = ContentAlignment.MiddleCenter;
      this.FormToolTip.Active = false;
      this.FormToolTip.AutomaticDelay = 300;
      this.FormToolTip.AutoPopDelay = 3000;
      this.FormToolTip.InitialDelay = 10;
      this.FormToolTip.ReshowDelay = 10;
      this.AccessibleDescription = "Video in Picture";
      this.AccessibleName = "ViP";
      this.AccessibleRole = AccessibleRole.Window;
      this.AutoScaleMode = AutoScaleMode.None;
      this.AutoValidate = AutoValidate.EnableAllowFocusChange;
      this.BackColor = Color.Black;
      this.BackgroundImageLayout = ImageLayout.None;
      this.ClientSize = new Size(307, 110);
      this.ContextMenuStrip = this.contextMenuStrip1;
      this.ControlBox = false;
      this.Controls.Add((Control) this.AuthorLabel);
      this.Controls.Add((Control) this.TitleLabel);
      this.Controls.Add((Control) this.image);
      this.DoubleBuffered = true;
      this.Font = new Font("Segoe UI", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.FormBorderStyle = FormBorderStyle.None;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ThumbnailWindow";
      this.Opacity = 0.0;
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.Manual;
      this.TransparencyKey = Color.Transparent;
      this.Deactivate += new EventHandler(this.ThumbnailWindow_Deactivate);
      this.Load += new EventHandler(this.Form1_Load);
      this.Shown += new EventHandler(this.ThumbnailWindow_Shown);
      this.Leave += new EventHandler(this.ThumbnailWindow_Leave);
      this.KeyUp += new KeyEventHandler(this.Form1_KeyUp);
      this.Move += new EventHandler(this.ThumbnailWindow_Move);
      this.FormClosing += new FormClosingEventHandler(this.ThumbnailWindow_FormClosing);
      this.Resize += new EventHandler(this.Form1_Resize);
      this.Validated += new EventHandler(this.ThumbnailWindow_Validated);
      this.KeyDown += new KeyEventHandler(this.Form1_KeyDown);
      this.contextMenuStrip1.ResumeLayout(false);
      ((ISupportInitialize) this.image).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    [DllImport("dwmapi.dll")]
    private static extern int DwmRegisterThumbnail(IntPtr dest, IntPtr src, out IntPtr thumb);

    [DllImport("dwmapi.dll")]
    private static extern int DwmUnregisterThumbnail(IntPtr thumb);

    [DllImport("dwmapi.dll")]
    private static extern int DwmQueryThumbnailSourceSize(IntPtr thumb, out PSIZE size);

    [DllImport("dwmapi.dll")]
    private static extern int DwmUpdateThumbnailProperties(IntPtr hThumb, ref DWM_THUMBNAIL_PROPERTIES props);

    [DllImport("SHELL32", CallingConvention = CallingConvention.StdCall)]
    private static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);

    [DllImport("USER32")]
    private static extern int GetSystemMetrics(int Index);

    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    private static extern bool MoveWindow(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);

    [DllImport("User32.dll", CharSet = CharSet.Auto)]
    private static extern int RegisterWindowMessage(string msg);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool IsIconic(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern ulong GetWindowLongA(IntPtr hWnd, int nIndex);

    [DllImport("user32.dll")]
    private static extern int EnumWindows(ThumbnailWindow.EnumWindowsCallback lpEnumFunc, int lParam);

    [DllImport("user32.dll")]
    public static extern void GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

    [DllImport("user32.dll")]
    private static extern bool SetForegroundWindow(int hwnd);

    [DllImport("user32.dll")]
    private static extern int GetDesktopWindow();

    [DllImport("user32.dll")]
    private static extern int FindWindow(string className, string windowText);

    [DllImport("user32")]
    public static extern int MoveWindow(int hwnd, int x, int y, int nWidth, int nHeight, int bRepaint);

    [DllImport("user32.dll")]
    private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);

    [DllImport("user32.dll")]
    public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

    [DllImport("user32.dll")]
    private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll")]
    private static extern uint SendMessage(IntPtr hWnd, uint msg, uint wParam, uint lParam);

    [DllImport("user32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    public static extern int SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPos(int hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    [DllImport("user32.dll")]
    public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

    [DllImport("user32")]
    public static extern int SetCursorPos(int x, int y);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);

    [DllImport("user32.dll")]
    private static extern long GetWindowLong(IntPtr hWnd, int nIndex);

    [DllImport("user32.dll", SetLastError = true)]
    private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, IntPtr windowTitle);

    [DllImport("user32")]
    public static extern int SetParent(int hWndChild, int hWndNewParent);

    [DllImport("user32")]
    private static extern int SetLayeredWindowAttributes(IntPtr hWnd, byte crey, byte alpha, int flags);

    [DllImport("user32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool ShowWindow(IntPtr hWnd, ThumbnailWindow.ShowWindowEnum flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, [In] ref RECT lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool RedrawWindow(IntPtr hWnd, IntPtr lprcUpdate, IntPtr hrgnUpdate, uint flags);

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool GetWindowPlacement(IntPtr hWnd, ref ThumbnailWindow.WINDOWPLACEMENT lpwndpl);

    [DllImport("user32.dll")]
    private static extern bool SetWindowPlacement(IntPtr hWnd, [In] ref ThumbnailWindow.WINDOWPLACEMENT lpwndpl);

    public void RestoreTransparency()
    {
      ThumbnailWindow.SetLayeredWindowAttributes(this.Handle, (byte) 0, (byte) ((int) byte.MaxValue - UserSettings.Default.BackTransparency), 2);
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      if (this.initializeparam == 1)
        return;
      if (this.initializeparam == 2)
      {
        if (UserSettings.Default.ShowBordersByDefault)
          this.FormBorderStyle = FormBorderStyle.Sizable;
        else
          this.FormBorderStyle = FormBorderStyle.None;
        if (UserSettings.Default.ShowBackText)
        {
          this.TitleLabel.Visible = true;
          this.AuthorLabel.Visible = true;
        }
        else
        {
          this.TitleLabel.Visible = false;
          this.AuthorLabel.Visible = false;
        }
        if (this.thumb != IntPtr.Zero)
          ThumbnailWindow.DwmUnregisterThumbnail(this.thumb);
        ThumbnailWindow.DwmRegisterThumbnail(this.Handle, this.initializetaskbarsrc, out this.thumb);
        this.currentwindowhandle = this.initializetaskbarsrc;
        this.SetDefaultViewNoPosition();
      }
      else if (this.initializeparam == 3)
      {
        if (UserSettings.Default.ShowBordersByDefault)
          this.FormBorderStyle = FormBorderStyle.Sizable;
        else
          this.FormBorderStyle = FormBorderStyle.None;
        if (UserSettings.Default.ShowBackText)
        {
          this.TitleLabel.Visible = true;
          this.AuthorLabel.Visible = true;
        }
        else
        {
          this.TitleLabel.Visible = false;
          this.AuthorLabel.Visible = false;
        }
        if (this.thumb != IntPtr.Zero)
          ThumbnailWindow.DwmUnregisterThumbnail(this.thumb);
        ThumbnailWindow.DwmRegisterThumbnail(this.Handle, this.initializetaskbarsrc, out this.thumb);
        this.currentwindowhandle = this.initializetaskbarsrc;
        this.SetDefaultViewNoPosition();
      }
      else
      {
        if (this.initializeparam != 4)
          return;
        if (UserSettings.Default.ShowBordersByDefault)
          this.FormBorderStyle = FormBorderStyle.Sizable;
        else
          this.FormBorderStyle = FormBorderStyle.None;
        if (UserSettings.Default.ShowBackText)
        {
          this.TitleLabel.Visible = true;
          this.AuthorLabel.Visible = true;
        }
        else
        {
          this.TitleLabel.Visible = false;
          this.AuthorLabel.Visible = false;
        }
        if (this.thumb != IntPtr.Zero)
          ThumbnailWindow.DwmUnregisterThumbnail(this.thumb);
        ThumbnailWindow.DwmRegisterThumbnail(this.Handle, this.initializetaskbarsrc, out this.thumb);
        this.currentwindowhandle = this.initializetaskbarsrc;
        this.UseCacheParams = true;
        this.SetDefaultViewNoPosition2();
        this.UseCacheParams = false;
      }
    }

    public void LoadPreviewCacheParams()
    {
      this.aspectratio = this.CachedPreviewParams.aspectratio;
      this.resizeratio = this.CachedPreviewParams.resizeratio;
      this.xsizethumb = this.CachedPreviewParams.xsizethumb;
      this.ysizethumb = this.CachedPreviewParams.ysizethumb;
      this.cropbottomoffset = this.CachedPreviewParams.cropbottomoffset;
      this.croprightoffset = this.CachedPreviewParams.croprightoffset;
      this.croptopoffset = this.CachedPreviewParams.croptopoffset;
      this.cropleftoffset = this.CachedPreviewParams.cropleftoffset;
      int index = this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle);
      this.qparent.AlwaysOnTop[index] = (object) (bool) (this.CachedPreviewParams.alwaysontop ? 1 : 0);
      this.qparent.NoMove[index] = (object) (bool) (this.CachedPreviewParams.nomove ? 1 : 0);
      this.Location = new Point(this.CachedPreviewParams.locationx, this.CachedPreviewParams.locationy);
    }

    public void SavePreviewCacheParams()
    {
      int index1 = this.qparent.CacheTargetHandles.IndexOf((object) this.currentwindowhandle);
      if (index1 < 0)
        return;
      CacheWindowSize cacheWindowSize = new CacheWindowSize();
      cacheWindowSize.aspectratio = this.aspectratio;
      cacheWindowSize.resizeratio = this.resizeratio;
      cacheWindowSize.xsizethumb = this.xsizethumb;
      cacheWindowSize.ysizethumb = this.ysizethumb;
      cacheWindowSize.cropbottomoffset = this.cropbottomoffset;
      cacheWindowSize.croprightoffset = this.croprightoffset;
      cacheWindowSize.croptopoffset = this.croptopoffset;
      cacheWindowSize.cropleftoffset = this.cropleftoffset;
      int index2 = this.qparent.ViPPreviewHandles.IndexOf((object) this.Handle);
      cacheWindowSize.alwaysontop = (bool) this.qparent.AlwaysOnTop[index2];
      cacheWindowSize.nomove = (bool) this.qparent.NoMove[index2];
      cacheWindowSize.locationx = this.Location.X;
      cacheWindowSize.locationy = this.Location.Y;
      this.qparent.CachePreviewSize[index1] = (object) cacheWindowSize;
    }

    public void SetDefaultView()
    {
      this.ResetSettings();
      this.Hide();
      this.SizeWindow(1);
      this.Position_Screen();
      this.Show();
    }

    public void SetDefaultViewNoPosition()
    {
      this.ResetSettings();
      this.Hide();
      this.SizeWindow(1);
      this.Position_Screen_Start();
    }

    public void SetDefaultViewNoPosition2()
    {
      this.ResetSettings();
      this.Hide();
      this.SizeWindow(1);
    }

    public void ResetSettings()
    {
      this.zoomfactor = UserSettings.Default.ZoomDefaultFactor;
      this.deltapanx = 0;
      this.deltapany = 0;
      this.croppedratioxleft = 0.0;
      this.croppedratioxright = 0.0;
      this.croppedratioytop = 0.0;
      this.croppedratioybottom = 0.0;
      this.zoomscalingfactor = UserSettings.Default.ZoomScalingFactor;
      this.zoombyxpixels = 0;
      this.zoombyypixels = 0;
      this.zoomleftoffset = 0;
      this.zoomtopoffset = 0;
      this.zoomrightoffset = 0;
      this.zoombottomoffset = 0;
      this.cropbottomoffset = 0;
      this.cropleftoffset = 0;
      this.croptopoffset = 0;
      this.croprightoffset = 0;
      this.TargetShown = false;
    }

    private void Position_Screen()
    {
      if (this.initializeparam == 1)
        return;
      APPBARDATA pData = new APPBARDATA();
      int num1 = (int) ThumbnailWindow.SHAppBarMessage(5, ref pData);
      int num2 = (int) ThumbnailWindow.SHAppBarMessage(4, ref pData);
      int num3 = 0;
      int num4 = 0;
      int num5 = 0;
      int x1 = 0;
      int num6 = 0;
      int y1 = 0;
      int num7 = 0;
      if (UserSettings.Default.CustomTaskbarMargins)
      {
        x1 = UserSettings.Default.CustomTaskbarMarginsLeft;
        num7 = -UserSettings.Default.CustomTaskbarMarginsBottom;
        y1 = UserSettings.Default.CustomTaskbarMarginsTop;
        num6 = -UserSettings.Default.CustomTaskbarMarginsRight;
      }
      else if (pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
      {
        num5 = 1;
        num4 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
      }
      else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
      {
        num5 = 2;
        num4 = pData.rc.bottom;
      }
      else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
      {
        num5 = 3;
        num3 = pData.rc.right;
      }
      else if (pData.rc.top == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
      {
        num5 = 4;
        num3 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
      }
      Point location;
      if (UserSettings.Default.DockPreview && UserSettings.Default.PreviewGridAlign == "None")
      {
        if (UserSettings.Default.startincenter)
        {
          int right = Screen.PrimaryScreen.Bounds.Right;
          Rectangle bounds = Screen.PrimaryScreen.Bounds;
          int num8 = bounds.Right / 2;
          int x2 = right - num8 - this.Size.Width / 2;
          bounds = Screen.PrimaryScreen.Bounds;
          int bottom = bounds.Bottom;
          bounds = Screen.PrimaryScreen.Bounds;
          int num9 = bounds.Bottom / 2;
          int y2 = bottom - num9 - this.Size.Height / 2;
          this.Location = new Point(x2, y2);
        }
        else if (UserSettings.Default.startbottomleft)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
        }
        else if (UserSettings.Default.startbottomright)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          int right = Screen.PrimaryScreen.Bounds.Right;
          Size size = this.Size;
          int width = size.Width;
          int x2 = right - width + num3 + num6;
          int bottom = Screen.PrimaryScreen.Bounds.Bottom;
          size = this.Size;
          int height = size.Height;
          int y2 = bottom - height + num4 + num7;
          this.Location = new Point(x2, y2);
        }
        else if (UserSettings.Default.startright)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y < y1)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, y1);
            }
            else
            {
              int num8 = this.Location.Y + this.Size.Height;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Height + num7;
              if (num8 > num9)
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                int x2 = right - width + num3 + num6;
                size = SystemInformation.PrimaryMonitorSize;
                int num10 = size.Height + num7;
                size = this.Size;
                int height = size.Height;
                int y2 = num10 - height;
                this.Location = new Point(x2, y2);
              }
              else
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                this.Location = new Point(right - width + num3 + num6, this.Location.Y);
              }
            }
          }
          else
          {
            if (num5 == 3)
              num3 = 0;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, this.Location.Y);
          }
        }
        else if (UserSettings.Default.startleft)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y < y1)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, y1);
            }
            else
            {
              int num8 = this.Location.Y + this.Size.Height;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Height + num7;
              if (num8 > num9)
              {
                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                size = SystemInformation.PrimaryMonitorSize;
                int num10 = size.Height + num7;
                size = this.Size;
                int height = size.Height;
                int y2 = num10 - height;
                this.Location = new Point(x2, y2);
              }
              else
                this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, this.Location.Y);
            }
          }
          else
          {
            if (num5 == 4)
              num3 = 0;
            int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
            location = this.Location;
            int y2 = location.Y;
            this.Location = new Point(x2, y2);
          }
        }
        else if (UserSettings.Default.startbottommiddle)
        {
          if (num5 == 2)
            num4 = 0;
          int num8 = Screen.PrimaryScreen.Bounds.Right / 2;
          Size size = this.Size;
          int num9 = size.Width / 2;
          int x2 = num8 - num9 + num3;
          int bottom = Screen.PrimaryScreen.Bounds.Bottom;
          size = this.Size;
          int height = size.Height;
          int y2 = bottom - height + num4 + num7;
          this.Location = new Point(x2, y2);
        }
        else if (UserSettings.Default.startbottom)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X < x1)
            {
              this.Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
            }
            else
            {
              int num8 = this.Location.X + this.Size.Width;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Width + num6;
              if (num8 > num9)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num10 = size.Width + num6;
                size = this.Size;
                int width = size.Width;
                int x2 = num10 - width;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y2 = bottom - height + num4 + num7;
                this.Location = new Point(x2, y2);
              }
              else
              {
                int x2 = this.Location.X;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y2 = bottom - height + num4 + num7;
                this.Location = new Point(x2, y2);
              }
            }
          }
          else
          {
            if (num5 == 2)
              num4 = 0;
            this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
          }
        }
        else if (UserSettings.Default.starttopleft)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
        }
        else if (UserSettings.Default.starttopright)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
        }
        else if (UserSettings.Default.starttopmiddle)
        {
          if (num5 == 1)
            num4 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Right / 2 - this.Size.Width / 2 + num3, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
        }
        else if (UserSettings.Default.starttop)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X < x1)
            {
              this.Location = new Point(x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
            else
            {
              int num8 = this.Location.X + this.Size.Width;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Width + num6;
              if (num8 > num9)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num10 = size.Width + num6;
                size = this.Size;
                int width = size.Width;
                this.Location = new Point(num10 - width, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
              }
              else
                this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
          }
          else
          {
            if (num5 == 1)
              num4 = 0;
            this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
          }
        }
        else if (UserSettings.Default.StartCustomLocation)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            int x2 = UserSettings.Default.startpositionx;
            int y2 = UserSettings.Default.startpositiony;
            Size size;
            if (x2 < x1)
            {
              x2 = x1;
            }
            else
            {
              int num8 = x2 + this.Size.Width;
              size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Width + num6;
              if (num8 > num9)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num10 = size.Width + num6;
                size = this.Size;
                int width = size.Width;
                x2 = num10 - width;
              }
            }
            if (y2 < y1)
            {
              y2 = y1;
            }
            else
            {
              int num8 = y2;
              size = this.Size;
              int height = size.Height;
              int num9 = num8 + height;
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Height + num7;
              if (num9 > num10)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num11 = size.Height + num7;
                size = this.Size;
                int width = size.Width;
                y2 = num11 - width;
              }
            }
            this.Location = new Point(x2, y2);
          }
          else
            this.Location = new Point(UserSettings.Default.startpositionx, UserSettings.Default.startpositiony);
        }
        else
        {
          int x2 = this.Location.X;
          location = this.Location;
          int y2 = location.Y;
          Size size;
          if (x2 < x1)
          {
            x2 = x1;
          }
          else
          {
            int num8 = x2 + this.Size.Width;
            size = SystemInformation.PrimaryMonitorSize;
            int num9 = size.Width + num6;
            if (num8 > num9)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Width + num6;
              size = this.Size;
              int width = size.Width;
              x2 = num10 - width;
            }
          }
          if (y2 < y1)
          {
            y2 = y1;
          }
          else
          {
            int num8 = y2;
            size = this.Size;
            int height1 = size.Height;
            int num9 = num8 + height1;
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Height + num7;
            if (num9 > num10)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Height + num7;
              size = this.Size;
              int height2 = size.Height;
              y2 = num11 - height2;
            }
          }
          this.Location = new Point(x2, y2);
        }
      }
      else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormEdgeDocking && (this.DockinW || this.DockinE || this.DockinN || this.DockinS))
      {
        if (this.DockinW && this.DockinS)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
        }
        else if (this.DockinE && this.DockinS)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          int right = Screen.PrimaryScreen.Bounds.Right;
          Size size = this.Size;
          int width = size.Width;
          int x2 = right - width + num3 + num6;
          int bottom = Screen.PrimaryScreen.Bounds.Bottom;
          size = this.Size;
          int height = size.Height;
          int y2 = bottom - height + num4 + num7;
          this.Location = new Point(x2, y2);
        }
        else if (this.DockinW && this.DockinN)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
        }
        else if (this.DockinE && this.DockinN)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
        }
        else if (this.DockinE)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y < y1)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, this.Location.Y);
            }
            else
            {
              int num8 = this.Location.Y + this.Size.Height;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Height + num7;
              if (num8 > num9)
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                int x2 = right - width + num3 + num6;
                size = SystemInformation.PrimaryMonitorSize;
                int height1 = size.Height;
                size = this.Size;
                int height2 = size.Height;
                int y2 = height1 - height2 + num7;
                this.Location = new Point(x2, y2);
              }
              else
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                this.Location = new Point(right - width + num3 + num6, this.Location.Y);
              }
            }
          }
          else
          {
            if (num5 == 3)
              num3 = 0;
            int x2 = Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3;
            location = this.Location;
            int y2 = location.Y;
            this.Location = new Point(x2, y2);
          }
        }
        else if (this.DockinW)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y < y1)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, this.Location.Y);
            }
            else
            {
              int num8 = this.Location.Y + this.Size.Height;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Height + num7;
              if (num8 > num9)
              {
                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                size = SystemInformation.PrimaryMonitorSize;
                int height1 = size.Height;
                size = this.Size;
                int height2 = size.Height;
                int y2 = height1 - height2 + num7;
                this.Location = new Point(x2, y2);
              }
              else
              {
                if (num5 == 3)
                  num3 = 0;
                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                location = this.Location;
                int y2 = location.Y;
                this.Location = new Point(x2, y2);
              }
            }
          }
          else
          {
            if (num5 == 4)
              num3 = 0;
            int x2 = Screen.PrimaryScreen.Bounds.Left + num3;
            location = this.Location;
            int y2 = location.Y;
            this.Location = new Point(x2, y2);
          }
        }
        else if (this.DockinS)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X < x1)
            {
              this.Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
            }
            else
            {
              int num8 = this.Location.X - this.Size.Width;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Width + num6;
              if (num8 > num9)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num10 = size.Width + num6;
                size = this.Size;
                int width = size.Width;
                int x2 = num10 - width;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y2 = bottom - height + num4 + num7;
                this.Location = new Point(x2, y2);
              }
              else
              {
                int x2 = this.Location.X;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y2 = bottom - height + num4 + num7;
                this.Location = new Point(x2, y2);
              }
            }
          }
          else
          {
            if (num5 == 2)
              num4 = 0;
            this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
          }
        }
        else if (this.DockinN)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X < x1)
            {
              this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
            else
            {
              int num8 = this.Location.X - this.Size.Width;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Width + num6;
              if (num8 > num9)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num10 = size.Width + num6;
                size = this.Size;
                int width = size.Width;
                this.Location = new Point(num10 - width, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
              }
              else
                this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
          }
          else
          {
            if (num5 == 1)
              num4 = 0;
            this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
          }
        }
      }
      else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormCentreDocking && this.DockinCentre)
      {
        int right = Screen.PrimaryScreen.Bounds.Right;
        Rectangle bounds = Screen.PrimaryScreen.Bounds;
        int num8 = bounds.Right / 2;
        int x2 = right - num8 - this.Size.Width / 2;
        bounds = Screen.PrimaryScreen.Bounds;
        int bottom = bounds.Bottom;
        bounds = Screen.PrimaryScreen.Bounds;
        int num9 = bounds.Bottom / 2;
        int y2 = bottom - num9 - this.Size.Height / 2;
        this.Location = new Point(x2, y2);
      }
      else
      {
        int x2 = this.Location.X;
        location = this.Location;
        int y2 = location.Y;
        location = this.Location;
        Size size;
        if (location.X < x1)
        {
          x2 = x1;
        }
        else
        {
          location = this.Location;
          int num8 = location.X + this.Size.Width;
          size = SystemInformation.PrimaryMonitorSize;
          int num9 = size.Width + num6;
          if (num8 > num9)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Width + num6;
            size = this.Size;
            int width = size.Width;
            x2 = num10 - width;
          }
        }
        location = this.Location;
        if (location.Y < y1)
        {
          y2 = y1;
        }
        else
        {
          location = this.Location;
          int y3 = location.Y;
          size = this.Size;
          int height1 = size.Height;
          int num8 = y3 + height1;
          size = SystemInformation.PrimaryMonitorSize;
          int num9 = size.Height + num7;
          if (num8 > num9)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Height + num7;
            size = this.Size;
            int height2 = size.Height;
            y2 = num10 - height2;
          }
        }
        this.Location = new Point(x2, y2);
      }
      location = this.Location;
      this.storedwindowlocationx = location.X;
      location = this.Location;
      this.storedwindowlocationy = location.Y;
    }

    private void Position_Screen_With_Offset(int Forceoffsetx, int Forceoffsety)
    {
      if (this.initializeparam == 1)
        return;
      APPBARDATA pData = new APPBARDATA();
      int num1 = (int) ThumbnailWindow.SHAppBarMessage(5, ref pData);
      int num2 = (int) ThumbnailWindow.SHAppBarMessage(4, ref pData);
      int num3 = 0;
      int num4 = 0;
      int num5 = 0;
      int x1 = 0;
      int num6 = 0;
      int num7 = 0;
      int num8 = 0;
      if (UserSettings.Default.CustomTaskbarMargins)
      {
        x1 = UserSettings.Default.CustomTaskbarMarginsLeft;
        num8 = -UserSettings.Default.CustomTaskbarMarginsBottom;
        num7 = UserSettings.Default.CustomTaskbarMarginsTop;
        num6 = -UserSettings.Default.CustomTaskbarMarginsRight;
      }
      else if (pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
      {
        num5 = 1;
        num4 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
      }
      else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
      {
        num5 = 2;
        num4 = pData.rc.bottom;
      }
      else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
      {
        num5 = 3;
        num3 = pData.rc.right;
      }
      else if (pData.rc.top == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
      {
        num5 = 4;
        num3 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
      }
      Point location;
      if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.DockPreview)
      {
        if (UserSettings.Default.startincenter)
        {
          int right = Screen.PrimaryScreen.Bounds.Right;
          Rectangle bounds = Screen.PrimaryScreen.Bounds;
          int num9 = bounds.Right / 2;
          int x2 = right - num9 - this.Size.Width / 2;
          bounds = Screen.PrimaryScreen.Bounds;
          int bottom = bounds.Bottom;
          bounds = Screen.PrimaryScreen.Bounds;
          int num10 = bounds.Bottom / 2;
          int y = bottom - num10 - this.Size.Height / 2;
          this.Location = new Point(x2, y);
        }
        else if (UserSettings.Default.startbottomleft)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num8);
        }
        else if (UserSettings.Default.startbottomright)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          int right = Screen.PrimaryScreen.Bounds.Right;
          Size size = this.Size;
          int width = size.Width;
          int x2 = right - width + num3 + num6;
          int bottom = Screen.PrimaryScreen.Bounds.Bottom;
          size = this.Size;
          int height = size.Height;
          int y = bottom - height + num4 + num8;
          this.Location = new Point(x2, y);
        }
        else if (UserSettings.Default.startright)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y < num7)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, num7 + Forceoffsety);
            }
            else
            {
              int num9 = this.Location.Y + this.Size.Height;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Height + num8;
              if (num9 > num10)
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                int x2 = right - width + num3 + num6;
                size = SystemInformation.PrimaryMonitorSize;
                int num11 = size.Height + num8;
                size = this.Size;
                int height = size.Height;
                int y = num11 - height + Forceoffsety;
                this.Location = new Point(x2, y);
              }
              else
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                this.Location = new Point(right - width + num3 + num6, this.Location.Y + Forceoffsety);
              }
            }
          }
          else
          {
            if (num5 == 3)
              num3 = 0;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, this.Location.Y + Forceoffsety);
          }
        }
        else if (UserSettings.Default.startleft)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y < num7)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, num7 + Forceoffsety);
            }
            else
            {
              int num9 = this.Location.Y + this.Size.Height;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Height + num8;
              if (num9 > num10)
              {
                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                size = SystemInformation.PrimaryMonitorSize;
                int num11 = size.Height + num8;
                size = this.Size;
                int height = size.Height;
                int y = num11 - height + Forceoffsety;
                this.Location = new Point(x2, y);
              }
              else
                this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, this.Location.Y + Forceoffsety);
            }
          }
          else
          {
            if (num5 == 4)
              num3 = 0;
            int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
            location = this.Location;
            int y = location.Y + Forceoffsety;
            this.Location = new Point(x2, y);
          }
        }
        else if (UserSettings.Default.startbottommiddle)
        {
          if (num5 == 2)
            num4 = 0;
          int num9 = Screen.PrimaryScreen.Bounds.Right / 2;
          Size size = this.Size;
          int num10 = size.Width / 2;
          int x2 = num9 - num10 + num3;
          int bottom = Screen.PrimaryScreen.Bounds.Bottom;
          size = this.Size;
          int height = size.Height;
          int y = bottom - height + num4 + num8;
          this.Location = new Point(x2, y);
        }
        else if (UserSettings.Default.startbottom)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X < x1)
            {
              this.Location = new Point(x1 + Forceoffsetx, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num8);
            }
            else
            {
              int num9 = this.Location.X + this.Size.Width;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Width + num6;
              if (num9 > num10)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num11 = size.Width + num6;
                size = this.Size;
                int width = size.Width;
                int x2 = num11 - width + Forceoffsetx;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y = bottom - height + num4 + num8;
                this.Location = new Point(x2, y);
              }
              else
              {
                int x2 = this.Location.X + Forceoffsetx;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y = bottom - height + num4 + num8;
                this.Location = new Point(x2, y);
              }
            }
          }
          else
          {
            if (num5 == 2)
              num4 = 0;
            this.Location = new Point(this.Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num8);
          }
        }
        else if (UserSettings.Default.starttopleft)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
        }
        else if (UserSettings.Default.starttopright)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
        }
        else if (UserSettings.Default.starttopmiddle)
        {
          if (num5 == 1)
            num4 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Right / 2 - this.Size.Width / 2 + num3, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
        }
        else if (UserSettings.Default.starttop)
        {
          if (num5 == 1)
            num4 = 0;
          int x2 = this.Location.X + Forceoffsetx;
          Rectangle bounds = Screen.PrimaryScreen.Bounds;
          int y1 = bounds.Top + num4;
          this.Location = new Point(x2, y1);
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X < x1)
            {
              int x3 = x1 + Forceoffsetx;
              bounds = Screen.PrimaryScreen.Bounds;
              int y2 = bounds.Top + num4 + num7;
              this.Location = new Point(x3, y2);
            }
            else
            {
              int num9 = this.Location.X + this.Size.Width;
              Size size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Width + num6;
              if (num9 > num10)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num11 = size.Width + num6;
                size = this.Size;
                int width = size.Width;
                int x3 = num11 - width + Forceoffsetx;
                bounds = Screen.PrimaryScreen.Bounds;
                int y2 = bounds.Top + num4 + num7;
                this.Location = new Point(x3, y2);
              }
              else
              {
                int x3 = this.Location.X + Forceoffsetx;
                bounds = Screen.PrimaryScreen.Bounds;
                int y2 = bounds.Top + num4 + num7;
                this.Location = new Point(x3, y2);
              }
            }
          }
          else
          {
            if (num5 == 1)
              num4 = 0;
            int x3 = this.Location.X + Forceoffsetx;
            bounds = Screen.PrimaryScreen.Bounds;
            int y2 = bounds.Top + num4 + num7;
            this.Location = new Point(x3, y2);
          }
        }
        else if (UserSettings.Default.StartCustomLocation)
        {
          int x2 = UserSettings.Default.startpositionx + Forceoffsetx;
          int y = UserSettings.Default.startpositiony + Forceoffsety;
          Size size;
          if (x2 < x1)
          {
            x2 = x1;
          }
          else
          {
            int num9 = x2 + this.Size.Width;
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Width + num6;
            if (num9 > num10)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Width + num6;
              size = this.Size;
              int width = size.Width;
              x2 = num11 - width;
            }
          }
          if (y < num7)
          {
            y = num7;
          }
          else
          {
            int num9 = y;
            size = this.Size;
            int height1 = size.Height;
            int num10 = num9 + height1;
            size = SystemInformation.PrimaryMonitorSize;
            int num11 = size.Height + num8;
            if (num10 > num11)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num12 = size.Height + num8;
              size = this.Size;
              int height2 = size.Height;
              y = num12 - height2;
            }
          }
          this.Location = new Point(x2, y);
        }
        else
        {
          int x2 = this.Location.X + Forceoffsetx;
          int y = this.Location.Y + Forceoffsety;
          Size size;
          if (x2 < x1)
          {
            x2 = x1;
          }
          else
          {
            int num9 = x2 + this.Size.Width;
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Width + num6;
            if (num9 > num10)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Width + num6;
              size = this.Size;
              int width = size.Width;
              x2 = num11 - width;
            }
          }
          if (y < num7)
          {
            y = num7;
          }
          else
          {
            int num9 = y;
            size = this.Size;
            int height1 = size.Height;
            int num10 = num9 + height1;
            size = SystemInformation.PrimaryMonitorSize;
            int num11 = size.Height + num8;
            if (num10 > num11)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num12 = size.Height + num8;
              size = this.Size;
              int height2 = size.Height;
              y = num12 - height2;
            }
          }
          this.Location = new Point(x2, y);
        }
      }
      else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormEdgeDocking && (this.DockinW || this.DockinE || this.DockinN || this.DockinS))
      {
        if (this.DockinW && this.DockinS)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num8);
        }
        else if (this.DockinE && this.DockinS)
        {
          if (num5 == 2)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          int right = Screen.PrimaryScreen.Bounds.Right;
          Size size = this.Size;
          int width = size.Width;
          int x2 = right - width + num3 + num6;
          int bottom = Screen.PrimaryScreen.Bounds.Bottom;
          size = this.Size;
          int height = size.Height;
          int y = bottom - height + num4 + num8;
          this.Location = new Point(x2, y);
        }
        else if (this.DockinW && this.DockinN)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 4)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
        }
        else if (this.DockinE && this.DockinN)
        {
          if (num5 == 1)
            num4 = 0;
          if (num5 == 3)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + num7);
        }
        else if (this.DockinE)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y + Forceoffsety < num7)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, this.Location.Y + Forceoffsety);
            }
            else
            {
              int num9 = this.Location.Y + Forceoffsety;
              Size size = this.Size;
              int height1 = size.Height;
              int num10 = num9 + height1;
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Height + num8;
              if (num10 > num11)
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                int x2 = right - width + num3 + num6;
                size = SystemInformation.PrimaryMonitorSize;
                int height2 = size.Height;
                size = this.Size;
                int height3 = size.Height;
                int y = height2 - height3 + num8;
                this.Location = new Point(x2, y);
              }
              else
              {
                int right = Screen.PrimaryScreen.Bounds.Right;
                size = this.Size;
                int width = size.Width;
                this.Location = new Point(right - width + num6 + num3, this.Location.Y + Forceoffsety);
              }
            }
          }
          else
          {
            if (num5 == 3)
              num3 = 0;
            int x2 = Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3;
            location = this.Location;
            int y = location.Y + Forceoffsety;
            this.Location = new Point(x2, y);
          }
        }
        else if (this.DockinW)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.Y + Forceoffsety < num7)
            {
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + x1 + num3, this.Location.Y + Forceoffsety);
            }
            else
            {
              int num9 = this.Location.Y + Forceoffsety;
              Size size = this.Size;
              int height1 = size.Height;
              int num10 = num9 + height1;
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Height + num8;
              if (num10 > num11)
              {
                int x2 = Screen.PrimaryScreen.Bounds.Left + x1 + num3;
                size = SystemInformation.PrimaryMonitorSize;
                int height2 = size.Height;
                size = this.Size;
                int height3 = size.Height;
                int y = height2 - height3 + num8;
                this.Location = new Point(x2, y);
              }
              else
              {
                if (num5 == 3)
                  num3 = 0;
                int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
                location = this.Location;
                int y = location.Y + Forceoffsety;
                this.Location = new Point(x2, y);
              }
            }
          }
          else
          {
            if (num5 == 4)
              num3 = 0;
            int x2 = Screen.PrimaryScreen.Bounds.Left + num3;
            location = this.Location;
            int y = location.Y + Forceoffsety;
            this.Location = new Point(x2, y);
          }
        }
        else if (this.DockinS)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X + Forceoffsetx < x1)
            {
              this.Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num8 + num4);
            }
            else
            {
              int num9 = this.Location.X + Forceoffsetx;
              Size size = this.Size;
              int width1 = size.Width;
              int num10 = num9 - width1;
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Width + num6;
              if (num10 > num11)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num12 = size.Width + num6;
                size = this.Size;
                int width2 = size.Width;
                int x2 = num12 - width2;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y = bottom - height + num8 + num4;
                this.Location = new Point(x2, y);
              }
              else
              {
                int x2 = this.Location.X + Forceoffsetx;
                int bottom = Screen.PrimaryScreen.Bounds.Bottom;
                size = this.Size;
                int height = size.Height;
                int y = bottom - height + num8 + num4;
                this.Location = new Point(x2, y);
              }
            }
          }
          else
          {
            if (num5 == 2)
              num4 = 0;
            this.Location = new Point(this.Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4);
          }
        }
        else if (this.DockinN)
        {
          if (UserSettings.Default.CustomTaskbarMargins)
          {
            if (this.Location.X + Forceoffsetx < x1)
            {
              this.Location = new Point(this.Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Top + num7 + num4);
            }
            else
            {
              int num9 = this.Location.X + Forceoffsetx;
              Size size = this.Size;
              int width1 = size.Width;
              int num10 = num9 - width1;
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Width + num6;
              if (num10 > num11)
              {
                size = SystemInformation.PrimaryMonitorSize;
                int num12 = size.Width + num6;
                size = this.Size;
                int width2 = size.Width;
                this.Location = new Point(num12 - width2, Screen.PrimaryScreen.Bounds.Top + num7 + num4);
              }
              else
                this.Location = new Point(this.Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Top + num7 + num4);
            }
          }
          else
          {
            if (num5 == 1)
              num4 = 0;
            this.Location = new Point(this.Location.X + Forceoffsetx, Screen.PrimaryScreen.Bounds.Top + num4);
          }
        }
      }
      else if (UserSettings.Default.PreviewGridAlign == "None" && UserSettings.Default.FreeFormCentreDocking && this.DockinCentre)
      {
        int right = Screen.PrimaryScreen.Bounds.Right;
        Rectangle bounds = Screen.PrimaryScreen.Bounds;
        int num9 = bounds.Right / 2;
        int x2 = right - num9 - this.Size.Width / 2;
        bounds = Screen.PrimaryScreen.Bounds;
        int bottom = bounds.Bottom;
        bounds = Screen.PrimaryScreen.Bounds;
        int num10 = bounds.Bottom / 2;
        int y = bottom - num10 - this.Size.Height / 2;
        this.Location = new Point(x2, y);
      }
      else
      {
        int x2 = this.Location.X + Forceoffsetx;
        int y = this.Location.Y + Forceoffsety;
        Size size;
        if (x2 < x1)
        {
          x2 = x1;
        }
        else
        {
          int num9 = x2 + this.Size.Width;
          size = SystemInformation.PrimaryMonitorSize;
          int num10 = size.Width + num6;
          if (num9 > num10)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int num11 = size.Width + num6;
            size = this.Size;
            int width = size.Width;
            x2 = num11 - width;
          }
        }
        if (y < num7)
        {
          y = num7;
        }
        else
        {
          int num9 = y;
          size = this.Size;
          int height1 = size.Height;
          int num10 = num9 + height1;
          size = SystemInformation.PrimaryMonitorSize;
          int num11 = size.Height + num8;
          if (num10 > num11)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int num12 = size.Height + num8;
            size = this.Size;
            int height2 = size.Height;
            y = num12 - height2;
          }
        }
        this.Location = new Point(x2, y);
      }
      location = this.Location;
      this.storedwindowlocationx = location.X;
      location = this.Location;
      this.storedwindowlocationy = location.Y;
    }

    public void Position_Screen_Start()
    {
      if (this.initializeparam == 1)
        return;
      APPBARDATA pData = new APPBARDATA();
      int num1 = (int) ThumbnailWindow.SHAppBarMessage(5, ref pData);
      int num2 = (int) ThumbnailWindow.SHAppBarMessage(4, ref pData);
      int num3 = 0;
      int num4 = 0;
      int num5 = 0;
      int x1 = 0;
      int num6 = 0;
      int y1 = 0;
      int num7 = 0;
      if (UserSettings.Default.CustomTaskbarMargins)
      {
        x1 = UserSettings.Default.CustomTaskbarMarginsLeft;
        num7 = -UserSettings.Default.CustomTaskbarMarginsBottom;
        y1 = UserSettings.Default.CustomTaskbarMarginsTop;
        num6 = -UserSettings.Default.CustomTaskbarMarginsRight;
      }
      else if (pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
      {
        num5 = 1;
        num4 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
      }
      else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width)
      {
        num5 = 2;
        num4 = pData.rc.bottom;
      }
      else if (pData.rc.top == 0 && pData.rc.left == 0 && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
      {
        num5 = 3;
        num3 = pData.rc.right;
      }
      else if (pData.rc.top == 0 && pData.rc.right == SystemInformation.PrimaryMonitorSize.Width && pData.rc.bottom == SystemInformation.PrimaryMonitorSize.Height)
      {
        num5 = 4;
        num3 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
      }
      Point location;
      if (UserSettings.Default.startincenter)
      {
        int right = Screen.PrimaryScreen.Bounds.Right;
        Rectangle bounds = Screen.PrimaryScreen.Bounds;
        int num8 = bounds.Right / 2;
        int x2 = right - num8 - this.Size.Width / 2;
        bounds = Screen.PrimaryScreen.Bounds;
        int bottom = bounds.Bottom;
        bounds = Screen.PrimaryScreen.Bounds;
        int num9 = bounds.Bottom / 2;
        int y2 = bottom - num9 - this.Size.Height / 2;
        this.Location = new Point(x2, y2);
      }
      else if (UserSettings.Default.startbottomleft)
      {
        if (num5 == 2)
          num4 = 0;
        if (num5 == 4)
          num3 = 0;
        this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
      }
      else if (UserSettings.Default.startbottomright)
      {
        if (num5 == 2)
          num4 = 0;
        if (num5 == 3)
          num3 = 0;
        int right = Screen.PrimaryScreen.Bounds.Right;
        Size size = this.Size;
        int width = size.Width;
        int x2 = right - width + num3 + num6;
        int bottom = Screen.PrimaryScreen.Bounds.Bottom;
        size = this.Size;
        int height = size.Height;
        int y2 = bottom - height + num4 + num7;
        this.Location = new Point(x2, y2);
      }
      else if (UserSettings.Default.startright)
      {
        if (UserSettings.Default.CustomTaskbarMargins)
        {
          if (this.Location.Y < y1)
          {
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, y1);
          }
          else
          {
            int num8 = this.Location.Y + this.Size.Height;
            Size size = SystemInformation.PrimaryMonitorSize;
            int num9 = size.Height + num7;
            if (num8 > num9)
            {
              int right = Screen.PrimaryScreen.Bounds.Right;
              size = this.Size;
              int width = size.Width;
              int x2 = right - width + num3 + num6;
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Height + num7;
              size = this.Size;
              int height = size.Height;
              int y2 = num10 - height;
              this.Location = new Point(x2, y2);
            }
            else
            {
              int right = Screen.PrimaryScreen.Bounds.Right;
              size = this.Size;
              int width = size.Width;
              this.Location = new Point(right - width + num3 + num6, this.Location.Y);
            }
          }
        }
        else
        {
          if (num5 == 3)
            num3 = 0;
          this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, this.Location.Y);
        }
      }
      else if (UserSettings.Default.startleft)
      {
        if (UserSettings.Default.CustomTaskbarMargins)
        {
          if (this.Location.Y < y1)
          {
            this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, y1);
          }
          else
          {
            int num8 = this.Location.Y + this.Size.Height;
            Size size = SystemInformation.PrimaryMonitorSize;
            int num9 = size.Height + num7;
            if (num8 > num9)
            {
              int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Height + num7;
              size = this.Size;
              int height = size.Height;
              int y2 = num10 - height;
              this.Location = new Point(x2, y2);
            }
            else
              this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, this.Location.Y);
          }
        }
        else
        {
          if (num5 == 4)
            num3 = 0;
          int x2 = Screen.PrimaryScreen.Bounds.Left + num3 + x1;
          location = this.Location;
          int y2 = location.Y;
          this.Location = new Point(x2, y2);
        }
      }
      else if (UserSettings.Default.startbottommiddle)
      {
        if (num5 == 2)
          num4 = 0;
        int num8 = Screen.PrimaryScreen.Bounds.Right / 2;
        Size size = this.Size;
        int num9 = size.Width / 2;
        int x2 = num8 - num9 + num3;
        int bottom = Screen.PrimaryScreen.Bounds.Bottom;
        size = this.Size;
        int height = size.Height;
        int y2 = bottom - height + num4 + num7;
        this.Location = new Point(x2, y2);
      }
      else if (UserSettings.Default.startbottom)
      {
        if (UserSettings.Default.CustomTaskbarMargins)
        {
          if (this.Location.X < x1)
          {
            this.Location = new Point(x1, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
          }
          else
          {
            int num8 = this.Location.X + this.Size.Width;
            Size size = SystemInformation.PrimaryMonitorSize;
            int num9 = size.Width + num6;
            if (num8 > num9)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Width + num6;
              size = this.Size;
              int width = size.Width;
              int x2 = num10 - width;
              int bottom = Screen.PrimaryScreen.Bounds.Bottom;
              size = this.Size;
              int height = size.Height;
              int y2 = bottom - height + num4 + num7;
              this.Location = new Point(x2, y2);
            }
            else
            {
              int x2 = this.Location.X;
              int bottom = Screen.PrimaryScreen.Bounds.Bottom;
              size = this.Size;
              int height = size.Height;
              int y2 = bottom - height + num4 + num7;
              this.Location = new Point(x2, y2);
            }
          }
        }
        else
        {
          if (num5 == 2)
            num4 = 0;
          this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Bottom - this.Size.Height + num4 + num7);
        }
      }
      else if (UserSettings.Default.starttopleft)
      {
        if (num5 == 1)
          num4 = 0;
        if (num5 == 4)
          num3 = 0;
        this.Location = new Point(Screen.PrimaryScreen.Bounds.Left + num3 + x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
      }
      else if (UserSettings.Default.starttopright)
      {
        if (num5 == 1)
          num4 = 0;
        if (num5 == 3)
          num3 = 0;
        this.Location = new Point(Screen.PrimaryScreen.Bounds.Right - this.Size.Width + num3 + num6, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
      }
      else if (UserSettings.Default.starttopmiddle)
      {
        if (num5 == 1)
          num4 = 0;
        this.Location = new Point(Screen.PrimaryScreen.Bounds.Right / 2 - this.Size.Width / 2 + num3, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
      }
      else if (UserSettings.Default.starttop)
      {
        if (UserSettings.Default.CustomTaskbarMargins)
        {
          if (this.Location.X < x1)
          {
            this.Location = new Point(x1, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
          }
          else
          {
            int num8 = this.Location.X + this.Size.Width;
            Size size = SystemInformation.PrimaryMonitorSize;
            int num9 = size.Width + num6;
            if (num8 > num9)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Width + num6;
              size = this.Size;
              int width = size.Width;
              this.Location = new Point(num10 - width, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
            }
            else
              this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
          }
        }
        else
        {
          if (num5 == 1)
            num4 = 0;
          this.Location = new Point(this.Location.X, Screen.PrimaryScreen.Bounds.Top + num4 + y1);
        }
      }
      else if (UserSettings.Default.StartCustomLocation)
      {
        if (UserSettings.Default.CustomTaskbarMargins)
        {
          int x2 = UserSettings.Default.startpositionx;
          int y2 = UserSettings.Default.startpositiony;
          location = this.Location;
          Size size;
          if (location.X < x1)
          {
            x2 = x1;
          }
          else
          {
            location = this.Location;
            int num8 = location.X + this.Size.Width;
            size = SystemInformation.PrimaryMonitorSize;
            int num9 = size.Width + num6;
            if (num8 > num9)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Width + num6;
              size = this.Size;
              int width = size.Width;
              x2 = num10 - width;
            }
          }
          location = this.Location;
          if (location.Y < y1)
          {
            y2 = y1;
          }
          else
          {
            location = this.Location;
            int y3 = location.Y;
            size = this.Size;
            int height = size.Height;
            int num8 = y3 + height;
            size = SystemInformation.PrimaryMonitorSize;
            int num9 = size.Height + num7;
            if (num8 > num9)
            {
              size = SystemInformation.PrimaryMonitorSize;
              int num10 = size.Height + num7;
              size = this.Size;
              int width = size.Width;
              y2 = num10 - width;
            }
          }
          this.Location = new Point(x2, y2);
        }
        else
          this.Location = new Point(UserSettings.Default.startpositionx, UserSettings.Default.startpositiony);
      }
      else
      {
        int x2 = this.Location.X;
        location = this.Location;
        int y2 = location.Y;
        location = this.Location;
        Size size;
        if (location.X < x1)
        {
          x2 = x1;
        }
        else
        {
          location = this.Location;
          int num8 = location.X + this.Size.Width;
          size = SystemInformation.PrimaryMonitorSize;
          int num9 = size.Width + num6;
          if (num8 > num9)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Width + num6;
            size = this.Size;
            int width = size.Width;
            x2 = num10 - width;
          }
        }
        location = this.Location;
        if (location.Y < y1)
        {
          y2 = y1;
        }
        else
        {
          location = this.Location;
          int y3 = location.Y;
          size = this.Size;
          int height1 = size.Height;
          int num8 = y3 + height1;
          size = SystemInformation.PrimaryMonitorSize;
          int num9 = size.Height + num7;
          if (num8 > num9)
          {
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Height + num7;
            size = this.Size;
            int height2 = size.Height;
            y2 = num10 - height2;
          }
        }
        this.Location = new Point(x2, y2);
      }
      location = this.Location;
      this.storedwindowlocationx = location.X;
      location = this.Location;
      this.storedwindowlocationy = location.Y;
    }

    private void GetWindows()
    {
      this.windows.Clear();
      ThumbnailWindow.EnumWindows(new ThumbnailWindow.EnumWindowsCallback(this.Callback), 0);
      if (!UserSettings.Default.ShowWindowListInContextMenu)
        return;
      int num = 0;
      foreach (Window window in this.windows)
      {
        ++num;
        this.contextMenuStrip1.Items.Add(num.ToString() + ".  " + window.ToString());
      }
    }

    private bool Callback(IntPtr hwnd, int lParam)
    {
      if (this.Handle != hwnd && ((long) ThumbnailWindow.GetWindowLongA(hwnd, ThumbnailWindow.GWL_STYLE) & (long) ThumbnailWindow.TARGETWINDOW) == (long) ThumbnailWindow.TARGETWINDOW)
      {
        StringBuilder lpString = new StringBuilder(100);
        ThumbnailWindow.GetWindowText(hwnd, lpString, lpString.Capacity);
        this.windows.Add(new Window()
        {
          Handle = hwnd,
          Title = lpString.ToString()
        });
      }
      return true;
    }

    public void SizeWindow(int state)
    {
      if (this.triploop || this.minimizing || this.initializeparam == 1)
        return;
      this.FindFrameBorderSize(ref this.FrameBorderSizeWidth, ref this.FrameBorderSizeHeight);
      PSIZE size1;
      ThumbnailWindow.DwmQueryThumbnailSourceSize(this.thumb, out size1);
      DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
      props.fVisible = true;
      props.dwFlags = ThumbnailWindow.DWM_TNP_VISIBLE | ThumbnailWindow.DWM_TNP_RECTDESTINATION | ThumbnailWindow.DWM_TNP_OPACITY | ThumbnailWindow.DWM_TNP_RECTSOURCE;
      props.opacity = (byte) ((int) byte.MaxValue - UserSettings.Default.ThumbTransparency);
      props.rcSource = new Rect(-this.FrameBorderSizeWidth, -this.FrameBorderSizeHeight, size1.x + this.FrameBorderSizeWidth, size1.y + this.FrameBorderSizeHeight);
      this.currentwindow = this.thumb.ToString();
      int index = 0;
      foreach (Window window in this.windows)
      {
        if (this.currentwindowhandle == this.windows[index].Handle)
        {
          this.currentwindowname = window.Title;
          break;
        }
        ++index;
      }
      if (state == 1)
      {
        this.ResetSettings();
        if (this.UseCacheParams)
        {
          this.LoadPreviewCacheParams();
          this.TargetShown = true;
          this.hideTargetWindowToolStripMenuItem_Click((object) this, (EventArgs) null);
          this.croppedratioxleft = (double) this.cropleftoffset / (double) this.xsizethumb;
          this.croppedratioytop = (double) this.croptopoffset / (double) this.ysizethumb;
          this.croppedratioxright = (double) this.croprightoffset / (double) this.xsizethumb;
          this.croppedratioybottom = (double) this.cropbottomoffset / (double) this.ysizethumb;
          this.xsizeform = this.xsizethumb + this.TotalBorderThicknessX();
          this.ysizeform = this.ysizethumb + this.TotalBorderThicknessY();
          this.ResizeFormP(this.xsizeform, this.ysizeform);
          props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
          ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
          this.PreviousXSizeThumb = this.xsizethumb;
          this.PreviousYSizeThumb = this.ysizethumb;
          this.PreviousXImageSize = this.image.Size.Width;
          Size size2 = this.image.Size;
          this.PreviousYImageSize = size2.Height;
          size2 = this.Size;
          this.PreviousXFormSize = size2.Width;
          size2 = this.Size;
          this.PreviousYFormSize = size2.Height;
          Point location = this.Location;
          this.storedwindowlocationx = location.X;
          location = this.Location;
          this.storedwindowlocationy = location.Y;
        }
        else
        {
          this.TargetShown = true;
          this.hideTargetWindowToolStripMenuItem_Click((object) this, (EventArgs) null);
          if (!UserSettings.Default.MoveTargetOffscreen)
            ;
          this.aspectratio = (double) size1.x / (double) size1.y;
          this.xsizethumb = (int) ((double) size1.x * UserSettings.Default.scalingfactor);
          this.ysizethumb = (int) ((double) size1.y * UserSettings.Default.scalingfactor);
          if (this.xsizethumb < UserSettings.Default.minpixelsx || this.ysizethumb < UserSettings.Default.minpixelsy)
          {
            this.xsizethumb = (int) ((double) UserSettings.Default.minpixelsy * this.aspectratio);
            this.ysizethumb = UserSettings.Default.minpixelsy;
          }
          else if (this.xsizethumb > UserSettings.Default.maxpixelsx || this.ysizethumb > UserSettings.Default.maxpixelsy)
          {
            this.xsizethumb = (int) ((double) UserSettings.Default.maxpixelsy * this.aspectratio);
            this.ysizethumb = UserSettings.Default.maxpixelsy;
          }
          props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
          ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
          this.xsizeform = this.xsizethumb + this.TotalBorderThicknessX();
          this.ysizeform = this.ysizethumb + this.TotalBorderThicknessY();
          this.ResizeFormP(this.xsizeform, this.ysizeform);
          this.PreviousXSizeThumb = this.xsizethumb;
          this.PreviousYSizeThumb = this.ysizethumb;
          Size size2 = this.image.Size;
          this.PreviousXImageSize = size2.Width;
          size2 = this.image.Size;
          this.PreviousYImageSize = size2.Height;
          size2 = this.Size;
          this.PreviousXFormSize = size2.Width;
          size2 = this.Size;
          this.PreviousYFormSize = size2.Height;
          Point location = this.Location;
          this.storedwindowlocationx = location.X;
          location = this.Location;
          this.storedwindowlocationy = location.Y;
        }
      }
      else if (state == 2)
      {
        this.cropleftoffset = (int) ((double) this.PreviousXSizeThumb * this.croppedratioxleft);
        this.croptopoffset = (int) ((double) this.PreviousYSizeThumb * this.croppedratioytop);
        this.croprightoffset = (int) ((double) this.PreviousXSizeThumb * this.croppedratioxright);
        this.cropbottomoffset = (int) ((double) this.PreviousYSizeThumb * this.croppedratioybottom);
        this.ResizeFormP(this.PreviousXFormSize, this.PreviousYFormSize);
        this.xsizethumb = (int) (double) this.image.Size.Width;
        Size size2 = this.image.Size;
        this.ysizethumb = (int) (double) size2.Height;
        if (!this.isbrowsermode)
        {
          props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
          ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
        }
        this.storedwindowlocationx = this.Location.X;
        this.storedwindowlocationy = this.Location.Y;
        this.PreviousXSizeThumb = this.xsizethumb;
        this.PreviousYSizeThumb = this.ysizethumb;
        size2 = this.image.Size;
        this.PreviousXImageSize = size2.Width;
        size2 = this.image.Size;
        this.PreviousYImageSize = size2.Height;
        size2 = this.Size;
        this.PreviousXFormSize = size2.Width;
        size2 = this.Size;
        this.PreviousYFormSize = size2.Height;
      }
      else if (state == 3)
      {
        double num1 = (double) this.image.Size.Height * this.aspectratio * this.resizeratio;
        Size size2 = this.image.Size;
        double num2 = (double) size2.Height * this.resizeratio;
        this.xsizethumb = (int) num1;
        this.ysizethumb = (int) num2;
        if (this.ysizethumb == this.PreviousYSizeThumb && this.resizeratio > 1.0)
        {
          size2 = this.image.Size;
          num1 = ((double) size2.Height + 1.0) * this.aspectratio * this.resizeratio;
          size2 = this.image.Size;
          num2 = ((double) size2.Height + 1.0) * this.resizeratio;
          this.xsizethumb = (int) num1;
          this.ysizethumb = (int) num2;
        }
        this.cropleftoffset = (int) (num1 * this.croppedratioxleft);
        this.croptopoffset = (int) (num2 * this.croppedratioytop);
        this.croprightoffset = (int) (num1 * this.croppedratioxright);
        this.cropbottomoffset = (int) (num2 * this.croppedratioybottom);
        this.xsizeform = this.xsizethumb + this.TotalBorderThicknessX();
        this.ysizeform = this.ysizethumb + this.TotalBorderThicknessY();
        this.ResizeFormP(this.xsizeform, this.ysizeform);
        if (!this.isbrowsermode)
        {
          props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
          ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
        }
        this.storedwindowlocationx = this.Location.X;
        this.storedwindowlocationy = this.Location.Y;
        this.PreviousXSizeThumb = this.xsizethumb;
        this.PreviousYSizeThumb = this.ysizethumb;
        size2 = this.image.Size;
        this.PreviousXImageSize = size2.Width;
        size2 = this.image.Size;
        this.PreviousYImageSize = size2.Height;
        size2 = this.Size;
        this.PreviousXFormSize = size2.Width;
        size2 = this.Size;
        this.PreviousYFormSize = size2.Height;
      }
      else if (state == 4)
      {
        this.croppedratioxleft = this.croppedratioxleft * this.resizeratio;
        this.croppedratioytop = this.croppedratioytop * this.resizeratio;
        this.croppedratioxright = this.croppedratioxright * this.resizeratio;
        this.croppedratioybottom = this.croppedratioybottom * this.resizeratio;
        this.cropleftoffset = (int) ((double) this.xsizethumb * this.croppedratioxleft);
        this.croptopoffset = (int) ((double) this.ysizethumb * this.croppedratioytop);
        this.croprightoffset = (int) ((double) this.xsizethumb * this.croppedratioxright);
        this.cropbottomoffset = (int) ((double) this.ysizethumb * this.croppedratioybottom);
        props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
        ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
      }
      else
      {
        if (state != 5)
          return;
        this.resizeratio = (double) SystemInformation.PrimaryMonitorMaximizedWindowSize.Height / (double) this.image.Size.Height;
        this.xsizethumb = (int) ((double) this.image.Size.Height * this.aspectratio * this.resizeratio);
        this.ysizethumb = (int) ((double) this.image.Size.Height * this.resizeratio);
        this.cropleftoffset = (int) ((double) this.PreviousXSizeThumb * this.croppedratioxleft * this.resizeratio);
        this.croptopoffset = (int) ((double) this.PreviousYSizeThumb * this.croppedratioytop * this.resizeratio);
        this.croprightoffset = (int) ((double) this.PreviousXSizeThumb * this.croppedratioxright * this.resizeratio);
        this.cropbottomoffset = (int) ((double) this.PreviousYSizeThumb * this.croppedratioybottom * this.resizeratio);
        Size size2;
        if (this.ysizethumb == this.PreviousYSizeThumb && this.resizeratio > 1.0)
        {
          size2 = this.image.Size;
          this.xsizethumb = (int) (((double) size2.Height + 1.0) * this.aspectratio * this.resizeratio);
          size2 = this.image.Size;
          this.ysizethumb = (int) (((double) size2.Height + 1.0) * this.resizeratio);
        }
        this.xsizeform = this.xsizethumb + this.TotalBorderThicknessX();
        this.ysizeform = this.ysizethumb + this.TotalBorderThicknessY();
        this.ResizeFormP(this.xsizeform, this.ysizeform);
        if (!this.isbrowsermode)
        {
          props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
          ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
        }
        this.storedwindowlocationx = this.Location.X;
        this.storedwindowlocationy = this.Location.Y;
        this.PreviousXSizeThumb = this.xsizethumb;
        this.PreviousYSizeThumb = this.ysizethumb;
        size2 = this.image.Size;
        this.PreviousXImageSize = size2.Width;
        size2 = this.image.Size;
        this.PreviousYImageSize = size2.Height;
        size2 = this.Size;
        this.PreviousXFormSize = size2.Width;
        size2 = this.Size;
        this.PreviousYFormSize = size2.Height;
        this.Location = new Point(0, 0);
      }
    }

    private void Form1_Resize(object sender, EventArgs e)
    {
      if (!this.controlkeyisdown)
        return;
      this.FindFrameBorderSize(ref this.FrameBorderSizeWidth, ref this.FrameBorderSizeHeight);
      PSIZE size1;
      ThumbnailWindow.DwmQueryThumbnailSourceSize(this.thumb, out size1);
      DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
      props.fVisible = true;
      props.dwFlags = ThumbnailWindow.DWM_TNP_VISIBLE | ThumbnailWindow.DWM_TNP_RECTDESTINATION | ThumbnailWindow.DWM_TNP_OPACITY | ThumbnailWindow.DWM_TNP_RECTSOURCE;
      props.opacity = (byte) ((int) byte.MaxValue - UserSettings.Default.ThumbTransparency);
      props.rcSource = new Rect(-this.FrameBorderSizeWidth, -this.FrameBorderSizeHeight, size1.x + this.FrameBorderSizeWidth, size1.y + this.FrameBorderSizeHeight);
      this.currentwindow = this.thumb.ToString();
      Point location;
      Size size2;
      if (this.Location.X > this.storedwindowlocationx)
      {
        int num1 = this.cropleftoffset;
        location = this.Location;
        int num2 = location.X - this.storedwindowlocationx;
        this.cropleftoffset = num1 - num2;
        int num3 = (int) (double) this.xsizethumb;
        location = this.Location;
        int num4 = location.X - this.storedwindowlocationx;
        this.xsizethumb = num3 - num4;
        this.croppedratioxleft = (double) this.cropleftoffset / (double) this.xsizethumb;
      }
      else if (this.Location.X < this.storedwindowlocationx)
      {
        int num1 = this.cropleftoffset;
        location = this.Location;
        int num2 = location.X - this.storedwindowlocationx;
        this.cropleftoffset = num1 - num2;
        int num3 = (int) (double) this.xsizethumb;
        location = this.Location;
        int num4 = location.X - this.storedwindowlocationx;
        this.xsizethumb = num3 - num4;
        this.croppedratioxleft = (double) this.cropleftoffset / (double) this.xsizethumb;
      }
      else if (this.Size.Width > this.PreviousXFormSize)
      {
        int num1 = this.croprightoffset;
        size2 = this.Size;
        int num2 = size2.Width - this.PreviousXFormSize;
        this.croprightoffset = num1 - num2;
        int num3 = (int) (double) this.xsizethumb;
        size2 = this.Size;
        int num4 = size2.Width - this.PreviousXFormSize;
        this.xsizethumb = num3 + num4;
        this.croppedratioxright = (double) this.croprightoffset / (double) this.xsizethumb;
      }
      else if (this.Size.Width < this.PreviousXFormSize)
      {
        int num1 = this.croprightoffset;
        size2 = this.Size;
        int num2 = size2.Width - this.PreviousXFormSize;
        this.croprightoffset = num1 - num2;
        int num3 = (int) (double) this.xsizethumb;
        size2 = this.Size;
        int num4 = size2.Width - this.PreviousXFormSize;
        this.xsizethumb = num3 + num4;
        this.croppedratioxright = (double) this.croprightoffset / (double) this.xsizethumb;
      }
      location = this.Location;
      if (location.Y > this.storedwindowlocationy)
      {
        int num1 = this.croptopoffset;
        location = this.Location;
        int num2 = location.Y - this.storedwindowlocationy;
        this.croptopoffset = num1 - num2;
        int num3 = (int) (double) this.ysizethumb;
        location = this.Location;
        int num4 = location.Y - this.storedwindowlocationy;
        this.ysizethumb = num3 - num4;
        this.croppedratioytop = (double) this.croptopoffset / (double) this.ysizethumb;
      }
      else
      {
        location = this.Location;
        if (location.Y < this.storedwindowlocationy)
        {
          int num1 = this.croptopoffset;
          location = this.Location;
          int num2 = location.Y - this.storedwindowlocationy;
          this.croptopoffset = num1 - num2;
          int num3 = (int) (double) this.ysizethumb;
          location = this.Location;
          int num4 = location.Y - this.storedwindowlocationy;
          this.ysizethumb = num3 - num4;
          this.croppedratioytop = (double) this.croptopoffset / (double) this.ysizethumb;
        }
        else
        {
          size2 = this.Size;
          if (size2.Height > this.PreviousYFormSize)
          {
            int num1 = this.cropbottomoffset;
            size2 = this.Size;
            int num2 = size2.Height - this.PreviousYFormSize;
            this.cropbottomoffset = num1 - num2;
            int num3 = (int) (double) this.ysizethumb;
            size2 = this.Size;
            int num4 = size2.Height - this.PreviousYFormSize;
            this.ysizethumb = num3 + num4;
            this.croppedratioybottom = (double) this.cropbottomoffset / (double) this.ysizethumb;
          }
          else
          {
            size2 = this.Size;
            if (size2.Height < this.PreviousYFormSize)
            {
              int num1 = this.cropbottomoffset;
              size2 = this.Size;
              int num2 = size2.Height - this.PreviousYFormSize;
              this.cropbottomoffset = num1 - num2;
              int num3 = (int) (double) this.ysizethumb;
              int num4 = this.PreviousYFormSize;
              size2 = this.Size;
              int height = size2.Height;
              int num5 = num4 - height;
              this.ysizethumb = num3 - num5;
              this.croppedratioybottom = (double) this.cropbottomoffset / (double) this.ysizethumb;
            }
          }
        }
      }
      this.croppedratioxleft = (double) this.cropleftoffset / (double) this.xsizethumb;
      this.croppedratioxright = (double) this.croprightoffset / (double) this.xsizethumb;
      this.croppedratioytop = (double) this.croptopoffset / (double) this.ysizethumb;
      this.croppedratioybottom = (double) this.cropbottomoffset / (double) this.ysizethumb;
      if (!this.isbrowsermode)
      {
        props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
        ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
      }
      this.aspectratio = (double) this.image.Width / (double) this.image.Height;
      location = this.Location;
      this.storedwindowlocationx = location.X;
      location = this.Location;
      this.storedwindowlocationy = location.Y;
      this.PreviousXSizeThumb = this.xsizethumb;
      this.PreviousYSizeThumb = this.ysizethumb;
      size2 = this.image.Size;
      this.PreviousXImageSize = size2.Width;
      size2 = this.image.Size;
      this.PreviousYImageSize = size2.Height;
      size2 = this.Size;
      this.PreviousXFormSize = size2.Width;
      size2 = this.Size;
      this.PreviousYFormSize = size2.Height;
    }

    public void ResizeFormP(int NewX, int NewY)
    {
      if (this.triploop || this.minimizing || this.isbrowsermode || this.initializeparam == 1)
        Console.WriteLine("trip loop");
      if (this.controlkeyisdown)
        return;
      this.FindFrameBorderSize(ref this.FrameBorderSizeWidth, ref this.FrameBorderSizeHeight);
      PSIZE size1;
      ThumbnailWindow.DwmQueryThumbnailSourceSize(this.thumb, out size1);
      DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
      props.fVisible = true;
      props.dwFlags = ThumbnailWindow.DWM_TNP_VISIBLE | ThumbnailWindow.DWM_TNP_RECTDESTINATION | ThumbnailWindow.DWM_TNP_OPACITY | ThumbnailWindow.DWM_TNP_RECTSOURCE;
      props.opacity = (byte) ((int) byte.MaxValue - UserSettings.Default.ThumbTransparency);
      props.rcSource = new Rect(-this.FrameBorderSizeWidth, -this.FrameBorderSizeHeight, size1.x + this.FrameBorderSizeWidth, size1.y + this.FrameBorderSizeHeight);
      this.currentwindow = this.thumb.ToString();
      this.xsizethumb = (int) ((double) NewY * this.aspectratio);
      this.ysizethumb = (int) (double) NewY;
      this.cropleftoffset = (int) ((double) this.xsizethumb * this.croppedratioxleft);
      this.croptopoffset = (int) ((double) this.ysizethumb * this.croppedratioytop);
      this.croprightoffset = (int) ((double) this.xsizethumb * this.croppedratioxright);
      this.cropbottomoffset = (int) ((double) this.ysizethumb * this.croppedratioybottom);
      if (!this.isbrowsermode)
      {
        props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
        ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
      }
      int width = this.xsizethumb + this.TotalBorderThicknessX();
      int height = this.ysizethumb + this.TotalBorderThicknessY();
      Size size2 = this.Size;
      int num;
      if (size2.Width == width)
      {
        size2 = this.Size;
        num = size2.Height == height ? 1 : 0;
      }
      else
        num = 0;
      if (num == 0)
        this.Size = new Size(width, height);
      this.storedwindowlocationx = this.Location.X;
      this.storedwindowlocationy = this.Location.Y;
      this.PreviousXSizeThumb = this.xsizethumb;
      this.PreviousYSizeThumb = this.ysizethumb;
      size2 = this.image.Size;
      this.PreviousXImageSize = size2.Width;
      size2 = this.image.Size;
      this.PreviousYImageSize = size2.Height;
      size2 = this.Size;
      this.PreviousXFormSize = size2.Width;
      size2 = this.Size;
      this.PreviousYFormSize = size2.Height;
    }

    public int TotalBorderThicknessX()
    {
      if (this.FormBorderStyle.ToString() == "None")
        return 0;
      return 2 * SystemInformation.FrameBorderSize.Width;
    }

    public int TotalBorderThicknessY()
    {
      if (this.FormBorderStyle.ToString() == "None")
        return 0;
      return 2 * SystemInformation.FrameBorderSize.Height;
    }

    public void ChangeTransparency(int thumbtransparencyvalue, int backtransparencyvalue)
    {
      this.FindFrameBorderSize(ref this.FrameBorderSizeWidth, ref this.FrameBorderSizeHeight);
      PSIZE size;
      ThumbnailWindow.DwmQueryThumbnailSourceSize(this.thumb, out size);
      DWM_THUMBNAIL_PROPERTIES props = new DWM_THUMBNAIL_PROPERTIES();
      props.fVisible = true;
      props.dwFlags = ThumbnailWindow.DWM_TNP_VISIBLE | ThumbnailWindow.DWM_TNP_RECTDESTINATION | ThumbnailWindow.DWM_TNP_OPACITY | ThumbnailWindow.DWM_TNP_RECTSOURCE;
      props.opacity = (byte) ((int) byte.MaxValue - thumbtransparencyvalue);
      props.rcSource = new Rect(-this.FrameBorderSizeWidth, -this.FrameBorderSizeHeight, size.x + this.FrameBorderSizeWidth, size.y + this.FrameBorderSizeHeight);
      ThumbnailWindow.SetLayeredWindowAttributes(this.Handle, (byte) 0, (byte) ((int) byte.MaxValue - backtransparencyvalue), 2);
      props.rcDestination = new Rect(this.cropleftoffset, this.croptopoffset, this.xsizethumb + this.croprightoffset, this.ysizethumb + this.cropbottomoffset);
      ThumbnailWindow.DwmUpdateThumbnailProperties(this.thumb, ref props);
    }

    public void ToggleBackTitles(bool TurnOn)
    {
      if (TurnOn)
      {
        this.TitleLabel.Visible = true;
        this.AuthorLabel.Visible = true;
      }
      else
      {
        this.TitleLabel.Visible = false;
        this.AuthorLabel.Visible = false;
      }
    }

    private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
    {
      this.contextMenuStrip1.Items.Clear();
      this.contextMenuStrip1.Items.Add((ToolStripItem) this.showTargetWindowToolStripMenuItem);
      this.contextMenuStrip1.Items.Add((ToolStripItem) this.hideTargetWindowToolStripMenuItem);
      this.contextMenuStrip1.Items.Add((ToolStripItem) this.minimizeToolStripMenuItem);
      this.contextMenuStrip1.Items.Add((ToolStripItem) this.alwaysOnTopNoMoveToolStripMenuItem);
      this.alwaysOnTopNoMoveToolStripMenuItem.Checked = (bool) this.qparent.AlwaysOnTop[this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle)];
      this.contextMenuStrip1.Visible = true;
      this.contextMenuStrip1.Show();
      this.contextMenuStrip1.BringToFront();
    }

    public void alwaysOnTopNoMoveToolStripMenuItem_Click(object sender, EventArgs e)
    {
      int index = this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle);
      if (index < 0)
        return;
      if (this.alwaysOnTopNoMoveToolStripMenuItem.Checked)
      {
        this.qparent.AlwaysOnTop[index] = (object) true;
        this.qparent.NoMove[index] = (object) true;
        ThumbnailWindow thumbnailWindow = (ThumbnailWindow) this.qparent.PreviewControls[index];
        thumbnailWindow.TopMost = true;
        thumbnailWindow.BringToFront();
      }
      else
      {
        this.qparent.AlwaysOnTop[index] = (object) false;
        this.qparent.NoMove[index] = (object) false;
        ThumbnailWindow thumbnailWindow = (ThumbnailWindow) this.qparent.PreviewControls[index];
        if (UserSettings.Default.PinToTop)
        {
          thumbnailWindow.TopMost = true;
          thumbnailWindow.BringToFront();
        }
        else if (UserSettings.Default.PinToNormal)
          thumbnailWindow.TopMost = false;
        else if (UserSettings.Default.PinToBottom)
        {
          thumbnailWindow.TopMost = false;
          thumbnailWindow.SendToBack();
        }
      }
    }

    public void SetAlwaysTopNoMove()
    {
      this.alwaysOnTopNoMoveToolStripMenuItem.Checked = true;
      this.alwaysOnTopNoMoveToolStripMenuItem_Click((object) this, (EventArgs) null);
    }

    private void quitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    public void minimizeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ThumbnailWindow.SetForegroundWindow(ThumbnailWindow.GetDesktopWindow());
      this.isProcessing = true;
      this.Hide();
      this.hideTargetWindowToolStripMenuItem_Click((object) this, (EventArgs) null);
      this.isProcessing = false;
    }

    private void closeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      if (!(e.ClickedItem.Text != "Close") || !(e.ClickedItem.Text != "Quit") || (!(e.ClickedItem.Text != "Minimize") || !(e.ClickedItem.Text != "Settings")) || (!(e.ClickedItem.Text != "Learn...") || !(e.ClickedItem.Text != "Hide Target Window") || !(e.ClickedItem.Text != "Show Target Window")) || e.ClickedItem == this.alwaysOnTopNoMoveToolStripMenuItem)
        return;
      if (this.thumb != IntPtr.Zero)
        ThumbnailWindow.DwmUnregisterThumbnail(this.thumb);
      this.currentwindowname = e.ClickedItem.Text;
      this.userwindowindexnum = int.Parse(this.currentwindowname.Substring(0, this.currentwindowname.IndexOf('.')));
      int num = ThumbnailWindow.DwmRegisterThumbnail(this.Handle, this.windows[this.userwindowindexnum - 1].Handle, out this.thumb);
      ThumbnailWindow.MoveWindow(this.currentwindowhandle, this.winfo2.rcWindow.left, this.winfo2.rcWindow.top, this.winfo2.rcWindow.right - this.winfo2.rcWindow.left, this.winfo2.rcWindow.bottom - this.winfo2.rcWindow.top, true);
      this.currentwindowhandle = this.windows[this.userwindowindexnum - 1].Handle;
      ThumbnailWindow.GetWindowInfo(this.currentwindowhandle, ref this.winfo);
      this.winfo2 = this.winfo;
      if (this.winfo.rcWindow.right < -30000)
      {
        ThumbnailWindow.SendMessage(this.currentwindowhandle, 274, 61728, 0);
        ThumbnailWindow.GetWindowInfo(this.currentwindowhandle, ref this.winfo);
        this.winfo2 = this.winfo;
      }
      PSIZE size;
      ThumbnailWindow.DwmQueryThumbnailSourceSize(this.thumb, out size);
      this.currentwindowsize.x = size.x;
      this.currentwindowsize.y = size.y;
      if (num == 0)
      {
        this.SetDefaultView();
        if (UserSettings.Default.MoveTargetOffscreen)
          ThumbnailWindow.MoveWindow(this.currentwindowhandle, SystemInformation.PrimaryMonitorSize.Width, 0, this.winfo.rcWindow.right - this.winfo.rcWindow.left, this.winfo.rcWindow.bottom - this.winfo.rcWindow.top, true);
      }
    }

    private void contextMenuStrip1_MouseUp(object sender, MouseEventArgs e)
    {
    }

    private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }

    private void contextMenuStrip1_MouseLeave(object sender, EventArgs e)
    {
    }

    private void learnToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }

    [DllImport("user32.dll")]
    private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);

    [DllImport("user32")]
    private static extern uint GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

    public static int GetWindowProcessID(int hwnd)
    {
      int lpdwProcessId = 1;
      int num = (int) ThumbnailWindow.GetWindowThreadProcessId(hwnd, out lpdwProcessId);
      return lpdwProcessId;
    }

    public void hideTargetWindowToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.isProcessing = true;
      this.qparent.ToggleShowTaskbarTab(this.currentwindowhandle, false);
      ThumbnailWindow.SetWindowLong(this.currentwindowhandle, -20, this.winLong | 524288L | 134217728L | 32L);
      ThumbnailWindow.SetLayeredWindowAttributes(this.currentwindowhandle, (byte) 0, (byte) 0, 2);
      bool flag1;
      if (ThumbnailWindow.IsIconic(this.currentwindowhandle))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        ThumbnailWindow.ShowWindow(this.currentwindowhandle, 9);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      StringBuilder lpString = new StringBuilder(100);
      ThumbnailWindow.GetWindowText(this.currentwindowhandle, lpString, lpString.Capacity);
      if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(ThumbnailWindow.GetWindowProcessID((int) this.currentwindowhandle)).ProcessName.Equals("Photoshop"))
      {
        bool flag2 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag2 = true;
        }
        this.placement = new ThumbnailWindow.WINDOWPLACEMENT();
        this.placement.length = Marshal.SizeOf((object) this.placement);
        ThumbnailWindow.GetWindowPlacement(this.currentwindowhandle, ref this.placement);
        this.placement.showCmd = this.placement.flags != 2 ? 1 : 3;
        ThumbnailWindow.SetWindowPos(this.currentwindowhandle, ThumbnailWindow.HWND_BOTTOM, SystemInformation.PrimaryMonitorSize.Width - 1, SystemInformation.PrimaryMonitorSize.Height - 1, 0, 0, 17U);
        if (flag2)
        {
          XPAppearance.MinAnimate = true;
          flag1 = false;
        }
      }
      this.qparent.HideTaskbarTab();
      this.TargetShown = false;
      this.isProcessing = false;
    }

    public void showTargetWindowToolStripMenuItem_Click(object sender, EventArgs e)
    {
      bool flag1 = false;
      this.GetWindows();
      foreach (Window window in this.windows)
      {
        if (window.Handle == this.currentwindowhandle)
        {
          flag1 = true;
          break;
        }
      }
      if (!flag1)
        return;
      this.isProcessing = true;
      if (this.TargetShown)
      {
        ThumbnailWindow.SetForegroundWindow((int) this.currentwindowhandle);
      }
      else
      {
        bool flag2;
        if (ThumbnailWindow.IsIconic(this.currentwindowhandle))
        {
          bool flag3 = false;
          if (XPAppearance.MinAnimate)
          {
            XPAppearance.MinAnimate = false;
            flag3 = true;
          }
          ThumbnailWindow.ShowWindow(this.currentwindowhandle, 9);
          if (flag3)
          {
            XPAppearance.MinAnimate = true;
            flag2 = false;
          }
        }
        StringBuilder lpString = new StringBuilder(100);
        ThumbnailWindow.GetWindowText(this.currentwindowhandle, lpString, lpString.Capacity);
        if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(ThumbnailWindow.GetWindowProcessID((int) this.currentwindowhandle)).ProcessName.Equals("Photoshop"))
        {
          bool flag3 = false;
          if (XPAppearance.MinAnimate)
          {
            XPAppearance.MinAnimate = false;
            flag3 = true;
          }
          ThumbnailWindow.SetWindowPos(this.currentwindowhandle, IntPtr.Zero, 0, 0, 0, 0, 19U);
          ThumbnailWindow.SetWindowPlacement(this.currentwindowhandle, ref this.placement);
          if (flag3)
          {
            XPAppearance.MinAnimate = true;
            flag2 = false;
          }
        }
        ThumbnailWindow.SetWindowLong(this.currentwindowhandle, -20, this.winLong);
        this.qparent.ToggleShowTaskbarTab(this.currentwindowhandle, true);
        this.qparent.ShowTaskbarTab(this.currentwindowhandle);
        ThumbnailWindow.SetForegroundWindow((int) this.currentwindowhandle);
        this.TargetShown = true;
        this.isProcessing = false;
      }
    }

    public void showTargetWindowMinimized(object sender, EventArgs e)
    {
      bool flag1 = false;
      this.GetWindows();
      foreach (Window window in this.windows)
      {
        if (window.Handle == this.currentwindowhandle)
        {
          flag1 = true;
          break;
        }
      }
      if (!flag1)
        return;
      this.isProcessing = true;
      if (this.TargetShown)
        return;
      StringBuilder lpString = new StringBuilder(100);
      ThumbnailWindow.GetWindowText(this.currentwindowhandle, lpString, lpString.Capacity);
      bool flag2;
      if (lpString.ToString().Contains("Adobe Photoshop") && Process.GetProcessById(ThumbnailWindow.GetWindowProcessID((int) this.currentwindowhandle)).ProcessName.Equals("Photoshop"))
      {
        bool flag3 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag3 = true;
        }
        ThumbnailWindow.SetWindowPos(this.currentwindowhandle, IntPtr.Zero, 0, 0, 0, 0, 19U);
        ThumbnailWindow.SetWindowPlacement(this.currentwindowhandle, ref this.placement);
        if (flag3)
        {
          XPAppearance.MinAnimate = true;
          flag2 = false;
        }
      }
      this.qparent.ToggleShowTaskbarTab(this.currentwindowhandle, true);
      this.qparent.ShowTaskbarTab(this.currentwindowhandle);
      ThumbnailWindow.SetWindowLong(this.currentwindowhandle, -20, this.winLong);
      if (!ThumbnailWindow.IsIconic(this.currentwindowhandle))
      {
        bool flag3 = false;
        if (XPAppearance.MinAnimate)
        {
          XPAppearance.MinAnimate = false;
          flag3 = true;
        }
        ThumbnailWindow.ShowWindow(this.currentwindowhandle, 7);
        if (flag3)
        {
          XPAppearance.MinAnimate = true;
          flag2 = false;
        }
      }
      this.TargetShown = true;
      this.isProcessing = false;
    }

    public bool GetProcessingState()
    {
      return this.isProcessing;
    }

    public bool GetShownState()
    {
      return this.TargetShown;
    }

    [DllImport("user32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool EnumChildWindows(IntPtr window, ThumbnailWindow.EnumWindowProc callback, IntPtr i);

    public static List<IntPtr> GetChildWindows(IntPtr parent)
    {
      List<IntPtr> list = new List<IntPtr>();
      GCHandle gcHandle = GCHandle.Alloc((object) list);
      try
      {
        ThumbnailWindow.EnumWindowProc callback = new ThumbnailWindow.EnumWindowProc(ThumbnailWindow.EnumWindow);
        ThumbnailWindow.EnumChildWindows(parent, callback, GCHandle.ToIntPtr(gcHandle));
      }
      finally
      {
        if (gcHandle.IsAllocated)
          gcHandle.Free();
      }
      return list;
    }

    private static bool EnumWindow(IntPtr handle, IntPtr pointer)
    {
      List<IntPtr> list = GCHandle.FromIntPtr(pointer).Target as List<IntPtr>;
      if (list == null)
        throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
      list.Add(handle);
      return true;
    }

    private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      this.click_timer.Stop();
      this.click_timer.Enabled = false;
      this.Show();
      this.WindowState = FormWindowState.Normal;
      this.minimizing = false;
      this.SizeWindow(2);
      try
      {
        this.notifyIcon1.Visible = false;
      }
      catch (Exception ex)
      {
        Debug.WriteLine("NotifyIcon already disposed");
      }
      this.Position_Screen();
      this.restorewindow = false;
    }

    private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Left)
        return;
      this.click_timer.Start();
    }

    private void click_timer_Tick(object sender, EventArgs e)
    {
      this.click_timer.Stop();
      this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
      ThumbnailWindow.INPUT[] pInputs = new ThumbnailWindow.INPUT[2];
      ThumbnailWindow.MOUSEINPUT mouseinput1 = new ThumbnailWindow.MOUSEINPUT();
      mouseinput1.dwFlags = 32776U;
      mouseinput1.dx = 0;
      mouseinput1.dy = 0;
      mouseinput1.time = 0U;
      mouseinput1.mouseData = 0U;
      ThumbnailWindow.MOUSEINPUT mouseinput2 = new ThumbnailWindow.MOUSEINPUT();
      mouseinput2.dwFlags = 32784U;
      mouseinput2.dx = 0;
      mouseinput2.dy = 0;
      mouseinput2.time = 0U;
      mouseinput2.mouseData = 0U;
      pInputs[0] = new ThumbnailWindow.INPUT();
      pInputs[0].type = 0;
      pInputs[0].mi = mouseinput1;
      pInputs[1] = new ThumbnailWindow.INPUT();
      pInputs[1].type = 0;
      pInputs[1].mi = mouseinput2;
      int num = (int) ThumbnailWindow.Win32.SendInput(2U, pInputs, Marshal.SizeOf((object) pInputs[0]));
    }

    private void image_MouseDown(object sender, MouseEventArgs e)
    {
      this.image.Focus();
      if (this.inRegion && e.Button == MouseButtons.Left && !this.controlkeyisdown)
      {
        this.clicked = true;
        if (e.Button != MouseButtons.Left)
          return;
        int x1 = e.X;
        Point location = this.Location;
        int x2 = location.X;
        int x3 = x1 + x2;
        int y1 = e.Y;
        location = this.Location;
        int y2 = location.Y;
        int y3 = y1 + y2;
        this.ptOffset = new Point(x3, y3);
      }
      else
      {
        if (this.controlkeyisdown || e.Button != MouseButtons.Left || e.Button != MouseButtons.Left)
          return;
        int x;
        int y;
        if (this.FormBorderStyle == FormBorderStyle.Sizable)
        {
          x = -e.X - SystemInformation.FrameBorderSize.Width;
          y = -e.Y - SystemInformation.FrameBorderSize.Height;
        }
        else
        {
          x = -e.X;
          y = -e.Y;
        }
        this.mouseOffset = new Point(x, y);
        this.isMouseDown = true;
        this.panoffsetx1 = this.panoffsetx2;
        this.panoffsetx2 = x;
        this.panoffsety1 = this.panoffsety2;
        this.panoffsety2 = y;
      }
    }

    private void image_MouseMove(object sender, MouseEventArgs e)
    {
      if (!this.controlkeyisdown)
      {
        if (e.X <= this.West)
          this.inW = true;
        if (e.X >= this.East)
          this.inE = true;
        if (e.Y <= this.North)
          this.inN = true;
        if (e.Y >= this.South)
          this.inS = true;
        if (!this.clicked)
        {
          if (this.inN && this.inE)
          {
            this.Cursor = Cursors.PanNE;
            this.inRegion = true;
          }
          else if (this.inN && this.inW)
          {
            this.Cursor = Cursors.PanNW;
            this.inRegion = true;
          }
          else if (this.inS && this.inE)
          {
            this.Cursor = Cursors.PanSE;
            this.inRegion = true;
          }
          else if (this.inS && this.inW)
          {
            this.Cursor = Cursors.PanSW;
            this.inRegion = true;
          }
          else if (this.inW)
          {
            this.Cursor = Cursors.PanWest;
            this.inRegion = true;
          }
          else if (this.inE)
          {
            this.Cursor = Cursors.PanEast;
            this.inRegion = true;
          }
          else if (this.inN)
          {
            this.Cursor = Cursors.PanNorth;
            this.inRegion = true;
          }
          else if (this.inS)
          {
            this.Cursor = Cursors.PanSouth;
            this.inRegion = true;
          }
          else if (!this.inN && !this.inS && !this.inE && !this.inW)
          {
            this.Cursor = Cursors.Default;
            this.inRegion = false;
          }
        }
      }
      if (this.clicked && this.inRegion && !this.controlkeyisdown)
      {
        this.PreviousSize = this.Size;
        this.LockedPoint = this.Location;
        this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle);
        Point point;
        if (this.inN && this.inE)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int num1 = point.Y - this.ptOffset.Y;
          point = Control.MousePosition;
          int num2 = point.X - this.ptOffset.X;
          int num3 = num1 - num2;
          int NewY = height - num3;
          this.ResizeFormP(width, NewY);
          point = this.Location;
          int x = point.X;
          int y1 = this.LockedPoint.Y;
          size = this.Size;
          int num4 = size.Height - this.PreviousSize.Height;
          int y2 = y1 - num4;
          this.Location = new Point(x, y2);
        }
        else if (this.inN && this.inW)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int num1 = point.Y - this.ptOffset.Y;
          int num2 = height - num1;
          point = Control.MousePosition;
          int num3 = point.X - this.ptOffset.X;
          int NewY = num2 - num3;
          this.ResizeFormP(width, NewY);
          int x1 = this.LockedPoint.X;
          size = this.Size;
          int num4 = size.Width - this.PreviousSize.Width;
          int x2 = x1 - num4;
          int y1 = this.LockedPoint.Y;
          size = this.Size;
          int num5 = size.Height - this.PreviousSize.Height;
          int y2 = y1 - num5;
          this.Location = new Point(x2, y2);
        }
        else if (this.inS && this.inE)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int num1 = point.Y - this.ptOffset.Y;
          int num2 = height + num1;
          point = Control.MousePosition;
          int num3 = point.X - this.ptOffset.X;
          int NewY = num2 + num3;
          this.ResizeFormP(width, NewY);
        }
        else if (this.inS && this.inW)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int num1 = point.Y - this.ptOffset.Y;
          int num2 = height + num1;
          point = Control.MousePosition;
          int num3 = point.X - this.ptOffset.X;
          int NewY = num2 - num3;
          this.ResizeFormP(width, NewY);
          int x1 = this.LockedPoint.X;
          size = this.Size;
          int num4 = size.Width - this.PreviousSize.Width;
          int x2 = x1 - num4;
          point = this.Location;
          int y = point.Y;
          this.Location = new Point(x2, y);
        }
        else if (this.inW)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int num1 = point.X - this.ptOffset.X;
          int NewY = height - num1;
          this.ResizeFormP(width, NewY);
          int x1 = this.LockedPoint.X;
          size = this.Size;
          int num2 = size.Width - this.PreviousSize.Width;
          int x2 = x1 - num2;
          int y1 = this.LockedPoint.Y;
          size = this.Size;
          int num3 = (size.Height - this.PreviousSize.Height) / 2;
          int y2 = y1 - num3;
          this.Location = new Point(x2, y2);
        }
        else if (this.inE)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int x1 = point.X;
          int NewY = height + x1 - this.ptOffset.X;
          this.ResizeFormP(width, NewY);
          point = this.Location;
          int x2 = point.X;
          int y1 = this.LockedPoint.Y;
          size = this.Size;
          int num = (size.Height - this.PreviousSize.Height) / 2;
          int y2 = y1 - num;
          this.Location = new Point(x2, y2);
        }
        else if (this.inN)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int num1 = point.Y - this.ptOffset.Y;
          int NewY = height - num1;
          this.ResizeFormP(width, NewY);
          int x1 = this.LockedPoint.X;
          size = this.Size;
          int num2 = (size.Width - this.PreviousSize.Width) / 2;
          int x2 = x1 - num2;
          int y1 = this.LockedPoint.Y;
          size = this.Size;
          int num3 = size.Height - this.PreviousSize.Height;
          int y2 = y1 - num3;
          this.Location = new Point(x2, y2);
        }
        else if (this.inS)
        {
          Size size = this.Size;
          int width = size.Width;
          size = this.Size;
          int height = size.Height;
          point = Control.MousePosition;
          int y1 = point.Y;
          int NewY = height + y1 - this.ptOffset.Y;
          this.ResizeFormP(width, NewY);
          int x1 = this.LockedPoint.X;
          size = this.Size;
          int num = (size.Width - this.PreviousSize.Width) / 2;
          int x2 = x1 - num;
          point = this.Location;
          int y2 = point.Y;
          this.Location = new Point(x2, y2);
        }
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        Point& local1 = @this.ptOffset;
        point = Control.MousePosition;
        int x3 = point.X;
        // ISSUE: explicit reference operation
        (^local1).X = x3;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        Point& local2 = @this.ptOffset;
        point = Control.MousePosition;
        int y3 = point.Y;
        // ISSUE: explicit reference operation
        (^local2).Y = y3;
      }
      else if (this.isMouseDown && !this.controlkeyisdown)
      {
        Point mousePosition = Control.MousePosition;
        mousePosition.Offset(this.mouseOffset.X, this.mouseOffset.Y);
        this.Location = mousePosition;
      }
      else
      {
        this.inN = false;
        this.inS = false;
        this.inE = false;
        this.inW = false;
      }
    }

    private void image_MouseUp(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left)
      {
        this.isMouseDown = false;
        this.clicked = false;
        this.inRegion = false;
        this.Cursor = Cursors.Default;
      }
      if (this.Moved && UserSettings.Default.PreviewGridAlign == "Snap" && !(bool) this.qparent.AlwaysOnTop[this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle)])
      {
        Point p;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        Point& local = @p;
        Point mousePosition = Control.MousePosition;
        int x = mousePosition.X;
        mousePosition = Control.MousePosition;
        int y = mousePosition.Y;
        // ISSUE: explicit reference operation
        ^local = new Point(x, y);
        this.qparent.CheckAndInsert(this.Handle, p);
      }
      this.Moved = false;
    }

    private void image_MouseClick(object sender, MouseEventArgs e)
    {
      this.image.Focus();
    }

    private void image_DoubleClick(object sender, EventArgs e)
    {
      if (this.zoomin())
      {
        this.resizeratio = UserSettings.Default.ZoomScalingFactor * UserSettings.Default.ZoomClickSensitivity;
        this.SizeWindow(3);
        this.Position_Screen();
      }
      else if (this.zoomout())
      {
        this.resizeratio = 1.0 / (UserSettings.Default.ZoomScalingFactor * UserSettings.Default.ZoomClickSensitivity);
        this.SizeWindow(3);
        this.Position_Screen();
      }
      else if (this.defaultviewbuttonclicked())
        this.SetDefaultView();
      else
        this.Close();
    }

    private void ThumbnailWindow_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.isProcessing = true;
      this.SavePreviewCacheParams();
      if (this.thumb != IntPtr.Zero)
        ThumbnailWindow.DwmUnregisterThumbnail(this.thumb);
      try
      {
        if (this.qparent.quitting)
          this.showTargetWindowMinimized((object) this, (EventArgs) null);
        else
          this.showTargetWindowToolStripMenuItem_Click((object) this, (EventArgs) null);
      }
      catch (Exception ex)
      {
      }
      try
      {
        this.notifyIcon1.Visible = false;
      }
      catch (Exception ex)
      {
        Debug.WriteLine("NotifyIcon already disposed");
      }
      this.isProcessing = false;
    }

    private void image_Click(object sender, EventArgs e)
    {
    }

    private void image_MouseEnter(object sender, EventArgs e)
    {
    }

    private void image_MouseLeave(object sender, EventArgs e)
    {
      if (this.controlkeyisdown)
        return;
      this.Position_Screen();
      int index = this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle);
      if (UserSettings.Default.PreviewGridAlign == "Snap")
        this.qparent.ReAlign();
      if (!(bool) this.qparent.AlwaysOnTop[index] && !this.contextMenuStrip1.Visible && (!UserSettings.Default.PinToTop && !UserSettings.Default.PinToNormal) && UserSettings.Default.PinToBottom)
        this.SendToBack();
      if (!this.contextMenuStrip1.Visible)
        ThumbnailWindow.SetForegroundWindow(ThumbnailWindow.GetDesktopWindow());
    }

    private void image_MouseHover(object sender, EventArgs e)
    {
      if (this.contextMenuStrip1.Visible)
        return;
      this.Activate();
      this.image.Focus();
      this.image.BringToFront();
    }

    public bool zoomin()
    {
      Point point1 = Control.MousePosition;
      int x1 = point1.X;
      point1 = this.Location;
      int num1 = point1.X + (int) ((1.0 - UserSettings.Default.ZoomButtonsSizeFactorX) * (double) this.image.Size.Width);
      int num2;
      if (x1 >= num1)
      {
        Point point2 = Control.MousePosition;
        int x2 = point2.X;
        point2 = this.Location;
        int num3 = point2.X + this.Size.Width;
        if (x2 <= num3)
        {
          point2 = Control.MousePosition;
          int y1 = point2.Y;
          point2 = this.Location;
          int num4 = point2.Y + (int) ((1.0 - UserSettings.Default.ZoomButtonsSizeFactorY) * (double) this.image.Size.Height);
          if (y1 >= num4)
          {
            point2 = Control.MousePosition;
            int y2 = point2.Y;
            point2 = this.Location;
            int num5 = point2.Y + this.Size.Height;
            num2 = y2 <= num5 ? 1 : 0;
            goto label_5;
          }
        }
      }
      num2 = 0;
label_5:
      return num2 != 0;
    }

    public bool zoomout()
    {
      Point point1 = Control.MousePosition;
      int x1 = point1.X;
      point1 = this.Location;
      int x2 = point1.X;
      int num1;
      if (x1 >= x2)
      {
        Point point2 = Control.MousePosition;
        int x3 = point2.X;
        point2 = this.Location;
        int x4 = point2.X;
        Size size = this.Size;
        int width = size.Width;
        int num2 = x4 + width;
        double num3 = 1.0 - UserSettings.Default.ZoomButtonsSizeFactorX;
        size = this.image.Size;
        double num4 = (double) size.Width;
        int num5 = (int) (num3 * num4);
        int num6 = num2 - num5;
        if (x3 <= num6)
        {
          point2 = Control.MousePosition;
          int y1 = point2.Y;
          point2 = this.Location;
          int y2 = point2.Y;
          double num7 = 1.0 - UserSettings.Default.ZoomButtonsSizeFactorY;
          size = this.image.Size;
          double num8 = (double) size.Height;
          int num9 = (int) (num7 * num8);
          int num10 = y2 + num9;
          if (y1 >= num10)
          {
            point2 = Control.MousePosition;
            int y3 = point2.Y;
            point2 = this.Location;
            int y4 = point2.Y;
            size = this.Size;
            int height = size.Height;
            int num11 = y4 + height;
            num1 = y3 <= num11 ? 1 : 0;
            goto label_5;
          }
        }
      }
      num1 = 0;
label_5:
      return num1 != 0;
    }

    public bool defaultviewbuttonclicked()
    {
      Point point1 = Control.MousePosition;
      int x1 = point1.X;
      point1 = this.Location;
      int num1 = point1.X + (int) (UserSettings.Default.ZoomButtonsSizeFactorX * (double) this.image.Size.Width);
      int num2;
      if (x1 >= num1)
      {
        Point point2 = Control.MousePosition;
        int x2 = point2.X;
        point2 = this.Location;
        int num3 = point2.X + this.Size.Width - (int) (UserSettings.Default.ZoomButtonsSizeFactorX * (double) this.image.Size.Width);
        if (x2 <= num3)
        {
          point2 = Control.MousePosition;
          int y1 = point2.Y;
          point2 = this.Location;
          int num4 = point2.Y + (int) ((1.0 - UserSettings.Default.ZoomButtonsSizeFactorY) * (double) this.image.Size.Height);
          if (y1 >= num4)
          {
            point2 = Control.MousePosition;
            int y2 = point2.Y;
            point2 = this.Location;
            int num5 = point2.Y + this.Size.Height;
            num2 = y2 <= num5 ? 1 : 0;
            goto label_5;
          }
        }
      }
      num2 = 0;
label_5:
      return num2 != 0;
    }

    public void Zoom(string UpDown)
    {
      if (UpDown == "Down")
      {
        this.resizeratio = 1.0 / UserSettings.Default.MouseScrollZoomSensitivity;
        this.SizeWindow(3);
      }
      else
      {
        this.resizeratio = UserSettings.Default.MouseScrollZoomSensitivity;
        this.SizeWindow(3);
      }
    }

    public void Form1_KeyDown(object sender, KeyEventArgs e)
    {
      if (this.initializeparam == 1)
        return;
      int count = this.windows.Count;
      Size size = this.Size;
      int width1 = size.Width;
      size = this.Size;
      int height1 = size.Height;
      int index = this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle);
      switch (e.KeyCode.ToString())
      {
        case "Escape":
          this.Close();
          break;
        case "Down":
          if (this.ShiftisDown)
            this.qparent.ZoomAll("Down");
          this.resizeratio = 1.0 / UserSettings.Default.ZoomScalingFactor;
          this.SizeWindow(3);
          if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool) this.qparent.AlwaysOnTop[index])
          {
            this.qparent.ReAlign();
            break;
          }
          size = this.Size;
          int Forceoffsetx1 = -(size.Width - width1) / 2;
          size = this.Size;
          int Forceoffsety1 = -(size.Height - height1) / 2;
          this.Position_Screen_With_Offset(Forceoffsetx1, Forceoffsety1);
          break;
        case "Up":
          if (this.ShiftisDown)
            this.qparent.ZoomAll("Up");
          this.resizeratio = UserSettings.Default.ZoomScalingFactor;
          this.SizeWindow(3);
          if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool) this.qparent.AlwaysOnTop[index])
          {
            this.qparent.ReAlign();
            break;
          }
          size = this.Size;
          int Forceoffsetx2 = -(size.Width - width1) / 2;
          size = this.Size;
          int Forceoffsety2 = -(size.Height - height1) / 2;
          this.Position_Screen_With_Offset(Forceoffsetx2, Forceoffsety2);
          break;
        case "Left":
          if (!(UserSettings.Default.PreviewGridAlign == "Snap"))
            break;
          int OldIndex1 = this.qparent.PreviewControls.IndexOf((object) this);
          if (OldIndex1 - 1 < 0)
            break;
          if (OldIndex1 - 1 == 0)
          {
            Point location = ((Form) this.qparent.PreviewControls[OldIndex1 - 1]).Location;
            int x = location.X;
            location = this.Location;
            int y = location.Y;
            this.Location = new Point(x, y);
          }
          else if (OldIndex1 == 0)
          {
            ThumbnailWindow thumbnailWindow1 = (ThumbnailWindow) this.qparent.PreviewControls[OldIndex1 + 1];
            ThumbnailWindow thumbnailWindow2 = thumbnailWindow1;
            Point location = this.Location;
            int x = location.X;
            location = thumbnailWindow1.Location;
            int y = location.Y;
            Point point = new Point(x, y);
            thumbnailWindow2.Location = point;
          }
          this.qparent.SwapAndRemove(OldIndex1 - 1, OldIndex1);
          this.qparent.ReAlign();
          break;
        case "Right":
          if (!(UserSettings.Default.PreviewGridAlign == "Snap"))
            break;
          int OldIndex2 = this.qparent.PreviewControls.IndexOf((object) this);
          if (OldIndex2 + 1 >= this.qparent.PreviewControls.Count)
            break;
          if (OldIndex2 == 0)
          {
            ThumbnailWindow thumbnailWindow1 = (ThumbnailWindow) this.qparent.PreviewControls[OldIndex2 + 1];
            ThumbnailWindow thumbnailWindow2 = thumbnailWindow1;
            Point location = this.Location;
            int x = location.X;
            location = thumbnailWindow1.Location;
            int y = location.Y;
            Point point = new Point(x, y);
            thumbnailWindow2.Location = point;
          }
          this.qparent.SwapAndRemove(OldIndex2 + 1, OldIndex2);
          this.qparent.ReAlign();
          break;
        case "ControlKey":
          size = this.Size;
          int width2 = size.Width;
          size = SystemInformation.MinimumWindowSize;
          int width3 = size.Width;
          if (width2 < width3)
          {
            this.controlkeyisdown = false;
            this.FormToolTip.SetToolTip((Control) this.image, "Preview is too small to crop. Please enlarge.");
            this.FormToolTip.Active = true;
            break;
          }
          size = this.Size;
          int height2 = size.Height;
          size = SystemInformation.MinimumWindowSize;
          int height3 = size.Height;
          if (height2 < height3)
          {
            this.controlkeyisdown = false;
            this.FormToolTip.SetToolTip((Control) this.image, "Preview is too small to crop. Please enlarge.");
            this.FormToolTip.Active = true;
            break;
          }
          this.triploop = true;
          if (this.FormBorderStyle.ToString() == "None")
            this.FormBorderStyle = FormBorderStyle.Sizable;
          this.triploop = false;
          this.controlkeyisdown = true;
          Point location1 = this.Location;
          this.storedwindowlocationx = location1.X;
          location1 = this.Location;
          this.storedwindowlocationy = location1.Y;
          this.PreviousXSizeThumb = this.xsizethumb;
          this.PreviousYSizeThumb = this.ysizethumb;
          size = this.image.Size;
          this.PreviousXImageSize = size.Width;
          size = this.image.Size;
          this.PreviousYImageSize = size.Height;
          size = this.Size;
          this.PreviousXFormSize = size.Width;
          size = this.Size;
          this.PreviousYFormSize = size.Height;
          break;
        case "ShiftKey":
          this.ShiftisDown = true;
          break;
      }
    }

    private IntPtr GetDesktopListViewHandle()
    {
      IntPtr num1 = IntPtr.Zero;
      IntPtr hwndParent = (IntPtr) ThumbnailWindow.FindWindow("Progman", (string) null);
      IntPtr num2 = IntPtr.Zero;
      if (hwndParent != IntPtr.Zero)
      {
        IntPtr windowEx = ThumbnailWindow.FindWindowEx(hwndParent, IntPtr.Zero, "SHELLDLL_DefView", (string) null);
        if (windowEx != IntPtr.Zero)
          num1 = ThumbnailWindow.FindWindowEx(windowEx, IntPtr.Zero, "SysListView32", (string) null);
      }
      return num1;
    }

    private IntPtr GetDesktopListViewHandle2()
    {
      IntPtr num1 = IntPtr.Zero;
      IntPtr hwndParent = (IntPtr) ThumbnailWindow.FindWindow("Progman", (string) null);
      IntPtr num2 = IntPtr.Zero;
      if (hwndParent != IntPtr.Zero)
      {
        IntPtr windowEx = ThumbnailWindow.FindWindowEx(hwndParent, IntPtr.Zero, "SHELLDLL_DefView", (string) null);
        if (windowEx != IntPtr.Zero)
          num1 = ThumbnailWindow.FindWindowEx(windowEx, IntPtr.Zero, "SysListView32", (string) null);
      }
      return hwndParent;
    }

    private void Form1_KeyUp(object sender, KeyEventArgs e)
    {
      this.FormToolTip.Active = false;
      this.controlkeyisdown = false;
      this.ShiftisDown = false;
      if (this.FormBorderStyle == FormBorderStyle.Sizable)
        this.FormBorderStyle = FormBorderStyle.None;
      Point location = this.Location;
      this.storedwindowlocationx = location.X;
      location = this.Location;
      this.storedwindowlocationy = location.Y;
      this.PreviousXSizeThumb = this.xsizethumb;
      this.PreviousYSizeThumb = this.ysizethumb;
      this.PreviousXImageSize = this.image.Size.Width;
      this.PreviousYImageSize = this.image.Size.Height;
      this.PreviousXFormSize = this.Size.Width;
      this.PreviousYFormSize = this.Size.Height;
      this.Position_Screen();
    }

    private void ThumbnailWindow_Move(object sender, EventArgs e)
    {
      this.DockinN = false;
      this.DockinS = false;
      this.DockinE = false;
      this.DockinW = false;
      this.DockinCentre = false;
      if (UserSettings.Default.FreeFormEdgeDocking || UserSettings.Default.FreeFormCentreDocking)
      {
        APPBARDATA pData = new APPBARDATA();
        int num1 = (int) ThumbnailWindow.SHAppBarMessage(5, ref pData);
        int num2 = 0;
        int num3 = 0;
        Point point;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        Point& local = @point;
        int x1 = SystemInformation.PrimaryMonitorSize.Width / 2;
        Size size = SystemInformation.PrimaryMonitorSize;
        int y1 = size.Height / 2;
        // ISSUE: explicit reference operation
        ^local = new Point(x1, y1);
        if (UserSettings.Default.CustomTaskbarMargins)
        {
          int taskbarMarginsLeft = UserSettings.Default.CustomTaskbarMarginsLeft;
          int num4 = -UserSettings.Default.CustomTaskbarMarginsBottom;
          int taskbarMarginsTop = UserSettings.Default.CustomTaskbarMarginsTop;
          int num5 = -UserSettings.Default.CustomTaskbarMarginsRight;
          Point location = this.Location;
          if (location.X <= taskbarMarginsLeft + UserSettings.Default.FreeFormEdgeDockThickness)
            this.DockinW = true;
          location = this.Location;
          int x2 = location.X;
          size = this.Size;
          int width1 = size.Width;
          int num6 = x2 + width1;
          size = SystemInformation.PrimaryMonitorSize;
          int num7 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness + num5;
          if (num6 >= num7)
            this.DockinE = true;
          location = this.Location;
          if (location.Y <= taskbarMarginsTop + UserSettings.Default.FreeFormEdgeDockThickness)
            this.DockinN = true;
          location = this.Location;
          int y2 = location.Y;
          size = this.Size;
          int height1 = size.Height;
          int num8 = y2 + height1;
          size = SystemInformation.PrimaryMonitorSize;
          int num9 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness + num2 + num4;
          if (num8 >= num9)
            this.DockinS = true;
          location = this.Location;
          int x3 = location.X;
          size = this.Size;
          int width2 = size.Width;
          int num10;
          if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
          {
            location = this.Location;
            if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
            {
              location = this.Location;
              int y3 = location.Y;
              size = this.Size;
              int height2 = size.Height;
              if (y3 + height2 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
              {
                location = this.Location;
                num10 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                goto label_15;
              }
            }
          }
          num10 = 1;
label_15:
          if (num10 == 0)
            this.DockinCentre = true;
        }
        else
        {
          int num4 = pData.rc.bottom;
          size = SystemInformation.PrimaryMonitorSize;
          int height1 = size.Height;
          int num5;
          if (num4 == height1 && pData.rc.left == 0)
          {
            int num6 = pData.rc.right;
            size = SystemInformation.PrimaryMonitorSize;
            int width = size.Width;
            num5 = num6 != width ? 1 : 0;
          }
          else
            num5 = 1;
          if (num5 == 0)
          {
            num3 = 1;
            int num6 = -(Screen.PrimaryScreen.Bounds.Bottom - pData.rc.top);
            if (this.Location.X <= UserSettings.Default.FreeFormEdgeDockThickness)
              this.DockinW = true;
            Point location = this.Location;
            int x2 = location.X;
            size = this.Size;
            int width1 = size.Width;
            int num7 = x2 + width1;
            size = SystemInformation.PrimaryMonitorSize;
            int num8 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness;
            if (num7 >= num8)
              this.DockinE = true;
            location = this.Location;
            if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness)
              this.DockinN = true;
            location = this.Location;
            int y2 = location.Y;
            size = this.Size;
            int height2 = size.Height;
            int num9 = y2 + height2;
            size = SystemInformation.PrimaryMonitorSize;
            int num10 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness + num6;
            if (num9 >= num10)
              this.DockinS = true;
            location = this.Location;
            int x3 = location.X;
            size = this.Size;
            int width2 = size.Width;
            int num11;
            if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
            {
              location = this.Location;
              if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
              {
                location = this.Location;
                int y3 = location.Y;
                size = this.Size;
                int height3 = size.Height;
                if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                {
                  location = this.Location;
                  num11 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                  goto label_34;
                }
              }
            }
            num11 = 1;
label_34:
            if (num11 == 0)
              this.DockinCentre = true;
          }
          else
          {
            int num6;
            if (pData.rc.top == 0 && pData.rc.left == 0)
            {
              int num7 = pData.rc.right;
              size = SystemInformation.PrimaryMonitorSize;
              int width = size.Width;
              num6 = num7 != width ? 1 : 0;
            }
            else
              num6 = 1;
            if (num6 == 0)
            {
              num3 = 2;
              int num7 = pData.rc.bottom;
              if (this.Location.X <= UserSettings.Default.FreeFormEdgeDockThickness)
                this.DockinW = true;
              Point location = this.Location;
              int x2 = location.X;
              size = this.Size;
              int width1 = size.Width;
              int num8 = x2 + width1;
              size = SystemInformation.PrimaryMonitorSize;
              int num9 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness;
              if (num8 >= num9)
                this.DockinE = true;
              location = this.Location;
              if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness + num7)
                this.DockinN = true;
              location = this.Location;
              int y2 = location.Y;
              size = this.Size;
              int height2 = size.Height;
              int num10 = y2 + height2;
              size = SystemInformation.PrimaryMonitorSize;
              int num11 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness;
              if (num10 >= num11)
                this.DockinS = true;
              location = this.Location;
              int x3 = location.X;
              size = this.Size;
              int width2 = size.Width;
              int num12;
              if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
              {
                location = this.Location;
                if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                {
                  location = this.Location;
                  int y3 = location.Y;
                  size = this.Size;
                  int height3 = size.Height;
                  if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                  {
                    location = this.Location;
                    num12 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                    goto label_53;
                  }
                }
              }
              num12 = 1;
label_53:
              if (num12 == 0)
                this.DockinCentre = true;
            }
            else
            {
              int num7;
              if (pData.rc.top == 0 && pData.rc.left == 0)
              {
                int num8 = pData.rc.bottom;
                size = SystemInformation.PrimaryMonitorSize;
                int height2 = size.Height;
                num7 = num8 != height2 ? 1 : 0;
              }
              else
                num7 = 1;
              if (num7 == 0)
              {
                num3 = 3;
                if (this.Location.X <= UserSettings.Default.FreeFormEdgeDockThickness + pData.rc.right)
                  this.DockinW = true;
                Point location = this.Location;
                int x2 = location.X;
                size = this.Size;
                int width1 = size.Width;
                int num8 = x2 + width1;
                size = SystemInformation.PrimaryMonitorSize;
                int num9 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness;
                if (num8 >= num9)
                  this.DockinE = true;
                location = this.Location;
                if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness)
                  this.DockinN = true;
                location = this.Location;
                int y2 = location.Y;
                size = this.Size;
                int height2 = size.Height;
                int num10 = y2 + height2;
                size = SystemInformation.PrimaryMonitorSize;
                int num11 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness;
                if (num10 >= num11)
                  this.DockinS = true;
                location = this.Location;
                int x3 = location.X;
                size = this.Size;
                int width2 = size.Width;
                int num12;
                if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
                {
                  location = this.Location;
                  if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                  {
                    location = this.Location;
                    int y3 = location.Y;
                    size = this.Size;
                    int height3 = size.Height;
                    if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                    {
                      location = this.Location;
                      num12 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                      goto label_72;
                    }
                  }
                }
                num12 = 1;
label_72:
                if (num12 == 0)
                  this.DockinCentre = true;
              }
              else
              {
                int num8;
                if (pData.rc.top == 0)
                {
                  int num9 = pData.rc.right;
                  size = SystemInformation.PrimaryMonitorSize;
                  int width = size.Width;
                  if (num9 == width)
                  {
                    int num10 = pData.rc.bottom;
                    size = SystemInformation.PrimaryMonitorSize;
                    int height2 = size.Height;
                    num8 = num10 != height2 ? 1 : 0;
                    goto label_78;
                  }
                }
                num8 = 1;
label_78:
                if (num8 == 0)
                {
                  num3 = 4;
                  int num9 = -(Screen.PrimaryScreen.Bounds.Right - pData.rc.left);
                  if (this.Location.X <= UserSettings.Default.FreeFormEdgeDockThickness)
                    this.DockinW = true;
                  Point location = this.Location;
                  int x2 = location.X;
                  size = this.Size;
                  int width1 = size.Width;
                  int num10 = x2 + width1;
                  size = SystemInformation.PrimaryMonitorSize;
                  int num11 = size.Width - UserSettings.Default.FreeFormEdgeDockThickness + num9;
                  if (num10 >= num11)
                    this.DockinE = true;
                  location = this.Location;
                  if (location.Y <= UserSettings.Default.FreeFormEdgeDockThickness)
                    this.DockinN = true;
                  location = this.Location;
                  int y2 = location.Y;
                  size = this.Size;
                  int height2 = size.Height;
                  int num12 = y2 + height2;
                  size = SystemInformation.PrimaryMonitorSize;
                  int num13 = size.Height - UserSettings.Default.FreeFormEdgeDockThickness;
                  if (num12 >= num13)
                    this.DockinS = true;
                  location = this.Location;
                  int x3 = location.X;
                  size = this.Size;
                  int width2 = size.Width;
                  int num14;
                  if (x3 + width2 >= point.X - UserSettings.Default.FreeFormCentreDockThickness)
                  {
                    location = this.Location;
                    if (location.X <= point.X + UserSettings.Default.FreeFormCentreDockThickness)
                    {
                      location = this.Location;
                      int y3 = location.Y;
                      size = this.Size;
                      int height3 = size.Height;
                      if (y3 + height3 >= point.Y - UserSettings.Default.FreeFormCentreDockThickness)
                      {
                        location = this.Location;
                        num14 = location.Y > point.Y + UserSettings.Default.FreeFormCentreDockThickness ? 1 : 0;
                        goto label_92;
                      }
                    }
                  }
                  num14 = 1;
label_92:
                  if (num14 == 0)
                    this.DockinCentre = true;
                }
              }
            }
          }
        }
      }
      if (this.qparent.FirstPreviewNotAlwaysOnTop() == this.Handle && UserSettings.Default.PreviewGridAlign == "Snap")
        this.qparent.ReAlign();
      this.Moved = true;
    }

    private void ThumbnailWindow_Leave(object sender, EventArgs e)
    {
      this.storedwindowlocationx = this.Location.X;
      this.storedwindowlocationy = this.Location.Y;
    }

    protected override void WndProc(ref Message m)
    {
      if (m.Msg == 522)
      {
        int num = (int) m.WParam <= 0 ? -1 : 1;
        if (!this.controlkeyisdown && !this.ShiftisDown)
        {
          if (num > 0)
          {
            this.resizeratio = UserSettings.Default.MouseScrollZoomSensitivity;
            int width = this.Size.Width;
            int height = this.Size.Height;
            this.SizeWindow(3);
            if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool) this.qparent.AlwaysOnTop[this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle)])
              this.qparent.ReAlign();
            else
              this.Position_Screen_With_Offset(-(this.Size.Width - width) / 2, -(this.Size.Height - height) / 2);
          }
          else
          {
            if (num >= 0)
              return;
            this.resizeratio = 1.0 / UserSettings.Default.MouseScrollZoomSensitivity;
            Size size = this.Size;
            int width = size.Width;
            size = this.Size;
            int height = size.Height;
            this.SizeWindow(3);
            if (UserSettings.Default.PreviewGridAlign == "Snap" && !(bool) this.qparent.AlwaysOnTop[this.qparent.TargettedWindows.IndexOf((object) this.currentwindowhandle)])
            {
              this.qparent.ReAlign();
            }
            else
            {
              size = this.Size;
              int Forceoffsetx = -(size.Width - width) / 2;
              size = this.Size;
              int Forceoffsety = -(size.Height - height) / 2;
              this.Position_Screen_With_Offset(Forceoffsetx, Forceoffsety);
            }
          }
        }
        else
        {
          if (!this.ShiftisDown)
            return;
          if (num < 0)
            this.qparent.ZoomAll("Up");
          else if (num > 0)
            this.qparent.ZoomAll("Down");
        }
      }
      else if (m.Msg == 274 && m.WParam == (IntPtr) 61728 && m.LParam == (IntPtr) 0)
      {
        Debug.WriteLine("Restoring preview");
        if (this.notifyIcon1.Visible)
        {
          this.Show();
          this.WindowState = FormWindowState.Normal;
          this.minimizing = false;
          this.SizeWindow(2);
          try
          {
            this.notifyIcon1.Visible = false;
          }
          catch (Exception ex)
          {
            Debug.WriteLine("NotifyIcon already disposed");
          }
          this.Position_Screen();
          this.restorewindow = false;
        }
        this.Show();
        this.WindowState = FormWindowState.Normal;
        this.BringToFront();
        this.Activate();
      }
      else
        base.WndProc(ref m);
    }

    public bool CompareTitle(string CTtitle)
    {
      return false;
    }

    private void ThumbnailWindow_Validated(object sender, EventArgs e)
    {
      this.storedwindowlocationx = this.Location.X;
      this.storedwindowlocationy = this.Location.Y;
    }

    private void ThumbnailWindow_Deactivate(object sender, EventArgs e)
    {
      this.controlkeyisdown = false;
      this.triploop = true;
      if (this.FormBorderStyle == FormBorderStyle.Sizable)
        this.FormBorderStyle = FormBorderStyle.None;
      this.triploop = false;
      Point location = this.Location;
      this.storedwindowlocationx = location.X;
      location = this.Location;
      this.storedwindowlocationy = location.Y;
      this.PreviousXSizeThumb = this.xsizethumb;
      this.PreviousYSizeThumb = this.ysizethumb;
      this.PreviousXImageSize = this.image.Size.Width;
      Size size = this.image.Size;
      this.PreviousYImageSize = size.Height;
      size = this.Size;
      this.PreviousXFormSize = size.Width;
      size = this.Size;
      this.PreviousYFormSize = size.Height;
      this.isMouseDown = false;
      this.clicked = false;
      this.inRegion = false;
      this.Cursor = Cursors.Default;
    }

    private void contextMenuStrip1_Closing(object sender, ToolStripDropDownClosingEventArgs e)
    {
      this.contextMenuStrip1.Hide();
      this.contextMenuStrip1.Visible = false;
    }

    private void ThumbnailWindow_Shown(object sender, EventArgs e)
    {
    }

    [DllImport("Gdi32.dll")]
    private static extern IntPtr CreateRoundRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, int nWidthEllipse, int nHeightEllipse);

    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern void DwmExtendFrameIntoClientArea(IntPtr hwnd, ref ThumbnailWindow.MARGINS margins);

    [DllImport("dwmapi.dll", PreserveSig = false)]
    public static extern bool DwmIsCompositionEnabled();

    public void FindFrameBorderSize(ref int width, ref int height)
    {
      if (UserSettings.Default.EnableAeroBackground && ThumbnailWindow.DwmIsCompositionEnabled())
      {
        Size frameBorderSize = SystemInformation.FrameBorderSize;
        this.FrameBorderSizeWidth = frameBorderSize.Width;
        frameBorderSize = SystemInformation.FrameBorderSize;
        this.FrameBorderSizeHeight = frameBorderSize.Height;
      }
      else
      {
        this.FrameBorderSizeWidth = 0;
        this.FrameBorderSizeHeight = 0;
      }
    }

    protected override void OnLoad(EventArgs e)
    {
      if (UserSettings.Default.EnableAeroBackground && ThumbnailWindow.DwmIsCompositionEnabled())
      {
        this.margins = new ThumbnailWindow.MARGINS();
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        ThumbnailWindow.MARGINS& local1 = @this.margins;
        Size size = this.Size;
        int height1 = size.Height;
        // ISSUE: explicit reference operation
        (^local1).Top = height1;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        ThumbnailWindow.MARGINS& local2 = @this.margins;
        size = this.Size;
        int width1 = size.Width;
        // ISSUE: explicit reference operation
        (^local2).Left = width1;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        ThumbnailWindow.MARGINS& local3 = @this.margins;
        size = this.Size;
        int height2 = size.Height;
        // ISSUE: explicit reference operation
        (^local3).Bottom = height2;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        ThumbnailWindow.MARGINS& local4 = @this.margins;
        size = this.Size;
        int width2 = size.Width;
        // ISSUE: explicit reference operation
        (^local4).Right = width2;
        ThumbnailWindow.DwmExtendFrameIntoClientArea(this.Handle, ref this.margins);
      }
      base.OnLoad(e);
    }

    protected override void OnPaintBackground(PaintEventArgs e)
    {
      e.Graphics.Clear(Color.Transparent);
      if (UserSettings.Default.EnableAeroBackground && ThumbnailWindow.DwmIsCompositionEnabled())
      {
        this.margins.Top = this.Size.Height;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        ThumbnailWindow.MARGINS& local1 = @this.margins;
        Size size = this.Size;
        int width1 = size.Width;
        // ISSUE: explicit reference operation
        (^local1).Left = width1;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        ThumbnailWindow.MARGINS& local2 = @this.margins;
        size = this.Size;
        int height = size.Height;
        // ISSUE: explicit reference operation
        (^local2).Bottom = height;
        // ISSUE: explicit reference operation
        // ISSUE: variable of a reference type
        ThumbnailWindow.MARGINS& local3 = @this.margins;
        size = this.Size;
        int width2 = size.Width;
        // ISSUE: explicit reference operation
        (^local3).Right = width2;
        ThumbnailWindow.DwmExtendFrameIntoClientArea(this.Handle, ref this.margins);
      }
      else if (!UserSettings.Default.EnableAeroBackground && ThumbnailWindow.DwmIsCompositionEnabled())
      {
        this.margins.Top = 0;
        this.margins.Left = 0;
        this.margins.Bottom = 0;
        this.margins.Right = 0;
        ThumbnailWindow.DwmExtendFrameIntoClientArea(this.Handle, ref this.margins);
      }
      this.OnPaint(e);
    }

    public struct MOUSEINPUT
    {
      public int dx;
      public int dy;
      public uint mouseData;
      public uint dwFlags;
      public uint time;
      public IntPtr dwExtraInfo;
    }

    public struct KEYBDINPUT
    {
      public ushort wVk;
      public ushort wScan;
      public uint dwFlags;
      public uint time;
      public IntPtr dwExtraInfo;
    }

    public struct HARDWAREINPUT
    {
      public uint uMsg;
      public ushort wParamL;
      public ushort wParamH;
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct INPUT
    {
      [FieldOffset(0)]
      public int type;
      [FieldOffset(4)]
      public ThumbnailWindow.MOUSEINPUT mi;
      [FieldOffset(4)]
      public ThumbnailWindow.KEYBDINPUT ki;
      [FieldOffset(4)]
      public ThumbnailWindow.HARDWAREINPUT hi;
    }

    public static class Win32
    {
      public const int INPUT_MOUSE = 0;
      public const int INPUT_KEYBOARD = 1;
      public const int INPUT_HARDWARE = 2;
      public const uint KEYEVENTF_EXTENDEDKEY = 1U;
      public const uint KEYEVENTF_KEYUP = 2U;
      public const uint KEYEVENTF_UNICODE = 4U;
      public const uint KEYEVENTF_SCANCODE = 8U;
      public const uint XBUTTON1 = 1U;
      public const uint XBUTTON2 = 2U;
      public const uint MOUSEEVENTF_MOVE = 1U;
      public const uint MOUSEEVENTF_LEFTDOWN = 2U;
      public const uint MOUSEEVENTF_LEFTUP = 4U;
      public const uint MOUSEEVENTF_RIGHTDOWN = 8U;
      public const uint MOUSEEVENTF_RIGHTUP = 16U;
      public const uint MOUSEEVENTF_MIDDLEDOWN = 32U;
      public const uint MOUSEEVENTF_MIDDLEUP = 64U;
      public const uint MOUSEEVENTF_XDOWN = 128U;
      public const uint MOUSEEVENTF_XUP = 256U;
      public const uint MOUSEEVENTF_WHEEL = 2048U;
      public const uint MOUSEEVENTF_VIRTUALDESK = 16384U;
      public const uint MOUSEEVENTF_ABSOLUTE = 32768U;

      [DllImport("user32.dll", SetLastError = true)]
      public static extern uint SendInput(uint nInputs, ThumbnailWindow.INPUT[] pInputs, int cbSize);

      [DllImport("user32.dll")]
      public static extern IntPtr GetMessageExtraInfo();
    }

    private delegate bool EnumWindowsCallback(IntPtr hwnd, int lParam);

    public enum MouseEventType
    {
      LeftDown = 2,
      LeftUp = 4,
      RightDown = 8,
      RightUp = 16,
    }

    public struct POINTAPI
    {
      public int x;
      public int y;
    }

    public struct WINDOWPLACEMENT
    {
      public int length;
      public int flags;
      public int showCmd;
      public ThumbnailWindow.POINTAPI ptMinPosition;
      public ThumbnailWindow.POINTAPI ptMaxPosition;
      public RECT rcNormalPosition;
    }

    private enum ShowWindowEnum
    {
      Hide = 0,
      ShowNormal = 1,
      ShowMinimized = 2,
      Maximize = 3,
      ShowMaximized = 3,
      ShowNormalNoActivate = 4,
      Show = 5,
      Minimize = 6,
      ShowMinNoActivate = 7,
      ShowNoActivate = 8,
      Restore = 9,
      ShowDefault = 10,
      ForceMinimized = 11,
    }

    public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

    public struct MARGINS
    {
      public int Left;
      public int Right;
      public int Top;
      public int Bottom;
    }
  }
}
