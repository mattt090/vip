﻿// Decompiled with JetBrains decompiler
// Type: ThumbnailPersist.Properties.Resources
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace ThumbnailPersist.Properties
{
  [DebuggerNonUserCode]
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) Resources.resourceMan, (object) null))
          Resources.resourceMan = new ResourceManager("ThumbnailPersist.Properties.Resources", typeof (Resources).Assembly);
        return Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return Resources.resourceCulture;
      }
      set
      {
        Resources.resourceCulture = value;
      }
    }

    internal static Bitmap Desktop
    {
      get
      {
        return (Bitmap) Resources.ResourceManager.GetObject("Desktop", Resources.resourceCulture);
      }
    }

    internal static Bitmap TransparentBackground
    {
      get
      {
        return (Bitmap) Resources.ResourceManager.GetObject("TransparentBackground", Resources.resourceCulture);
      }
    }

    internal Resources()
    {
    }
  }
}
