﻿// Decompiled with JetBrains decompiler
// Type: Hook.Hooks
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Runtime.InteropServices;

namespace Hook
{
  public class Hooks
  {
    private const uint WINEVENT_OUTOFCONTEXT = 0U;
    private Hooks.WinEventDelegate dEvent;
    private IntPtr pHook;
    private IntPtr qHook;
    private IntPtr rHook;
    private IntPtr sHook;
    private IntPtr tHook;
    public OnForegroundWindowChangedDelegate OnForegroundWindowChanged;
    public OnWindowMinimizeStartDelegate OnWindowMinimizeStart;
    public OnWindowMinimizeEndDelegate OnWindowMinimizeEnd;
    public OnWindowDestroyDelegate OnWindowDestroy;
    public OnWindowCreateDelegate OnWindowCreate;

    public Hooks()
    {
      this.dEvent = new Hooks.WinEventDelegate(this.WinEvent);
      this.pHook = Hooks.SetWinEventHook(32769U, 32769U, IntPtr.Zero, this.dEvent, 0U, 0U, 0U);
      this.qHook = Hooks.SetWinEventHook(22U, 22U, IntPtr.Zero, this.dEvent, 0U, 0U, 0U);
      this.rHook = Hooks.SetWinEventHook(23U, 23U, IntPtr.Zero, this.dEvent, 0U, 0U, 0U);
      this.sHook = Hooks.SetWinEventHook(3U, 3U, IntPtr.Zero, this.dEvent, 0U, 0U, 0U);
      GC.KeepAlive((object) this.dEvent);
      GC.KeepAlive((object) this.qHook);
      GC.KeepAlive((object) this.pHook);
      GC.KeepAlive((object) this.rHook);
      GC.KeepAlive((object) this.sHook);
    }

    ~Hooks()
    {
      try
      {
        IntPtr num = IntPtr.Zero;
        if (!num.Equals((object) this.qHook))
          Hooks.UnhookWinEvent(this.qHook);
        num = IntPtr.Zero;
        if (!num.Equals((object) this.pHook))
          Hooks.UnhookWinEvent(this.pHook);
        num = IntPtr.Zero;
        if (!num.Equals((object) this.rHook))
          Hooks.UnhookWinEvent(this.rHook);
        num = IntPtr.Zero;
        if (!num.Equals((object) this.sHook))
          Hooks.UnhookWinEvent(this.sHook);
        num = IntPtr.Zero;
        if (!num.Equals((object) this.sHook))
          Hooks.UnhookWinEvent(this.tHook);
      }
      catch (Exception ex)
      {
      }
      this.pHook = IntPtr.Zero;
      this.qHook = IntPtr.Zero;
      this.rHook = IntPtr.Zero;
      this.sHook = IntPtr.Zero;
      this.tHook = IntPtr.Zero;
      this.dEvent = (Hooks.WinEventDelegate) null;
      this.OnWindowMinimizeStart = (OnWindowMinimizeStartDelegate) null;
      this.OnWindowDestroy = (OnWindowDestroyDelegate) null;
      this.OnWindowMinimizeEnd = (OnWindowMinimizeEndDelegate) null;
      this.OnForegroundWindowChanged = (OnForegroundWindowChangedDelegate) null;
      this.OnWindowCreate = (OnWindowCreateDelegate) null;
    }

    [DllImport("User32.dll", SetLastError = true)]
    private static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, Hooks.WinEventDelegate lpfnWinEventProc, uint idProcess, uint idThread, uint dwFlags);

    [DllImport("user32.dll")]
    private static extern bool UnhookWinEvent(IntPtr hWinEventHook);

    private void WinEvent(IntPtr hWinEventHook, uint eventType, IntPtr hWnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
    {
      GC.KeepAlive((object) this.dEvent);
      GC.KeepAlive((object) this.qHook);
      GC.KeepAlive((object) this.pHook);
      GC.KeepAlive((object) this.rHook);
      GC.KeepAlive((object) this.sHook);
      switch (eventType)
      {
        case 3U:
          if (this.OnForegroundWindowChanged == null)
            break;
          this.OnForegroundWindowChanged(hWnd);
          break;
        case 22U:
          if (this.OnWindowMinimizeStart == null)
            break;
          this.OnWindowMinimizeStart(hWnd);
          break;
        case 23U:
          if (this.OnWindowMinimizeEnd == null)
            break;
          this.OnWindowMinimizeEnd(hWnd);
          break;
        case 32769U:
          if (this.OnWindowDestroy == null)
            break;
          this.OnWindowDestroy(hWnd);
          break;
      }
    }

    private enum SystemEvents : uint
    {
      EVENT_SYSTEM_CREATE = 3U,
      EVENT_SYSTEM_FOREGROUND = 3U,
      EVENT_SYSTEM_MINIMIZESTART = 22U,
      EVENT_SYSTEM_MINIMIZEEND = 23U,
      EVENT_SYSTEM_DESTROY = 32769U,
    }

    private delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hWnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime);

    private delegate int HookProc(int nCode, int wParam, IntPtr lParam);
  }
}
