﻿// Decompiled with JetBrains decompiler
// Type: CodeProject.Win32.Kernel32
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Runtime.InteropServices;

namespace CodeProject.Win32
{
  public class Kernel32
  {
    [DllImport("kernel32.dll")]
    public static extern int GlobalAddAtom(string Name);

    [DllImport("kernel32.dll")]
    public static extern int GlobalDeleteAtom(int atom);

    [DllImport("kernel32.dll")]
    public static extern IntPtr GlobalLock(IntPtr hMem);

    [DllImport("kernel32.dll")]
    public static extern bool GlobalUnlock(IntPtr hMem);
  }
}
