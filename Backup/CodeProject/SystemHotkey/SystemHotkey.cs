﻿// Decompiled with JetBrains decompiler
// Type: CodeProject.SystemHotkey.SystemHotkey
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using CodeProject.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace CodeProject.SystemHotkey
{
  public class SystemHotkey : Component, IDisposable
  {
    private Container components = (Container) null;
    protected DummyWindowWithEvent m_Window = new DummyWindowWithEvent();
    protected Shortcut m_HotKey = Shortcut.None;
    protected bool isRegistered = false;

    public bool IsRegistered
    {
      get
      {
        return this.isRegistered;
      }
    }

    [DefaultValue(Shortcut.None)]
    public Shortcut Shortcut
    {
      get
      {
        return this.m_HotKey;
      }
      set
      {
        if (this.DesignMode)
        {
          this.m_HotKey = value;
        }
        else
        {
          if (this.isRegistered && this.m_HotKey != value)
          {
            if (this.UnregisterHotkey())
            {
              Debug.WriteLine("Unreg: OK");
              this.isRegistered = false;
            }
            else
            {
              if (this.Error != null)
                this.Error((object) this, EventArgs.Empty);
              Debug.WriteLine("Unreg: ERR");
            }
          }
          if (value == Shortcut.None)
          {
            this.m_HotKey = value;
          }
          else
          {
            if (this.RegisterHotkey(value))
            {
              Debug.WriteLine("Reg: OK");
              this.isRegistered = true;
            }
            else
            {
              if (this.Error != null)
                this.Error((object) this, EventArgs.Empty);
              Debug.WriteLine("Reg: ERR");
            }
            this.m_HotKey = value;
          }
        }
      }
    }

    public event EventHandler Pressed;

    public event EventHandler Error;

    public SystemHotkey(IContainer container)
    {
      container.Add((IComponent) this);
      this.InitializeComponent();
      this.m_Window.ProcessMessage += new MessageEventHandler(this.MessageEvent);
    }

    public SystemHotkey()
    {
      this.InitializeComponent();
      if (this.DesignMode)
        return;
      this.m_Window.ProcessMessage += new MessageEventHandler(this.MessageEvent);
    }

    public new void Dispose()
    {
      if (this.isRegistered && this.UnregisterHotkey())
        Debug.WriteLine("Unreg: OK");
      Debug.WriteLine("Disposed");
    }

    private void InitializeComponent()
    {
      this.components = new Container();
    }

    protected void MessageEvent(object sender, ref Message m, ref bool Handled)
    {
      if (m.Msg != 786 || !(m.WParam == (IntPtr) this.GetType().GetHashCode()))
        return;
      Handled = true;
      Debug.WriteLine("HOTKEY pressed!");
      if (this.Pressed != null)
        this.Pressed((object) this, EventArgs.Empty);
    }

    protected bool UnregisterHotkey()
    {
      return User32.UnregisterHotKey(this.m_Window.Handle, this.GetType().GetHashCode());
    }

    protected bool RegisterHotkey(Shortcut key)
    {
      int fsModifiers = 0;
      Keys keys = Keys.None;
      if ((key & (Shortcut) 262144) == (Shortcut) 262144)
      {
        ++fsModifiers;
        keys = Keys.Alt;
      }
      if ((key & (Shortcut) 65536) == (Shortcut) 65536)
      {
        fsModifiers += 4;
        keys = Keys.Shift;
      }
      if ((key & (Shortcut) 131072) == (Shortcut) 131072)
      {
        fsModifiers += 2;
        keys = Keys.Control;
      }
      Debug.Write(fsModifiers.ToString() + " ");
      Debug.WriteLine(((int) key - (int) keys).ToString());
      return User32.RegisterHotKey(this.m_Window.Handle, this.GetType().GetHashCode(), fsModifiers, (int) key - (int) keys);
    }
  }
}
