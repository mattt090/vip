﻿// Decompiled with JetBrains decompiler
// Type: SingleInstance.SingleApplication
// Assembly: Video in Picture for Vista, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A196A431-E5E5-46B9-B474-6479C91693DC
// Assembly location: C:\Users\mquinlan\Downloads\VIP0_2_9Final\Video in Picture for Vista.exe

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace SingleInstance
{
    public class SingleApplication
    {
        private const int SW_RESTORE = 9;
        private static Mutex mutex;

        [DllImport("user32.dll")]
        private static extern int ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern int SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern int IsIconic(IntPtr hWnd);

        private static IntPtr GetCurrentInstanceWindowHandle()
        {
            IntPtr num = IntPtr.Zero;
            Process currentProcess = Process.GetCurrentProcess();
            foreach (Process process in Process.GetProcessesByName(currentProcess.ProcessName))
            {
                if (process.Id != currentProcess.Id && process.MainModule.FileName == currentProcess.MainModule.FileName && process.MainWindowHandle != IntPtr.Zero)
                {
                    num = process.MainWindowHandle;
                    break;
                }
            }
            return num;
        }

        private static void SwitchToCurrentInstance()
        {
            IntPtr instanceWindowHandle = GetCurrentInstanceWindowHandle();
            if (!(instanceWindowHandle != IntPtr.Zero))
                return;
            if (IsIconic(instanceWindowHandle) != 0)
                ShowWindow(instanceWindowHandle, 9);
            SetForegroundWindow(instanceWindowHandle);
        }


        public static bool Run(Form frmMain)
        {
            if (IsAlreadyRunning())
            {
                SwitchToCurrentInstance();
                return false;
            }
            try
            {
                Application.Run(frmMain);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Run exception");
            }
            return true;
        }

        public static bool Run()
        {
            return !IsAlreadyRunning();
        }

        private static bool IsAlreadyRunning()
        {
            bool createdNew;
            mutex = new Mutex(true, "Global\\" + new FileInfo(Assembly.GetExecutingAssembly().Location).Name, out createdNew);
            if (createdNew)
                mutex.ReleaseMutex();
            return !createdNew;
        }
    }
}
